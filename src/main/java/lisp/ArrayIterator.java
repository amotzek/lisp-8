package lisp;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.Iterator;
import java.util.NoSuchElementException;

/*
 * Created by andreasm 02.05.13 20:20
 */
final class ArrayIterator<E> implements Iterator<E>
{
    private final Object[] elements;
    private int index;
    /**
     * Constructs an Iterator for the given Array
     *
     * @param elements Array
     */
    ArrayIterator(E[] elements)
    {
        this.elements = elements;
    }
    /**
     * @see java.util.Iterator#hasNext()
     */
    public boolean hasNext()
    {
        return index < elements.length;
    }
    /**
     * @see java.util.Iterator#next()
     */
    @SuppressWarnings("unchecked")
    public E next()
    {
        if (index >= elements.length) throw new NoSuchElementException();
        //
        return (E) elements[index++];
    }
    /**
     * @see java.util.Iterator#remove()
     */
    public void remove()
    {
        throw new UnsupportedOperationException();
    }
}