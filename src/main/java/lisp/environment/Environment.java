package lisp.environment;
/*
 * Copyright (C) 2001, 2007, 2008, 2010 - 2012, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
import lisp.List;
import lisp.Sexpression;
import lisp.Symbol;
/*
 * Erstellungsdatum: (24.08.01 18:28:14)
 *
 * @author Andreasm
 */
public final class Environment
{
    private final SymbolMap map;
    private Environment parent;
    private boolean frozen;
    /**
     * Constructor for Environment
     *
     */
    public Environment()
    {
        this(null);
    }
    /**
     * Constructor for Environment
     *
     * @param parent Parent Environment
     */
    public Environment(Environment parent)
    {
        super();
        //
        this.parent = parent;
        //
        map = new SymbolMap(31);
    }
    /**
     * Costructor for Environment
     *
     * @param parent Parent Environment
     * @param names List of Names
     * @param values Array of Values
     */
    public Environment(Environment parent, List names, Sexpression[] values)
    {
        super();
        //
        this.parent = parent;
        //
        map = new SymbolMap(values.length);
        int position = 0;
        //
        while (names != null)
        {
            Symbol name = (Symbol) names.first();
            Sexpression value = values[position++];
            map.put(name, value);
            names = names.rest();
        }
    }
    /**
     * Returns the Parent Environment
     *
     * @return Environment that is the Parent
     */
    public Environment getParent()
    {
        return parent;
    }
    /**
     * Sets the Parent Environment
     *
     * @param parent Parent Environment
     */
    public void setParent(Environment parent)
    {
        this.parent = parent;
    }
    /**
     * Freezes this Environment
     */
    public void freeze()
    {
        if (parent != null) throw new IllegalStateException("only the root environment can be frozen");
        //
        frozen = true;
    }
    /**
     * Checks is the Object is frozen
     *
     * @return true, if the Object is frozen
     */
    public boolean isFrozen()
    {
        return frozen;
    }
    /**
     * Adds a binding to this environment
     *
     * @param override Override a binding?
     * @param name     Symbol to bind
     * @param value    Value for the Symbol
     */
    public synchronized void add(boolean override, Symbol name, Sexpression value)
    {
        if (override)
        {
            Environment current = this;
            Environment previous = null;
            //
            do
            {
                if (current.map.containsName(name))
                {
                    if (current.frozen)
                    {
                        previous.map.put(name, value);
                    }
                    else
                    {
                        current.map.put(name, value);
                    }
                    //
                    return;
                }
                //
                previous = current;
                current = current.parent;
            }
            while (current != null);
        }
        //
        map.put(name, value);
    }
    /**
     * Returns the Value for a Symbol
     *
     * @param name Symbol
     * @return Value for the Symbol in this Environment or its parents
     * @throws NotBoundException if the Symbol is not bound to a value
     */
    public synchronized Sexpression at(Symbol name) throws NotBoundException
    {
        Environment environment = this;
        //
        do
        {
            if (environment.map.containsName(name)) return environment.map.get(name);
            //
            environment = environment.parent;
        }
        while (environment != null);
        //
        throw new NotBoundException(name);
    }
    /**
     * Returns a Symbol for a Value
     *
     * @param value Value
     * @return Symbol that is bound to the Value
     * @throws NoSuchValueException if no Symbol is bound to this Value
     */
    public synchronized Symbol inverseAt(Sexpression value) throws NoSuchValueException
    {
        Environment environment = this;
        //
        do
        {
            Symbol name = environment.map.inverseGet(value);
            //
            if (name != null) return name;
            //
            environment = environment.parent;
        }
        while (environment != null);
        //
        throw new NoSuchValueException();
    }
    /**
     * Copies this Environment
     *
     * @return Environment with same bindings
     */
    public synchronized Environment copy()
    {
        Copier copier = new Copier();
        //
        return copier.copy(this);
    }
    /**
     * Returns the Symbols that are bound in this Environment
     *
     * @return List of Names bound in this Environment
     */
    public LinkedList<Symbol> getNames()
    {
        return map.nameList();
    }
}