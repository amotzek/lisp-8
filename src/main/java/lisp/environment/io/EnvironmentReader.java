package lisp.environment.io;
/*
 * Copyright (C) 2014 - 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Closure;
import lisp.Sexpression;
import lisp.environment.Environment;
import lisp.parser.Parser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
/*
 * Created by andreasm
 * Date: 18.03.14
 */
public final class EnvironmentReader
{
    private final Environment environment;
    //
    public EnvironmentReader(Environment environment)
    {
        super();
        //
        this.environment = environment;
    }
    //
    public void readFromResource(Class clazz, String name, String charset) throws IOException, CannotEvalException
    {
        URL url = clazz.getResource(name);
        readFrom(url, charset);
    }
    //
    public void readFrom(URL url, String charset) throws IOException, CannotEvalException
    {
        try (InputStream stream = url.openStream())
        {
            readFrom(stream, charset);
        }
    }
    //
    public void readFrom(InputStream stream, String charset) throws IOException, CannotEvalException
    {
        try (InputStreamReader reader = new InputStreamReader(stream, charset))
        {
            readFrom(reader);
        }
    }
    //
    public void readFrom(Reader reader) throws IOException, CannotEvalException
    {
        BufferedReader bufferedreader = new BufferedReader(reader);
        StringBuilder builder = new StringBuilder();
        String line;
        //
        do
        {
            line = bufferedreader.readLine();
            //
            if (line != null) builder.append(line);
        }
        while (line != null);
        //
        String code = builder.toString();
        readFrom(code);
    }
    //
    public void readFrom(String code) throws CannotEvalException
    {
        if (code == null) return;
        //
        int position = 0;
        //
        while (position < code.length())
        {
            Parser parser = new Parser(code, position);
            Sexpression sexpression = parser.getSexpression();
            Closure closure = new Closure(environment, sexpression);
            closure.eval();
            position = parser.getPosition();
        }
    }
}