package lisp.environment;
/*
 * Copyright (C) 2012, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.Symbol;
import java.util.LinkedList;
/*
 * Created by  andreasm 02.12.12 15:06
 */
final class SymbolMap
{
    private Sexpression[] keysandvalues;
    private int size;
    private int maxsize;
    //
    public SymbolMap(int length)
    {
        super();
        //
        size = 0;
        maxsize = (length <= 4) ? 8 : length << 1;
        keysandvalues = new Sexpression[maxsize << 1];
    }
    //
    private static boolean isEqual(Object object1, Object object2)
    {
        if (object1 == null) return object2 == null;
        //
        return object1.equals(object2);
    }
    //
    public void put(Symbol name, Sexpression value)
    {
        int index = findKey(name);
        //
        if (index < 0)
        {
            size++;
            //
            if (3 * size > 2 * maxsize) grow();
            //
            index = insertKey(name);
        }
        //
        index++;
        keysandvalues[index] = value;
    }
    //
    public boolean containsName(Symbol name)
    {
        return findKey(name) >= 0;
    }
    //
    public Sexpression get(Symbol name)
    {
        int index = findKey(name);
        //
        if (index < 0) return null;
        //
        index++;
        //
        return keysandvalues[index];
    }
    //
    public Symbol inverseGet(Sexpression value)
    {
        for (int i = 1; i <= keysandvalues.length - 2; i += 2)
        {
            if (isEqual(value, keysandvalues[i])) return (Symbol) keysandvalues[i - 1];
        }
        //
        return null;
    }
    //
    public LinkedList<Symbol> nameList()
    {
        LinkedList<Symbol> keys = new LinkedList<>();
        //
        for (int i = 0; i <= keysandvalues.length - 2; i += 2)
        {
            Object key = keysandvalues[i];
            //
            if (key != null) keys.addLast((Symbol) key);
        }
        //
        return keys;
    }
    //
    private int findKey(Symbol name)
    {
        int index = name.hashCode();
        index %= maxsize;
        index <<= 1;
        //
        for (int i = 0; i < maxsize; i++)
        {
            Sexpression key = keysandvalues[index];
            //
            if (key == null)
            {
                return -1;
            }
            else if (key == name)
            {
                return index;
            }
            //
            index += 2;
            //
            if (index >= keysandvalues.length) index = 0;
        }
        //
        return -1;
    }
    //
    private int insertKey(Symbol name)
    {
        int index = name.hashCode();
        index %= maxsize;
        index <<= 1;
        //
        for (int i = 0; i < maxsize; i++)
        {
            Sexpression key = keysandvalues[index];
            //
            if (key == null)
            {
                keysandvalues[index] = name;
                //
                return index;
            }
            else if (key == name)
            {
                return index;
            }
            //
            index += 2;
            //
            if (index >= keysandvalues.length) index = 0;
        }
        //
        throw new IllegalStateException();
    }
    //
    private void grow()
    {
        Sexpression[] previouskeysandvalues = keysandvalues;
        maxsize <<= 1;
        keysandvalues = new Sexpression[maxsize << 1];
        //
        for (int i = 0; i <= previouskeysandvalues.length - 2; i += 2)
        {
            Symbol key = (Symbol) previouskeysandvalues[i];
            //
            if (key != null) put(key, previouskeysandvalues[i + 1]);
        }
    }
}