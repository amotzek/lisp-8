package lisp.environment;
/*
 * Copyright (C) 2012, 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedList;
import lisp.Array;
import lisp.CannotEvalException;
import lisp.GenericFunction;
import lisp.GuardedMethod;
import lisp.HashTable;
import lisp.Lambda;
import lisp.List;
import lisp.Mlambda;
import lisp.RubyStyleObject;
import lisp.Sexpression;
import lisp.SimpleClass;
import lisp.Symbol;
/*
 * Created by  andreasm 05.01.12 12:03
 */
final class Copier
{
    private static final HashSet<String> mutabletypes = new HashSet<>();
    //
    static
    {
        mutabletypes.add("array");
        mutabletypes.add("hash-table");
        mutabletypes.add("generic-function");
        mutabletypes.add("method");
        mutabletypes.add("lambda");
        mutabletypes.add("mlambda");
        mutabletypes.add("instance");
        mutabletypes.add("class");
    }
    //
    private final IdentityHashMap<Environment, Environment> environments;
    private final IdentityHashMap<Sexpression, Sexpression> values;
    //
    public Copier()
    {
        super();
        //
        environments = new IdentityHashMap<>(20);
        values = new IdentityHashMap<>(1000);
    }
    /**
     * Copies an Environment
     * 
     * @param original Original Environment
     * @return Copy of Environment
     */
    public Environment copy(Environment original)
    {
        if (original == null) return null;
        //
        if (original.isFrozen()) return original;
        //
        Environment copy = environments.get(original);
        //
        if (copy != null) return copy;
        //
        Environment parent = copy(original.getParent());
        copy = new Environment(parent);
        environments.put(original, copy);
        //
        for (Symbol name : original.getNames())
        {
            try 
            {
                Sexpression value = copy(original.at(name));
                copy.add(false, name, value);
            }
            catch (NotBoundException e) 
            {
                assert false;
            }
        }
        //
        return copy;
    }
    /**
     * Copies Sexpressions
     *
     * @param original Original Sexpression
     * @return Original
     */
    private Sexpression copy(Sexpression original)
    {
        if (original == null) return null;
        //
        Sexpression copy = values.get(original);
        //
        if (copy != null) return copy;
        //
        if (original instanceof List)
        {
            copy = copyList((List) original);
        }
        else if (original instanceof RubyStyleObject)
        {
            copy = copyInstance((RubyStyleObject) original);
        }
        else if (original instanceof Array)
        {
            copy = copyArray((Array) original);
        }
        else if (original instanceof HashTable)
        {
            copy = copyHashTable((HashTable) original);
        }
        else if (original instanceof Lambda)
        {
            copy = copyLambda((Lambda) original);
        }
        else if (original instanceof Mlambda)
        {
            copy = copyMacro((Mlambda) original);
        }
        else if (original instanceof GuardedMethod)
        {
            copy = copyMethod((GuardedMethod) original);
        }
        else if (original instanceof GenericFunction)
        {
            copy = copyGenericFunction((GenericFunction) original);
        }
        else if (original instanceof SimpleClass)
        {
            copy = copyClass((SimpleClass) original);
        }
        else
        {
            copy = original;
        }
        //
        return copy;
    }
    /**
     * Copies a List
     * 
     * @param original Original List
     * @return Copy of List
     */
    private List copyList(List original)
    {
        if (isImmutable(original)) return original;
        //
        List list = original;
        List copy = null;
        //
        while (list != null)
        {
            Sexpression element = copy(list.first());
            copy = new List(element, copy);
            list = list.rest();
        }
        //
        copy = List.reverse(copy);
        values.put(original, copy);
        //
        return copy;
    }
    /**
     * Checks if the given List is immutable
     * 
     * @param list List
     * @return true if the List is immutable
     */
    private static boolean isImmutable(List list)
    {
        while (list != null)
        {
            Sexpression element = list.first();
            list = list.rest();
            //
            if (element == null) continue;
            //
            String type = element.getType();
            //
            if (mutabletypes.contains(type)) return false;
            //
            if (element instanceof List && !isImmutable((List) element)) return false;           
        }
        //
        return true;
    }
    /**
     * Copies an Instance
     * 
     * @param original Original Instance
     * @return Copy of Instance
     */
    private RubyStyleObject copyInstance(RubyStyleObject original)
    {
        SimpleClass classof = (SimpleClass) copy(original.classOf());
        RubyStyleObject copy = new RubyStyleObject(classof);
        values.put(original, copy);
        Iterator<Symbol> keys = original.keyIterator();
        //
        while (keys.hasNext())
        {
            Symbol key = keys.next();
            Sexpression value = copy(original.get(key));
            copy.put(key, value);
        }
        //
        if (original.isFrozen()) copy.freeze();
        //
        return copy;
    }
    /**
     * Copies an Array
     * 
     * @param original Original Array
     * @return Copy of Array
     */
    private Array copyArray(Array original)
    {
        Array copy = new Array(original.getDimensions());
        values.put(original, copy);
        Iterator<List> keys = original.keyIterator();
        //
        while (keys.hasNext())
        {
            List key = keys.next();
            Sexpression value = copy(original.get(key));
            copy.put(key, value);
        }
        //
        return copy;
    }
    /**
     * Copies a Hash Table
     *
     * @param original Original Hash Table
     * @return Copy of Hash Table
     */
    private HashTable copyHashTable(HashTable original)
    {
        HashTable copy = new HashTable();
        values.put(original, copy);
        Iterator<Sexpression> keys = original.keyIterator();
        //
        while (keys.hasNext())
        {
            Sexpression key = keys.next();
            Sexpression value = copy(original.get(key));
            copy.put(key, value);
        }
        //
        return copy;
    }
    /**
     * Copies a Class
     *
     * @param original Original Class
     * @return Copy of Class
     */
    private SimpleClass copyClass(SimpleClass original)
    {
        Symbol classname = original.getClassName();
        boolean allocatable = original.isAllocatable();
        List superclasses = original.getSuperclasses();
        LinkedList<SimpleClass> superclassescopy = new LinkedList<>();
        //
        while (superclasses != null)
        {
            SimpleClass superclass = (SimpleClass) copy(superclasses.first());
            superclassescopy.addLast(superclass);
            superclasses = superclasses.rest();
        }
        //
        SimpleClass copy = new SimpleClass(classname, allocatable, superclassescopy);
        values.put(original, copy);
        //
        return copy;
    }
    /**
     * Copies a Lambda
     * 
     * @param original Original Lambda
     * @return Copy of Lambda within copy of definition Environment
     */
    private Lambda copyLambda(Lambda original)
    {
        try
        {
            List parameters = original.getParameters();
            Sexpression body = copy(original.getBody());
            Sexpression compiledbody = copy(original.getCompiledBody());
            Environment environment = copy(original.getEnvironment());
            Lambda copy = new Lambda(parameters, body, compiledbody, environment);
            values.put(original, copy);
            //
            return copy;
        }
        catch (CannotEvalException e)
        {
            assert false;
        }
        //
        return null;
    }
    /**
     * Copies a Mlambda
     *
     * @param original Original Mlambda
     * @return Copy of Mlambda within copy of definition Environment
     */
    private Mlambda copyMacro(Mlambda original)
    {
        try
        {
            List parameters = original.getParameters();
            Sexpression body = copy(original.getBody());
            Sexpression compiledbody = copy(original.getCompiledBody());
            Environment environment = copy(original.getEnvironment());
            Mlambda copy = new Mlambda(parameters, body, compiledbody, environment);
            values.put(original, copy);
            //
            return copy;
        }
        catch (CannotEvalException e)
        {
            assert false;
        }
        //
        return null;
    }
    /**
     * Copies a Method
     *
     * @param original Original Method
     * @return Copy of Method within copy of definition Environment, with copied Specializer Classes
     */
    private GuardedMethod copyMethod(GuardedMethod original)
    {
        try
        {
            List parameters = original.getParameters();
            List specializers = original.getSpezializers();
            //
            if (original.isStale()) return null;
            //
            SimpleClass[] parameterclasses = new SimpleClass[List.length(specializers)];
            //
            for (int i = 0; i < parameterclasses.length; i++)
            {
                parameterclasses[i] = (SimpleClass) copy(specializers.first());
                specializers = specializers.rest();
            }
            //
            Sexpression guard = copy(original.getGuard());
            Sexpression body = copy(original.getBody());
            Sexpression compiledbody = copy(original.getCompiledBody());
            Environment environment = copy(original.getEnvironment());
            GuardedMethod copy = new GuardedMethod(parameters, parameterclasses, guard, body, compiledbody, environment);
            values.put(original, copy);
            //
            return copy;
        }
        catch (CannotEvalException e)
        {
            assert false;
        }
        //
        return null;
    }
    /**
     * Copies a Generic Function
     * 
     * @param original Original Generic Function
     * @return Copy of Generic Function with copies of Methods
     */
    private GenericFunction copyGenericFunction(GenericFunction original)
    {
        Symbol functionname = original.getFunctionName();
        GenericFunction copy = new GenericFunction(functionname);
        values.put(original, copy);
        List methods = original.getMethods();
        //
        while (methods != null)
        {
            GuardedMethod method = (GuardedMethod) copy(methods.first());
            //
            if (method != null) copy.addMethod(method);
            //
            methods = methods.rest();
        }
        //
        return copy;
    }
}