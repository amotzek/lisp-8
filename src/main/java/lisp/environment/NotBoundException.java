package lisp.environment;
/*
 * Copyright (C) 2007, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Symbol;
/*
 * Created on 06.01.2007
 */
public final class NotBoundException extends Exception
{
    private final Symbol symbol;
    /**
     * Constructor for NotBoundException
     */
    public NotBoundException(Symbol symbol)
    {
        super();
        //
        this.symbol = symbol;
    }
    /**
     * Returns the unbound Symbol
     *
     * @return unbound Symbol
     */
    public Symbol getUnboundSymbol()
    {
        return symbol;
    }
    /*
     * @see java.lang.Throwable#fillInStackTrace()
     */
    @Override
    public Throwable fillInStackTrace()
    {
        return this;
    }
}
