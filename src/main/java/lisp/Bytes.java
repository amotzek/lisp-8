/*
 * Copyright (C) 2007, 2008, 2010, 2011, 2017, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp;
//
import lisp.formatter.Formattable;
import java.util.Arrays;
import java.util.Base64;
/*
 * @author Andreasm
 */
public final class Bytes extends Constant implements Sexpression, Formattable
{
    private final byte[] bytearray;
    /**
     * Constructor of Bytes
     *
     * @param bytearray Byte array contained
     */
    public Bytes(byte[] bytearray)
    {
        super();
        //
        this.bytearray = bytearray;
    }
    /**
     * Method getByteArray
     *
     * @return Byte array contained
     */
    public byte[] getByteArray()
    {
        return bytearray;
    }
    /**
     * @see Sexpression#getType()
     */
    public String getType()
    {
        return "bytes";
    }
    /**
     * @see Object#hashCode()
     */
    public int hashCode()
    {
        return Arrays.hashCode(bytearray);
    }
    /**
     * @see Object#equals(Object)
     */
    public boolean equals(Object obj)
    {
        if (obj == null) return false;
        //
        if (obj instanceof Bytes)
        {
            Bytes bytes = (Bytes) obj;
            //
            return Arrays.equals(bytearray, bytes.bytearray);
        }
        //
        return false;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        Base64.Encoder encoder = Base64.getEncoder();
        builder.append("#.(base64-decode \"");
        builder.append(encoder.encodeToString(bytearray));
        builder.append("\")");
        //
        return builder.toString();
    }
}