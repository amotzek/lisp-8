/*
 * Copyright (C) 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.evaluator;
//
import lisp.Sexpression;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
/*
 * @author Andreasm
 */
public final class ConstantEvaluator extends Evaluator
{
    private final Sexpression constant;
    /**
     * Constructor for ConstantEvaluator
     *
     * @param environment Environment
     * @param constant Constant Sexpression
     * @param succeed Continuation for Success
     * @param fail Continuation for Failure
     */
    public ConstantEvaluator(Environment environment, Sexpression constant, SuccessContinuation succeed, FailureContinuation fail)
    {
        super(environment, succeed, fail);
        //
        this.constant = constant;
    }
    /**
     *
     */
    public void run()
    {
        succeed.succeed(constant);
    }
}