package lisp.evaluator;
/*
 * Copyright (C) 2011, 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.continuation.TraceBuilder;
import lisp.environment.Environment;
import lisp.environment.NotBoundException;
/*
 * @author Andreasm
 */
public final class GetValueEvaluator extends Evaluator
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars UNBOUNDSYMBOL = new Chars("unbound symbol");
    private static final Chars NOENVIRONMENT = new Chars("no environment");
    //
    private final Symbol symbol;
    /**
     * Constructor for Evaluator
     *
     * @param environment Environment
     * @param symbol Symbol
     * @param succeed Continuation for Success
     * @param fail Continuation for Failure
     */
    public GetValueEvaluator(Environment environment, Symbol symbol, SuccessContinuation succeed, FailureContinuation fail)
    {
        super(environment, succeed, fail);
        //
        this.symbol = symbol;
    }
    /**
     *
     */
    public void run()
    {
        try
        {
            final Sexpression value = environment.at(symbol);
            succeed.succeed(value);
        }
        catch (NotBoundException e)
        {
            TraceBuilder builder = new TraceBuilder(ERROR, UNBOUNDSYMBOL, succeed);
            builder.append(" ");
            builder.append(symbol);
            builder.build();
            builder.invoke(fail);
        }
        catch (NullPointerException e)
        {
            TraceBuilder builder = new TraceBuilder(ERROR, NOENVIRONMENT, succeed);
            builder.build();
            builder.invoke(fail);
        }
    }
}