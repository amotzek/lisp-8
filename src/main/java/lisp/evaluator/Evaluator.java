package lisp.evaluator;
/*
 * Copyright (C) 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
/*
 * @author Andreasm
 */
public abstract class Evaluator implements Runnable
{
    protected final Environment environment;
    protected final SuccessContinuation succeed;
    protected final FailureContinuation fail;
    /**
     * Constructor for Evaluator
     *
     * @param environment Environment
     * @param succeed Continuation for Success
     * @param fail Continuation for Failure
     */
    protected Evaluator(Environment environment, SuccessContinuation succeed, FailureContinuation fail)
    {
        super();
        //
        this.environment = environment;
        this.succeed = succeed;
        this.fail = fail;
    }
}
