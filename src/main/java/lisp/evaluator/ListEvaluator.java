package lisp.evaluator;
/*
 * Copyright (C) 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.List;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.ApplyValue;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * @author Andreasm
 */
public final class ListEvaluator extends Evaluator
{
    private final RunnableQueue runnablequeue;
    private final List list;
    /**
     * Constructor for ListEvaluator
     *
     * @param runnablequeue RunnableQueue
     * @param environment Environment
     * @param list List
     * @param succeed Continuation for Success
     * @param fail Continuation for Failure
     */
    public ListEvaluator(RunnableQueue runnablequeue, Environment environment, List list, SuccessContinuation succeed, FailureContinuation fail)
    {
        super(environment, succeed, fail);
        //
        this.runnablequeue = runnablequeue;
        this.list = list;
    }
    /**
     *
     */
    public void run()
    {
        final Sexpression first = list.first();
        //
        if (first == null)
        {
            fail.fail(Symbol.createSymbol("error"), new Chars("not a function null"));
            //
            return;
        }
        //
        final List rest = list.rest();
        ApplyValue applyvalue = new ApplyValue(runnablequeue, environment, rest, succeed, fail);
        //
        if (first.isConstant())
        {
            applyvalue.succeed(first);
            //
            return;
        }
        //
        first.enQueueEvaluator(runnablequeue, environment, applyvalue, fail);
    }
}