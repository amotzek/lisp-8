package lisp;
/*
 * Copyright (C) 2001, 2006, 2007, 2010, 2011, 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.evaluator.Evaluator;
import lisp.evaluator.GetValueEvaluator;
import lisp.concurrent.RunnableQueue;
import java.lang.ref.ReferenceQueue;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
/*
 * Erstellungsdatum: (24.08.01 18:16:52)
 *
 * @author Andreasm
 */
public final class Symbol implements Sexpression, Comparable<Symbol>
{
    private static final ReferenceQueue<Symbol> queue = new ReferenceQueue<>();
    private static final TreeMap<String, SymbolReference> symbols = new TreeMap<>();
    private static final AtomicInteger sequence = new AtomicInteger(0);
    //
    private final String name;
    private final String externalname;
    private final int hashcode;
    /**
     * Constructor for Symbol
     *
     * @param name Name of Symbol
     */
    private Symbol(String name)
    {
        super();
        //
        this.name = name;
        //
        externalname = getExternalName(name);
        hashcode = sequence.incrementAndGet();
    }
    /**
     * Method createSymbol
     *
     * @param name Name of Symbol
     * @return Symbol created
     */
    public static synchronized Symbol createSymbol(String name)
    {
        SymbolReference reference = symbols.get(name);
        //
        if (reference != null)
        {
            Symbol symbol = reference.get();
            //
            if (symbol != null) return symbol;
        }
        //
        for (int count = 0; count < 16; count++)
        {
            SymbolReference clearedreference = (SymbolReference) queue.poll();
            //
            if (clearedreference == null) break;
            /*
             * remove cleared references from the Map
             */
            String clearedname = clearedreference.getName();
            SymbolReference removedreference = symbols.remove(clearedname);
            //
            if (clearedreference != removedreference)
            {
                /*
                 * The removed reference was not the reference that was cleared.
                 * This happens if the cleared reference is replaced before it is returned 
                 * by ReferenceQueue.poll(). In this case, the reference is put back. 
                 */
                symbols.put(clearedname, removedreference);
            }
        }
        //
        Symbol symbol = new Symbol(name);
        reference = new SymbolReference(symbol, queue);
        symbols.put(name, reference);
        //
        return symbol;
    }
    /**
     * Method getName
     *
     * @return Name of Symbol
     */
    public String getName()
    {
        return name;
    }
    /**
     * @see Sexpression#getType()
     */
    public String getType()
    {
        return "atom";
    }
    /**
     * @see Sexpression#isConstant()
     */
    public boolean isConstant()
    {
        return false;
    }
    /**
     * @see Sexpression#enQueueEvaluator(lisp.concurrent.RunnableQueue, lisp.environment.Environment, lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void enQueueEvaluator(RunnableQueue runnablequeue, Environment environment, SuccessContinuation succeed, FailureContinuation fail)
    {
        Evaluator evaluator = new GetValueEvaluator(environment, this, succeed, fail);
        runnablequeue.add(evaluator);
    }
    /**
     * @see Comparable#compareTo(Object)
     */
    public int compareTo(Symbol symbol)
    {
        return name.compareTo(symbol.name);
    }
    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return hashcode;
    }
    /**
     * @see Object#equals(Object)
     */
    public boolean equals(Object obj)
    {
        return obj == this;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return externalname;
    }
    /**
     * Creates the external Name for the given Name
     *
     * @param name Name
     * @return External Name
     */
    private static String getExternalName(String name)
    {
        if (!isSpecial(name)) return name;
        //
        StringBuilder builder = new StringBuilder();
        builder.append("#.(intern \"");
        //
        for (char c : name.toCharArray())
        {
            if (c == '"')
            {
                builder.append("\\\"");
            }
            else
            {
                builder.append(c);
            }
        }
        //
        builder.append("\")");
        //
        return builder.toString();
    }
    /**
     * Checks if this Name needs special treatment
     * when it is written to a String
     *
     * @param name Name
     * @return true if the Name will not be parsed as a Symbol
     */
    private static boolean isSpecial(String name)
    {
        if (name.length() == 0) return true;
        //
        char[] chars = name.toCharArray();
        char first = chars[0];
        //
        if (first >= '0' && first <= '9') return true;
        //
        if (first == '"') return true;
        //
        for (char c : chars)
        {
            if (c == ' ' || c == '"') return true;
        }
        //
        return false;
    }
}