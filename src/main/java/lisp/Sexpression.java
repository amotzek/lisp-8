package lisp;
/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * @author Andreasm
 */
public interface Sexpression
{
    /**
     * Returns the Name of Type
     *
     * @return Typename
     */
    public String getType();
    /**
     * Checks if this is a Constant
     *
     * @return true, if Sexpression evaluates to itself
     */
    public boolean isConstant();
    /**
     * Enqueues the Evaluator
     *
     * @param runnablequeue RunnableQueue
     * @param environment Environment
     * @param succeed Continuation for Success
     * @param fail Continuation for Failure
     */
    public void enQueueEvaluator(RunnableQueue runnablequeue, Environment environment, SuccessContinuation succeed, FailureContinuation fail);
}