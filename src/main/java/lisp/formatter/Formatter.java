package lisp.formatter;
/*
 * Copyright (C) 2012, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.AssociativeContainer;
import lisp.List;
import lisp.Sexpression;
import lisp.formatter.strategy.FormattingStrategy;
import lisp.formatter.strategy.StrategyFactory;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
/*
 * Created by andreasm on 21.12.12 at 12:23
 */
public final class Formatter
{
    private static final Long NO_ID = 0L;
    //
    private final StringBuilder builder;
    private final IdentityHashMap<Formattable, Long> idsbyformattable;
    private final HashSet<Long> ids;
    private long nextid;
    //
    public Formatter()
    {
        super();
        //
        builder = new StringBuilder();
        idsbyformattable = new IdentityHashMap<>();
        ids = new HashSet<>();
        nextid = 1L;
    }
    //
    public void format(Sexpression sexpression)
    {
        addReferences(sexpression);
        append(sexpression);
    }
    //
    public String toString()
    {
        return builder.toString();
    }
    //
    public void append(Sexpression sexpression)
    {
        if (needsFormatting(sexpression))
        {
            FormattingStrategy strategy = StrategyFactory.findStrategyFor(sexpression);
            strategy.format(sexpression, this);
        }
    }
    //
    public void append(String string)
    {
        builder.append(string);
    }
    //
    @SuppressWarnings("unchecked")
    private void addReferences(Sexpression sexpression)
    {
        if (addReferenceIfNeeded(sexpression))
        {
            if (sexpression instanceof List)
            {
                List list = (List) sexpression;
                Iterator<Sexpression> iterator = list.elementIterator();
                //
                while (iterator.hasNext())
                {
                    Sexpression element = iterator.next();
                    addReferences(element);
                }
            }
            else if (sexpression instanceof AssociativeContainer)
            {
                AssociativeContainer<Sexpression> container = (AssociativeContainer<Sexpression>) sexpression;
                Iterator<Sexpression> iterator = container.keyIterator();
                //
                while (iterator.hasNext())
                {
                    Sexpression key = iterator.next();
                    Sexpression value = container.get(key);
                    //
                    if (container.hasComplexKeys()) addReferences(key);
                    //
                    addReferences(value);
                }
            }
        }
    }
    //
    private boolean addReferenceIfNeeded(Sexpression sexpression)
    {
        if (sexpression instanceof Formattable)
        {
            Formattable formattable = (Formattable) sexpression;
            Long id = idsbyformattable.get(formattable);
            //
            if (id == null)
            {
                idsbyformattable.put(formattable, NO_ID);
                //
                return true;
            }
            //
            if (id == NO_ID) idsbyformattable.put(formattable, nextid++);
        }
        //
        return false;
    }
    //
    private boolean needsFormatting(Sexpression sexpression)
    {
        if (sexpression instanceof Formattable)
        {
            Long id = idsbyformattable.get(sexpression);
            //
            if (id == null) throw new IllegalStateException();
            //
            if (id == NO_ID) return true;
            //
            if (ids.contains(id))
            {
                builder.append("#");
                builder.append(id);
                builder.append("#");
                //
                return false;
            }
            //
            ids.add(id);
            builder.append("#");
            builder.append(id);
            builder.append("=");
        }
        //
        return true;
    }
}