package lisp.formatter.strategy;
/*
 * Copyright (C) 2012, 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.RubyStyleObject;
import lisp.Sexpression;
import lisp.SimpleClass;
import lisp.Symbol;
/*
 * Created by andreasm on 21.12.12 at 17:36
 */
final class InstanceStrategy extends AssociativeContainerStrategy
{
    @Override
    public String getPreamble(Sexpression sexpression)
    {
        RubyStyleObject object = (RubyStyleObject) sexpression;
        Symbol classname = getClassName(object);
        StringBuilder builder = new StringBuilder();
        builder.append("#.(let ((instance (allocate-instance ");
        //
        if (classname == null)
        {
            builder.append("nil");
        }
        else
        {
            builder.append(classname);
        }
        //
        builder.append("))) (progn ");
        //
        return builder.toString();
    }
    //
    @Override
    public String getSeparator()
    {
        return " ";
    }
    //
    @Override
    public String getBeforeKey(Sexpression key)
    {
        return "(.= instance ";
    }
    //
    @Override
    public String getAfterKey(Sexpression key)
    {
        return " ";
    }
    //
    @Override
    public String getBeforeValue(Sexpression value)
    {
        if (isConstant(value)) return "";
        //
        return "(quote ";
    }
    //
    @Override
    public String getAfterValue(Sexpression value)
    {
        if (isConstant(value)) return ")";
        //
        return "))";
    }
    //
    @Override
    public String getAppendix(Sexpression sexpression)
    {
        RubyStyleObject object = (RubyStyleObject) sexpression;
        //
        if (object.isFrozen()) return " (freeze instance)))";
        //
        return " instance))";
    }
    //
    private static Symbol getClassName(RubyStyleObject object)
    {
        SimpleClass clazz = object.classOf();
        //
        if (clazz == null) return null;
        //
        return clazz.getClassName();
    }
}