package lisp.formatter.strategy;
/*
 * Copyright (C) 2012, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Array;
import lisp.Sexpression;
/*
 * Created by andreasm on 21.12.12 at 17:28
 */
final class ArrayStrategy extends AssociativeContainerStrategy
{
    @Override
    public String getPreamble(Sexpression sexpression)
    {
        Array array = (Array) sexpression;
        int[] dimensions = array.getDimensions();
        StringBuilder builder = new StringBuilder();
        builder.append("#.(let ((array (make-array (quote (");
        //
        boolean first = true;
        //
        for (int dimension : dimensions)
        {
            if (first)
            {
                first = false;
            }
            else
            {
                builder.append(" ");
            }
            //
            builder.append(dimension);
        }
        //
        builder.append("))))) (progn ");
        //
        return builder.toString();
    }
    //
    @Override
    public String getSeparator()
    {
        return " ";
    }
    //
    @Override
    public String getBeforeKey(Sexpression key)
    {
        return "(set-array-element array (quote ";
    }
    //
    @Override
    public String getAfterKey(Sexpression key)
    {
        return ") ";
    }
    //
    @Override
    public String getBeforeValue(Sexpression value)
    {
        if (isConstant(value)) return "";
        //
        return "(quote ";
    }
    //
    @Override
    public String getAfterValue(Sexpression value)
    {
        if (isConstant(value)) return ")";
        //
        return "))";
    }
    //
    @Override
    public String getAppendix(Sexpression sexpression)
    {
        Array array = (Array) sexpression;
        //
        if (array.isFrozen()) return " (freeze array)))";
        //
        return " array))";
    }
}