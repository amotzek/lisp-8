package lisp.formatter.strategy;
/*
 * Copyright (C) 2012, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.formatter.Formattable;
import java.util.HashMap;
/*
 * Created by  andreasm 24.12.12 10:16
 */
public final class StrategyFactory
{
    private static final FormattingStrategy NIL_STRATEGY = new NilStrategy();
    private static final FormattingStrategy TO_STRING_STRATEGY = new ToStringStrategy();
    //
    private static final HashMap<String, FormattingStrategy> strategiesbytype = new HashMap<>();
    //
    static
    {
        strategiesbytype.put("array", new ArrayStrategy());
        strategiesbytype.put("hash-table", new HashTableStrategy());
        strategiesbytype.put("bytes", TO_STRING_STRATEGY);
        strategiesbytype.put("instance", new InstanceStrategy());
        strategiesbytype.put("list", new ListStrategy());
    }
    //
    private StrategyFactory()
    {
    }
    //
    public static FormattingStrategy findStrategyFor(Sexpression sexpression)
    {
        if (sexpression == null) return NIL_STRATEGY;
        //
        if (sexpression instanceof Formattable)
        {
            String type = sexpression.getType();
            FormattingStrategy strategy = strategiesbytype.get(type);
            //
            if (strategy == null) throw new IllegalStateException("no formatting strategy for type " + type);
            //
            return strategy;
        }
        //
        return TO_STRING_STRATEGY;
    }
}