package lisp.formatter.strategy;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.HashTable;
import lisp.Sexpression;
/*
 * Created by andreasm on 26.04.13 at 15:18
 */
final class HashTableStrategy extends AssociativeContainerStrategy
{
    @Override
    public String getPreamble(Sexpression sexpression)
    {
        return "#.(let ((hash-table (make-hash-table))) (progn ";
    }
    //
    @Override
    public String getSeparator()
    {
        return " ";
    }
    //
    @Override
    public String getBeforeKey(Sexpression key)
    {
        if (isConstant(key)) return "(put-hash-table-value hash-table ";
        //
        return "(put-hash-table-value hash-table (quote ";
    }
    //
    @Override
    public String getAfterKey(Sexpression key)
    {
        if (isConstant(key)) return " ";
        //
        return ") ";
    }
    //
    @Override
    public String getBeforeValue(Sexpression value)
    {
        if (isConstant(value)) return "";
        //
        return "(quote ";
    }
    //
    @Override
    public String getAfterValue(Sexpression value)
    {
        if (isConstant(value)) return ")";
        //
        return "))";
    }
    //
    @Override
    public String getAppendix(Sexpression sexpression)
    {
        HashTable hashtable = (HashTable) sexpression;
        //
        if (hashtable.isFrozen()) return " (freeze hash-table)))";
        //
        return " hash-table))";
    }
}