package lisp;
/*
 * Copyright (C) 2012, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.junction.FutureJunction;
/*
 * Created by  andreasm 18.11.12 17:48
 */
public final class Future extends FutureJunction
{
    public Future()
    {
        super();
    }
    //
    public String getType()
    {
        return "future";
    }
    //
    @Override
    public String toString()
    {
        return "#.(throw (quote error) \"futures cannot be read or written\")";
    }
}