package lisp;
/*
 * Copyright (C) 2010 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
/*
 * Created 23.2.2010
 * 
 * @author andreasm
 */
final class SymbolReference extends WeakReference<Symbol>
{
    private final String name;
    /**
     * Constructor for SymbolReference
     * 
     * @param symbol Symbol
     * @param queue Reference Queue
     */
    SymbolReference(Symbol symbol, ReferenceQueue<? super Symbol> queue)
    {
        super(symbol, queue);
        //
        name = symbol.getName();
    }
    /**
     * Method getName
     * 
     * @return Name
     */
    public String getName()
    {
        return name;
    }
}