package lisp;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.junction.LockJunction;
/*
 * Created by andreasm 03.03.13 08:57
 */
public final class Lock extends LockJunction
{
    /**
     * Constructor for Lock
     */
    public Lock()
    {
        super();
    }
    /**
     * @see lisp.Sexpression#getType()
     */
    public String getType()
    {
        return "lock";
    }
    /**
     * @see Object#toString()
     */
    @Override
    public String toString()
    {
        return "#.(throw (quote error) \"locks cannot be read or written\")";
    }
}