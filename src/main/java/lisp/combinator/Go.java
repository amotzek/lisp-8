package lisp.combinator;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Combinator;
import lisp.Future;
import lisp.Sexpression;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Created by  andreasm 18.11.12 18:02
 */
public final class Go extends Combinator
{
    public Go()
    {
        super(1, 1);
    }
    //
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        Sexpression sexpression = arguments[0];
        Future future = new Future();
        //
        if (isConstant(sexpression))
        {
            future.succeed(sexpression);
        }
        else
        {
            sexpression.enQueueEvaluator(runnablequeue, environment, future, future);
        }
        //
        succeed.succeed(future);
    }
    //
    public String toString()
    {
        return "go";
    }
}