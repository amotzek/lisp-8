package lisp.combinator;
/*
 * Copyright (C) 2019 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
/*
 * Erstellungsdatum: (09.10.2019)
 *
 * @author Andreasm
 */
public final class RegexpMatches extends TypeCheckCombinator
{
    /**
     * Constructor for RegexpMatches
     */
    public RegexpMatches()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(Environment, Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Chars regexp = getChars(arguments, 0);
        Chars str = getChars(arguments, 1);
        //
        try
        {
            Pattern pattern = Pattern.compile(regexp.getString());
            Matcher matcher = pattern.matcher(str.getString());
            //
            if (matcher.matches())
            {
                List groups = null;
                //
                for (int groupindex = matcher.groupCount(); groupindex >= 0; groupindex--)
                {
                    String grp = matcher.group(groupindex);
                    Chars group = grp == null ? null : new Chars(grp);
                    groups = new List(group, groups);
                }
                //
                return groups;
            }
            //
            return null;
        }
        catch (PatternSyntaxException e)
        {
            throw new CannotEvalException(createErrorMessage("regular expression", regexp));
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "regexp-matches";
    }
}