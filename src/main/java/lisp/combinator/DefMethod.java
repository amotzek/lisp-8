package lisp.combinator;
/*
 * Copyright (C) 2011, 2013, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Combinator;
import lisp.GenericFunction;
import lisp.GuardedMethod;
import lisp.Lambda;
import lisp.List;
import lisp.Sexpression;
import lisp.SimpleClass;
import lisp.Symbol;
import lisp.compiler.Compiler;
import lisp.environment.Environment;
import lisp.environment.NotBoundException;
import static lisp.List.length;
import static lisp.List.reverse;
import lisp.trait.ValidationException;
import lisp.trait.Validator;
import lisp.trait.ValidatorFactory;
/*
 * Erstellungsdatum: (14.10.2011)
 *
 * @author Andreasm
 */
public final class DefMethod extends TypeCheckCombinator
{
    /**
     * Constructor for DefMethod
     */
    public DefMethod()
    {
        super(15, 4);
    }
    /**
     * Ensures that the Function Name is bound to a Generic Function
     *
     * @param environment Environment
     * @param functionname Name of Generic Function
     * @return Generic Function
     * @throws CannotEvalException if the Name is bound to a Sexpression that cannot converted to a Method
     */
    private static GenericFunction ensureGenericFunction(Environment environment, Symbol functionname) throws CannotEvalException
    {
        Sexpression value = null;
        //
        try
        {
            value = environment.at(functionname);
        }
        catch (NotBoundException e)
        {
            // value = null
        }
        //
        if (value instanceof GenericFunction) return (GenericFunction) value;
        //
        GenericFunction function = new GenericFunction(functionname);
        //
        if (value instanceof Lambda)
        {
            function.addLambda((Lambda) value);
        }
        else if (value instanceof Combinator)
        {
            function.addCombinator((Combinator) value);
        }
        else if (value != null)
        {
            throw new CannotEvalException("cannot convert " + value + " into a method");
        }
        //
        environment.add(true, functionname, function);
        //
        return function;
    }
    /**
     * Extracts the Parameters from the Specialized Parameter List
     *
     * @param specializedparameters Specialized Parameter List
     * @return Parameters
     * @throws CannotEvalException if a malformed List was provided
     */
    private static List getParameters(List specializedparameters) throws CannotEvalException
    {
        List parameters = null;
        //
        while (specializedparameters != null)
        {
            Sexpression specializedparameter = specializedparameters.first();
            Symbol parameter = getParameter(specializedparameter);
            parameters = new List(parameter, parameters);
            specializedparameters = specializedparameters.rest();
        }
        //
        return reverse(parameters);
    }
    /**
     * Tries to extract a Parameter
     *
     * @param specializedparameter Specialized Parameter
     * @return Class Constraint or null
     * @throws CannotEvalException if a malformed Specialized Parameter was provided
     */
    private static Symbol getParameter(Sexpression specializedparameter) throws CannotEvalException
    {
        try
        {
            if (specializedparameter instanceof List)
            {
                List list = (List) specializedparameter;
                //
                return (Symbol) list.first();
            }
            //
            return (Symbol) specializedparameter;
        }
        catch (ClassCastException e)
        {
            // throw CannotEvalException
        }
        //
        throw new CannotEvalException(specializedparameter + " is not a well-formed specialized parameter");
    }
    /**
     * Extracts the Class Constraints from the Specialized Parameter List
     *
     * @param environment Environment
     * @param specializedparameters Specialized Parameter List
     * @return Class Constraints
     * @throws CannotEvalException if a malformed List was provided
     */
    private static SimpleClass[] getConstraints(Environment environment, List specializedparameters) throws CannotEvalException
    {
        SimpleClass[] constraints = new SimpleClass[length(specializedparameters)];
        int i = 0;
        //
        while (specializedparameters != null)
        {
            Sexpression specializedparameter = specializedparameters.first();
            constraints[i] = getConstraint(environment, specializedparameter);
            specializedparameters = specializedparameters.rest();
            i++;
        }
        //
        return constraints;
    }
    /**
     * Tries to extract a Class Constraint
     *
     * @param environment Environment
     * @param specializedparameter Specialized Parameter
     * @return Class Constraint or null
     * @throws CannotEvalException if a malformed Specialized Parameter was provided
     */
    private static SimpleClass getConstraint(Environment environment, Sexpression specializedparameter) throws CannotEvalException
    {
        try
        {
            if (specializedparameter instanceof List)
            {
                List list = (List) specializedparameter;
                list = list.rest();
                Symbol classname = (Symbol) list.first();
                //
                return (SimpleClass) environment.at(classname);
            }
            //
            return null;
        }
        catch (NullPointerException e)
        {
            // throw CannotEvalException
        }
        catch (ClassCastException e)
        {
            // throw CannotEvalException
        }
        catch (NotBoundException e)
        {
            // throw CannotEvalException
        }
        //
        throw new CannotEvalException(specializedparameter + " is not a well-formed specialized parameter");
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Symbol functionname = getSymbol(arguments, 0, false);
        List specializedparameters = getList(arguments, 1);
        GenericFunction function = ensureGenericFunction(environment, functionname);
        List parameters = getParameters(specializedparameters);
        SimpleClass[] constraints = getConstraints(environment, specializedparameters);
        Sexpression guard = arguments[2];
        Sexpression body = arguments[3];
        Compiler compiler = new Compiler(parameters, body, environment);
        compiler.compile();
        Sexpression compiledbody = compiler.getResult();
        GuardedMethod method = new GuardedMethod(parameters, constraints, guard, body, compiledbody, environment);
        Validator validator = ValidatorFactory.createValidator();
        //
        try
        {
            validator.validateMethod(function, method);
            function.addMethod(method);
            //
            return method;
        }
        catch (ValidationException e)
        {
            throw new CannotEvalException(e.getMessage());
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "defmethod";
    }
}