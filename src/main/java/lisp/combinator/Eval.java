package lisp.combinator;
/*
 * Copyright (C) 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Combinator;
import lisp.Sexpression;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Erstellungsdatum: (26.03.2014)
 *
 * @author andreasm
 */
public final class Eval extends Combinator
{
    /**
     * Constructor for Eval
     */
    public Eval()
    {
        super(0, 1);
    }
    //
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        Sexpression argument = arguments[0];
        //
        if (isConstant(argument))
        {
            succeed.succeed(argument);
            //
            return;
        }
        //
        argument.enQueueEvaluator(runnablequeue, environment, succeed, fail);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "eval";
    }
}