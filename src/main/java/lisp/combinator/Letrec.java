package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Combinator;
import lisp.List;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.BindValue;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
import java.util.concurrent.atomic.AtomicInteger;
/*
 * @author Andreasm
 */
public final class Letrec extends Combinator
{
    /**
     * Construtor for Letrec
     */
    public Letrec()
    {
        super(3, 2);
    }
    /**
     * @see Combinator#apply(lisp.concurrent.RunnableQueue, lisp.environment.Environment, lisp.Sexpression[], lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        Sexpression body = arguments[1];
        //
        if (isConstant(body))
        {
            succeed.succeed(body);
            //
            return;
        }
        //
        Environment child = new Environment(environment);
        //
        try
        {
            List definitions = (List) arguments[0];
            int definitioncount = List.length(definitions);
            AtomicInteger awaited = new AtomicInteger(definitioncount);
            boolean isawaited = false;
            //
            while (definitions != null)
            {
                List definition = (List) definitions.first();
                Symbol name = (Symbol) definition.first();
                definition = definition.rest();
                Sexpression value = definition.first();
                definition = definition.rest();
                //
                if (definition != null) throw new NullPointerException();
                //
                if (isConstant(value))
                {
                    child.add(false, name, value);
                    awaited.decrementAndGet();
                }
                else
                {
                    BindValue bindvalue = new BindValue(runnablequeue, child, name, awaited, body, succeed, fail);
                    value.enQueueEvaluator(runnablequeue, child, bindvalue, fail);
                    isawaited = true;
                }
                //
                definitions = definitions.rest();
            }
            //
            if (!isawaited) body.enQueueEvaluator(runnablequeue, child, succeed, fail);
            //
            return;
        }
        catch (ClassCastException e)
        {
        }
        catch (NullPointerException e)
        {
        }
        //
        StringBuilder builder = new StringBuilder();
        builder.append("syntax error in letrec ");
        builder.append(arguments[0]);
        fail.fail(Symbol.createSymbol("error"), new Chars(builder.toString()));
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "letrec";
    }
}