package lisp.combinator;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.List;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
import static lisp.List.reverse;
/*
 * Erstellungsdatum: (19.7.2012)
 *
 * @author Andreasm
 */
public final class Split extends TypeCheckCombinator
{
    /**
     * Constructor for Split
     */
    public Split()
    {
        super(0, 2);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        List list = getList(arguments, 0);
        Rational rational = getRational(arguments, 1);
        int count = rational.intValue();
        //
        if (count < 1) throw new CannotEvalException("cannot split into " + count + " parts");
        //
        List[] parts = new List[count];
        int index = 0;
        //
        while (list != null)
        {
            Sexpression element = list.first();
            parts[index] = new List(element, parts[index++]);
            //
            if (index >= count) index = 0;
            //
            list = list.rest();
        }
        //
        for (index = count - 1; index >= 0; index--)
        {
            list = new List(reverse(parts[index]), list);
        }
        //
        return list;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "split";
    }
}