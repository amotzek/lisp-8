package lisp.combinator;
/*
 * Copyright (C) 2011, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.SimpleClass;
import lisp.Symbol;
import lisp.environment.Environment;
import lisp.environment.NotBoundException;
import lisp.trait.ValidationException;
import lisp.trait.Validator;
import lisp.trait.ValidatorFactory;
import java.util.LinkedList;
/*
 * Erstellungsdatum: (11.10.2011)
 *
 * @author Andreasm
 */
public final class DefClass extends TypeCheckCombinator
{
    /**
     * Constructor for DefClass
     */
    public DefClass()
    {
        super(3, 2);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Symbol classname = getSymbol(arguments, 0, false);
        List superclassnames = getList(arguments, 1);
        LinkedList<SimpleClass> superclasses = new LinkedList<SimpleClass>();
        //
        try
        {
            Sexpression value = environment.at(classname);
            //
            if (value instanceof SimpleClass) return value;
            //
            if (value != null) throw new CannotEvalException(classname + " already set");
        }
        catch (NotBoundException e)
        {
        }
        //
        try
        {
            List list = superclassnames;
            //
            while (list != null)
            {
                Symbol superclassname = (Symbol) list.first();
                SimpleClass superclass = (SimpleClass) environment.at(superclassname);
                superclasses.addLast(superclass);
                list = list.rest();
            }
        }
        catch (ClassCastException e)
        {
            throw new CannotEvalException(superclassnames + " is not a list of class or trait names");
        }
        catch (NotBoundException e)
        {
            throw new CannotEvalException(e.getUnboundSymbol() + " is not a class or trait name");
        }
        //
        try
        {
            SimpleClass simpleclass = new SimpleClass(classname, true, superclasses);
            Validator validator = ValidatorFactory.createValidator();
            validator.validateClass(simpleclass);
            environment.add(true, classname, simpleclass);
            //
            return simpleclass;
        }
        catch (ValidationException e)
        {
            throw new CannotEvalException(e.getMessage());
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "defclass";
    }
}