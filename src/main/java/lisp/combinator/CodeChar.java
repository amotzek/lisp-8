/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.combinator;
//
// CodeChar.java
// JL
//
// Created by andreasm on Fri Aug 31 2001.
//
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * @author Andreasm
 */
public final class CodeChar extends TypeCheckCombinator
{
    /**
     * Constructor for CodeChar
     */
    public CodeChar()
    {
        super(0, 1);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Rational rational = getRational(arguments, 0);
        int integer = rational.intValue();
        String string = Character.toString((char) integer);
        //
        return new Chars(string);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "code-char";
    }
}