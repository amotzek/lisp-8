package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011, 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Combinator;
import lisp.Sexpression;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Erstellungsdatum: (10.5.2013)
 *
 * @author Andreasm
 */
public final class Or2 extends Combinator
{
    /**
     * Constructor for Or2
     */
    public Or2()
    {
        super(2, 2);
    }
    /**
     * @see lisp.Function#apply(lisp.concurrent.RunnableQueue, lisp.environment.Environment, lisp.Sexpression[], lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        Sexpression condition = arguments[0];
        Sexpression action = arguments[1];
        //
        if (condition != null)
        {
            succeed.succeed(condition);
            //
            return;
        }
        //
        if (isConstant(action))
        {
            succeed.succeed(action);
            //
            return;
        }
        //
        action.enQueueEvaluator(runnablequeue, environment, succeed, fail);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "or2";
    }
}