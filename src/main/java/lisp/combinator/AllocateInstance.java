package lisp.combinator;
/*
 * Copyright (C) 2011, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.RubyStyleObject;
import lisp.Sexpression;
import lisp.SimpleClass;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (11.10.2011)
 *
 * @author andreasm
 */
public final class AllocateInstance extends TypeCheckCombinator
{
    /**
     * Constructor for AllocateInstance
     */
    public AllocateInstance()
    {
        super(0, 1);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        SimpleClass simpleclass = getClass(arguments, 0);
        //
        if (simpleclass.isAllocatable()) return new RubyStyleObject(simpleclass);
        //
        throw new CannotEvalException("cannot allocate instance of trait " + simpleclass.getClassName());
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "allocate-instance";
    }
}