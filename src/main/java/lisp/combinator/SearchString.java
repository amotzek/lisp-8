package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (25.08.2001)
 *
 * @author Andreasm
 */
public final class SearchString extends TypeCheckCombinator
{
    /**
     * Constructor for SearchString
     */
    public SearchString()
    {
        super(0, 3);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Chars part = getChars(arguments, 0);
        Chars whole = getChars(arguments, 1);
        Rational start = getRational(arguments, 2);
        int position = whole.getString().indexOf(part.getString(), start.intValue() - 1);
        //
        if (position >= 0) return new Rational(position + 1);
        //
        return null;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "search-string";
    }
}