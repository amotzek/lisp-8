package lisp.combinator;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Sexpression;
import lisp.environment.Environment;
import lisp.Place;
import java.util.NoSuchElementException;
/*
 * Created by andreasm 17.10.13 21:45
 */
public final class MakePlace extends TypeCheckCombinator
{
    public MakePlace()
    {
        super(0, 1);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Chars chars = getChars(arguments, 0);
        String string = chars.getString();
        //
        try
        {
            return Place.getInstance(string);
        }
        catch (NoSuchElementException ignored)
        {
        }
        catch (NumberFormatException ignored)
        {
        }
        //
        throw new CannotEvalException("invalid syntax for a place: " + chars);
    }
    //
    @Override
    public String toString()
    {
        return "make-place";
    }
}