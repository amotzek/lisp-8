package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (25.08.2001)
 *
 * @author Andreasm
 */
public final class IsEqual extends Predicate
{
    /**
     * Constructor for IsEqual
     */
    public IsEqual()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Sexpression sexpr1 = getSexpression(arguments, 0);
        Sexpression sexpr2 = getSexpression(arguments, 1);
        //
        return createBoolean((sexpr1 == null) ? (sexpr2 == null) : sexpr1.equals(sexpr2));
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "eq?";
    }
}