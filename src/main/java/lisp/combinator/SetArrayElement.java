/*
 * Copyright (C) 2006, 2010, 2011, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.combinator;
/*
 * Created 01.11.2006
 */
import lisp.Array;
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * @author Andreasm
 */
public final class SetArrayElement extends TypeCheckCombinator
{
    /**
     * Constructor for SetArrayElement
     */
    public SetArrayElement()
    {
        super(0, 3);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Array array = getArray(arguments, 0);
        List subscripts = getNonEmptyList(arguments, 1);
        Sexpression value = getSexpression(arguments, 2);
        array.checkBounds(subscripts);
        //
        if (array.isFrozen()) throw new CannotEvalException("array " + array + " is frozen in " + this);
        //
        array.put(subscripts, value);
        //
        return value;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "set-array-element";
    }
}