package lisp.combinator;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (23.11.2012)
 *
 * @author Andreasm
 */
public final class IsMember extends TypeCheckCombinator
{
    /**
     * Constructor for IsMember
     */
    public IsMember()
    {
        super(0, 2);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Sexpression element = getSexpression(arguments, 0);
        List list = getList(arguments, 1);
        //
        if (list == null) return null;
        //
        return list.isMember(element);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "member?";
    }
}