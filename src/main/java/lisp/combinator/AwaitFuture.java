package lisp.combinator;
/*
 * Copyright (C) 2012, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Combinator;
import lisp.Future;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Created by  andreasm 18.11.12 18:11
 */
public final class AwaitFuture extends Combinator
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars ARGUMENT_TYPE_WRONG = new Chars("argument must be future in await-future");
    private static final Chars FUTURE_NIL = new Chars("nil not allowed as future for await-future");
    /**
     * Constructor for AwaitFuture
     */
    public AwaitFuture()
    {
        super(0, 1);
    }
    //
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        try
        {
            Future future = (Future) arguments[0];
            future.addWaiter(runnablequeue, succeed, fail);
        }
        catch (ClassCastException e)
        {
            fail.fail(ERROR, ARGUMENT_TYPE_WRONG);
        }
        catch (NullPointerException e)
        {
            fail.fail(ERROR, FUTURE_NIL);
        }
    }
    //
    public String toString()
    {
        return "await-future";
    }
}