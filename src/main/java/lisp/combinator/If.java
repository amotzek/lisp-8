package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011, 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Combinator;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.Sexpression;
import lisp.concurrent.RunnableQueue;
/*
 * Erstellungsdatum: (25.08.2001)
 *
 * @author Andreasm
 */
public final class If extends Combinator
{
    /**
     * Constructor for If
     */
    public If()
    {
        super(6, 3);
    }
    /**
     * @see lisp.Function#apply(lisp.concurrent.RunnableQueue, lisp.environment.Environment, lisp.Sexpression[], lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        Sexpression condition = arguments[0];
        Sexpression action = (condition == null) ? arguments[2] : arguments[1];
        //
        if (isConstant(action))
        {
            succeed.succeed(action);
            //
            return;
        }
        //
        action.enQueueEvaluator(runnablequeue, environment, succeed, fail);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "if";
    }
}