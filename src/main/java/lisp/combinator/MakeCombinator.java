package lisp.combinator;
/*
 * Copyright (C) 2002, 2006, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Combinator;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * Erstellungsdatum: (18.02.2002 23:56:25)
 *
 * @author Andreasm
 */
public final class MakeCombinator extends TypeCheckCombinator
{
    private static final Class<?>[] PARAMETERTYPES = new Class[] {Sexpression.class};
    //
    private static final Logger logger = Logger.getLogger("lisp.combinator.MakeCombinator");
    /**
     * Constructor for MakeCombinator
     */
    public MakeCombinator()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Chars classname = getChars(arguments, 0);
        Sexpression initarg = getSexpression(arguments, 1);
        //
        try
        {
            Class<?> clazz = Class.forName(classname.getString());
            Class<? extends Combinator> combinatorclass = clazz.asSubclass(Combinator.class);
            //
            try
            {
                Constructor<? extends Combinator> constructor = combinatorclass.getConstructor(PARAMETERTYPES);
                Object[] initargs = new Object[] {initarg};
                //
                return constructor.newInstance(initargs);
            }
            catch (NoSuchMethodException e)
            {
                if (initarg != null) throw e;
                //
                return combinatorclass.newInstance();
            }
        }
        catch (InvocationTargetException e1)
        {
            Throwable e2 = e1.getCause();
            //
            if (e2 == null) e2 = e1;
            //
            logger.log(Level.WARNING, "cannot make combinator", e2);
            //
            throw new CannotEvalException("cannot make combinator " + classname + ": " + e2);
        }
        catch (Exception e)
        {
            logger.log(Level.WARNING, "cannot make combinator", e);
            //
            throw new CannotEvalException("cannot make combinator " + classname + ": " + e);
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "make-combinator";
    }
}