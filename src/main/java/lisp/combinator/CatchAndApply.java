package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011, 2012, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Combinator;
import lisp.Function;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.CatchThrow;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
//
// CatchAndApply.java
// JL
//
// Created by andreasm on Fri Aug 31 2001.
//
/*
 * @author Andreasm
 */
public final class CatchAndApply extends Combinator
{
    /**
     * Constructor for CatchAndApply
     */
    public CatchAndApply()
    {
        super(4, 3);
    }
    /**
     * @see lisp.Function#apply(lisp.concurrent.RunnableQueue, lisp.environment.Environment, lisp.Sexpression[], lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        try
        {
            Symbol name = (Symbol) arguments[0];
            Function function = (Function) arguments[1];
            Sexpression sexpression = arguments[2];
            //
            if (function == null || !function.acceptsArgumentCount(2))
            {
                StringBuilder builder = new StringBuilder();
                builder.append(function);
                builder.append(" is not a function with two parameters in catch-and-apply");
                fail.fail(Symbol.createSymbol("error"), new Chars(builder.toString()));
                //
                return;
            }
            //
            if (isConstant(sexpression))
            {
                succeed.succeed(sexpression);
                //
                return;
            }
            //
            CatchThrow catchthrow = new CatchThrow(runnablequeue, environment, name, function, succeed, fail);
            sexpression.enQueueEvaluator(runnablequeue, environment, succeed, catchthrow);
        }
        catch (ClassCastException e)
        {
            StringBuilder builder = new StringBuilder();
            builder.append(arguments[0]);
            builder.append(" must be a symbol and ");
            builder.append(arguments[1]);
            builder.append(" must be a lambda in catch-and-apply");
            fail.fail(Symbol.createSymbol("error"), new Chars(builder.toString()));
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "catch-and-apply";
    }
}