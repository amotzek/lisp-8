/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.combinator;
//
// WriteToString.java
// JL
//
// Created by andreasm on Wed Sep 05 2001.
//
import lisp.Chars;
import lisp.Combinator;
import lisp.Sexpression;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * @author Andreasm
 */
public final class WriteToString extends Combinator
{
    private static final Chars NIL = new Chars("nil");
    /**
     * Constructor for WriteToString
     */
    public WriteToString()
    {
        super(0, 1);
    }
    /**
     * @see Combinator#apply(lisp.concurrent.RunnableQueue, lisp.environment.Environment, lisp.Sexpression[], lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        Sexpression sexpression = arguments[0];
        //
        if (sexpression == null)
        {
            succeed.succeed(NIL);
            //
            return;
        }
        //
        String string = sexpression.toString();
        succeed.succeed(new Chars(string));
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "write-to-string";
    }
}