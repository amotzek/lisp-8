package lisp.combinator;
/*
 * Copyright (C) 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Combinator;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
/*
 * Created by Andreas on 01.03.2016.
 */
public final class ExecutorCombinator extends Combinator
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars REJECTED = new Chars("execution rejected");
    //
    private final Combinator combinator;
    private final Executor executor;
    //
    public ExecutorCombinator(Combinator combinator, Executor executor)
    {
        super(0, combinator.getParameterCount());
        //
        this.combinator = combinator;
        this.executor = executor;
    }
    //
    @Override
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        try
        {
            executor.execute(() -> combinator.apply(runnablequeue, environment, arguments, succeed, fail));
        }
        catch (RejectedExecutionException e)
        {
            fail.fail(ERROR, REJECTED);
        }
    }
    //
    @Override
    public String toString()
    {
        return combinator.toString();
    }
}