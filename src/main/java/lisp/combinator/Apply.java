package lisp.combinator;
/*
 * Copyright (C) 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Combinator;
import lisp.Function;
import lisp.List;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Erstellungsdatum: (27.10.2014)
 *
 * @author andreasm
 */
public final class Apply extends Combinator
{
    private static final Symbol QUOTE = Symbol.createSymbol("quote");
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars CANNOT_APPLY = new Chars("apply expects a function and a list");
    /**
     * Constructor for Apply
     */
    public Apply()
    {
        super(0, 2);
    }
    //
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        try
        {
            Function function = (Function) arguments[0];
            List values = (List) arguments[1];
            List intermediate = new List(function, null);
            //
            while (values != null)
            {
                Sexpression value = values.first();
                values = values.rest();
                List quotedvalue = List.list(QUOTE, value);
                intermediate = new List(quotedvalue, intermediate);
            }
            //
            intermediate = List.reverse(intermediate);
            intermediate.enQueueEvaluator(runnablequeue, environment, succeed, fail);
        }
        catch (Exception e)
        {
            fail.fail(ERROR, CANNOT_APPLY);
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "apply";
    }
}