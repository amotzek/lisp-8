package lisp.combinator;
/*
 * Copyright (C) 2009, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * 6.11.2009
 *
 * @author andreasm
 */
public final class CollectInOrder extends TypeCheckCombinator
{
    /**
     * Constructor for CollectInOrder
     */
    public CollectInOrder()
    {
        super(0, 1);
    }
    /**
     * GuardedMethod traverse
     *
     * @param tree      Tree
     * @param collector Visitor to traverse the Tree
     */
    private static void traverse(List tree, Collector collector)
    {
        if (tree == null) return;
        //
        tree = tree.rest();
        List left = (List) tree.first();
        tree = tree.rest();
        Sexpression value = tree.first();
        tree = tree.rest();
        List right = (List) tree.first();
        traverse(right, collector);
        collector.collect(value);
        traverse(left, collector);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        List tree = getList(arguments, 0);
        //
        if (tree == null) return null;
        //
        Collector collector = new Collector();
        traverse(tree, collector);
        //
        return collector.list;
    }
    /**
     * Inner class Collector
     */
    private static final class Collector
    {
        private List list;
        //
        private Collector()
        {
            super();
        }
        /*
        * GuardedMethod collect
        *
        * @param sexpression Value to cons
        */
        private void collect(Sexpression sexpression)
        {
            list = new List(sexpression, list);
        }
    }
}