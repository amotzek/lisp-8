package lisp.combinator;
/*
 * Copyright (C) 2001, 2009, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (25.08.2001)
 *
 * @author Andreasm
 */
public final class Quotient extends TypeCheckCombinator
{
    /**
     * Constructor for Quotient
     */
    public Quotient()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Rational divident = getRational(arguments, 0);
        Rational divisor = getRational(arguments, 1);
        //
        if (divisor.isZero()) throw new CannotEvalException("divide by zero");
        //
        Rational quotient = divisor.multiplicativeInverse();
        quotient.times(divident);
        //
        return quotient;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "quotient";
    }
}