package lisp.combinator;
/*
 * Copyright (C) 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (12.01.2018)
 *
 * @author Andreasm
 */
public final class LogXor2 extends TypeCheckCombinator
{
    /**
     * Constructor for LogXor2
     */
    public LogXor2()
    {
        super(0, 2);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Rational value1 = getRational(arguments, 0);
        Rational value2 = getRational(arguments, 1);
        //
        return new Rational(value1.bigIntegerValue().xor(value2.bigIntegerValue()));
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "logxor2";
    }
}