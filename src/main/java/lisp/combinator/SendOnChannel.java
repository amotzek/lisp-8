package lisp.combinator;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Channel;
import lisp.Chars;
import lisp.Combinator;
import lisp.Rational;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Created by  andreasm 17.11.12 17:14
 */
public final class SendOnChannel extends Combinator
{
    private static final Rational INFINITY = new Rational(Long.MAX_VALUE);
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars TIMEOUT_NEGATIVE = new Chars("timeout must not be negative in send-on-channel");
    private static final Chars TIMEOUT_NOT_LONG = new Chars("timeout must be a long in send-on-channel");
    private static final Chars ARGUMENT_TYPES_WRONG = new Chars("first argument must be a channel and second argument a number in send-on-channel");
    private static final Chars CHANNEL_NIL = new Chars("nil not allowed as channel for send-on-channel");
    /**
     * Constructor for SendOnChannel
     */
    public SendOnChannel()
    {
        super(0, 3);
    }
    //
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        try
        {
            Channel channel = (Channel) arguments[0];
            Sexpression message = arguments[1];
            Rational timeout = (Rational) arguments[2];
            //
            if (timeout == null)
            {
                timeout = INFINITY;
            }
            else if (timeout.isNegative())
            {
                fail.fail(ERROR, TIMEOUT_NEGATIVE);
                //
                return;
            }
            //
            channel.addSender(runnablequeue, succeed, fail, timeout.longValue(), message);
        }
        catch (ClassCastException e)
        {
            fail.fail(ERROR, ARGUMENT_TYPES_WRONG);
        }
        catch (NullPointerException e)
        {
            fail.fail(ERROR, CHANNEL_NIL);
        }
        catch (CannotEvalException e)
        {
            fail.fail(ERROR, TIMEOUT_NOT_LONG);
        }
    }
    //
    public String toString()
    {
        return "send-on-channel";
    }
}