/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.combinator;
/*
 * Created 26.04.2013
 */
import lisp.CannotEvalException;
import lisp.HashTable;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * @author Andreasm
 */
public final class ClearHashTable extends TypeCheckCombinator
{
    /**
     * Constructor for ClearHashTable
     */
    public ClearHashTable()
    {
        super(0, 1);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        HashTable hashtable = getHashTable(arguments, 0);
        //
        if (hashtable.isFrozen()) throw new CannotEvalException("hash-table " + hashtable + " is frozen in " + this);
        //
        hashtable.clear();
        //
        return hashtable;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "clear-hash-table";
    }
}