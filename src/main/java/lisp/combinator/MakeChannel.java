package lisp.combinator;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Channel;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (17.11.2012)
 *
 * @author andreasm
 */
public final class MakeChannel extends TypeCheckCombinator
{
    /**
     * Constructor for MakeChannel
     */
    public MakeChannel()
    {
        super(0, 1);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Rational ratiocapacity = getRational(arguments, 0);
        int intcapacity = ratiocapacity.intValue();
        //
        if (intcapacity < 0) throw new CannotEvalException("capacity must not be negative in make-channel");
        //
        return new Channel(intcapacity);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "make-channel";
    }
}