/*
 * Copyright (C) 2006, 2007, 2010 - 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.combinator;
/*
 * Created 01.11.2006
 */
import lisp.Array;
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * @author Andreasm
 */
public final class GetArrayElement extends TypeCheckCombinator
{
    /**
     * Constructor for GetArrayElement
     */
    public GetArrayElement()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Array array = getArray(arguments, 0);
        List subscripts = getNonEmptyList(arguments, 1);
        array.checkBounds(subscripts);
        //
        return array.get(subscripts);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "get-array-element";
    }
}