/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.combinator;
//
// CharCode.java
// JL
//
// Created by andreasm on Fri Aug 31 2001.
//
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * @author Andreasm
 */
public final class CharCode extends TypeCheckCombinator
{
    /**
     * Constructor for CharCode
     */
    public CharCode()
    {
        super(0, 1);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Chars chars = getChars(arguments, 0);
        String string = chars.getString();
        //
        if (string.length() == 0) throw new CannotEvalException("empty string in char-code");
        //
        return new Rational(string.charAt(0));
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "char-code";
    }
}