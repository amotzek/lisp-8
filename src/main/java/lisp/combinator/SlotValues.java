package lisp.combinator;
/*
 * Copyright (C) 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.List;
import lisp.RubyStyleObject;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.environment.Environment;
import java.util.Iterator;
/*
 * Erstellungsdatum: (9.12.2011)
 *
 * @author andreasm
 */
public final class SlotValues extends TypeCheckCombinator
{
    /**
     * Constructor for SlotValues
     */
    public SlotValues()
    {
        super(0, 1);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        RubyStyleObject object = getObject(arguments, 0);
        List slotvalues = null;
        Iterator<Symbol> keys = object.keyIterator();
        //
        while (keys.hasNext())
        {
            Symbol key = keys.next();
            Sexpression value = object.get(key);
            List slotvalue = new List(value, null);
            slotvalue = new List(key, slotvalue);
            slotvalues = new List(slotvalue, slotvalues);
        }
        //
        return slotvalues;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "slot-values";
    }
}