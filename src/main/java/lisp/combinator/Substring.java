package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (25.08.2001)
 *
 * @author Andreasm
 */
public final class Substring extends TypeCheckCombinator
{
    /**
     * @see Object#Object()
     */
    public Substring()
    {
        super(0, 3);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Chars chars = getChars(arguments, 0);
        Rational start = getRational(arguments, 1);
        Rational length = getRational(arguments, 2);
        String str = chars.getString();
        int first = start.intValue() - 1;
        int last = first + length.intValue();
        //
        try
        {
            return new Chars(str.substring(first, last));
        }
        catch (StringIndexOutOfBoundsException e)
        {
            StringBuilder builder = new StringBuilder();
            builder.append("substring index ");
            builder.append(start);
            builder.append(" or length ");
            builder.append(length);
            builder.append(" out of range for ");
            builder.append(chars);
            //
            throw new CannotEvalException(builder.toString());
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "substring";
    }
}