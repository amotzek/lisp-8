/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.combinator;
//
// Assoc.java
// JL
//
// Created by andreasm on Mon Sep 24 2001.
//
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.environment.Environment;
/*
 * @author Andreasm
 */
public final class Assoc extends TypeCheckCombinator
{
    /**
     * Constructor for Assoc
     */
    public Assoc()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[]) 
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Symbol name = getSymbol(arguments, 0, false);
        List alist = getList(arguments, 1);
        //
        if (alist == null) return null;
        //
        return alist.lookup(name);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "assoc";
    }
}