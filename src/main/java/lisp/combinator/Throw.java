/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.combinator;
//
// Throw.java
// JL
//
// Created by andreasm on Fri Aug 31 2001.
//
import lisp.Chars;
import lisp.Combinator;
import lisp.Function;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.continuation.TraceBuilder;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * @author Andreasm
 */
public final class Throw extends Combinator
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    //
    public Throw()
    {
        super(0, 2);
    }
    /**
     * @see Function#apply(lisp.concurrent.RunnableQueue, lisp.environment.Environment, lisp.Sexpression[], lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        try
        {
            Symbol name = (Symbol) arguments[0];
            Sexpression value = arguments[1];
            //
            if (name != null)
            {
                fail.fail(name, value);
                //
                return;
            }
        }
        catch (ClassCastException e)
        {
            // empty
        }
        //
        TraceBuilder builder = new TraceBuilder(ERROR, new Chars(arguments[0] + ""), succeed);
        builder.append(" is not a symbol in throw");
        builder.build();
        builder.invoke(fail);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "throw";
    }
}