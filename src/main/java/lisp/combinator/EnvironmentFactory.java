package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011, 2013, 2014, 2019 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import lisp.CannotEvalException;
import lisp.Rational;
import static lisp.Symbol.createSymbol;
import lisp.environment.Environment;
import lisp.environment.io.EnvironmentReader;
/*
 * @author Andreasm
 */
public final class EnvironmentFactory
{
    private static Environment environment;
    /**
     * Constructor for EnvironmentFactory
     */
    private EnvironmentFactory()
    {
        super();
    }
    /**
     * Creates the initial Environment
     *
     * @return Initial environment
     */
    public synchronized static Environment createEnvironment()
    {
        if (environment == null)
        {
            try
            {
                Environment parent = new Environment(null);
                environment = new Environment(parent);
                parent.add(false, createSymbol("acquire-lock"), new AcquireLock());
                parent.add(false, createSymbol("add"), new Add());
                parent.add(false, createSymbol("allocate-instance"), new AllocateInstance());
                parent.add(false, createSymbol("apply"), new Apply());
                parent.add(false, createSymbol("approximate"), new Approximate());
                parent.add(false, createSymbol("array-dimensions"), new ArrayDimensions());
                parent.add(false, createSymbol("ash"), new Ash());
                parent.add(false, createSymbol("assign"), new Assign());
                parent.add(false, createSymbol("assoc"), new Assoc());
                parent.add(false, createSymbol("at"), new At());
                parent.add(false, createSymbol("await-future"), new AwaitFuture());
                parent.add(false, createSymbol("base64-decode"), new Base64Decode());
                parent.add(false, createSymbol("body"), new Body());
                parent.add(false, createSymbol("bound?"), new IsBound());
                parent.add(false, createSymbol("catch-and-apply"), new CatchAndApply());
                parent.add(false, createSymbol("change-class"), new ChangeClass());
                parent.add(false, createSymbol("char-code"), new CharCode());
                parent.add(false, createSymbol("class-of"), new ClassOf());
                parent.add(false, createSymbol("clear-hash-table"), new ClearHashTable());
                parent.add(false, createSymbol("close-channel"), new CloseChannel());
                parent.add(false, createSymbol("closed-channel?"), new IsClosedChannel());
                parent.add(false, createSymbol("code-char"), new CodeChar());
                parent.add(false, createSymbol("concatenate-list"), new ConcatenateList());
                parent.add(false, createSymbol("cons"), new Cons());
                parent.add(false, createSymbol("defclass"), new DefClass());
                parent.add(false, createSymbol("defmethod"), new DefMethod());
                parent.add(false, createSymbol("deftrait"), new DefTrait());
                parent.add(false, createSymbol("denominator"), new Denominator());
                parent.add(false, createSymbol("direct-superclasses"), new DirectSuperclasses());
                parent.add(false, createSymbol("duplicate"), new Duplicate());
                parent.add(false, createSymbol("eq?"), new IsEqual());
                parent.add(false, createSymbol("eval"), new Eval());
                parent.add(false, createSymbol("exptmod"), new ExptMod());
                parent.add(false, createSymbol("first"), new First());
                parent.add(false, createSymbol("floor"), new Floor());
                parent.add(false, createSymbol("freeze"), new Freeze());
                parent.add(false, createSymbol("frozen?"), new IsFrozen());
                parent.add(false, createSymbol("gcd"), new Gcd());
                parent.add(false, createSymbol("generic-function-methods"), new GenericFunctionMethods());
                parent.add(false, createSymbol("gensym"), new Gensym());
                parent.add(false, createSymbol("get-array-element"), new GetArrayElement());
                parent.add(false, createSymbol("get-date-and-time"), new GetDateAndTime());
                parent.add(false, createSymbol("get-hash-table-value"), new GetHashTableValue());
                parent.add(false, createSymbol("get-places"), new GetPlaces());
                parent.add(false, createSymbol("go"), new Go());
                parent.add(false, createSymbol("hash"), new Hash());
                parent.add(false, createSymbol("hash-table-values"), new HashTableValues());
                parent.add(false, createSymbol("if"), new If());
                parent.add(false, createSymbol("integer-length"), new IntegerLength());
                parent.add(false, createSymbol("intern"), new Intern());
                parent.add(false, createSymbol("lambda"), new Lambda());
                parent.add(false, createSymbol("less?"), new IsLess());
                parent.add(false, createSymbol("letrec"), new Letrec());
                parent.add(false, createSymbol("logbit?"), new LogBit());
                parent.add(false, createSymbol("lognot"), new LogNot());
                parent.add(false, createSymbol("make-array"), new MakeArray());
                parent.add(false, createSymbol("make-channel"), new MakeChannel());
                parent.add(false, createSymbol("make-lock"), new MakeLock());
                parent.add(false, createSymbol("make-combinator"), new MakeCombinator());
                parent.add(false, createSymbol("make-hash-table"), new MakeHashTable());
                parent.add(false, createSymbol("make-place"), new MakePlace());
                parent.add(false, createSymbol("member?"), new IsMember());
                parent.add(false, createSymbol("method-guard"), new MethodGuard());
                parent.add(false, createSymbol("method-specializers"), new MethodSpecializers());
                parent.add(false, createSymbol("mlambda"), new Mlambda());
                parent.add(false, createSymbol("modinv"), new ModInv());
                parent.add(false, createSymbol("null?"), new IsNull());
                parent.add(false, createSymbol("numerator"), new Numerator());
                parent.add(false, createSymbol("parameters"), new Parameters());
                parent.add(false, createSymbol("prime?"), new IsPrime());
                parent.add(false, createSymbol("put-hash-table-value"), new PutHashTableValue());
                parent.add(false, createSymbol("quote"), new Quote());
                parent.add(false, createSymbol("quotient"), new Quotient());
                parent.add(false, createSymbol("random"), new Random());
                parent.add(false, createSymbol("read-from-string"), new ReadFromString());
                parent.add(false, createSymbol("receive-from-channels"), new ReceiveFromChannels());
                parent.add(false, createSymbol("regexp-matches"), new RegexpMatches());
                parent.add(false, createSymbol("release-lock"), new ReleaseLock());
                parent.add(false, createSymbol("rem"), new Rem());
                parent.add(false, createSymbol("remove-method"), new RemoveMethod());
                parent.add(false, createSymbol("respond-to?"), new RespondTo());
                parent.add(false, createSymbol("rest"), new Rest());
                parent.add(false, createSymbol("reverse"), new Reverse());
                parent.add(false, createSymbol("search-string"), new SearchString());
                parent.add(false, createSymbol("send-on-channel"), new SendOnChannel());
                parent.add(false, createSymbol("setq"), new Setq());
                parent.add(false, createSymbol("set-array-element"), new SetArrayElement());
                parent.add(false, createSymbol("sequential"), new Sequential());
                parent.add(false, createSymbol("slot-value"), new SlotValue());
                parent.add(false, createSymbol("slot-values"), new SlotValues());
                parent.add(false, createSymbol("split"), new Split());
                parent.add(false, createSymbol("string-capitalize"), new StringCapitalize());
                parent.add(false, createSymbol("string-downcase"), new StringDowncase());
                parent.add(false, createSymbol("string-length"), new StringLength());
                parent.add(false, createSymbol("string-trim"), new StringTrim());
                parent.add(false, createSymbol("string-upcase"), new StringUpcase());
                parent.add(false, createSymbol("sub"), new Sub());
                parent.add(false, createSymbol("substring"), new Substring());
                parent.add(false, createSymbol("throw"), new Throw());
                parent.add(false, createSymbol("times"), new Times());
                parent.add(false, createSymbol("tokenize-string"), new TokenizeString());
                parent.add(false, createSymbol("type-of"), new TypeOf());
                parent.add(false, createSymbol("unsplit"), new Unsplit());
                parent.add(false, createSymbol("write-to-string"), new WriteToString());
                parent.add(false, createSymbol("*thread-count*"), getThreadCount());
                //
                EnvironmentReader reader = new EnvironmentReader(parent);
                reader.readFromResource(EnvironmentFactory.class, "core.lisp", "UTF-8");
                parent.freeze();
            }
            catch (NoSuchAlgorithmException | NoSuchProviderException e)
            {
                throw new RuntimeException(e);
            }
            catch (IOException e)
            {
                throw new RuntimeException("bootstrap file missing", e);
            }
            catch (CannotEvalException e)
            {
                throw new RuntimeException("error in bootstrap file", e);
            }
        }
        //
        return environment;
    }
    /**
     * Returns the Thread Count
     *
     * @return Power of two, bigger or equal to Count of available Processors
     */
    private static Rational getThreadCount()
    {
        Runtime runtime = Runtime.getRuntime();
        long availableprocessors = runtime.availableProcessors();
        long threadcount = 1;
        //
        while (threadcount < availableprocessors)
        {
            threadcount *= 2L;
        }
        //
        return new Rational(threadcount);
    }
}