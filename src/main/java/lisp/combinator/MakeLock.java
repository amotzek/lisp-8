package lisp.combinator;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Combinator;
import lisp.Lock;
import lisp.Sexpression;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Erstellungsdatum: (03.03.2013)
 *
 * @author andreasm
 */
public final class MakeLock extends Combinator
{
    /**
     * Constructor for MakeLock
     */
    public MakeLock()
    {
        super(0, 0);
    }
    /**
     * @see Combinator#apply(lisp.concurrent.RunnableQueue, lisp.environment.Environment, lisp.Sexpression[], lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        Lock lock = new Lock();
        lock.release();
        succeed.succeed(lock);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "make-lock";
    }
}