package lisp.combinator;
/*
 * Copyright (C) 2012, 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * @author Andreasm
 */
public final class InlinedLambda extends TypeCheckCombinator
{
    private final List parameters;
    private final List rawbody;
    private final TypeCheckCombinator innercombinator;
    private final int innerparametercount;
    private final Sexpression[] constants;
    private final int[] permutation;
    /**
     * Constructor for InlinedLambda
     *
     * @param parameters Parameters
     * @param body Body
     */
    public InlinedLambda(List parameters, List body)
    {
        super(0, List.length(parameters));
        //
        this.parameters = parameters;
        this.rawbody = body;
        //
        innercombinator = (TypeCheckCombinator) body.first();
        innerparametercount = innercombinator.getParameterCount();
        constants = new Sexpression[innerparametercount];
        permutation = new int[innerparametercount];
        body = body.rest();
        int position = 0;
        //
        while (body != null)
        {
            Sexpression element = body.first();
            //
            if (isConstant(element))
            {
                constants[position] = element;
                permutation[position] = -1;
            }
            else
            {
                permutation[position] = getPosition(element, parameters);
            }
            //
            body = body.rest();
            position++;
        }
    }
    /**
     * Finds the Element in the List
     *
     * @param element Element
     * @param list List
     * @return Position of the Element or -1
     */
    private static int getPosition(Sexpression element, List list)
    {
        int position = 0;
        //
        while (list != null)
        {
            if (element == list.first()) return position;
            //
            list = list.rest();
            position++;
        }
        //
        return -1;
    }
    /**
     * Returns the Parameters
     *
     * @return List of Parameters
     */
    public List getParameters()
    {
        return parameters;
    }
    /**
     * Returns the uncompiled Body
     *
     * @return Body
     */
    public List getUncompiledBody()
    {
        return rawbody;
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Sexpression[] innerarguments = new Sexpression[innerparametercount];
        //
        for (int destination = 0; destination < innerparametercount; destination++)
        {
            int source = permutation[destination];
            innerarguments[destination] = (source >= 0) ? arguments[source] : constants[destination];
        }
        //
        return innercombinator.apply(null, innerarguments);
    }
    //
    @Override
    public String toString()
    {
        return "#.(throw (quote error) \"compiled lambdas cannot be read or written\")";
    }
}