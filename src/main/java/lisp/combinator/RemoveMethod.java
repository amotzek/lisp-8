package lisp.combinator;
/*
 * Copyright (C) 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.GenericFunction;
import lisp.GuardedMethod;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Created by andreasm
 * Date: 5.11.11
 */
public final class RemoveMethod extends TypeCheckCombinator
{
    /**
     * Constructor for RemoveMethod
     */
    public RemoveMethod()
    {
        super(0, 2);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        GenericFunction genericfunction = getGenericFunction(arguments, 0);
        GuardedMethod method = getMethod(arguments, 1);
        genericfunction.removeMethod(method);
        //
        return genericfunction;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "remove-method";
    }
}