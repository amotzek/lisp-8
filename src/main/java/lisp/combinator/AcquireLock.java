package lisp.combinator;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Combinator;
import lisp.Lock;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Created by andreasm 3.3.2013 9:20
 */
public final class AcquireLock extends Combinator
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars ARGUMENT_TYPE_WRONG = new Chars("argument of acquire-lock has to be a lock");
    private static final Chars LOCK_NIL = new Chars("nil not allowed as lock for acquire-lock");
    /**
     * Constructor for AcquireLock
     */
    public AcquireLock()
    {
        super(0, 1);
    }
    //
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        try
        {
            Lock lock = (Lock) arguments[0];
            lock.addAcquirer(runnablequeue, succeed, fail);
        }
        catch (ClassCastException e)
        {
            fail.fail(ERROR, ARGUMENT_TYPE_WRONG);
        }
        catch (NullPointerException e)
        {
            fail.fail(ERROR, LOCK_NIL);
        }
    }
    //
    public String toString()
    {
        return "acquire-lock";
    }
}