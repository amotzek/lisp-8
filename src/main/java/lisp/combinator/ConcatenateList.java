package lisp.combinator;
/*
 * Copyright (C) 2008, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (31.03.2008)
 *
 * @author andreasm
 */
public final class ConcatenateList extends TypeCheckCombinator
{
    /**
     * Constructor for ConcatenateList
     */
    public ConcatenateList()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        List list = getList(arguments, 0);
        Chars chars = getChars(arguments, 1);
        StringBuilder builder = new StringBuilder();
        String delimiter = chars.getString();
        //
        while (list != null)
        {
            final Sexpression element = list.first();
            list = list.rest();
            //
            if (builder.length() > 0) builder.append(delimiter);
            //
            if (element == null) continue;
            //
            if (element instanceof Chars)
            {
                chars = (Chars) element;
                builder.append(chars.getString());
            }
            else
            {
                builder.append(element.toString());
            }
        }
        //
        return new Chars(builder.toString());
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "concatenate-list";
    }
}