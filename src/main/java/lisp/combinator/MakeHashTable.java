package lisp.combinator;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.HashTable;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (26.04.2013)
 *
 * @author andreasm
 */
public final class MakeHashTable extends TypeCheckCombinator
{
    /**
     * Constructor for MakeHashTable
     */
    public MakeHashTable()
    {
        super(0, 0);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        return new HashTable();
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "make-hash-table";
    }
}