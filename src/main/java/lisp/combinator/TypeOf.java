package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011, 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.Symbol;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (25.08.2001)
 * 
 * @author Andreasm
 */
public final class TypeOf extends TypeCheckCombinator
{
    private static final Symbol LIST = Symbol.createSymbol("list");
    /**
     * Constructor for TypeOf
     */
    public TypeOf()
    {
        super(0, 1);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments)
    {
        Sexpression sexpression = arguments[0];
        //
        if (sexpression == null) return LIST;
        //
        String type = sexpression.getType();
        //
        return Symbol.createSymbol(type);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "type-of";
    }
}