package lisp.combinator;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.environment.Environment;
import java.util.concurrent.atomic.AtomicLong;
/*
 * Erstellungsdatum: (05.03.2013)
 *
 * @author andreasm
 */
public final class Gensym extends TypeCheckCombinator
{
    private static final AtomicLong counter = new AtomicLong();
    /**
     * Constructor for Gensym
     */
    public Gensym()
    {
        super(0, 0);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        StringBuilder name = new StringBuilder();
        name.append(counter.incrementAndGet());
        name.reverse();
        //
        while (name.length() % 5 != 0)
        {
            name.append('0');
        }
        //
        name.append('G');
        name.reverse();
        //
        return Symbol.createSymbol(name.toString());
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "gensym";
    }
}