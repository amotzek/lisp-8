package lisp.combinator;
/*
 * Copyright (C) 2012, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Channel;
import lisp.Chars;
import lisp.Combinator;
import lisp.List;
import lisp.Rational;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Created by  andreasm 18.11.12 13:03
 */
public final class ReceiveFromChannels extends Combinator
{
    private static final Rational INFINITY = new Rational(Long.MAX_VALUE);
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars TIMEOUT_NEGATIVE = new Chars("timeout must not be negative in receive-from-channel");
    private static final Chars TIMEOUT_NOT_LONG = new Chars("timeout must be a long in receive-from-channel");
    private static final Chars ARGUMENT_TYPES_WRONG = new Chars("first argument must be a list of channels and second argument a number in receive-from-channel");
    private static final Chars CHANNEL_NIL = new Chars("nil not allowed as channel for receive-from-channel");
    /**
     * Constructor for ReceiveFromChannels
     */
    public ReceiveFromChannels()
    {
        super(0, 2);
    }
    //
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        try
        {
            List channellist = (List) arguments[0];
            Rational timeout = (Rational) arguments[1];
            //
            if (timeout == null)
            {
                timeout = INFINITY;
            }
            else if (timeout.isNegative())
            {
                fail.fail(ERROR, TIMEOUT_NEGATIVE);
                //
                return;
            }
            //
            Channel.addReceiverTo(runnablequeue, succeed, fail, timeout.longValue(), channellist);
        }
        catch (ClassCastException e)
        {
            fail.fail(ERROR, ARGUMENT_TYPES_WRONG);
        }
        catch (NullPointerException e)
        {
            fail.fail(ERROR, CHANNEL_NIL);
        }
        catch (CannotEvalException e)
        {
            fail.fail(ERROR, TIMEOUT_NOT_LONG);
        }
    }
    //
    public String toString()
    {
        return "receive-from-channels";
    }
}