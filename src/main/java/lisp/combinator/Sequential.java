package lisp.combinator;
/*
 * Copyright (C) 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Combinator;
import lisp.List;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.ChainValues;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Created 13.8.2011
 * @author Andreasm
 */
public final class Sequential extends Combinator
{
    /**
     * Constructor for Sequential
     */
    public Sequential()
    {
        super(1, 1);
    }
    /**
     * @see lisp.Function#apply(lisp.concurrent.RunnableQueue, lisp.environment.Environment, lisp.Sexpression[], lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        try
        {
            List list = (List) arguments[0];
            ChainValues chainvalues = new ChainValues(runnablequeue, environment, list, null, succeed, fail);
            /*
             * Calling succeed(null) introduces an additional null into the result.
             * This has to be removed, see ChainValues#succeed.
             */
            chainvalues.succeed(null);
        }
        catch (ClassCastException e)
        {
            StringBuilder builder = new StringBuilder();
            builder.append(arguments[0]);
            builder.append(" must be a list in prog1, prog2 or progn");
            fail.fail(Symbol.createSymbol("error"), new Chars(builder.toString()));
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "sequential";
    }
}