/*
 * Copyright (C) 2001, 2004, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.combinator;
//
// Random.java
// JL
//
// Created by andreasm on Fri Aug 31 2001.
//
import lisp.CannotEvalException;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
/*
 * @author Andreasm
 */
public final class Random extends TypeCheckCombinator
{
    private final SecureRandom random;
    /**
     * Constructor for Random
     *
     * @throws java.security.NoSuchProviderException  if Provider SUN is not supported
     * @throws java.security.NoSuchAlgorithmException if Algorithm SHA1PRNG is not supported
     */
    public Random() throws NoSuchAlgorithmException, NoSuchProviderException
    {
        super(0, 1);
        //
        random = SecureRandom.getInstance("SHA1PRNG", "SUN");
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Rational rational = getRational(arguments, 0);
        BigInteger modulus = rational.bigIntegerValue();
        //
        if (modulus.signum() <= 0)
        {
            StringBuilder builder = new StringBuilder();
            builder.append(rational);
            builder.append(" is not positive");
            //
            throw new CannotEvalException(builder.toString());
        }
        //
        if (BigInteger.ONE.equals(modulus)) return new Rational(0);
        //
        BigInteger rnd = new BigInteger(modulus.bitLength() + 32, random);
        //
        return new Rational(rnd.abs().mod(modulus));
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "random";
    }
}