package lisp.combinator;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (07.10.2013)
 *
 * @author Andreasm
 */
public final class IsPrime extends Predicate
{
    private static final Rational TWO = new Rational(2L);
    private static final int CERTAINTY = 40;
    /**
     * Constructor for IsPrime
     */
    public IsPrime()
    {
        super(0, 1);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Rational value = getRational(arguments, 0);
        //
        if (!value.isInteger()) throw new CannotEvalException("argument must be an integer");
        //
        if (value.compareTo(TWO) < 0) return null;
        //
        return createBoolean(value.getNumerator().isProbablePrime(CERTAINTY));
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "prime?";
    }
}