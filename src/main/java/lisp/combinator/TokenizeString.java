package lisp.combinator;
/*
 * Copyright (C) 2006, 2010, 2011, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.util.HashMap;
/*
 * Erstellungsdatum: (14.07.2006)
 * 
 * @author Andreasm
 */
public final class TokenizeString extends TypeCheckCombinator
{
    /**
     * Constructor for TokenizeStrings
     */
    public TokenizeString()
    {
        super(0, 3);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        String string = getChars(arguments, 0).getString();
        String delimiters = getChars(arguments, 1).getString();
        Sexpression returntokens = getSexpression(arguments, 2);
        List list = null;
        StringBuilder token = new StringBuilder();
        //
        if (returntokens == null)
        {
            for (int i = 0; i < string.length(); i++)
            {
                char c = string.charAt(i);
                //
                if (delimiters.indexOf(c) >= 0)
                {
                    if (token.length() > 0)
                    {
                        Chars chars = new Chars(token.toString());
                        token = new StringBuilder();
                        list = new List(chars, list);
                    }
                }
                else
                {
                    token.append(c);
                }
            }
        }
        else
        {
            HashMap<Character, Chars> flyweights = new HashMap<>();
            //
            for (int i = 0; i < string.length(); i++)
            {
                char c = string.charAt(i);
                //
                if (delimiters.indexOf(c) >= 0)
                {
                    if (token.length() > 0)
                    {
                        Chars chars = new Chars(token.toString());
                        token = new StringBuilder();
                        list = new List(chars, list);
                    }
                    //
                    Chars chars = flyweights.computeIfAbsent(c, d -> new Chars(d.toString()));
                    list = new List(chars, list);
                }
                else
                {
                    token.append(c);
                }
            }
        }
        //
        if (token.length() > 0)
        {
            Chars chars = new Chars(token.toString());
            list = new List(chars, list);
        }
        //
        return List.reverse(list);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "tokenize-string";
    }
}