package lisp.combinator;
/*
 * Copyright (C) 2012, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
import static lisp.List.length;
import static lisp.List.reverse;
/*
 * Erstellungsdatum: (19.7.2012)
 *
 * @author Andreasm
 */
public final class Unsplit extends TypeCheckCombinator
{
    /**
     * Constructor for Unsplit
     */
    public Unsplit()
    {
        super(0, 1);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        List list = getNonEmptyList(arguments, 0);
        int count = length(list);
        List[] parts = new List[count];
        int index = 0;
        //
        try
        {
            while (list != null)
            {
                parts[index++] = (List) list.first();
                list = list.rest();
            }
        }
        catch (ClassCastException e)
        {
            throw new CannotEvalException("not a list of lists in unsplit");
        }
        //
        boolean haselements;
        //
        do
        {
            haselements = false;
            //
            for (index = 0; index < count; index++)
            {
                if (parts[index] != null)
                {
                    list = new List(parts[index].first(), list);
                    parts[index] = parts[index].rest();
                    haselements = true;
                }
            }
        }
        while (haselements);
        //
        return reverse(list);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "unsplit";
    }
}