package lisp.combinator;
/*
 * Copyright (C) 2011, 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.ParameterizedBody;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Created by andreasm
 * Date: 6.11.11
 */
public final class Body extends TypeCheckCombinator
{
    /**
     * Constructor for Body
     */
    public Body()
    {
        super(0, 1);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        ParameterizedBody parameterizedbody = getParameterizedBody(arguments, 0);
        //
        return parameterizedbody.getBody();
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "body";
    }
}