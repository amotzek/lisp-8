package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011, 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.parser.Parser;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (25.08.2001)
 *
 * @author Andreasm
 */
public final class ReadFromString extends TypeCheckCombinator
{
    /**
     * @see Object#Object()
     */
    public ReadFromString()
    {
        super(0, 1);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Chars chars = getChars(arguments, 0);
        Parser parser = new Parser(environment, chars.getString(), 0);
        //
        try
        {
            return parser.getSexpression();
        }
        catch (NumberFormatException e)
        {
            throw new CannotEvalException("malformed number in " + chars);
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "read-from-string";
    }
}