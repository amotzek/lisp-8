package lisp.combinator;
/*
 * Copyright (C) 2012, 2017 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Bytes;
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.util.Base64;
/*
 * Created by  andreasm 19.12.12 18:19
 */
public final class Base64Decode extends TypeCheckCombinator
{
    public Base64Decode()
    {
        super(0, 1);
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Chars chars = getChars(arguments, 0);
        String string = chars.getString();
        Base64.Decoder decoder = Base64.getDecoder();
        //
        try
        {
            byte[] bytearray = decoder.decode(string);
            //
            return new Bytes(bytearray);
        }
        catch (IllegalArgumentException e)
        {
            throw new CannotEvalException("not a valid Base64 string");
        }
    }
    //
    public String toString()
    {
        return "base64-decode";
    }
}