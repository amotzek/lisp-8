package lisp.combinator;
/*
 * Copyright (C) 2011, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.environment.Environment;
//
public final class Quote extends TypeCheckCombinator
{
    /**
     * Constructor for Quote
     */
    public Quote()
    {
        super(1, 1);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        return arguments[0];
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "quote";
    }
}