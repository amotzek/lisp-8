package lisp.combinator;
/*
 * Copyright (C) 2001, 2007, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.environment.Environment;
import lisp.environment.NotBoundException;
/*
 * @author Andreasm
 */
public final class IsBound extends Predicate
{
    public IsBound()
    {
        super(0, 1);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Symbol name = getSymbol(arguments, 0, false);
        //
        try
        {
            environment.at(name);
            //
            return createBoolean(true);
        }
        catch (NotBoundException e)
        {
        }
        //
        return createBoolean(false);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "bound?";
    }
}