package lisp.combinator;
/*
 * Copyright (C) 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.RubyStyleObject;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (11.10.2011)
 *
 * @author andreasm
 */
public final class SlotValue extends TypeCheckCombinator
{
    /**
     * Constructor for SlotValue
     */
    public SlotValue()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        RubyStyleObject object = getObject(arguments, 0);
        Symbol key = getSymbol(arguments, 1, false);
        //
        return object.get(key);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "slot-value";
    }
}