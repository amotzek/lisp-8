package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Rational;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (25.08.2001 14:26:19)
 *
 * @author Andreasm
 */
public final class Add extends TypeCheckCombinator
{
    /**
     * Constructor for Add
     */
    public Add()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Rational addent1 = getRational(arguments, 0);
        Rational addent2 = getRational(arguments, 1);
        Rational sum = new Rational(addent1);
        sum.add(addent2);
        //
        return sum;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "add";
    }
}