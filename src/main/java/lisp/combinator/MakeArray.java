package lisp.combinator;
/*
 * Copyright (C) 2006, 2010, 2011, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Array;
import lisp.CannotEvalException;
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (01.11.2006)
 *
 * @author andreasm
 */
public final class MakeArray extends TypeCheckCombinator
{
    /**
     * Constructor for MakeArray
     */
    public MakeArray()
    {
        super(0, 1);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        List dimensions = getNonEmptyList(arguments, 0);
        //
        return new Array(dimensions);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "make-array";
    }
}