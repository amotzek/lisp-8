package lisp.combinator;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Combinator;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.Place;
import lisp.remote.protocol.Protocol;
import lisp.remote.server.Server;
import lisp.remote.server.ServerFactory;
import lisp.concurrent.RunnableQueue;
import java.io.PrintWriter;
import java.io.StringWriter;
/*
 * Created by andreasm 15.10.13 13:47
 */
public final class At extends Combinator
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    //
    public At()
    {
        super(2, 2);
    }
    //
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        try
        {
            Place callee = (Place) arguments[0];
            Sexpression expression = arguments[1];
            //
            if (expression == null)
            {
                succeed.succeed(null);
                //
                return;
            }
            //
            if (callee == null)
            {
                expression.enQueueEvaluator(runnablequeue, environment, succeed, fail);
                //
                return;
            }
            //
            Server server = ServerFactory.getServer();
            Protocol protocol = server.getProtocol();
            protocol.invoke(callee, expression, environment, succeed, fail);
        }
        catch (ClassCastException e)
        {
            fail.fail(ERROR, new Chars("at expects a place as first argument, but got " + arguments[0]));
        }
        catch (Exception e) // occurs in Bitbucket pipeline
        {
            StringWriter stringwriter = new StringWriter();
            PrintWriter printwriter = new PrintWriter(stringwriter);
            e.printStackTrace(printwriter);
            printwriter.flush();
            fail.fail(ERROR, new Chars("cannot start remote call: " + stringwriter.toString()));
        }
    }
    //
    @Override
    public String toString()
    {
        return "at";
    }
}