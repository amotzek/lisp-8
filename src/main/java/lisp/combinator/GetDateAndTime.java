/*
 * Copyright (C) 2006, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.combinator;
/*
 * Created 20.9.2006
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Sexpression;
import lisp.environment.Environment;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
/*
 * @author Andreasm
 */
public final class GetDateAndTime extends TypeCheckCombinator
{
    /**
     * Constructor for GetDateAndTime
     */
    public GetDateAndTime()
    {
        super(0, 0);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateformat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String string = dateformat.format(date);
        //
        return new Chars(string);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "get-date-and-time";
    }
}