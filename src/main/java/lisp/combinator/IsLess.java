package lisp.combinator;
/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (25.08.2001)
 *
 * @author Andreasm
 */
public final class IsLess extends Predicate
{
    /**
     * Constructor for IsLess
     */
    public IsLess()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    @SuppressWarnings("unchecked")
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        Comparable comparable1 = getComparable(arguments, 0);
        Comparable comparable2 = getComparable(arguments, 1);
        //
        try
        {
            return createBoolean(comparable1.compareTo(comparable2) < 0);
        }
        catch (ClassCastException e)
        {
            StringBuilder builder = new StringBuilder();
            builder.append("arguments ");
            builder.append(comparable1);
            builder.append(" and ");
            builder.append(comparable2);
            builder.append(" of less? have different types");
            //
            throw new CannotEvalException(builder.toString());
        }
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "less?";
    }
}