package lisp.combinator;
/*
 * Copyright (C) 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.GenericFunction;
import lisp.RubyStyleObject;
import lisp.Sexpression;
import lisp.SimpleClass;
import lisp.environment.Environment;
/*
 * Created by andreasm
 * Date: 16.10.11
 * Time: 16:14
 */
public final class RespondTo extends Predicate
{
    public RespondTo()
    {
        super(0, 2);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        RubyStyleObject object = getObject(arguments, 0);
        GenericFunction genericfunction = getGenericFunction(arguments, 1);
        SimpleClass simpleclass = object.classOf();
        //
        return createBoolean(genericfunction.acceptsFirstArgumentOf(simpleclass));
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "respond-to?";
    }
}