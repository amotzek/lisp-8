/*
 * Copyright (C) 2009, 2010 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp.combinator;
/*
 * Created on 21.09.2009
 */
abstract class StringCaseCombinator extends TypeCheckCombinator
{
    /**
     * Constructor for StringCaseCombinator
     *
     * @param quotemask  Bitmask for quoted parameters
     * @param parameters Count of parameters
     */
    protected StringCaseCombinator(int quotemask, int parameters)
    {
        super(quotemask, parameters);
    }
    /**
     * Converts a Char to upper case
     *
     * @param c Character
     * @return Character as upper case
     */
    protected static char toUpperCase(char c)
    {
        char upper = Character.toUpperCase(c);
        //
        if ('?' == upper) return c;
        //
        return upper;
    }
    /**
     * Converts a Char to lower case
     *
     * @param c Character
     * @return Character as lower case
     */
    protected static char toLowerCase(char c)
    {
        char lower = Character.toLowerCase(c);
        //
        if ('?' == lower) return c;
        //
        return lower;
    }
}