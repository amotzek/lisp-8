package lisp.combinator;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.environment.Environment;
import static lisp.List.reverse;
/*
 * Erstellungsdatum: (19.7.2012)
 *
 * @author Andreasm
 */
public final class Reverse extends TypeCheckCombinator
{
    /**
     * Constructor for Reverse
     */
    public Reverse()
    {
        super(0, 1);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        return reverse(getList(arguments, 0));
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "reverse";
    }
}