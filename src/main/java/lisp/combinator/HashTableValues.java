package lisp.combinator;
/*
 * Copyright (C) 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.Iterator;
import lisp.CannotEvalException;
import lisp.HashTable;
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Erstellungsdatum: (16.7.2014)
 *
 * @author andreasm
 */
public final class HashTableValues extends TypeCheckCombinator
{
    /**
     * Constructor for HashTableValues
     */
    public HashTableValues()
    {
        super(0, 1);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        HashTable table = getHashTable(arguments, 0);
        List tablevalues = null;
        Iterator<Sexpression> keys = table.keyIterator();
        //
        while (keys.hasNext())
        {
            Sexpression key = keys.next();
            Sexpression value = table.get(key);
            List tablevalue = new List(value, null);
            tablevalue = new List(key, tablevalue);
            tablevalues = new List(tablevalue, tablevalues);
        }
        //
        return tablevalues;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "hash-table-values";
    }
}