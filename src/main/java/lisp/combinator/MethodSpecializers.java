package lisp.combinator;
/*
 * Copyright (C) 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.GuardedMethod;
import lisp.Sexpression;
import lisp.environment.Environment;
/*
 * Created by andreasm
 * Date: 5.11.11
 */
public final class MethodSpecializers extends TypeCheckCombinator
{
    /**
     * Constructor for MethodSpecializers
     */
    public MethodSpecializers()
    {
        super(0, 1);
    }
    /**
     * @see TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        GuardedMethod method = getMethod(arguments, 0);
        //
        return method.getSpezializers();
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        return "method-specializers";
    }
}