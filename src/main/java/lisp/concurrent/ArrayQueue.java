package lisp.concurrent;
/*
 * Copyright (C) 2014, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by andreasm
 * Date: 08.01.14
 * Time: 17:33
 */
final class ArrayQueue<E>
{
    private static final int INITIAL_SIZE = 8;
    //
    private int head;
    private int tail;
    private Object[] queue;
    private int count;
    //
    ArrayQueue()
    {
        super();
        //
        queue = new Object[INITIAL_SIZE];
    }
    //
    public void put(E element)
    {
        enQueue(element);
    }
    //
    public E poll()
    {
        if (count > 0) return deQueue();
        //
        return null;
    }
    //
    public int size()
    {
        return count;
    }
    //
    private void enQueue(E element)
    {
        if (count >= queue.length) grow();
        //
        count++;
        queue[head] = element;
        head++;
        //
        if (head == queue.length) head = 0;
    }
    //
    @SuppressWarnings("unchecked")
    private E deQueue()
    {
        E  element = (E) queue[tail];
        queue[tail] = null;
        tail++;
        //
        if (tail == queue.length) tail = 0;
        //
        count--;
        //
        return element;
    }
    //
    private void grow()
    {
        int length = queue.length;
        Object[] nextqueue = new Object[length << 1];
        int i = 0;
        //
        do
        {
            nextqueue[i++] = queue[tail++];
            //
            if (tail == queue.length) tail = 0;
        }
        while (tail != head);
        //
        tail = 0;
        head = i;
        queue = nextqueue;
    }
}