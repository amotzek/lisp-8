package lisp.concurrent;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.concurrent.atomic.AtomicInteger;
/**
 * Created by andreasm on 12.02.13
 */
final class LimitedRunnableQueue implements RunnableQueue
{
    private final RunnableQueue delegate;
    private final AtomicInteger count;
    //
    LimitedRunnableQueue(RunnableQueue delegate, int limit)
    {
        super();
        //
        this.delegate = delegate;
        //
        count = new AtomicInteger(limit);
    }
    //
    public void add(Runnable runnable)
    {
        if (count.decrementAndGet() <= 0) throw new LimitExceededException();
        //
        delegate.add(runnable);
    }
    //
    public Runnable poll()
    {
        if (count.get() <= 0) throw new LimitExceededException();
        //
        return delegate.poll();
    }
    //
    public boolean isPassive()
    {
        return delegate.isPassive();
    }
}