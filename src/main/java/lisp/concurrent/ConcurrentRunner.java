package lisp.concurrent;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Created by andreasm 27.06.12 20:24
 */
final class ConcurrentRunner extends Thread implements Runner
{
    private static final Logger logger = Logger.getLogger("threads.RunnableQueue");
    //
    private final ArrayBlockingQueue<Runnable> queue;
    private final AtomicInteger need;
    private final ArrayStack<Runnable> stack;
    //
    ConcurrentRunner(ArrayBlockingQueue<Runnable> queue, AtomicInteger need)
    {
        super();
        //
        this.queue = queue;
        this.need = need;
        //
        stack = new ArrayStack<>();
    }
    //
    public void addLocal(Runnable runnable)
    {
        stack.addFirst(runnable);
    }
    //
    @Override
    public void run()
    {
        while (true)
        {
            try
            {
                Runnable runnable = pollLocal();
                //
                if (runnable == null)
                {
                    publishNeed();
                    runnable = deQueue();
                }
                else if (hasNeedAndCanGive())
                {
                    giveAway();
                }
                //
                runnable.run();
            }
            catch (Throwable e)
            {
                logger.log(Level.WARNING, "runnable ended unexpectedly", e);
            }
        }
    }
    //
    private Runnable pollLocal()
    {
        return stack.removeFirst();
    }
    //
    private void publishNeed()
    {
        while (true)
        {
            int current = need.get();
            //
            if (current > 8) return;
            //
            if (need.compareAndSet(current, current + 1)) return;
        }
    }
    //
    private Runnable deQueue()
    {
        return queue.take();
    }
    //
    private boolean hasNeedAndCanGive()
    {
        return stack.size() > 4 && need.intValue() > 0;
    }
    //
    private void giveAway()
    {
        do
        {
            if (stack.size() < 2) return;
            //
            Runnable runnable = stack.removeLast();
            queue.put(runnable);
        }
        while (need.decrementAndGet() >= 0);
    }
}