package lisp.concurrent;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by andreasm 18.08.12 16:02
 */
final class ArrayStack<E>
{
    private static final int INITIAL_SIZE = 32;
    //
    private int head;
    private int tail;
    private Object[] stack;
    private int count;
    //
    ArrayStack()
    {
        super();
        //
        stack = new Object[INITIAL_SIZE];
    }
    //
    public int size()
    {
        return count;
    }
    //
    public void addFirst(E element)
    {
        if (count >= stack.length) grow();
        //
        if (head == stack.length) head = 0;
        //
        stack[head] = element;
        head++;
        count++;
    }
    //
    @SuppressWarnings("unchecked")
    public E removeFirst()
    {
        if (count <= 0) return null;
        //
        count--;
        head--;
        //
        if (head < 0) head += stack.length;
        //
        E element = (E) stack[head];
        stack[head] = null;
        //
        return element;
    }
    //
    @SuppressWarnings("unchecked")
    public E removeLast()
    {
        if (count <= 0) return null;
        //
        count--;
        E element = (E) stack[tail];
        stack[tail] = null;
        tail++;
        //
        if (tail == stack.length) tail = 0;
        //
        return element;
    }
    //
    private void grow()
    {
        int length = stack.length;
        Object[] nextstack = new Object[length << 1];
        System.arraycopy(stack, 0, nextstack, 0, length);
        tail = 0;
        head = length;
        stack = nextstack;
    }
}