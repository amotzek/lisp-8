package lisp.concurrent;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by andreasm on 29.06.12
 */
public final class RunnableQueueFactory
{
    private static final RunnableQueue concurrentqueue = createConcurrentRunnableQueue(threadCount());
    //
    private RunnableQueueFactory()
    {
    }
    //
    private static int threadCount()
    {
        Runtime runtime = Runtime.getRuntime();
        //
        return runtime.availableProcessors();
    }
    //
    public static RunnableQueue getConcurrentRunnableQueue()
    {
        return concurrentqueue;
    }
    //
    public static RunnableQueue createConcurrentRunnableQueue(int threadcount)
    {
        return new ConcurrentRunnableQueue(threadcount);
    }
    //
    public static RunnableQueue createPassiveRunnableQueue()
    {
        return new PassiveRunnableQueue();
    }
    //
    public static RunnableQueue limit(RunnableQueue queue, int limit)
    {
        return new LimitedRunnableQueue(queue, limit);
    }
    //
    public static StoppableRunnableQueue stoppable(RunnableQueue queue)
    {
        if (queue instanceof StoppableRunnableQueue) return (StoppableRunnableQueue) queue;
        //
        return new StoppableRunnableQueue(queue);
    }
}