package lisp.concurrent;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * Created by andreasm 28.11.13 19:45
 */
final class SingleRunner extends Thread implements Runner
{
    private static final Logger logger = Logger.getLogger("threads.RunnableQueue");
    //
    private final ArrayBlockingQueue<Runnable> queue;
    private final ArrayStack<Runnable> stack;
    //
    SingleRunner(ArrayBlockingQueue<Runnable> queue)
    {
        super();
        //
        this.queue = queue;
        //
        stack = new ArrayStack<>();
    }
    //
    public void addLocal(Runnable runnable)
    {
        stack.addFirst(runnable);
    }
    //
    @Override
    public void run()
    {
        while (true)
        {
            try
            {
                Runnable runnable = pollLocal();
                //
                if (runnable == null) runnable = deQueue();
                //
                runnable.run();
            }
            catch (Throwable e)
            {
                logger.log(Level.WARNING, "runnable ended unexpectedly", e);
            }
        }
    }
    //
    private Runnable pollLocal()
    {
        return stack.removeFirst();
    }
    //
    private Runnable deQueue()
    {
        return queue.take();
    }
}