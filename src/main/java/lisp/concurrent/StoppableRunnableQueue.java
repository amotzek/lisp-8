package lisp.concurrent;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by andreasm 11.12.13 08:24
 */
public final class StoppableRunnableQueue implements RunnableQueue
{
    private final RunnableQueue delegate;
    private volatile boolean stopped;
    //
    StoppableRunnableQueue(RunnableQueue delegate)
    {
        super();
        this.delegate = delegate;
    }
    //
    public void stop()
    {
        stopped = true;
    }
    //
    public void add(Runnable runnable)
    {
        if (stopped) throw new StoppedException();
        //
        delegate.add(runnable);
    }
    //
    public Runnable poll()
    {
        if (stopped) throw new StoppedException();
        //
        return delegate.poll();
    }
    //
    public boolean isPassive()
    {
        return delegate.isPassive();
    }
}