package lisp.concurrent;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by andreasm on 29.06.12
 */
final class PassiveRunnableQueue implements RunnableQueue
{
    private final ArrayQueue<Runnable> queue;
    //
    PassiveRunnableQueue()
    {
        super();
        //
        queue = new ArrayQueue<>();
    }
    //
    public void add(Runnable runnable)
    {
        queue.put(runnable);
    }
    //
    public Runnable poll()
    {
        return queue.poll();
    }
    //
    public boolean isPassive()
    {
        return true;
    }
}