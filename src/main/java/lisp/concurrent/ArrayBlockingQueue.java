package lisp.concurrent;
/*
 * Copyright (C) 2012, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
/*
 * Created by andreasm 23.06.12 22:01
 */
final class ArrayBlockingQueue<E>
{
    private static final int INITIAL_SIZE = 64;
    private final AtomicInteger count;
    private final Semaphore permits;
    private final Object readlock;
    private final Object writelock;
    //
    private int head;
    private int tail;
    private AtomicReferenceArray<E> queue;
    //
    ArrayBlockingQueue()
    {
        super();
        //
        queue = new AtomicReferenceArray<>(INITIAL_SIZE);
        count = new AtomicInteger(0);
        permits = new Semaphore(0);
        readlock = new Object();
        writelock = new Object();
    }
    //
    public void put(E element)
    {
        enQueue(element);
        permits.release();
    }
    //
    public E poll()
    {
        if (permits.tryAcquire()) return deQueue();
        //
        return null;
    }
    //
    public E take()
    {
        permits.acquireUninterruptibly();
        //
        return deQueue();
    }
    //
    public int size()
    {
        return count.get();
    }
    //
    private void enQueue(E element)
    {
        synchronized (writelock)
        {
            if (count.incrementAndGet() >= queue.length()) grow();
            //
            queue.set(head, element);
            head++;
            //
            if (head == queue.length()) head = 0;
        }
    }
    //
    private E deQueue()
    {
        E element;
        //
        synchronized (readlock)
        {
            element = queue.getAndSet(tail, null);
            tail++;
            //
            if (tail == queue.length()) tail = 0;
            //
            count.decrementAndGet();
        }
        //
        return element;
    }
    //
    private void grow()
    {
        synchronized (readlock)
        {
            int length = queue.length();
            AtomicReferenceArray<E> nextqueue = new AtomicReferenceArray<>(length << 1);
            int index = 0;
            //
            while (tail != head)
            {
                nextqueue.set(index, queue.get(tail));
                index++;
                tail++;
                //
                if (tail == length) tail = 0;
            }
            //
            tail = 0;
            head = index;
            queue = nextqueue;
        }
    }
}