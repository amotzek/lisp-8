package lisp.concurrent;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * User: andreasm
 * Date: 09.10.13
 * Time: 08:33
 */
final class FallbackThread extends Thread
{
    private static final Logger logger = Logger.getLogger("threads.RunnableQueue");
    //
    private final ArrayBlockingQueue<Runnable> queue;
    //
    FallbackThread(ArrayBlockingQueue<Runnable> queue)
    {
        super();
        //
        this.queue = queue;
    }
    //
    @Override
    public void run()
    {
        while (true)
        {
            try
            {
                Runnable runnable = queue.take();
                runnable.run();
            }
            catch (Throwable e)
            {
                logger.log(Level.WARNING, "runnable ended unexpectedly", e);
            }
        }
    }
}