package lisp.concurrent;
/*
 * Copyright (C) 2007, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.concurrent.atomic.AtomicInteger;
/*
 * Created on 13.10.2007
 */
final class ConcurrentRunnableQueue implements RunnableQueue
{
    private final ArrayBlockingQueue<Runnable> queue;
    //
    ConcurrentRunnableQueue(int threadcount)
    {
        super();
        //
        queue = new ArrayBlockingQueue<>();
        //
        if (threadcount <= 1)
        {
            configureAndStart(new SingleRunner(queue));
            configureAndStart(new FallbackThread(queue));
            //
            return;
        }
        //
        AtomicInteger need = null;
        //
        for (int i = 0; i < lessOrMultipleOf8(threadcount); i++)
        {
            if ((i & 7) == 0) need = new AtomicInteger(0);
            //
            configureAndStart(new ConcurrentRunner(queue, need));
        }
    }
    //
    public void add(Runnable runnable)
    {
        if (runnable == null) return;
        //
        Thread thread = Thread.currentThread();
        //
        if (thread instanceof Runner)
        {
            Runner runner = (Runner) thread;
            runner.addLocal(runnable);
            //
            return;
        }
        //
        queue.put(runnable);
    }
    //
    public Runnable poll()
    {
        return queue.poll();
    }
    //
    public boolean isPassive()
    {
        return false;
    }
    //
    private static int lessOrMultipleOf8(int n)
    {
        if (n <= 8) return n;
        //
        while ((n & 7) > 0) n++;
        //
        return n;
    }
    //
    private static void configureAndStart(Thread thread)
    {
        thread.setDaemon(true);
        thread.start();
    }
}