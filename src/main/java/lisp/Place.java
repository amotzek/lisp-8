package lisp;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.StringTokenizer;
/*
 * Instances represent (remote) Places where computations can execute
 *
 * Created by andreasm 16.10.13 07:18
 */
public final class Place extends Constant implements Comparable<Place>
{
    private final String host;
    private final int port;
    //
    public Place(String host, int port)
    {
        super();
        //
        this.host = host;
        this.port = port;
    }
    /**
     * Creates a Place from a Name
     *
     * @param name Name
     * @return Place
     * @throws java.util.NoSuchElementException if there is no Port
     * @throws NumberFormatException if the Port is not a number
     */
    public static Place getInstance(String name)
    {
        StringTokenizer tokenizer = new StringTokenizer(name, ":", false);
        String host = tokenizer.nextToken();
        int port = Integer.parseInt(tokenizer.nextToken());
        //
        return new Place(host, port);
    }
    /**
     * Returns the Host of this Place
     *
     * @return Host
     */
    public String getHost()
    {
        return host;
    }
    /**
     * Returns the Port of this Place
     *
     * @return Port
     */
    public int getPort()
    {
        return port;
    }
    /**
     * Returns the Name
     *
     * @return Name of this Place
     */
    public String getName()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(host);
        builder.append(":");
        builder.append(port);
        //
        return builder.toString();
    }
    /**
     * @see lisp.Sexpression#getType()
     */
    public String getType()
    {
        return "place";
    }
    /**
     * @see Comparable#compareTo(Object)
     */
    public int compareTo(Place that)
    {
        int result = host.compareTo(that.host);
        //
        if (result != 0) return result;
        //
        return port - that.port;
    }
    /**
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(Object that)
    {
        if (this == that) return true;
        //
        if (that == null || getClass() != that.getClass()) return false;
        //
        Place place = (Place) that;
        //
        return place.port == port && place.host.equals(host);
    }
    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int result = host.hashCode();
        result = 31 * result + port;
        //
        return result;
    }
    /**
     * @see Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("#.(make-place \"");
        builder.append(host);
        builder.append(":");
        builder.append(port);
        builder.append("\")");
        //
        return builder.toString();
    }
}