/*
 * Copyright (C) 2013, 2014, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp;
/*
 * Created on 26.04.2013
 */
import lisp.formatter.Formattable;
import lisp.traversal.IsMutable;
import lisp.traversal.Traversal;
import lisp.traversal.Visitor;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
/*
 * @author andreasm
 */
public final class HashTable extends AssociativeContainer<Sexpression> implements Formattable
{
    //
    private static final Visitor<Boolean> ismutable = new IsMutable();
    private final HashMap<Sexpression, Sexpression> map;
    /**
     * Constructor for HashTable
     */
    public HashTable()
    {
        super();
        //
        map = new HashMap<>();
    }
    /**
     * Checks if the S-Expression contains a mutable part,
     * that means a part that can change its hash code
     *
     * @param sexpression S-Expression
     * @return true if the S-Expression contains a mutable part
     */
    private static boolean isMutable(Sexpression sexpression)
    {
        if (sexpression == null) return false;
        //
        Traversal traversal = new Traversal(sexpression);
        Boolean mutable = traversal.accept(ismutable);
        //
        if (mutable == null) return false;
        //
        return mutable;
    }
    /**
     * @see AssociativeContainer#size()
     */
    @Override
    public int size()
    {
        lockShared();
        //
        try
        {
            return map.size();
        }
        finally
        {
            unlockShared();
        }
    }
    /**
     * @see AssociativeContainer#hasComplexKeys()
     */
    @Override
    public boolean hasComplexKeys()
    {
        return true;
    }
    /**
     * @see AssociativeContainer#keyIterator()
     */
    @Override
    public Iterator<Sexpression> keyIterator()
    {
        int index = 0;
        Sexpression[] keys;
        lockShared();
        //
        try
        {
            keys = new Sexpression[map.size()];
            //
            for (Sexpression key : map.keySet())
            {
                keys[index++] = key;
            }
        }
        finally
        {
            unlockShared();
        }
        //
        SexpressionComparator comparator = new SexpressionComparator();
        Arrays.sort(keys, comparator);
        //
        return new ArrayIterator<>(keys);
    }
    /**
     * @see AssociativeContainer#get(Object)
     */
    @Override
    public Sexpression get(Object object)
    {
        if (object == null || object instanceof Sexpression)
        {
            Sexpression key = (Sexpression) object;
            //
            if (isMutable(key)) return null;
            //
            lockShared();
            //
            try
            {
                return map.get(key);
            }
            finally
            {
                unlockShared();
            }
        }
        //
        return null;
    }
    /**
     * @see AssociativeContainer#put(Sexpression, Sexpression)
     */
    @Override
    public void put(Sexpression key, Sexpression value)
    {
        if (isMutable(key)) throw new IllegalArgumentException("key " + key + " is mutable");
        //
        lockExclusively();
        //
        try
        {
            if (value == null)
            {
                map.remove(key);
                //
                return;
            }
            //
            map.put(key, value);
        }
        finally
        {
            unlockExclusively();
        }
    }
    /**
     * @see AssociativeContainer#duplicate()
     */
    @Override
    public HashTable duplicate()
    {
        lockShared();
        //
        try
        {
            HashTable duplicate = new HashTable();
            duplicate.map.putAll(map);
            //
            return duplicate;
        }
        finally
        {
            unlockShared();
        }
    }
    /**
     * Method clear
     *
     * Removes all entries
     */
    public void clear()
    {
        lockExclusively();
        //
        try
        {
            map.clear();
        }
        finally
        {
            unlockExclusively();
        }
    }
    /**
     * @see AssociativeContainer#metaHashCode()
     */
    @Override
    public int metaHashCode()
    {
        return HashTable.class.hashCode();
    }
    //
    /*
     * @see lisp.Sexpression#getType()
     */
    public String getType()
    {
        return "hash-table";
    }
}