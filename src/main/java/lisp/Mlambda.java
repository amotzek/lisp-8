package lisp;
/*
 * Copyright (C) 2001, 2007, 2010, 2011, 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.continuation.EvalValue;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Erstellungsdatum: (24.08.01 18:51:59)
 *
 * @author Andreasm
 */
public final class Mlambda extends ParameterizedBody implements Function
{
    /**
     * Constructor for Mlambda
     *
     * @param parameters Parameters
     * @param body Body
     * @param compiledbody Compiled Body
     * @param environment Environment
     * @throws CannotEvalException if List of Parameters contains a non Symbol
     */
    public Mlambda(List parameters, Sexpression body, Sexpression compiledbody, Environment environment) throws CannotEvalException
    {
        super(parameters, body, compiledbody, environment);
    }
    /**
     * @see Function#acceptsArgumentCount(int)
     */
    public boolean acceptsArgumentCount(int argumentcount)
    {
        return true;
    }
    /**
     * @see Function#isMacro()
     */
    public boolean isMacro()
    {
        return true;
    }
    /**
     * @see Function#mustBeQuoted(int)
     */
    public boolean mustBeQuoted(int position)
    {
        return true;
    }
    /**
     * @see Function#apply(lisp.concurrent.RunnableQueue, lisp.environment.Environment, Sexpression[], lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        if (isConstant(compiledbody))
        {
            succeed.succeed(compiledbody);
            //
            return;
        }
        //
        Environment child = bind(arguments);
        EvalValue evalvalue = new EvalValue(runnablequeue, environment, succeed, fail);
        compiledbody.enQueueEvaluator(runnablequeue, child, evalvalue, fail);
    }
    /**
     * @see Sexpression#getType()
     */
    public String getType()
    {
        return "mlambda";
    }
}