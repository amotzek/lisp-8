package lisp;
/*
 * Copyright (C) 2011 - 2014, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.formatter.Formattable;
import java.util.Arrays;
import java.util.IdentityHashMap;
import java.util.Iterator;
/*
 * Created by andreasm on 11.10.11 at 16:57
 */
public class RubyStyleObject extends AssociativeContainer<Symbol> implements Formattable
{
    private final SimpleClass classof;
    private final IdentityHashMap<Symbol, Sexpression> slots;
    /**
     * Constructor for RubyStyleObject
     *
     * @param classof Class of Object
     */
    public RubyStyleObject(SimpleClass classof)
    {
        super();
        //
        this.classof = classof;
        //
        slots = new IdentityHashMap<>(13);
    }
    /**
     * Constructor for RubyStyleObject
     *
     * @param classof Class of Object
     * @param slots Slots
     */
    private RubyStyleObject(SimpleClass classof, IdentityHashMap<Symbol, Sexpression> slots)
    {
        super();
        //
        this.classof = classof;
        this.slots = slots;
        //
        freeze();
    }
    /**
     * Returns the Class of this Object
     *
     * @return Class
     */
    public final SimpleClass classOf()
    {
        return classof;
    }
    /**
     * @see AssociativeContainer#size()
     */
    @Override
    public final int size()
    {
        lockShared();
        //
        try
        {
            return slots.size();
        }
        finally
        {
            unlockShared();
        }
    }
    /**
     * @see AssociativeContainer#hasComplexKeys()
     */
    @Override
    public final boolean hasComplexKeys()
    {
        return false;
    }
    /**
     * @see AssociativeContainer#keyIterator()
     */
    @Override
    public final Iterator<Symbol> keyIterator()
    {
        int index = 0;
        Symbol[] keys;
        lockShared();
        //
        try
        {
            keys = new Symbol[slots.size()];
            //
            for (Symbol key : slots.keySet())
            {
                keys[index++] = key;
            }
        }
        finally
        {
            unlockShared();
        }
        //
        Arrays.sort(keys);
        //
        return new ArrayIterator<>(keys);
    }
    /**
     * @see AssociativeContainer#get(Object)
     */
    @Override
    public final Sexpression get(Object key)
    {
        lockShared();
        //
        try
        {
            return slots.get(key);
        }
        finally
        {
            unlockShared();
        }
    }
    /**
     * @see AssociativeContainer#put(Sexpression, Sexpression)
     */
    @Override
    public final void put(Symbol key, Sexpression value)
    {
        lockExclusively();
        //
        try
        {
            slots.put(key, value);
        }
        finally
        {
            unlockExclusively();
        }
    }
    /**
     * Tests whether the specified Key has an associated Value
     *
     * @param key Key
     * @return true, if the Key has an associated Value
     */
    public final boolean containsKey(Symbol key)
    {
        lockShared();
        //
        try
        {
            return slots.containsKey(key);
        }
        finally
        {
            unlockShared();
        }
    }
    /**
     * Removes the Instance Variable with the given Name
     *
     * @param key Name of Variable
     * @return Value of Variable
     */
    public final Sexpression remove(Symbol key)
    {
        lockExclusively();
        //
        try
        {
            return slots.remove(key);
        }
        finally
        {
            unlockExclusively();
        }
    }
    /**
     * Duplicates this Object, the duplicated Object will not be frozen
     *
     * @return Duplicated Object
     */
    @Override
    public final RubyStyleObject duplicate()
    {
        RubyStyleObject duplicate = new RubyStyleObject(classof);
        lockShared();
        //
        try
        {
            duplicate.slots.putAll(slots);
        }
        finally
        {
            unlockShared();
        }
        //
        return duplicate;
    }
    /**
     * Returns a Object with the given Class
     * and the same Instance Variables as this Object
     *
     * @param classof Class of Object
     * @return Object with given Class
     */
    public final RubyStyleObject changeClass(SimpleClass classof)
    {
        if (isFrozen()) return new RubyStyleObject(classof, slots);
        //
        RubyStyleObject duplicate = new RubyStyleObject(classof);
        lockShared();
        //
        try
        {
            duplicate.slots.putAll(slots);
        }
        finally
        {
            unlockShared();
        }
        //
        return duplicate;
    }
    /**
     * @see AssociativeContainer#metaHashCode()
     */
    @Override
    public int metaHashCode()
    {
        return classof.hashCode();
    }
    /**
     * @see lisp.Sexpression#getType()
     */
    public final String getType()
    {
        return "instance";
    }
}