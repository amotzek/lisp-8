package lisp;
/*
 * Copyright (C) 2011, 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import static lisp.Constant.isConstant;
import static lisp.concurrent.RunnableQueueFactory.stoppable;
import lisp.continuation.AwaitValue;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
import lisp.concurrent.RunnableQueueFactory;
import lisp.concurrent.StoppableRunnableQueue;
//
public final class Closure
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars EVALUATION_STOPPED = new Chars("evaluation stopped");
    //
    private final Environment environment;
    private final Sexpression sexpression;
    private RunnableQueue runnablequeue;
    private AwaitValue awaitvalue;
    /**
     * Constructor for Closure
     *
     * @param runnablequeue RunnableQueue
     * @param environment Environment
     * @param sexpression Sexpression
     */
    public Closure(RunnableQueue runnablequeue, Environment environment, Sexpression sexpression)
    {
        super();
        //
        this.runnablequeue = runnablequeue;
        this.environment = environment;
        this.sexpression = sexpression;
    }
    /**
     * Constructor for Closure
     *
     * @param environment Environment
     * @param sexpression Sexpression
     */
    public Closure(Environment environment, Sexpression sexpression)
    {
        this(RunnableQueueFactory.getConcurrentRunnableQueue(), environment, sexpression);
    }
    /**
     * Makes this Closure stoppable
     */
    public synchronized void setStoppable()
    {
        if (awaitvalue != null) throw new IllegalStateException("already started");
        //
        runnablequeue = stoppable(runnablequeue);
    }
    /**
     * Evaluates the Closure
     *
     * @return Value
     * @throws CannotEvalException if the Evaluation fails
     */
    public Sexpression eval() throws CannotEvalException
    {
        synchronized (this)
        {
            if (awaitvalue != null) return awaitvalue.getValue();
            //
            awaitvalue = new AwaitValue();
        }
        //
        if (isConstant(sexpression))
        {
            awaitvalue.succeed(sexpression);
            //
            return sexpression;
        }
        //
        sexpression.enQueueEvaluator(runnablequeue, environment, awaitvalue, awaitvalue);
        //
        if (runnablequeue.isPassive())
        {
            try
            {
                while (awaitvalue.isWaiting())
                {
                    Runnable runnable = runnablequeue.poll();
                    //
                    if (runnable == null) break;
                    //
                    runnable.run();
                }
            }
            catch (RuntimeException e)
            {
                awaitvalue.fail(ERROR, EVALUATION_STOPPED);
            }
        }
        //
        return awaitvalue.getValue();
    }
    /**
     * Stops the Evaluation
     */
    public synchronized void stop()
    {
        if (!(runnablequeue instanceof StoppableRunnableQueue)) throw new IllegalStateException("not stoppable");
        //
        StoppableRunnableQueue stoppablerunnablequeue = (StoppableRunnableQueue) runnablequeue;
        stoppablerunnablequeue.stop();
        //
        if (awaitvalue == null) awaitvalue = new AwaitValue();
        //
        awaitvalue.fail(ERROR, EVALUATION_STOPPED);
    }
}