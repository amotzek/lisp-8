package lisp;
/*
 * Copyright (C) 2001, 2006, 2007, 2010 - 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.equality.EqualityTest;
import lisp.evaluator.Evaluator;
import lisp.evaluator.ListEvaluator;
import lisp.formatter.Formattable;
import lisp.formatter.Formatter;
import lisp.traversal.CalculateHashCode;
import lisp.traversal.Traversal;
import lisp.concurrent.RunnableQueue;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;
/*
 * @author Andreasm
 */
public final class List implements Sexpression, Formattable
{
    private Sexpression first;
    private List rest;
    private volatile SoftReference<IdentityHashMap<Symbol, List>> associationsreference;
    private volatile SoftReference<HashMap<Sexpression, List>> elementsreference;
    /**
     * Constructor for List
     *
     * @param first First element of List
     * @param rest  Rest elements of List
     */
    public List(Sexpression first, List rest)
    {
        super();
        //
        this.first = first;
        this.rest = rest;
    }
    /**
     * Method first
     *
     * @return First element of List
     */
    public Sexpression first()
    {
        return first;
    }
    /**
     * This Method is only to be used by the Parser.
     *
     * @param first First
     */
    public void setFirst(Sexpression first)
    {
        this.first = first;
    }
    /**
     * This Method is only to be used by the Marshaller.
     *
     * @param rest Rest
     */
    public void setRest(List rest)
    {
        this.rest = rest;
    }
    /**
     * Method rest
     *
     * @return Rest elements of List
     */
    public List rest()
    {
        return rest;
    }
    /**
     * Method getAssociations
     */
    private IdentityHashMap<Symbol, List> getAssociations()
    {
        if (associationsreference != null)
        {
            IdentityHashMap<Symbol, List> associations = associationsreference.get();
            //
            if (associations != null) return associations;
        }
        //
        IdentityHashMap<Symbol, List> associations = new IdentityHashMap<>();
        //
        for (List list = this; list != null; list = list.rest())
        {
            if (!(list.first instanceof List)) continue;
            //
            List firstlist = (List) list.first;
            //
            if (!(firstlist.first instanceof Symbol)) continue;
            //
            Symbol key = (Symbol) firstlist.first;
            associations.put(key, firstlist);
        }
        //
        associationsreference = new SoftReference<>(associations);
        //
        return associations;
    }
    /**
     * Method lookup
     *
     * @param symbol Symbol to look up
     * @return List with Symbol and Values
     */
    public List lookup(Symbol symbol)
    {
        IdentityHashMap<Symbol, List> associations = getAssociations();
        //
        return associations.get(symbol);
    }
    /**
     * Method getElements
     */
    private HashMap<Sexpression, List> getElements()
    {
        if (elementsreference != null)
        {
            HashMap<Sexpression, List> elements = elementsreference.get();
            //
            if (elements != null) return elements;
        }
        //
        HashMap<Sexpression, List> elements = new HashMap<>();
        //
        for (List list = this; list != null; list = list.rest())
        {
            Sexpression element = list.first();
            //
            if (elements.containsKey(element)) continue;
            //
            elements.put(element, list);
        }
        //
        elementsreference = new SoftReference<>(elements);
        //
        return elements;
    }
    /**
     * Method isMember
     *
     * @param element Element to check
     * @return not null if the Element is in the List
     */
    public List isMember(Sexpression element)
    {
        HashMap<Sexpression, List> elements = getElements();
        //
        return elements.get(element);
    }
    /**
     * @see Sexpression#getType()
     */
    public String getType()
    {
        return "list";
    }
    /**
     * @see Sexpression#isConstant()
     */
    public boolean isConstant()
    {
        return false;
    }
    /**
     * @see Sexpression#enQueueEvaluator(lisp.concurrent.RunnableQueue, lisp.environment.Environment, lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void enQueueEvaluator(RunnableQueue runnablequeue, Environment environment, SuccessContinuation succeed, FailureContinuation fail)
    {
        Evaluator evaluator = new ListEvaluator(runnablequeue, environment, this, succeed, fail);
        runnablequeue.add(evaluator);
    }
    /**
     * Returns an Iterator for the Elements
     *
     * @return Iterator for Elements
     */
    public Iterator<Sexpression> elementIterator()
    {
        return new IteratorImpl(this);
    }
    /**
     * Inner Implementation of Iterator
     */
    private static class IteratorImpl implements Iterator<Sexpression>
    {
        private List list;
        //
        IteratorImpl(List list)
        {
            super();
            //
            this.list = list;
        }
        //
        public boolean hasNext()
        {
            return list != null;
        }
        //
        public Sexpression next()
        {
            if (list == null) throw new NoSuchElementException();
            //
            Sexpression first = list.first;
            list = list.rest;
            //
            return first;
        }
        //
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }
    /**
     * @see Object#hashCode()
     */
    public int hashCode()
    {
        Traversal traversal = new Traversal(this);
        CalculateHashCode hashcode = new CalculateHashCode();
        traversal.accept(hashcode);
        //
        return hashcode.hashCode();
    }
    /**
     * @see Object#equals(Object)
     */
    public boolean equals(Object object)
    {
        if (this == object) return true;
        //
        if (object instanceof List)
        {
            List that = (List) object;
            EqualityTest test = new EqualityTest();
            //
            return test.areEqual(this, that);
        }
        //
        return false;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        Formatter formatter = new Formatter();
        formatter.format(this);
        //
        return formatter.toString();
    }
    /**
     * Method length
     *
     * @param list List
     * @return Length of List
     */
    public static int length(List list)
    {
        int count = 0;
        //
        while (list != null)
        {
            count++;
            list = list.rest();
        }
        //
        return count;
    }
    /**
     * Method reverse
     *
     * @param list List to reverse
     * @return Reversed List
     */
    public static List reverse(List list)
    {
        List listreversed = null;
        //
        while (list != null)
        {
            listreversed = new List(list.first(), listreversed);
            list = list.rest();
        }
        //
        return listreversed;
    }
    /**
     * Method firstOrNull
     *
     * @param pair List
     * @return First element of List
     */
    public static Sexpression firstOrNull(List pair)
    {
        if (pair == null) return null;
        //
        return pair.first();
    }
    /**
     * Method second
     *
     * @param pair List
     * @return Second element of List
     */
    public static Sexpression second(List pair)
    {
        pair = pair.rest();
        //
        return pair.first();
    }
    /**
     * Method secondOrNull
     *
     * @param pair List
     * @return Second element of List or null, if List has no second element
     */
    public static Sexpression secondOrNull(List pair)
    {
        if (pair == null) return null;
        //
        pair = pair.rest();
        //
        if (pair == null) return null;
        //
        return pair.first();
    }
    /**
     * Method acons
     *
     * @param name  Name
     * @param value Value
     * @param list  Association List
     * @return Association List with additional binding
     */
    public static List acons(Symbol name, Sexpression value, List list)
    {
        List pair = new List(value, null);
        pair = new List(name, pair);
        //
        return new List(pair, list);
    }
    /**
     * Method list
     *
     * @param sexpressions Some Sexpressions
     * @return List with given Sexpressions
     */
    public static List list(Sexpression... sexpressions)
    {
        List list = null;
        int count = sexpressions.length;
        //
        while (count > 0)
        {
            list = new List(sexpressions[--count], list);
        }
        //
        return list;
    }
}