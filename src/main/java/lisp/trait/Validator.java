package lisp.trait;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.GenericFunction;
import lisp.GuardedMethod;
import lisp.List;
import lisp.Sexpression;
import lisp.SimpleClass;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;
/*
 * Created by  andreasm 06.01.13 18:44
 */
public final class Validator
{
    private static final Object lock = new Object();
    //
    private final ListOfReference<SimpleClass> simpleclasses;
    private final ListOfReference<GenericFunction> functions;
    private final MapOfSet<GenericFunction, GuardedMethod> methodsbyfunction;
    private final MapOfSet<SimpleClass, SimpleClass> subclassesbyclass;
    /**
     * Constructor for Validator
     *
     * @param simpleclasses References to Classes
     * @param functions References to Generic Functions
     */
    Validator(ListOfReference<SimpleClass> simpleclasses, ListOfReference<GenericFunction> functions)
    {
        super();
        //
        this.simpleclasses = simpleclasses;
        this.functions = functions;
        //
        methodsbyfunction = new MapOfSet<>(simpleclasses.size());
        subclassesbyclass = new MapOfSet<>(functions.size());
    }
    /**
     * Checks if the two Sets have a non-empty Intersection
     *
     * @param simpleclasses1 First Set of Classes
     * @param simpleclasses2 Second Set of Classes
     * @return true if the Sets have a non-empty Intersection
     */
    private static boolean intersect(Set<SimpleClass> simpleclasses1, Set<SimpleClass> simpleclasses2)
    {
        if (simpleclasses1.size() > simpleclasses2.size()) return intersect(simpleclasses2, simpleclasses1);
        //
        for (SimpleClass simpleclass : simpleclasses1)
        {
            if (!isTrait(simpleclass) && simpleclasses2.contains(simpleclass)) return true;
        }
        //
        return false;
    }
    /**
     * Checks if the S-Expression is a Trait
     *
     * @param sexpression S-Expression
     * @return true if the S-Expression is a Trait
     */
    private static boolean isTrait(Sexpression sexpression)
    {
        if (sexpression == null) return false;
        //
        return "trait".equals(sexpression.getType());
    }
    /**
     * Checks if two Classes are related, that means they are same or Super- or Subclass of each other
     *
     * @param simpleclass1 First Class
     * @param simpleclass2 Second Class
     * @return true if the Classes are related
     */
    private static boolean areRelated(SimpleClass simpleclass1, SimpleClass simpleclass2)
    {
        return simpleclass1.isCompatibleTo(simpleclass2) || simpleclass2.isCompatibleTo(simpleclass1);
    }
    /**
     * Creates the Error Message
     *
     * @param function Generic Function
     * @param method1 First Method
     * @param method2 Second Method
     * @return Error Message
     */
    private static String createConflictingMethodsMessage(GenericFunction function, GuardedMethod method1, GuardedMethod method2)
    {
        /*
         * canonicalize message
         */
        String specializers1 = method1.getSpezializers().toString();
        String specializers2 = method2.getSpezializers().toString();
        //
        if (specializers1.compareTo(specializers2) > 0)
        {
            String help = specializers2;
            specializers2 = specializers1;
            specializers1 = help;
        }
        //
        StringBuilder builder = new StringBuilder();
        builder.append("method with specializers ");
        builder.append(specializers1);
        builder.append(" conflicts with method with specializers ");
        builder.append(specializers2);
        builder.append(" in generic function ");
        builder.append(function.getFunctionName());
        //
        return builder.toString();
    }
    /**
     * Creates the Error Message
     *
     * @param simpleclass Class with Conflict
     * @param superclass1 First Superclass
     * @param superclass2 Second Superclass
     * @return Error Message
     */
    private static String createConflictingClassesMessage(SimpleClass simpleclass, SimpleClass superclass1, SimpleClass superclass2)
    {
        /*
         * canonicalize message
         */
        String name1 = superclass1.toString();
        String name2 = superclass2.toString();
        //
        if (name1.compareTo(name2) > 0)
        {
            String help = name2;
            name2 = name1;
            name1 = help;
        }
        //
        StringBuilder builder = new StringBuilder();
        builder.append("superclasses ");
        builder.append(name1);
        builder.append(" and ");
        builder.append(name2);
        builder.append(" of class ");
        builder.append(simpleclass);
        builder.append(" are related");
        //
        return builder.toString();
    }
    /**
     * Checks if the given Class can be defined without conflicting Traits
     *
     * @param simpleclass Class
     */
    public void validateClass(SimpleClass simpleclass)
    {
        synchronized (lock)
        {
            prepare();
            prepare(simpleclass);
            validate();
            simpleclasses.add(simpleclass);
        }
    }
    /**
     * Checks if the given Method can be defined without conflicting Traits
     *
     * @param function Generic Function
     * @param method Method
     */
    public void validateMethod(GenericFunction function, GuardedMethod method)
    {
        synchronized (lock)
        {
            prepare();
            prepare(function, method);
            validate();
            functions.addIfNew(function);
        }
    }
    /**
     * Prepares Data Structures for Validation
     */
    private void prepare()
    {
        for (SimpleClass simpleclass : simpleclasses)
        {
            prepare(simpleclass);
        }
        //
        for (GenericFunction function : functions)
        {
            List methods = function.getMethods();
            //
            while (methods != null)
            {
                GuardedMethod method = (GuardedMethod) methods.first();
                //
                if (method != null) prepare(function, method);
                //
                methods = methods.rest();
            }
        }
    }
    /**
     * Prepares Data Structures for a Class
     *
     * @param subclass Class
     */
    private void prepare(SimpleClass subclass)
    {
        subclassesbyclass.initIfNeeded(subclass);
        List superclasses = subclass.getSuperclasses();
        //
        while (superclasses != null)
        {
            SimpleClass superclass = (SimpleClass) superclasses.first();
            subclassesbyclass.add(superclass, subclass);
            superclasses = superclasses.rest();
        }
    }
    /**
     * Prepares Data Structures for a Method
     *
     * @param function Generic Function
     * @param method Method
     */
    private void prepare(GenericFunction function, GuardedMethod method)
    {
        methodsbyfunction.add(function, method);
    }
    /**
     * Validates Traits, Classes and Methods
     */
    private void validate()
    {
        for (SimpleClass simpleclass : subclassesbyclass.keySet())
        {
            List superclasses1 = simpleclass.getSuperclasses();
            //
            while (superclasses1 != null)
            {
                SimpleClass superclass1 = (SimpleClass) superclasses1.first();
                superclasses1 = superclasses1.rest();
                List superclasses2 = superclasses1;
                //
                while (superclasses2 != null)
                {
                    SimpleClass superclass2 = (SimpleClass) superclasses2.first();
                    //
                    if (areRelated(superclass1, superclass2)) throw new ValidationException(createConflictingClassesMessage(simpleclass, superclass1, superclass2));
                    //
                    superclasses2 = superclasses2.rest();
                }
            }
        }
        //
        for (Entry<GenericFunction, Set<GuardedMethod>> entry : methodsbyfunction.entrySet())
        {
            GenericFunction function = entry.getKey();
            Set<GuardedMethod> methods = entry.getValue();
            //
            for (GuardedMethod method1 : methods)
            {
                for (GuardedMethod method2 : methods)
                {
                    if (inConflict(method1, method2)) throw new ValidationException(createConflictingMethodsMessage(function, method1, method2));
                }
            }
        }
    }
    /**
     * Checks if the two Methods are conflicting
     *
     * @param method1 First Method
     * @param method2 Second Method
     * @return true if the Methods are conflicting
     */
    private boolean inConflict(GuardedMethod method1, GuardedMethod method2)
    {
        /*
        Für jede Methode, die einen Trait als Spezialisierer hat:
        Suche Methoden mit gleicher Parameterzahl in der selben
        generischen Funktion, die einen anderen (aber weder Super-
        noch Subtrait) als Spezialisierer an der gleichen Position hat.
        Wenn es möglich ist, dass die beiden Methoden von den gleichen
        Argumenten aufgerufen werden können, dann löse eine Ausnahme
        aus (die Spezialisierer haben eine gemeinsame Subklasse).
         */
        if (method1 == method2) return false;
        //
        if (method1.getParameterCount() != method2.getParameterCount()) return false;
        //
        List specializers1 = method1.getSpezializers();
        List specializers2 = method2.getSpezializers();
        //
        if (!matchSameArguments(specializers1, specializers2)) return false;
        //
        while (specializers1 != null && specializers2 != null)
        {
            Sexpression specializer1 = specializers1.first();
            Sexpression specializer2 = specializers2.first();
            //
            if (isTrait(specializer1) && isTrait(specializer2))
            {
                SimpleClass simpleclass1 = (SimpleClass) specializer1;
                SimpleClass simpleclass2 = (SimpleClass) specializer2;
                //
                if (!areRelated(simpleclass1, simpleclass2)) return true;
            }
            //
            specializers1 = specializers1.rest();
            specializers2 = specializers2.rest();
        }
        //
        return false;
    }
    /**
     * Checks if there are Arguments that are compatible with both Specializer Lists
     *
     * @param specializers1 First List of Specializers
     * @param specializers2 Second List of Specializers
     * @return true if there are Arguments that are compatible with both Specializer Lists
     */
    private boolean matchSameArguments(List specializers1, List specializers2)
    {
        while (specializers1 != null && specializers2 != null)
        {
            SimpleClass specializer1 = (SimpleClass) specializers1.first();
            SimpleClass specializer2 = (SimpleClass) specializers2.first();
            //
            if (specializer1 != null && specializer2 != null && !haveCommonSubclass(specializer1, specializer2)) return false;
            //
            specializers1 = specializers1.rest();
            specializers2 = specializers2.rest();
        }
        //
        return true;
    }
    /**
     * Checks if the Classes have a common Subclass
     *
     * @param simpleclass1 First Class
     * @param simpleclass2 Second Class
     * @return true if the Classes have a common Subclass
     */
    private boolean haveCommonSubclass(SimpleClass simpleclass1, SimpleClass simpleclass2)
    {
        Set<SimpleClass> subclasses1 = findSubclasses(simpleclass1);
        Set<SimpleClass> subclasses2 = findSubclasses(simpleclass2);
        //
        return intersect(subclasses1, subclasses2);
    }
    /**
     * Returns all Subclasses of the given Class
     *
     * @param simpleclass Class
     * @return All Subclasses of the given Class
     */
    private Set<SimpleClass> findSubclasses(SimpleClass simpleclass)
    {
        HashSet<SimpleClass> allsubclasses = new HashSet<>();
        LinkedList<SimpleClass> agenda = new LinkedList<>();
        agenda.addFirst(simpleclass);
        //
        while (!agenda.isEmpty())
        {
            SimpleClass superclass = agenda.removeLast();
            //
            if (allsubclasses.contains(superclass)) continue;
            //
            allsubclasses.add(superclass);
            Set<SimpleClass> subclasses = subclassesbyclass.get(superclass);
            //
            if (subclasses == null) continue;
            //
            subclasses.forEach(agenda::addFirst);
        }
        //
        return allsubclasses;
    }
}