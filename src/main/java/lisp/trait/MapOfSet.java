package lisp.trait;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map.Entry;
import java.util.Set;
/*
 * Created by  andreasm 07.01.13 19:26
 */
final class MapOfSet<K, V>
{
    private final IdentityHashMap<K, Set<V>> valuesbykey;
    /**
     * Costructor for MapOfSet
     *
     * @param initialsize Initial Size of HashMap
     */
    public MapOfSet(int initialsize)
    {
        super();
        //
        valuesbykey = new IdentityHashMap<>(initialsize);
    }
    /**
     * Initializes Set of Values for the given Key if needed
     *
     * @param key Key
     */
    public void initIfNeeded(K key)
    {
        if (valuesbykey.get(key) != null) return;
        //
        valuesbykey.put(key, new HashSet<>());
    }
    /**
     * Adds a Value for the given Key
     *
     * @param key Key
     * @param value Value
     */
    public void add(K key, V value)
    {
        Set<V> values = valuesbykey.get(key);
        //
        if (values == null)
        {
            values = new HashSet<>();
            valuesbykey.put(key, values);
        }
        //
        values.add(value);
    }
    /**
     * Returns a Set of all Values for the given Key
     *
     * @param key Key
     * @return Set of Values for the given Key
     */
    public Set<V> get(K key)
    {
        return valuesbykey.get(key);
    }
    /**
     * Returns a Set of all Entries
     *
     * @return Entry Set
     */
    public Set<Entry<K, Set<V>>> entrySet()
    {
        return valuesbykey.entrySet();
    }
    /**
     * Returns a Set of all Keys
     *
     * @return Key Set
     */
    public Set<K> keySet()
    {
        return valuesbykey.keySet();
    }
}