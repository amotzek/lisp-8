package lisp.trait;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;
/*
 * Created by  andreasm 07.01.13 19:44
 */
final class ListOfReference<V> implements Iterable<V>
{
    private final LinkedList<WeakReference<V>> references;
    /**
     * Constructor for ListOfReference
     */
    public ListOfReference()
    {
        references = new LinkedList<>();
    }
    /**
     * Adds a Value
     *
     * @param value Value
     */
    public void add(V value)
    {
        WeakReference<V> reference = new WeakReference<>(value);
        references.addLast(reference);
    }
    /**
     * Adds a Value if it is not already present in the List
     *
     * @param value1 Value
     */
    public void addIfNew(V value1)
    {
        Iterator<WeakReference<V>> iterator = references.iterator();
        //
        while (iterator.hasNext())
        {
            WeakReference<V> reference = iterator.next();
            V value2 = reference.get();
            //
            if (value2 == null) iterator.remove();
            //
            if (value1 == value2) return;
        }
        //
        add(value1);
    }
    /**
     * Returns an Iterator for all Values
     *
     * @return Iterator
     */
    public Iterator<V> iterator()
    {
        return new ReferenceIterator(references.iterator());
    }
    /**
     * Returns the approximate Size
     *
     * @return Size
     */
    public int size()
    {
        return references.size();
    }
    /**
     * Inner Iterator Class
     */
    private class ReferenceIterator implements Iterator<V>
    {
        private final Iterator<WeakReference<V>> iterator;
        private V value;
        //
        private ReferenceIterator(Iterator<WeakReference<V>> iterator)
        {
            this.iterator = iterator;
        }
        //
        public boolean hasNext()
        {
            while (iterator.hasNext())
            {
                WeakReference<V> reference = iterator.next();
                value = reference.get();
                //
                if (value != null) return true;
                //
                iterator.remove();
            }
            //
            return false;
        }
        //
        public V next()
        {
            if (value == null) throw new IllegalStateException();
            //
            return value;
        }
        //
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }
}