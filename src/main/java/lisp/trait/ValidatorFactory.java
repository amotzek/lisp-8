package lisp.trait;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.GenericFunction;
import lisp.SimpleClass;
/*
 * Created by  andreasm 06.01.13 18:44
 */
public final class ValidatorFactory
{
    private static final ListOfReference<SimpleClass> simpleclasses = new ListOfReference<>();
    private static final ListOfReference<GenericFunction> functions = new ListOfReference<>();
    /**
     * No Instances of ValidatorFactory will be created
     */
    private ValidatorFactory()
    {
    }
    /**
     * Creates a Validator
     *
     * @return Validator
     */
    public static Validator createValidator()
    {
        return new Validator(simpleclasses, functions);
    }
}