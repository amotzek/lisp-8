package lisp.module;
/*
 * Copyright (C) 2015, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
/*
 * Created by andreasm on 08.06.15
 */
final class TopologicalComparator implements Comparator<Module>
{
    private final HashMap<String, Module> modulesbyname;
    //
    public TopologicalComparator()
    {
        super();
        //
        modulesbyname = new HashMap<>();
    }
    //
    public void prepare(Collection<Module> modules)
    {
        for (Module module : modules)
        {
            String name = module.getName();
            modulesbyname.put(name, module);
            module.setDepth(null);
        }
        //
        for (Module module : modules)
        {
            depthFirst(module);
        }
    }
    //
    private void depthFirst(Module module1)
    {
        if (module1.getDepth() != null) return;
        //
        module1.setDepth(0);
        Collection<ModuleDependency> dependencies = module1.getDependencies();
        //
        for (ModuleDependency dependency : dependencies)
        {
            Module module2 = getModule(dependency);
            //
            if (module2 == null) throw new IllegalStateException("comparator not prepared");
            //
            depthFirst(module2);
            int depth1 = module1.getDepth();
            int depth2 = module2.getDepth() + 1;
            //
            if (depth2 > depth1) module1.setDepth(depth2);
        }
    }
    //
    private Module getModule(ModuleDependency dependency)
    {
        String name = dependency.getName();
        //
        return modulesbyname.get(name);
    }
    //
    public int compare(Module module1, Module module2)
    {
        int depth1 = module1.getDepth();
        int depth2 = module2.getDepth();
        //
        if (depth1 < depth2) return -1;
        //
        if (depth1 > depth2) return 1;
        //
        String name1 = module1.getName();
        String name2 = module2.getName();
        //
        return name1.compareTo(name2);
    }
}