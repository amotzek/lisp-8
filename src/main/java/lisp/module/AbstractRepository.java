package lisp.module;
/*
 * Copyright (C) 2015 - 2017 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Combinator;
import lisp.GenericFunction;
import lisp.Lambda;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.environment.Environment;
import lisp.environment.NotBoundException;
import lisp.environment.io.EnvironmentReader;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeSet;
/*
 * Created by Andreas on 29.08.2015.
 */
public abstract class AbstractRepository
{
    private final HashMap<String, Module> modulesbyname;
    /**
     * Initializes the Repository
     */
    protected AbstractRepository()
    {
        super();
        //
        modulesbyname = new HashMap<>();
    }
    /**
     * Adds the given Module
     *
     * @param newmodule Module
     * @return added Module or null, if the Module was not added
     */
    @SuppressWarnings("all")
    public final Module addModule(Module newmodule)
    {
        String name = newmodule.getName();
        Module oldmodule = modulesbyname.get(name);
        //
        if (oldmodule != null && oldmodule.getVersion() >= newmodule.getVersion()) return null;
        //
        modulesbyname.put(name, newmodule);
        //
        return newmodule;
    }
    /**
     * Adds the given Modules
     *
     * @param modules Modules
     */
    public final void addModules(Collection<Module> modules)
    {
        for (Module module : modules)
        {
            addModule(module);
        }
    }
    /**
     * Adds a Module to the Repository
     *
     * @param dependency specifies the Module
     * @return added Module or null, if the Module was not added
     * @throws IOException if the Module cannot be read
     */
    @SuppressWarnings("all")
    public final Module addModule(ModuleDependency dependency) throws IOException
    {
        return addModule(dependency.getName(), dependency.getMinimumRequiredVersion(), dependency.getURI());
    }
    /**
     * Adds a Module to the Repository
     *
     * @param expectedname Name of Moduls
     * @param minimumrequiredversion Required Version
     * @param uri URI for loading the Module
     * @return added Module or null, if the Module was not added
     * @throws IOException if the Module cannot be read
     */
    @SuppressWarnings("all")
    public final Module addModule(String expectedname, int minimumrequiredversion, URI uri) throws IOException
    {
        Module module = modulesbyname.get(expectedname);
        //
        if (module != null && module.getVersion() >= minimumrequiredversion) return module;
        //
        module = loadModule(uri);
        String name = module.getName();
        int version = module.getVersion();
        //
        if (!expectedname.equals(name)) throw new IOException(String.format("expected module name %s but loaded %s", expectedname, name));
        //
        if (module.getVersion() < minimumrequiredversion) throw new IOException(String.format("minimum required version of module %s is %s but loaded version is %s", expectedname, minimumrequiredversion, version));
        //
        return addModule(module);
    }
    /**
     * Loads and adds a Module
     *
     * @param uri URI for loading the Module
     * @return added Module or null, if the Module was not added
     * @throws IOException if the Module cannot be read
     */
    public Module addModule(URI uri) throws IOException
    {
        Module module = loadModule(uri);
        //
        return addModule(module);
    }
    /**
     * Loads a Module from the URL
     *
     * @param uri URI
     * @return loaded Module
     * @throws IOException if the Module cannot be read
     */
    protected abstract Module loadModule(URI uri) throws IOException;
    /**
     * Removes the Module with the given Name
     *
     * @param name Name of Module
     * @return removed Module or null, if no Module was removed
     */
    @SuppressWarnings("unused")
    public final Module removeModule(String name)
    {
        return modulesbyname.remove(name);
    }
    /**
     * Removes all Modules
     */
    public final void clearModules()
    {
        modulesbyname.clear();
    }
    /**
     * Returns all Modules in topological order
     *
     * @return Modules
     */
    public final Collection<Module> getModules()
    {
        Collection<Module> modules = modulesbyname.values();
        TopologicalComparator comparator = new TopologicalComparator();
        comparator.prepare(modules);
        TreeSet<Module> sortedmodules = new TreeSet<>(comparator);
        sortedmodules.addAll(modules);
        //
        return Collections.unmodifiableCollection(sortedmodules);
    }
    /**
     * Makes sure that all needed Modules are loaded
     *
     * @throws IOException if the Dependencies cannot be resolved
     */
    public final void satisfyDependencies() throws IOException
    {
        LinkedList<ModuleDependency> agenda = new LinkedList<>();
        //
        for (Module module : modulesbyname.values())
        {
            Collection<ModuleDependency> dependencies = module.getDependencies();
            agenda.addAll(dependencies);
        }
        //
        while (!agenda.isEmpty())
        {
            ModuleDependency dependency = agenda.removeLast();
            Module module = addModule(dependency);
            //
            if (module == null) continue;
            //
            Collection<ModuleDependency> dependencies = module.getDependencies();
            agenda.addAll(dependencies);
        }
    }
    /**
     * Creates an Environment with the Exports of the Modules
     *
     * @param parent Parent Environment
     * @return created Environment
     * @throws CannotEvalException if the Modules cannot be evaluated
     */
    public final Environment createEnvironment(Environment parent) throws CannotEvalException
    {
        Collection<Module> modules = getModules();
        HashMap<String, Environment> environmentsbyname = new HashMap<>();
        //
        for (Module module : modules)
        {
            Environment child = new Environment(parent);
            Collection<ModuleDependency> dependencies = module.getDependencies();
            //
            for (ModuleDependency dependency : dependencies)
            {
                String dependencyname = dependency.getName();
                Module dependencymodule = modulesbyname.get(dependencyname);
                //
                if (dependencymodule == null) throw new CannotEvalException("dependencies not satisfied");
                //
                Collection<String> exports = dependencymodule.getExports();
                Environment sibling = environmentsbyname.get(dependencyname);
                copyExports(sibling, child, exports);
            }
            //
            String name = module.getName();
            String body = module.getBody();
            EnvironmentReader reader = new EnvironmentReader(child);
            reader.readFrom(body);
            environmentsbyname.put(name, child);
        }
        //
        Environment child = new Environment(parent);
        //
        for (Module module : modules)
        {
            String name = module.getName();
            Collection<String> exports = module.getExports();
            Environment sibling = environmentsbyname.get(name);
            copyExports(sibling, child, exports);
        }
        //
        return child;
    }
    //
    private static void copyExports(Environment source, Environment destination, Collection<String> exports) throws CannotEvalException
    {
        for (String export : exports)
        {
            Symbol symbol = Symbol.createSymbol(export);
            copySymbolValue(source, destination, symbol);
        }
    }
    //
    private static void copySymbolValue(Environment sourceenvironment, Environment destinationenvironment, Symbol symbol) throws CannotEvalException
    {
        try
        {
            Sexpression sourcevalue = sourceenvironment.at(symbol);
            //
            try
            {
                Sexpression destinationvalue = destinationenvironment.at(symbol);
                //
                if (sourcevalue == destinationvalue) return;
                //
                GenericFunction destinationfunction = toGenericFunction(symbol, destinationvalue);
                merge(sourcevalue, destinationfunction);
                destinationenvironment.add(false, symbol, destinationfunction);
            }
            catch (NotBoundException e)
            {
                destinationenvironment.add(false, symbol, sourcevalue);
            }
        }
        catch (NotBoundException e)
        {
            throw new CannotEvalException(String.format("cannot export %s", symbol));
        }
    }
    //
    private static GenericFunction toGenericFunction(Symbol symbol, Sexpression value) throws CannotEvalException
    {
        if (value instanceof GenericFunction) return (GenericFunction) value;
        //
        GenericFunction function = new GenericFunction(symbol);
        merge(value, function);
        //
        return function;
    }
    //
    private static void merge(Sexpression source, GenericFunction destination) throws CannotEvalException
    {
        if (source instanceof Lambda)
        {
            destination.addLambda((Lambda) source);
        }
        else if (source instanceof Combinator)
        {
            destination.addCombinator((Combinator) source);
        }
        else if (source instanceof GenericFunction)
        {
            destination.addGenericFunction((GenericFunction) source);
        }
        else
        {
            throw new CannotEvalException(String.format("cannot merge %s", destination.getFunctionName()));
        }
    }
}