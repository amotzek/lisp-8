package lisp.module;
/*
 * Copyright (C) 2015, 2017 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.net.URI;
import java.util.Collection;
import java.util.Collections;
/*
 * Created by andreasm on 04.06.15
 */
public final class Module
{
    private final String name;
    private final Integer version;
    private final URI uri;
    private final String commentde;
    private final String commenten;
    private final Collection<String> exports;
    private final String body;
    private final Collection<ModuleDependency> dependencies;
    private Integer depth;
    //
    public Module(String name, Integer version, URI uri, String commentde, String commenten, Collection<String> exports, String body, Collection<ModuleDependency> dependencies)
    {
        super();
        //
        this.name = name;
        this.version = version;
        this.uri = uri;
        this.commentde = commentde;
        this.commenten = commenten;
        this.exports = exports;
        this.body = body;
        this.dependencies = dependencies;
    }
    //
    public String getName()
    {
        return name;
    }
    //
    public Integer getVersion()
    {
        return version;
    }
    //
    public URI getURI()
    {
        return uri;
    }
    //
    public String getCommentDe()
    {
        return commentde;
    }
    //
    public String getCommentEn()
    {
        return commenten;
    }
    //
    public Collection<String> getExports()
    {
        return Collections.unmodifiableCollection(exports);
    }
    //
    public String getBody()
    {
        return body;
    }
    //
    public Collection<ModuleDependency> getDependencies()
    {
        return Collections.unmodifiableCollection(dependencies);
    }
    //
    Integer getDepth()
    {
        return depth;
    }
    //
    void setDepth(Integer depth)
    {
        this.depth = depth;
    }
    //
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        //
        if (!(o instanceof Module)) return false;
        //
        Module that = (Module) o;
        //
        if (!name.equals(that.name)) return false;
        //
        if (version == null) return that.version == null;
        //
        return version.equals(that.version);
    }
    //
    @Override
    public int hashCode()
    {
        return name.hashCode();
    }
}