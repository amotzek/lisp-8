package lisp.module;
/*
 * Copyright (C) 2015, 2017 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.net.URI;
/*
 * Created by andreasm on 07.06.15
 */
public final class ModuleDependency
{
    private final String name;
    private final Integer version;
    private final URI uri;
    private final Long id;
    //
    public ModuleDependency(String name, Integer version, URI uri, Long id)
    {
        this.name = name;
        this.version = version;
        this.uri = uri;
        this.id = id;
    }
    //
    public String getName()
    {
        return name;
    }
    //
    public Integer getMinimumRequiredVersion()
    {
        return version;
    }
    //
    public URI getURI()
    {
        return uri;
    }
    //
    public Long getId()
    {
        return id;
    }
    //
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        //
        if (!(o instanceof ModuleDependency)) return false;
        //
        ModuleDependency that = (ModuleDependency) o;
        //
        // Version wird nicht verglichen, weil dies sonst zu
        // unerwünschten Updates in PersistModules führt.
        //
        return name.equals(that.name);
    }
    //
    @Override
    public int hashCode()
    {
        return name.hashCode();
    }
}