package lisp;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.Comparator;
import java.util.Map;
/*
 * Created by  andreasm 02.01.13 15:04
 */
final class MethodComparator implements Comparator<GuardedMethod>
{
    private final Sexpression[] arguments;
    /**
     * Constructor for Comparator
     *
     * @param arguments Arguments to use as Context for Comparison
     */
    public MethodComparator(Sexpression[] arguments)
    {
        this.arguments = arguments;
    }
    /**
     * Compares two Classes in Context of an Argument
     *
     * @param leftclass First Class
     * @param rightclass Second Class
     * @param argument Argument
     * @return More specific Classes are smaller than less specific ones
     */
    private static int compareClasses(SimpleClass leftclass, SimpleClass rightclass, Sexpression argument)
    {
        if (leftclass == rightclass) return 0;
        /*
         * Sort Methods without Specializers to the bottom
         */
        if (leftclass == null) return 1;
        //
        if (rightclass == null) return -1;
        /*
         * Use the Linearization of the Superclasses of the Argument to compare Classes
         */
        RubyStyleObject argumentinstance = (RubyStyleObject) argument;
        SimpleClass argumentclass = argumentinstance.classOf();
        Map<SimpleClass, Integer> linearization = argumentclass.getLinearization();
        Integer leftposition = linearization.get(leftclass);
        Integer rightposition = linearization.get(rightclass);
        //
        if (leftposition != null && rightposition != null) return leftposition - rightposition;
        //
        throw new IllegalStateException();
    }
    /**
     * Returns the Size of a S-Expression
     *
     * @param sexpression S-Expression
     * @return Size
     */
    private static int size(Sexpression sexpression)
    {
        if (sexpression instanceof List)
        {
            int size = 0;
            List list = (List) sexpression;
            //
            while (list != null)
            {
                size += size(list.first());
                list = list.rest();
            }
            //
            return size;
        }
        //
        return 1;
    }
    //
    /**
     * Compares two Methods in Context of the Arguments
     *
     * @param leftmethod First Method
     * @param rightmethod Second Method
     * @return More specific Methods are smaller than more specific Methods
     * @see Comparator#compare(Object, Object)
     */
    public int compare(GuardedMethod leftmethod, GuardedMethod rightmethod)
    {
        int leftparametercount = leftmethod.getParameterCount();
        int rightparametercount = rightmethod.getParameterCount();
        int result = rightparametercount - leftparametercount;
        //
        if (result != 0) return result;
        //
        List leftspecializers = leftmethod.getSpezializers();
        List rightspecializers = rightmethod.getSpezializers();
        //
        for (int position = 0; position < leftparametercount; position++)
        {
            SimpleClass leftspecializer = (SimpleClass) leftspecializers.first();
            SimpleClass rightspecializer = (SimpleClass) rightspecializers.first();
            Sexpression argument = arguments[position];
            result = compareClasses(leftspecializer, rightspecializer, argument);
            //
            if (result != 0) return result;
            //
            leftspecializers = leftspecializers.rest();
            rightspecializers = rightspecializers.rest();
        }
        //
        int leftguardsize = size(leftmethod.getGuard());
        int rightguardsize = size(rightmethod.getGuard());
        //
        return rightguardsize - leftguardsize;
    }
}