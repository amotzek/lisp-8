package lisp;
/*
 * Copyright (C) 2001, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Erstellungsdatum: (24.08.01 18:26:30)
 *
 * @author Andreasm
 */
public interface Function extends Sexpression
{
    /**
     * Checks if an Invocation of the Function accepts the
     * given count of Arguments
     *
     * @param argumentcount Count of Arguments
     * @return true, if Argument Count is accepted
     */
    boolean acceptsArgumentCount(int argumentcount);
    /**
     * Checks if this is a Macro
     *
     * @return true, if Arguments are not processed individually
     */
    boolean isMacro();
    /**
     * Checks if an Argument has to be quoted
     *
     * @param position Parameter Position
     * @return true, if Parameter Position is quoted
     */
    boolean mustBeQuoted(int position);
    /**
     * Applies the Function to the Arguments
     *
     * @param runnablequeue RunnableQueue
     * @param environment Environment
     * @param arguments List of Arguments
     * @param succeed SuccessContinuation
     * @param fail FailureContinuation
     */
    void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail);
}