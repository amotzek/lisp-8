package lisp.parser;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Constant;
import lisp.Sexpression;
/*
 * Created by  andreasm 22.12.12 13:44
 */
final class Destination extends Constant
{
    private final Sexpression sexpression;
    private final Long id;
    /**
     * Constructor for Destination
     *
     * @param token Token
     * @param sexpression S-Expression
     */
    public Destination(String token, Sexpression sexpression)
    {
        super();
        //
        this.sexpression = sexpression;
        //
        id = new Long(token);
    }
    /**
     * Returns the Id for this Reference Destination
     *
     * @return Id
     */
    public Long getId()
    {
        return id;
    }
    /**
     * Returns the referenced S-Expression
     *
     * @return S-Expression
     */
    public Sexpression getSexpression()
    {
        return sexpression;
    }
    /**
     * @see lisp.Sexpression#getType()
     */
    public String getType()
    {
        return "destination";
    }
}