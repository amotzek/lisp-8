package lisp.parser;
/*
 * Copyright (C) 2001, 2007, 2009, 2010, 2012, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Array;
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Closure;
import lisp.HashTable;
import lisp.List;
import lisp.Rational;
import lisp.RubyStyleObject;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.combinator.EnvironmentFactory;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
import lisp.concurrent.RunnableQueueFactory;
import java.util.HashMap;
import java.util.Iterator;
/*
 * Erstellungsdatum: (25.08.2001 13:39:11)
 *
 * @author Andreasm
 */
public final class Parser
{
    private final Environment environment;
    private final Scanner scanner;
    private boolean hasreferences;
    /**
     * Constructor for Parser
     *
     * @param environment Environment for Read Time Evaluation
     * @param input    String to parse
     * @param position Position to start parsing
     */
    public Parser(Environment environment, String input, int position)
    {
        super();
        //
        this.environment = environment;
        //
        scanner = new Scanner(input, position);
    }
    /**
     * Constructor for Parser
     *
     * @param input    String to parse
     * @param position Position to start parsing
     */
    public Parser(String input, int position)
    {
        this(EnvironmentFactory.createEnvironment(), input, position);
    }
    /**
     * Returns the parsing Position
     *
     * @return Position
     */
    public int getPosition()
    {
        return scanner.getPosition();
    }
    /**
     * Returns a parsed S-Expression
     *
     * @return parsed S-Expression
     */
    public Sexpression getSexpression()
    {
        try
        {
            Sexpression sexpression = parseAtomAndList();
            //
            if (!hasreferences) return sexpression;
            //
            HashMap<Long, Sexpression> sexpressionsbyid = new HashMap<>();
            //
            return resolveReferences(sexpression, sexpressionsbyid);
        }
        catch (Exception e)
        {
            return null;
        }
        finally
        {
            hasreferences = false;
        }
    }
    /**
     * Parses a S-Exptession
     *
     * @return parsed Atom or List
     * @throws CannotEvalException if Read-Time Evaluation failed
     */
    private Sexpression parseAtomAndList() throws CannotEvalException
    {
        scanner.scan();
        //
        switch (scanner.getState())
        {
            case Scanner.SYMBOL:
            {
                String token = scanner.getToken();
                //
                if ("nil".equals(token)) return null;
                //
                return Symbol.createSymbol(token);
            }
            //
            case Scanner.STRING:
                //
                return new Chars(scanner.getToken());
            //
            case Scanner.NUMBER:
            case Scanner.FRACTION:
            case Scanner.NUMBER_END:
                //
                return new Rational(scanner.getToken());
            //
            case Scanner.OPEN:
                //
                return parseList();
            //
            case Scanner.READ_TIME_EVALUATION:
            {
                Sexpression sexpression = parseAtomAndList();
                RunnableQueue runnablequeue = RunnableQueueFactory.createPassiveRunnableQueue();
                Closure closure = new Closure(runnablequeue, environment, sexpression);
                //
                return closure.eval();
            }
            //
            case Scanner.SOURCE:
                hasreferences = true;
                //
                return new Source(scanner.getToken());
            //
            case Scanner.DESTINATION:
            {
                hasreferences = true;
                String token = scanner.getToken();
                Sexpression sexpression = parseAtomAndList();
                //
                return new Destination(token, sexpression);
            }
        }
        //
        return null;
    }
    /**
     * Parses a List
     *
     * @return parsed List
     * @throws CannotEvalException if Read-Time Evaluation failed
     */
    private Sexpression parseList() throws CannotEvalException
    {
        List list = null;
        //
        while (true)
        {
            Sexpression first = parseAtomAndList();
            //
            switch (scanner.getState())
            {
                case Scanner.ERROR:
                    return null;
                //
                case Scanner.CLOSE:
                    scanner.resetState(); // only close one List at a time
                    //
                    return List.reverse(list);
                //
                default:
                    list = new List(first, list);
            }
        }
    }
    /**
     * Replaces Sources and removes Destionations
     *
     * @param in S-Expression
     * @param sexpressionsbyid Map from Ids to S-Expressions
     * @return S-Expression without Sources and Destinations
     */
    private static Sexpression resolveReferences(Sexpression in, HashMap<Long, Sexpression> sexpressionsbyid)
    {
        if (in instanceof Source)
        {
            Source source = (Source) in;
            Long id = source.getId();
            Sexpression out = sexpressionsbyid.get(id);
            //
            if (out == null) throw new IllegalStateException();
            //
            return out;
        }
        //
        if (in instanceof Destination)
        {
            Destination destination = (Destination) in;
            Long id = destination.getId();
            Sexpression intermediate = destination.getSexpression();
            //
            if (sexpressionsbyid.put(id, intermediate) != null) throw new IllegalStateException();
            //
            Sexpression out = resolveReferences(intermediate, sexpressionsbyid);
            //
            if (out != intermediate) throw new IllegalStateException();
            //
            return out;
        }
        //
        if (in instanceof List)
        {
            List list = (List) in;
            //
            while (list != null)
            {
                Sexpression first = list.first();
                Sexpression intermediate = resolveReferences(first, sexpressionsbyid);
                //
                if (first != intermediate) list.setFirst(intermediate);
                //
                list = list.rest();
            }
            //
            return in;
        }
        //
        if (in instanceof Array)
        {
            Array array = (Array) in;
            Iterator<List> keys = array.keyIterator();
            //
            while(keys.hasNext())
            {
                List key = keys.next();
                Sexpression element = array.get(key);
                Sexpression intermediate = resolveReferences(element, sexpressionsbyid);
                //
                if (element != intermediate) array.put(key, intermediate);
            }
            //
            return in;
        }
        //
        if (in instanceof HashTable)
        {
            HashTable hashtable = (HashTable) in;
            Iterator<Sexpression> keys = hashtable.keyIterator();
            //
            while(keys.hasNext())
            {
                Sexpression key = keys.next();
                Sexpression element = hashtable.get(key);
                Sexpression intermediatekey = resolveReferences(key, sexpressionsbyid);
                Sexpression intermediateelement = resolveReferences(element, sexpressionsbyid);
                //
                if (key != intermediatekey)
                {
                    hashtable.put(key, null);
                    hashtable.put(intermediatekey, intermediateelement);
                }
                else if (element != intermediateelement)
                {
                    hashtable.put(key, intermediateelement);
                }
            }
            //
            return in;
        }
        //
        if (in instanceof RubyStyleObject)
        {
            RubyStyleObject instance = (RubyStyleObject) in;
            Iterator<Symbol> iterator = instance.keyIterator();
            //
            while (iterator.hasNext())
            {
                Symbol key = iterator.next();
                Sexpression element = instance.get(key);
                Sexpression intermediate = resolveReferences(element, sexpressionsbyid);
                //
                if (element != intermediate) instance.put(key, intermediate);
            }
            //
            return in;
        }
        //
        return in;
    }
}