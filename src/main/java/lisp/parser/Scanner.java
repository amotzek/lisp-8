package lisp.parser;
/*
 * Copyright (C) 2012, 2017 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by  andreasm 23.12.12 13:20
 */
final class Scanner
{
    public static final int START = 0;
    public static final int OPEN = 1;
    public static final int CLOSE = 2;
    public static final int NUMBER_OR_SYMBOL = 3;
    public static final int NUMBER = 4;
    public static final int FRACTION = 5;
    public static final int EXPONENT = 6;
    public static final int RATIO = 7;
    public static final int NUMBER_END = 8;
    public static final int STRING = 9;
    public static final int STRING_QUOTE = 10;
    public static final int SYMBOL = 11;
    public static final int ERROR = 12;
    public static final int SHARPSIGN = 13;
    public static final int READ_TIME_EVALUATION = 14;
    public static final int SOURCE_OR_DESTINATION = 15;
    public static final int SOURCE = 16;
    public static final int DESTINATION = 17;
    public static final int COMMENT = 18;
    public static final int COMMENT_END = 19;
    //
    private final String input;
    private int position;
    private int state;
    private StringBuilder token;
    /**
     * Constructor for Scanner
     *
     * @param input String to scan
     * @param position Start Position
     */
    public Scanner(String input, int position)
    {
        super();
        //
        this.input = input;
        this.position = position;
    }
    //
    private static boolean isBlank(char c)
    {
        return c <= (char) 32 || c == (char) 160;
    }
    //
    private static boolean isQuotationMark(char c)
    {
        return c == '"';
    }
    //
    private static boolean isOpeningBracket(char c)
    {
        return c == '(';
    }
    //
    private static boolean isClosingBracket(char c)
    {
        return c == ')';
    }
    //
    private static boolean isPlusOrMinusSign(char c)
    {
        return (c == '-') || (c == '+');
    }
    //
    private static boolean isDigit(char c)
    {
        return (c >= '0') && (c <= '9');
    }
    //
    private static boolean isSharpSign(char c)
    {
        return c == '#';
    }
    //
    private static boolean isBackslash(char c)
    {
        return c == '\\';
    }
    //
    private static boolean isSlash(char c)
    {
        return c == '/';
    }
    //
    private static boolean isDot(char c)
    {
        return c == '.';
    }
    //
    private static boolean isExponentSign(char c)
    {
        return c == 'e' || c == 'E';
    }
    //
    private static boolean isVerticalBar(char c)
    {
        return c == '|';
    }
    //
    private static boolean isEqualsSign(char c)
    {
        return c == '=';
    }
    /**
     * Scanns the next Token
     */
    public void scan()
    {
        state = START;
        token = null;
        //
        while (position < input.length())
        {
            char c = input.charAt(position);
            //
            switch (state)
            {
                case START:
                    if (isBlank(c))
                    {
                        skip();
                    }
                    else if (isQuotationMark(c))
                    {
                        initToken();
                        state = STRING;
                    }
                    else if (isOpeningBracket(c))
                    {
                        skip();
                        state = OPEN;
                        //
                        return;
                    }
                    else if (isClosingBracket(c))
                    {
                        skip();
                        state = CLOSE;
                        //
                        return;
                    }
                    else if (isPlusOrMinusSign(c))
                    {
                        emit(c);
                        state = NUMBER_OR_SYMBOL;
                    }
                    else if (isDigit(c))
                    {
                        emit(c);
                        state = NUMBER;
                    }
                    else if (isSharpSign(c))
                    {
                        skip();
                        state = SHARPSIGN;
                    }
                    else
                    {
                        emit(c);
                        state = SYMBOL;
                    }
                    //
                    break;
                //
                case STRING:
                    if (isBackslash(c))
                    {
                        skip();
                        state = STRING_QUOTE;
                    }
                    else if (!isQuotationMark(c))
                    {
                        emit(c);
                    }
                    else
                    {
                        skip();
                        //
                        return;
                    }
                    //
                    break;
                //
                case STRING_QUOTE:
                    if (isBackslash(c) || isQuotationMark(c))
                    {
                        emit(c);
                        state = STRING;
                    }
                    else if (c == 'n')
                    {
                        emit('\n');
                        state = STRING;
                    }
                    else if (c == 'r')
                    {
                        emit('\r');
                        state = STRING;
                    }
                    else
                    {
                        state = ERROR;
                        //
                        return;
                    }
                    //
                    break;
                //
                case SYMBOL:
                    if (!isOpeningBracket(c) && !isClosingBracket(c) && !isBlank(c))
                    {
                        emit(c);
                    }
                    else
                    {
                        return;
                    }
                    //
                    break;
                //
                case NUMBER_OR_SYMBOL:
                    if (isOpeningBracket(c) || isClosingBracket(c) || isBlank(c))
                    {
                        state = SYMBOL;
                        //
                        return;
                    }
                    else if (isDigit(c))
                    {
                        emit(c);
                        state = NUMBER;
                    }
                    else
                    {
                        emit(c);
                        state = SYMBOL;
                    }
                    //
                    break;
                //
                case NUMBER:
                    if (isDigit(c))
                    {
                        emit(c);
                    }
                    else if (isSlash(c))
                    {
                        emit(c);
                        state = RATIO;
                    }
                    else if (isDot(c))
                    {
                        emit(c);
                        state = FRACTION;
                    }
                    else if (isExponentSign(c))
                    {
                        emit(c);
                        state = EXPONENT;
                    }
                    else
                    {
                        return;
                    }
                    //
                    break;
                //
                case RATIO:
                    if (isDigit(c))
                    {
                        emit(c);
                        state = NUMBER_END;
                    }
                    else
                    {
                        state = ERROR;
                        //
                        return;
                    }
                    //
                    break;
                //
                case FRACTION:
                    if (isDigit(c))
                    {
                        emit(c);
                    }
                    else if (isExponentSign(c))
                    {
                        emit(c);
                        state = EXPONENT;
                    }
                    else
                    {
                        state = NUMBER_END;
                        //
                        return;
                    }
                    //
                    break;
                //
                case EXPONENT:
                    if (isPlusOrMinusSign(c) || isDigit(c))
                    {
                        emit(c);
                        state = NUMBER_END;
                    }
                    else
                    {
                        state = ERROR;
                        //
                        return;
                    }
                    //
                    break;
                //
                case NUMBER_END:
                    if (isDigit(c))
                    {
                        emit(c);
                    }
                    else
                    {
                        return;
                    }
                    //
                    break;
                //
                case SHARPSIGN:
                    if (isDot(c))
                    {
                        skip();
                        state = READ_TIME_EVALUATION;
                        //
                        return;
                    }
                    else if (isVerticalBar(c))
                    {
                        skip();
                        state = COMMENT;
                    }
                    else if (isDigit(c))
                    {
                        emit(c);
                        state = SOURCE_OR_DESTINATION;
                    }
                    else
                    {
                        state = ERROR;
                        //
                        return;
                    }
                    //
                    break;
                //
                case SOURCE_OR_DESTINATION:
                    if (isDigit(c))
                    {
                        emit(c);
                    }
                    else if (isSharpSign(c))
                    {
                        skip();
                        state = SOURCE;
                        //
                        return;
                    }
                    else if (isEqualsSign(c))
                    {
                        skip();
                        state = DESTINATION;
                        //
                        return;
                    }
                    else
                    {
                        state = ERROR;
                        //
                        return;
                    }
                    //
                    break;
                //
                case COMMENT:
                    if (isVerticalBar(c))
                    {
                        skip();
                        state = COMMENT_END;
                    }
                    else
                    {
                        skip();
                    }
                    //
                    break;
                //
                case COMMENT_END:
                    if (isVerticalBar(c))
                    {
                        skip();
                    }
                    else if (isSharpSign(c))
                    {
                        skip();
                        state = START;
                    }
                    else
                    {
                        skip();
                        state = COMMENT;
                    }
            }
        }
        //
        if ((state == EXPONENT)
                || (state == START)
                || (state == RATIO)
                || (state == SHARPSIGN)
                || (state == COMMENT)
                || (state == COMMENT_END)
                || (state == SOURCE_OR_DESTINATION)) state = ERROR;
    }
    /**
     * Copies a Char to the Token
     *
     * @param c Char
     */
    private void emit(char c)
    {
        initToken();
        token.append(c);
    }
    /**
     * Initializes the Token
     */
    private void initToken()
    {
        position++;
        //
        if (token == null) token = new StringBuilder();
    }
    /**
     * Skips a Char
     */
    private void skip()
    {
        position++;
    }
    /**
     * Returns the Token
     *
     * @return Token
     */
    public String getToken()
    {
        return token.toString();
    }
    /**
     * Returns the State
     *
     * @return State
     */
    public int getState()
    {
        return state;
    }
    /**
     * Resets the State to START
     */
    public void resetState()
    {
        state = START;
    }
    /**
     * Returns the current Position
     *
     * @return Position
     */
    public int getPosition()
    {
        return position;
    }
}