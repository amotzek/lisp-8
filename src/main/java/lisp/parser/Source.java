package lisp.parser;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Constant;
/*
 * Created by  andreasm 22.12.12 13:39
 */
final class Source extends Constant
{
    private final Long id;
    /**
     * Constructor for Source
     *
     * @param token Token
     */
    public Source(String token)
    {
        super();
        //
        id = new Long(token);
    }
    /**
     * Returns the Id for this Reference Source
     *
     * @return Id
     */
    public Long getId()
    {
        return id;
    }
    /**
     * @see lisp.Sexpression#getType()
     */
    public String getType()
    {
        return "source";
    }
}