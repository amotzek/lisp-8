package lisp;
/*
 * Copyright (C) 2011 - 2014, 2016, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.continuation.FailureContinuation;
import lisp.continuation.InvokeMethod;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
import java.util.HashMap;
import java.util.LinkedList;
import static java.util.Map.Entry;
/*
 * Created by andreasm on 13.10.11
 */
public final class GenericFunction extends MutableConstant implements Function
{
    private static final Symbol T = Symbol.createSymbol("t");
    private static final Environment ENVIRONMENT_T = new Environment(null);
    //
    static
    {
        ENVIRONMENT_T.add(true, T, T);
    }
    //
    private final Symbol functionname;
    private final HashMap<Integer, LinkedList<GuardedMethod>> methodsbyarity;
    private int minparametercount;
    private int maxparametercount;
    /**
     * Constructor for GenericFunction
     *
     * @param functionname Name of Generic Function
     */
    public GenericFunction(Symbol functionname)
    {
        super();
        //
        this.functionname = functionname;
        //
        methodsbyarity = new HashMap<>();
        minparametercount = Integer.MAX_VALUE;
        maxparametercount = -1;
    }
    /**
     * Checks if two Methods have the same signature
     *
     * @param method1 First Method
     * @param method2 Second Method
     * @return true if the Methods has the same signature
     */
    private static boolean matches(GuardedMethod method1, GuardedMethod method2)
    {
        List specializers1 = method1.getSpezializers();
        List specializers2 = method2.getSpezializers();
        //
        if (specializers1 == null)
        {
            if (specializers2 != null) return false;
        }
        else if (!specializers1.equals(specializers2))
        {
            return false;
        }
        //
        Sexpression guard1 = method1.getGuard();
        Sexpression guard2 = method2.getGuard();
        //
        if (guard1 == null) return guard2 == null;
        //
        return guard1.equals(guard2);
    }
    /**
     * Returns the Name of this Generic Function
     *
     * @return Function Name
     */
    public Symbol getFunctionName()
    {
        return functionname;
    }
    /**
     * Adds a Method
     *
     * @param method Method to add
     */
    public void addMethod(GuardedMethod method)
    {
        lockExclusively();
        //
        try
        {
            Integer arity = method.getParameterCount();
            //
            if (arity > maxparametercount) maxparametercount = arity;
            if (arity < minparametercount) minparametercount = arity;
            //
            LinkedList<GuardedMethod> methods = methodsbyarity.get(arity);
            //
            if (methods == null)
            {
                methods = new LinkedList<>();
                methodsbyarity.put(arity, methods);
            }
            else
            {
                methods.removeIf(previousmethod -> matches(previousmethod, method));
            }
            //
            methods.add(method);
        }
        finally
        {
            unlockExclusively();
        }
    }
    /**
     * Adds a Lambda as Method
     *
     * @param lambda Lambda
     * @throws CannotEvalException if the Lambda cannot be converted to a Method
     */
    public void addLambda(Lambda lambda) throws CannotEvalException
    {
        List parameters = lambda.getParameters();
        int parametercount = lambda.getParameterCount();
        SimpleClass[] constraints = new SimpleClass[parametercount];
        Sexpression body = lambda.getCompiledBody();
        Sexpression compiledbody = lambda.getCompiledBody();
        Environment definitionenvironment = lambda.getEnvironment();
        GuardedMethod method = new GuardedMethod(parameters, constraints, T, body, compiledbody, definitionenvironment);
        addMethod(method);
    }
    /**
     * Adds a Combinator as Method
     *
     * @param combinator Combinator
     * @throws CannotEvalException if the Combinator cannot be converted to a Method
     */
    public void addCombinator(Combinator combinator) throws CannotEvalException
    {
        if (combinator.hasQuotedParameters()) throw new CannotEvalException("cannot convert special form " + combinator + " into a method");
        //
        int parametercount = combinator.getParameterCount();
        List parameters = null;
        //
        for (int i = 0; i < parametercount; i++)
        {
            Symbol parameter = Symbol.createSymbol("p" + i);
            parameters = new List(parameter, parameters);
        }
        //
        SimpleClass[] constraints = new SimpleClass[parametercount];
        List body = new List(combinator, parameters);
        GuardedMethod method = new GuardedMethod(parameters, constraints, T, body, body, ENVIRONMENT_T);
        addMethod(method);
    }
    /**
     * Adds the Methods of another Generic Function into this Generic Function
     *
     * @param function Generic Function, thats methods get added
     */
    public void addGenericFunction(GenericFunction function)
    {
        List methods = function.getMethods();
        //
        while (methods != null)
        {
            GuardedMethod method = (GuardedMethod) methods.first();
            addMethod(method);
            methods = methods.rest();
        }
    }
    /**
     * Removes a Method
     *
     * @param method Method to remove
     */
    public void removeMethod(GuardedMethod method)
    {
        lockExclusively();
        //
        try
        {
            Integer arity = method.getParameterCount();
            LinkedList<GuardedMethod> methods = methodsbyarity.get(arity);
            //
            if (methods == null) return;
            //
            methods.remove(method);
            //
            if (methods.isEmpty())
            {
                methodsbyarity.remove(arity);
                //
                if (arity == minparametercount || arity == maxparametercount)
                {
                    minparametercount = Integer.MAX_VALUE;
                    maxparametercount = -1;
                    //
                    for (int otherarity : methodsbyarity.keySet())
                    {
                        if (otherarity > maxparametercount) maxparametercount = otherarity;
                        if (otherarity < minparametercount) minparametercount = otherarity;
                    }
                }
            }
        }
        finally
        {
            unlockExclusively();
        }
    }
    /**
     * Removes all stale methods
     */
    private void removeStaleMethods()
    {
        lockExclusively();
        //
        try
        {
            for (LinkedList<GuardedMethod> methods : methodsbyarity.values())
            {
                methods.removeIf(GuardedMethod::isStale);
            }
        }
        finally
        {
            unlockExclusively();
        }
    }
    /**
     * Returns a List of all Methods
     *
     * @return List of Methods
     */
    public List getMethods()
    {
        List list = null;
        boolean hasstalemethod = false;
        lockShared();
        //
        try
        {
            for (LinkedList<GuardedMethod> methods : methodsbyarity.values())
            {
                for (GuardedMethod method : methods)
                {
                    if (method.isStale())
                    {
                        hasstalemethod = true;
                        //
                        continue;
                    }
                    //
                    list = new List(method, list);
                }
            }
        }
        finally
        {
            unlockShared();
        }
        //
        if (hasstalemethod) removeStaleMethods();
        //
        return list;
    }
    /**
     * Checks if the Generic Function contains a Method that
     * accepts a first Argument of the given Class
     *
     * @param argumentclass Class
     * @return true, if first Arguments of the Class are accepted
     */
    public boolean acceptsFirstArgumentOf(SimpleClass argumentclass)
    {
        lockShared();
        //
        try
        {
            for (Entry<Integer, LinkedList<GuardedMethod>> entry : methodsbyarity.entrySet())
            {
                if (entry.getKey() == 0) continue;
                //
                for (GuardedMethod method : entry.getValue())
                {
                    if (method.acceptsFirstArgumentOf(argumentclass)) return true;
                }
            }
        }
        finally
        {
            unlockShared();
        }
        //
        return false;
    }
    /**
     * @see Function#acceptsArgumentCount(int)
     */
    public boolean acceptsArgumentCount(int argumentcount)
    {
        lockShared();
        //
        try
        {
            if (argumentcount > maxparametercount) return false;
            //
            return argumentcount >= minparametercount;
        }
        finally
        {
            unlockShared();
        }
    }
    /**
     * @see Function#isMacro
     */
    public boolean isMacro()
    {
        return false;
    }
    /**
     * @see Function#mustBeQuoted(int)
     */
    public boolean mustBeQuoted(int position)
    {
        return false;
    }
    /**
     * @see Function#apply(lisp.concurrent.RunnableQueue, lisp.environment.Environment, Sexpression[], lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        LinkedList<GuardedMethod> matchingmethods = new LinkedList<>();
        boolean hasstalemethod = false;
        lockShared();
        //
        try
        {
            LinkedList<GuardedMethod> methods = methodsbyarity.get(arguments.length);
            //
            if (methods != null)
            {
                for (GuardedMethod method : methods)
                {
                    if (method.matches(arguments) && method.getGuard() != null)
                    {
                        matchingmethods.add(method);
                        //
                        continue;
                    }
                    //
                    if (method.isStale()) hasstalemethod = true;
                }
            }
        }
        finally
        {
            unlockShared();
        }
        //
        if (hasstalemethod) removeStaleMethods();
        //
        MethodComparator comparator = new MethodComparator(arguments);
        matchingmethods.sort(comparator);
        InvokeMethod invokemethod = new InvokeMethod(runnablequeue, arguments, functionname, matchingmethods, succeed, fail);
        invokemethod.succeed(null);
    }
    /**
     * @see Sexpression#getType()
     */
    public String getType()
    {
        return "generic-function";
    }
    /**
     * @see Object#toString()
     */
    @Override
    public String toString()
    {
        return "#.(throw (quote error) \"generic-functions cannot be read or written\")";
    }
}