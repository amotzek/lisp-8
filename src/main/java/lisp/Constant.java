package lisp;
/*
 * Copyright (C) 2001, 2006, 2007, 2010, 2011, 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.evaluator.ConstantEvaluator;
import lisp.evaluator.Evaluator;
import lisp.concurrent.RunnableQueue;
/*
 * @author andreasm
 */
public abstract class Constant implements Sexpression
{
    /**
     * Constructor for Constant
     */
    public Constant()
    {
        super();
    }
    /**
     * @see Sexpression#isConstant()
     */
    public final boolean isConstant()
    {
        return true;
    }
    /**
     * @see Sexpression#enQueueEvaluator(lisp.concurrent.RunnableQueue, lisp.environment.Environment, lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public final void enQueueEvaluator(RunnableQueue runnablequeue, Environment environment, SuccessContinuation succeed, FailureContinuation fail)
    {
        Evaluator evaluator = new ConstantEvaluator(environment, this, succeed, fail);
        runnablequeue.add(evaluator);
    }
    /**
     * Checks if a S-Expression has a constant value
     *
     * @param sexpression S-Expression
     * @return true, if this S-Expression has a constant value
     */
    public static boolean isConstant(Sexpression sexpression)
    {
        return sexpression == null || sexpression.isConstant();
    }
}