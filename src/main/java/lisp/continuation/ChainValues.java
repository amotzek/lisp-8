package lisp.continuation;
/*
 * Copyright (C) 2011, 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import static lisp.Constant.isConstant;
import lisp.List;
import lisp.Sexpression;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
//
public final class ChainValues implements SuccessContinuation
{
    private final RunnableQueue runnablequeue;
    private final Environment environment;
    private List requests;
    private List results;
    private final SuccessContinuation succeed;
    private final FailureContinuation fail;
    /**
     * Constructor for ChainValues
     *
     * @param runnablequeue RunnableQueue
     * @param environment Environment
     * @param requests List of Sexpressions to evaluate sequentially
     * @param results List of Values in reverse order
     * @param succeed Continuation for Success
     * @param fail Continuation for Failure
     */
    public ChainValues(RunnableQueue runnablequeue, Environment environment, List requests, List results, SuccessContinuation succeed, FailureContinuation fail)
    {
        super();
        //
        this.runnablequeue = runnablequeue;
        this.environment = environment;
        this.requests = requests;
        this.results = results;
        this.succeed = succeed;
        this.fail = fail;
    }
    /**
     * @see lisp.continuation.SuccessContinuation#succeed(lisp.Sexpression)
     */
    public synchronized void succeed(Sexpression value)
    {
        results = new List(value, results);
        //
        while (requests != null)
        {
            Sexpression request = requests.first();
            requests = requests.rest();
            //
            if (isConstant(request))
            {
                results = new List(request, results);
                //
                continue;
            }
            //
            request.enQueueEvaluator(runnablequeue, environment, this, fail);
            //
            return;
        }
        /*
         * Reverse to correct order and remove additional null
         */
        results = List.reverse(results);
        results = results.rest();
        succeed.succeed(results);
    }
    /**
     * @see SuccessContinuation#getNext()
     */
    public SuccessContinuation getNext()
    {
        return succeed;
    }
}