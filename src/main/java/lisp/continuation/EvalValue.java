package lisp.continuation;
/*
 * Copyright (C) 2011 - 2013, 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import static lisp.Constant.isConstant;
import lisp.Sexpression;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
//
public final class EvalValue implements SuccessContinuation
{
    private final RunnableQueue runnablequeue;
    private final Environment environment;
    private final SuccessContinuation succeed;
    private final FailureContinuation fail;
    /**
     * Constructor for EvalValue
     *
     * @param runnablequeue RunnableQueue
     * @param environment Environment
     * @param succeed Continuation for Success
     * @param fail Continuation for Failure
     */
    public EvalValue(RunnableQueue runnablequeue, Environment environment, SuccessContinuation succeed, FailureContinuation fail)
    {
        super();
        //
        this.runnablequeue = runnablequeue;
        this.environment = environment;
        this.succeed = succeed;
        this.fail = fail;
    }
    /**
     * @see SuccessContinuation#succeed(lisp.Sexpression)
     */
    public void succeed(Sexpression value)
    {
        if (isConstant(value))
        {
            succeed.succeed(value);
            //
            return;
        }
        //
        value.enQueueEvaluator(runnablequeue, environment, succeed, fail);
    }
    /**
     * @see SuccessContinuation#getNext()
     */
    public SuccessContinuation getNext()
    {
        return succeed;
    }
}