package lisp.continuation;
/*
 * Copyright (C) 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.Symbol;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
import java.util.concurrent.atomic.AtomicInteger;
//
public final class BindValue implements SuccessContinuation
{
    private final RunnableQueue runnablequeue;
    private final Environment environment;
    private final Symbol name;
    private final AtomicInteger awaited;
    private final Sexpression sexpression;
    private final SuccessContinuation succeed;
    private final FailureContinuation fail;
    /**
     * Constructor for BindValue
     *
     * @param runnablequeue RunnableQueue
     * @param environment Environment
     * @param name Name
     * @param awaited Number of awaited Values
     * @param sexpression Sexpression
     * @param succeed SuccessContinuation
     * @param fail FailureContinuation
     */
    public BindValue(RunnableQueue runnablequeue, Environment environment, Symbol name, AtomicInteger awaited, Sexpression sexpression, SuccessContinuation succeed, FailureContinuation fail)
    {
        super();
        //
        this.runnablequeue = runnablequeue;
        this.environment = environment;
        this.name = name;
        this.awaited = awaited;
        this.sexpression = sexpression;
        this.succeed = succeed;
        this.fail = fail;
    }
    /**
     * @see SuccessContinuation#succeed(lisp.Sexpression)
     */
    public void succeed(Sexpression value)
    {
        environment.add(false, name, value);
        //
        if (awaited.decrementAndGet() == 0) sexpression.enQueueEvaluator(runnablequeue, environment, succeed, fail);
    }
    /**
     * @see SuccessContinuation#getNext()
     */
    public SuccessContinuation getNext()
    {
        return succeed;
    }
}