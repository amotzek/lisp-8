package lisp.continuation;
/*
 * Copyright (C) 2011, 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Function;
import lisp.Sexpression;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
import java.util.concurrent.atomic.AtomicInteger;
//
public final class ReceiveArgument implements SuccessContinuation, Runnable
{
    private final RunnableQueue runnablequeue;
    private final Environment environment;
    private final Sexpression[] arguments;
    private final int index;
    private final AtomicInteger awaited;
    private final Function function;
    private final SuccessContinuation succeed;
    private final FailureContinuation fail;
    /**
     * Constructor for ReceiveArgument
     *
     * @param runnablequeue RunnableQueue
     * @param environment Environment
     * @param arguments Arguments
     * @param index Index
     * @param awaited Number of awaited Arguments
     * @param function Function
     * @param succeed SuccessContinuation
     * @param fail FailureContinuation
     */
    public ReceiveArgument(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, int index, AtomicInteger awaited, Function function, SuccessContinuation succeed, FailureContinuation fail)
    {
        super();
        //
        this.runnablequeue = runnablequeue;
        this.environment = environment;
        this.arguments = arguments;
        this.index = index;
        this.awaited = awaited;
        this.function = function;
        this.succeed = succeed;
        this.fail = fail;
    }
    /**
     * @see SuccessContinuation#succeed(lisp.Sexpression)
     */
    public void succeed(Sexpression value)
    {
        /*
         * No synchronized is needed here
         * because the decrement of the
         * AtomicInteger establishes a
         * happens-before relationship.
         */
        arguments[index] = value;
        //
        if (awaited.decrementAndGet() > 0) return;
        //
        runnablequeue.add(this);
    }
    /**
     * @see SuccessContinuation#getNext()
     */
    public SuccessContinuation getNext()
    {
        return succeed;
    }
    /**
     * Returns the Function that will be applied
     *
     * @return Function
     */
    public Sexpression getFunction()
    {
        return function;
    }
    //
    public void run()
    {
        function.apply(runnablequeue, environment, arguments, succeed, fail);
    }
}