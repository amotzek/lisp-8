package lisp.continuation;
/*
 * Copyright (C) 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.Symbol;
//
final class Result
{
    private final Sexpression value;
    private final Symbol name;
    /**
     * Constructor for Result
     *
     * @param name Thrown Symbol
     * @param value Thrown Value
     */
    public Result(Symbol name, Sexpression value)
    {
        super();
        //
        this.value = value;
        this.name = name;
    }
    /**
     * Constructor for Result
     *
     * @param value Value
     */
    public Result(Sexpression value)
    {
        this(null, value);
    }
    /**
     * Returns the Value
     *
     * @return Value
     * @throws CannotEvalException if the Evaluation failed
     */
    public Sexpression getValue() throws CannotEvalException
    {
        if (name != null) throw new CannotEvalException(name, value);
        //
        return value;
    }
}