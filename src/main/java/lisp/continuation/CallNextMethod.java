package lisp.continuation;
/*
 * Copyright (C) 2012 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Combinator;
import lisp.GuardedMethod;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
import java.util.LinkedList;
/**
 * Created by  andreasm 06.12.12 19:39
 */
final class CallNextMethod extends Combinator
{
    private final Sexpression[] innerarguments;
    private final Symbol functionname;
    private final LinkedList<GuardedMethod> matchingmethods;
    /**
     * Constructor for CallNextMethod
     *
     * @param innerarguments Method Arguments
     * @param functionname Name of Generic Function
     * @param matchingmethods Remaining Methods
     */
    public CallNextMethod(Sexpression[] innerarguments, Symbol functionname, LinkedList<GuardedMethod> matchingmethods)
    {
        super(0, 0);
        //
        this.innerarguments = innerarguments;
        this.functionname = functionname;
        this.matchingmethods = matchingmethods;
    }
    /**
     * @see lisp.Function#apply(lisp.concurrent.RunnableQueue, lisp.environment.Environment, Sexpression[], lisp.continuation.SuccessContinuation, lisp.continuation.FailureContinuation)
     */
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        InvokeMethod invokemethod = new InvokeMethod(runnablequeue, innerarguments, functionname, matchingmethods, succeed, fail);
        invokemethod.succeed(null);
    }
}