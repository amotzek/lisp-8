package lisp.continuation;
/*
 * Copyright (C) 2011 - 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import static lisp.Constant.isConstant;
import lisp.Function;
import lisp.List;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
import java.util.concurrent.atomic.AtomicInteger;
//
public final class ApplyValue implements SuccessContinuation
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars WRONGNUMBEROFARGUMENTS = new Chars("wrong number of arguments");
    //
    private final RunnableQueue runnablequeue;
    private final Environment environment;
    private final List arguments;
    private final SuccessContinuation succeed;
    private final FailureContinuation fail;
    /**
     * Constructor for ApplyValue
     *
     * @param runnablequeue RunnableQueue
     * @param environment Environment
     * @param arguments List of Arguments
     * @param succeed Continuation for Success
     * @param fail Continuation for Failure
     */
    public ApplyValue(RunnableQueue runnablequeue, Environment environment, List arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        super();
        //
        this.runnablequeue = runnablequeue;
        this.environment = environment;
        this.arguments = arguments;
        this.succeed = succeed;
        this.fail = fail;
    }
    /**
     * @see SuccessContinuation#succeed(lisp.Sexpression)
     */
    public void succeed(Sexpression value)
    {
        if (!(value instanceof Function))
        {
            failNotAFunction(value);
            //
            return;
        }
        //
        final Function function = (Function) value;
        List arguments = this.arguments;
        //
        if (function.isMacro())
        {
            function.apply(runnablequeue, environment, new Sexpression[]{ arguments }, succeed, fail);
            //
            return;
        }
        //
        final int argumentcount = List.length(arguments);
        //
        if (!function.acceptsArgumentCount(argumentcount))
        {
            failNumberOfArguments(function);
            //
            return;
        }
        //
        final AtomicInteger awaited = new AtomicInteger(1);
        final Sexpression[] evaluatedarguments = new Sexpression[argumentcount];
        //
        for (int position = 0; position < argumentcount; position++)
        {
            final Sexpression argument = arguments.first();
            arguments = arguments.rest();
            //
            if (isConstant(argument) || function.mustBeQuoted(position))
            {
                evaluatedarguments[position] = argument;
                //
                continue;
            }
            //
            awaited.incrementAndGet();
            final ReceiveArgument continuation = new ReceiveArgument(runnablequeue, environment, evaluatedarguments, position, awaited, function, succeed, fail);
            argument.enQueueEvaluator(runnablequeue, environment, continuation, fail);
        }
        //
        if (awaited.decrementAndGet() > 0) return;
        //
        function.apply(runnablequeue, environment, evaluatedarguments, succeed, fail);
    }
    /**
     * @see SuccessContinuation#getNext()
     */
    public SuccessContinuation getNext()
    {
        return succeed;
    }
    //
    private void failNotAFunction(Sexpression value)
    {
        TraceBuilder builder = new TraceBuilder(ERROR, new Chars(value + ""), succeed);
        builder.append(" is not a function");
        builder.build();
        builder.invoke(fail);
    }
    //
    private void failNumberOfArguments(Function function)
    {
        TraceBuilder builder = new TraceBuilder(ERROR, WRONGNUMBEROFARGUMENTS, succeed);
        builder.append(" for function ");
        builder.append(function);
        builder.append(" and arguments ");
        builder.append(arguments);
        builder.build();
        builder.invoke(fail);
    }
}