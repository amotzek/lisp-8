package lisp.continuation;
/*
 * Copyright (C) 2011, 2012, 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import static lisp.Constant.isConstant;
import lisp.GuardedMethod;
import lisp.RubyStyleObject;
import lisp.Sexpression;
import lisp.SimpleClass;
import lisp.Symbol;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
import java.util.LinkedList;
/**
 * Created andreasm
 * Date: 15.10.11
 * Time: 11:35
 */
public final class InvokeMethod implements SuccessContinuation
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Symbol CALLNEXTMETHOD = Symbol.createSymbol("call-next-method");
    private static final Chars NOMATCHINGMETHOD = new Chars("no matching method");
    //
    private final RunnableQueue runnablequeue;
    private final Sexpression[] arguments;
    private final Symbol functionname;
    private final LinkedList<GuardedMethod> matchingmethods;
    private final SuccessContinuation succeed;
    private final FailureContinuation fail;
    private Environment environment;
    private Sexpression body;
    /**
     * Constructor for InvokeMethod
     *
     * @param runnablequeue RunnableQueue
     * @param arguments Arguments for Invocation
     * @param functionname Name of Generic Function
     * @param matchingmethods Possible Methods
     * @param succeed SuccessContinuation
     * @param fail FailureContinuation
     */
    public InvokeMethod(RunnableQueue runnablequeue, Sexpression[] arguments, Symbol functionname, LinkedList<GuardedMethod> matchingmethods, SuccessContinuation succeed, FailureContinuation fail)
    {
        super();
        //
        this.runnablequeue = runnablequeue;
        this.arguments = arguments;
        this.functionname = functionname;
        this.matchingmethods = matchingmethods;
        this.succeed = succeed;
        this.fail = fail;
    }
    /**
     * @see SuccessContinuation#succeed(lisp.Sexpression)
     */
    public synchronized void succeed(Sexpression value)
    {
        if (value == null)
        {
            if (matchingmethods.isEmpty())
            {
                failNoMatchingMethod();
                //
                return;
            }
            /*
             * Check next Guard
             */
            GuardedMethod method = matchingmethods.removeFirst();
            Sexpression guard = method.getGuard();
            /*
             * Memorize Environment and Body
             * for Evaluation of Body
             * if value of Guard is not nil
             */
            environment = method.bind(arguments);
            body = method.getCompiledBody();
            //
            if (!guard.isConstant())
            {
                /*
                 * Evaluate Guard
                 */
                guard.enQueueEvaluator(runnablequeue, environment, this, fail);
                //
                return;
            }
            /*
             * Guard is constant and not nil
             */
        }
        /*
         * Evaluated Guard is not nil
         */
        if (isConstant(body))
        {
            succeed.succeed(body);
            //
            return;
        }
        /*
         * Define call-next-method
         */
        CallNextMethod callnextmethod = new CallNextMethod(arguments, functionname, matchingmethods);
        environment.add(false, CALLNEXTMETHOD, callnextmethod);
        /*
         * Evaluate Method Body
         */
        body.enQueueEvaluator(runnablequeue, environment, succeed, fail);
    }
    /**
     * @see SuccessContinuation#getNext()
     */
    public SuccessContinuation getNext()
    {
        return succeed;
    }
    /**
     * Fails with no matching Method
     */
    private void failNoMatchingMethod()
    {
        TraceBuilder builder = new TraceBuilder(ERROR, NOMATCHINGMETHOD, succeed);
        builder.append(" in ");
        builder.append(functionname);
        builder.append(" for argument classes / types");
        //
        for (Sexpression argument : arguments)
        {
            builder.append(" ");
            //
            if (argument instanceof RubyStyleObject)
            {
                RubyStyleObject object = (RubyStyleObject) argument;
                SimpleClass simpleclass = object.classOf();
                //
                if (simpleclass == null)
                {
                    builder.append("<anonymous>");
                }
                else
                {
                    builder.append(simpleclass.getClassName());
                }
            }
            else if (argument != null)
            {
                builder.append(argument.getType());
            }
            else
            {
                builder.append("nil");
            }
        }
        //
        builder.build();
        builder.invoke(fail);
    }
}