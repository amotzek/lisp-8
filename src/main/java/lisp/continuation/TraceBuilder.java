package lisp.continuation;
/*
 * Copyright (C) 2015, 2017 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Combinator;
import lisp.Function;
import lisp.GenericFunction;
import lisp.Lambda;
import lisp.List;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.compiler.GeneratedTypeCheckCombinator;
import lisp.environment.Environment;
import lisp.environment.NoSuchValueException;
/**
 * Created by andreasm on 20.05.15
 */
public final class TraceBuilder
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    //
    private final Symbol name;
    private final Sexpression value;
    private final SuccessContinuation start;
    private final StringBuilder builder;
    private Chars trace;
    /**
     * Creates a Trace Builder
     *
     * @param name Thrown Symbol
     * @param value Thrown S-Expression
     * @param start Continuation to trace
     */
    public TraceBuilder(Symbol name, Sexpression value, SuccessContinuation start)
    {
        super();
        //
        this.name = name;
        this.value = value;
        this.start = start;
        //
        if (name == ERROR && value instanceof Chars)
        {
            Chars chars = (Chars) value;
            builder = new StringBuilder(chars.getString());
        }
        else
        {
            builder = null;
        }
    }
    /**
     * Creates a Trace Builder
     *
     * @param e Exception with thrown Symbol and thrown S-Expression
     * @param start Continuation to trace
     */
    public TraceBuilder(CannotEvalException e, SuccessContinuation start)
    {
        this(e.getSymbol(), e.getSexpression(), start);
    }
    /**
     * Tries to find the Name for the given Function
     *
     * @param function Function
     * @return Name of the Function
     */
    private static String getName(Sexpression function)
    {
        if (function == null) return null;
        //
        if (function instanceof GenericFunction)
        {
            GenericFunction genericfunction = (GenericFunction) function;
            Symbol symbol = genericfunction.getFunctionName();
            //
            return symbol.getName();
        }
        //
        if (function instanceof Lambda)
        {
            Lambda lambda = (Lambda) function;
            //
            try
            {
                Environment environment = lambda.getEnvironment();
                Symbol symbol = environment.inverseAt(lambda);
                //
                return symbol.getName();
            }
            catch (NoSuchValueException e)
            {
                return null;
            }
        }
        //
        if (function instanceof GeneratedTypeCheckCombinator) return null;
        //
        if (function instanceof Combinator) return function.toString();
        //
        return null;
    }
    /**
     * Append the given String
     *
     * @param string String to append
     */
    public void append(String string)
    {
        if (builder == null) return;
        //
        builder.append(string);
    }
    /**
     * Append the Name of the given Symbol
     *
     * @param symbol Symbol
     */
    public void append(Symbol symbol)
    {
        append(symbol.getName());
    }
    /**
     * Append the given List
     *
     * @param list List
     */
    public void append(List list)
    {
        if (list == null)
        {
            append("nil");
        }
        else
        {
            append(list.toString());
        }
    }
    /**
     * Appends the Name of the given Function
     *
     * @param function Function
     */
    public void append(Function function)
    {
        String name = getName(function);
        //
        if (name == null)
        {
            append("<unknown>");
        }
        else
        {
            append(name);
        }
    }
    /**
     * Appends a Trace to the thrown S-Expression, if it is a String
     */
    public void build()
    {
        if (builder == null) return;
        //
        int steps = 0;
        //
        for (SuccessContinuation continuation = start; continuation != null; continuation = continuation.getNext())
        {
            if (continuation instanceof ReceiveArgument)
            {
                if (steps > 12)
                {
                    builder.append(" ...");
                    //
                    break;
                }
                //
                ReceiveArgument receiveargument = (ReceiveArgument) continuation;
                Sexpression function = receiveargument.getFunction();
                String name = getName(function);
                //
                if (name == null) continue;
                //
                builder.append(" in ");
                builder.append(name);
                steps++;
            }
        }
        //
        trace = new Chars(builder.toString());
    }
    /**
     * Invokes the Failure Continuation
     *
     * @param fail Failure Continuation
     */
    public void invoke(FailureContinuation fail)
    {
        if (trace != null)
        {
            fail.fail(name, trace);
            //
            return;
        }
        //
        fail.fail(name, value);
    }
}