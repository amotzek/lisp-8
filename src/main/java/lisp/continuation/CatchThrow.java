package lisp.continuation;
/*
 * Copyright (C) 2011, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Function;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
import java.util.concurrent.atomic.AtomicBoolean;
//
public final class CatchThrow implements FailureContinuation
{
    private final RunnableQueue runnablequeue;
    private final Environment environment;
    private final Symbol name;
    private final Function function;
    private final AtomicBoolean firstfail;
    private final SuccessContinuation succeed;
    private final FailureContinuation fail;
    /**
     * Constructor for CatchThrow
     *
     * @param runnablequeue RunnableQueue
     * @param environment Environment
     * @param name Name of Throw that should be catched
     * @param function Function to apply on thrown name and value
     * @param succeed SuccessContinuation
     * @param fail FailureContinuation
     */
    public CatchThrow(RunnableQueue runnablequeue, Environment environment, Symbol name, Function function, SuccessContinuation succeed, FailureContinuation fail)
    {
        super();
        //
        this.runnablequeue = runnablequeue;
        this.environment = environment;
        this.name = name;
        this.function = function;
        this.succeed = succeed;
        this.fail = fail;
        //
        firstfail = new AtomicBoolean(false);
    }
    /**
     * @see FailureContinuation#fail(lisp.Symbol, lisp.Sexpression)
     */
    public void fail(Symbol name, Sexpression value)
    {
        if (!firstfail.compareAndSet(false, true)) return;
        //
        if (this.name == null || this.name == name)
        {
            function.apply(runnablequeue, environment, new Sexpression[]{name, value}, succeed, fail);
            //
            return;
        }
        //
        fail.fail(name, value);
    }
}