package lisp.continuation;
/*
 * Copyright (C) 2011, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.Symbol;
import java.util.concurrent.CountDownLatch;
//
public final class AwaitValue implements SuccessContinuation, FailureContinuation
{
    private final CountDownLatch latch;
    private volatile Result result;
    /**
     * Constructor for AwaitValue
     */
    public AwaitValue()
    {
        super();
        //
        latch = new CountDownLatch(1);
    }
    /**
     * @see SuccessContinuation#succeed(lisp.Sexpression)
     */
    public void succeed(Sexpression value)
    {
        synchronized (this)
        {
            if (result != null) return;
            //
            result = new Result(value);
        }
        //
        latch.countDown();
    }
    /**
     * @see SuccessContinuation#getNext()
     */
    public SuccessContinuation getNext()
    {
        return null;
    }
    /**
     * @see FailureContinuation#fail(lisp.Symbol, lisp.Sexpression)
     */
    public void fail(Symbol name, Sexpression value)
    {
        synchronized (this)
        {
            if (result != null) return;
            //
            result = new Result(name, value);
        }
        //
        latch.countDown();
    }
    /**
     * Returns true if still no Value arrived
     *
     * @return true if there is no Value
     */
    public boolean isWaiting()
    {
        return result == null;
    }
    /**
     * Waits for the Value of the Evaluation
     *
     * @return Value
     * @throws CannotEvalException if the Evaluation failed
     */
    public Sexpression getValue() throws CannotEvalException
    {
        try
        {
            latch.await();
            //
            return result.getValue();
        }
        catch (InterruptedException e)
        {
            throw new CannotEvalException("interrupted");
        }
    }
}