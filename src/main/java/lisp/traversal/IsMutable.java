package lisp.traversal;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.AssociativeContainer;
import lisp.Sexpression;
import java.util.HashSet;
/*
 * Created by andreas-motzek@t-online.de 02.05.13 18:25
 */
public final class IsMutable implements Visitor<Boolean>
{
    //
    private static final HashSet<String> immutabletypes = new HashSet<>();
    //
    static
    {
        immutabletypes.add("atom");
        immutabletypes.add("bytes");
        immutabletypes.add("channel");
        immutabletypes.add("class");
        immutabletypes.add("combinator");
        immutabletypes.add("future");
        immutabletypes.add("generic-function");
        immutabletypes.add("method");
        immutabletypes.add("lambda");
        immutabletypes.add("list");
        immutabletypes.add("lock");
        immutabletypes.add("mlambda");
        immutabletypes.add("integer");
        immutabletypes.add("ratio");
        immutabletypes.add("string");
        immutabletypes.add("trait");
    }
    public Boolean visit(Sexpression sexpression)
    {
        if (sexpression instanceof AssociativeContainer)
        {
            AssociativeContainer container = (AssociativeContainer) sexpression;
            /*
             * If the container is not frozen, it is mutable
             */
            if (!container.isFrozen()) return true;
        }
        else
        {
            String type = sexpression.getType();
            /*
             * If the type is not know as immutable, it is assumed mutable
             */
            if (!immutabletypes.contains(type)) return true;
        }
        //
        return null;
    }
}