package lisp.traversal;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.AssociativeContainer;
import lisp.HashTable;
import lisp.List;
import lisp.Sexpression;
/*
 * Created by andreas-motzek@t-online.de 02.05.13 18:30
 */
public final class CalculateHashCode implements Visitor<Object>
{
    private boolean useunordered;
    private int ordered;
    private int unordered;
    //
    public CalculateHashCode()
    {
        super();
    }
    //
    public Object visit(Sexpression sexpression)
    {
        if (sexpression instanceof List)
        {
            ordered += 1;
            unordered += 1;
        }
        else if (sexpression instanceof AssociativeContainer)
        {
            AssociativeContainer container = (AssociativeContainer) sexpression;
            int code = container.metaHashCode();
            ordered *= 29;
            ordered += code;
            unordered += code;
            /*
             * If a hash table with more than one element is involved
             * then the hash code must be independant from the element order.
             * Elements in a hash table have no stable order.
             */
            if (container instanceof HashTable && container.size() > 1) useunordered = true;
        }
        else
        {
            int code = sexpression.hashCode();
            ordered *= 31;
            ordered += code;
            unordered += code;
        }
        //
        return null;
    }
    //
    public int hashCode()
    {
        if (useunordered) return unordered;
        //
        return ordered;
    }
}