package lisp.junction;
/*
 * Copyright (C) 2012, 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * Created by  andreasm 18.11.12 11:46
 */
final class TimeoutList implements Runnable
{
    private static final int LOOP_LIMIT = 32;
    private static final int ACTION_LIMIT = 256;
    //
    private static final Logger logger = Logger.getLogger("TimoutList");
    //
    private final Thread thread;
    private final PriorityQueue<TimedFlow> flows;
    private int loops;
    //
    public TimeoutList()
    {
        super();
        //
        flows = new PriorityQueue<>(64);
        thread = new Thread(this);
        thread.setName("channel-timeouts");
        thread.setDaemon(true);
    }
    //
    public void start()
    {
        thread.start();
    }
    //
    public synchronized void add(TimedFlow flow)
    {
        flows.add(flow);
        Flow nextflow = flows.peek();
        //
        if (flow == nextflow) thread.interrupt();
    }
    //
    public void run()
    {
        while (true)
        {
            long now = System.currentTimeMillis();
            processTimeouts(now);
            cleanupIfNeeded();
        }
    }
    //
    private synchronized void processTimeouts(long now)
    {
        try
        {
            TimedFlow flow = flows.peek();
            //
            if (flow == null)
            {
                wait();
                //
                return;
            }
            //
            do
            {
                long validuntil = flow.getValidUntil();
                long duration = validuntil - now;
                //
                if (duration > 0L)
                {
                    wait(duration);
                    //
                    return;
                }
                //
                OrderedLockSet locks = new OrderedLockSet();
                locks.addLocksOf(flow);
                locks.lockAll();
                //
                if (!flow.isDone()) flow.abort();
                //
                locks.unlockAll();
                flows.poll();
                flow = flows.peek();
            }
            while (flow != null);
        }
        catch (InterruptedException ignored)
        {
        }
        catch (Exception e)
        {
            logger.log(Level.INFO, "unexpected exception", e);
        }
    }
    //
    private synchronized void cleanupIfNeeded()
    {
        if (++loops < LOOP_LIMIT) return;
        //
        loops = 0;
        //
        if (flows.size() < ACTION_LIMIT) return;
        //
        LinkedList<TimedFlow> nextflows = new LinkedList<>();
        //
        for (TimedFlow flow : flows)
        {
            if (flow.wasContinued()) continue;
            //
            nextflows.add(flow);
        }
        //
        flows.clear();
        flows.addAll(nextflows);
    }
}