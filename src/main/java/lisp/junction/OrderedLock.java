package lisp.junction;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
/*
 * Created by andreasm 07.11.13 19:47
 */
final class OrderedLock implements Comparable<OrderedLock>
{
    private static final AtomicLong counter = new AtomicLong();
    //
    private final Lock lock;
    private final long id;
    //
    public OrderedLock()
    {
        super();
        //
        lock = new ReentrantLock(true);
        id = counter.incrementAndGet();
    }
    //
    public void lock()
    {
        lock.lock();
    }
    //
    public void unlock()
    {
        lock.unlock();
    }
    //
    public int compareTo(OrderedLock that)
    {
        if (this == that) return 0;
        //
        if (that.id > id) return 1;
        //
        return -1;
    }
    //
    @Override
    public boolean equals(Object that)
    {
        return this == that;
    }
    //
    @Override
    public int hashCode()
    {
        return (int) (id ^ (id >>> 32));
    }
}