package lisp.junction;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.concurrent.RunnableQueue;
/*
 * Created by andreasm 06.11.13 20:02
 */
final class Await extends ContinuableFlow
{
    public Await(RunnableQueue runnablequeue, SuccessContinuation succeed, FailureContinuation fail)
    {
        super(runnablequeue, succeed, fail);
    }
    //
    @Override
    public boolean isDone()
    {
        return wasContinued();
    }
}