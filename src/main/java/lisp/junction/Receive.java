package lisp.junction;
/*
 * Copyright (C) 2012, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.concurrent.RunnableQueue;
/*
 * Created by  andreasm 17.11.12 11:45
 */
final class Receive extends TimedFlow
{
    private int remainingchannels;
    //
    public Receive(RunnableQueue runnablequeue, SuccessContinuation succeed, FailureContinuation fail, long timeout, int initialchannels)
    {
        super(runnablequeue, succeed, fail, timeout);
        //
        this.remainingchannels = initialchannels;
    }
    //
    @Override
    public boolean isDone()
    {
        return wasContinued();
    }
    //
    @Override
    public void junctionClosed()
    {
        remainingchannels--;
        //
        if (remainingchannels == 0) continueWith(null, null);
    }
    //
    @Override
    public void abort()
    {
        continueWith(null, null);
    }
}