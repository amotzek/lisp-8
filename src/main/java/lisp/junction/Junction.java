package lisp.junction;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Constant;
import java.util.LinkedList;
/*
 * Created by andreasm 06.11.13 19:23
 */
abstract class Junction extends Constant
{
    protected final OrderedLock lock;
    protected final LinkedList<Flow> flows;
    private volatile boolean closed;
    //
    public Junction()
    {
        super();
        //
        lock = new OrderedLock();
        flows = new LinkedList<>();
    }
    //
    protected final void add(Flow flow)
    {
        OrderedLockSet locks = new OrderedLockSet();
        locks.addLock(lock);
        locks.addLocksOf(flow);
        lock.lock();
        locks.addLocksOf(flows);
        lock.unlock();
        locks.lockAll();
        //
        if (closed)
        {
            flow.junctionClosed();
        }
        else
        {
            flow.addLock(lock);
            flows.add(flow);
            scheduleIfReady();
        }
        //
        locks.unlockAll();
    }
    //
    private void scheduleIfReady()
    {
        LinkedList<Flow> nextflows = new LinkedList<>();
        //
        while (!flows.isEmpty())
        {
            Flow flow1 = flows.removeFirst();
            //
            if (flow1.isDone()) continue;
            //
            for (Flow flow2 : flows)
            {
                if (flow2.isDone()) continue;
                //
                flow1.crossWith(flow2);
                //
                if (flow1.isDone()) break;
                //
                if (flow2.isDone()) continue;
                //
                flow2.crossWith(flow1);
                //
                if (flow1.isDone()) break;
            }
            //
            if (flow1.isDone()) continue;
            //
            nextflows.addLast(flow1);
        }
        //
        flows.addAll(nextflows);
    }
    //
    public final void close()
    {
        OrderedLockSet locks = new OrderedLockSet();
        locks.addLock(lock);
        lock.lock();
        locks.addLocksOf(flows);
        lock.unlock();
        locks.lockAll();
        //
        if (!closed)
        {
            while (!flows.isEmpty())
            {
                Flow flow = flows.removeFirst();
                flow.junctionClosed();
            }
            //
            closed = true;
        }
        //
        locks.unlockAll();
    }
    //
    public final boolean isClosed()
    {
        return closed;
    }
}