package lisp.junction;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.Symbol;
/*
 * Created by andreasm 06.11.13 20:09
 */
final class Awaited extends Flow
{
    private final Symbol name;
    private final Sexpression value;
    private boolean done;
    //
    public Awaited(Symbol name, Sexpression value)
    {
        super();
        //
        this.name = name;
        this.value = value;
    }
    //
    @Override
    public void crossWith(Flow flow)
    {
        if (flow instanceof Await)
        {
            Await await = (Await) flow;
            await.continueWith(name, value);
            //
            return;
        }
        //
        if (flow instanceof Awaited)
        {
            Awaited awaited = (Awaited) flow;
            awaited.done = true;
        }
    }
    //
    @Override
    public boolean isDone()
    {
        return done;
    }
}