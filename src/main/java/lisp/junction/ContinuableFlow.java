package lisp.junction;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.concurrent.RunnableQueue;
/*
 * Created by andreasm 06.11.13 20:13
 */
abstract class ContinuableFlow extends Flow
{
    private final RunnableQueue runnablequeue;
    private final SuccessContinuation succeed;
    private final FailureContinuation fail;
    private volatile boolean continued;
    //
    public ContinuableFlow(RunnableQueue runnablequeue, SuccessContinuation succeed, FailureContinuation fail)
    {
        super();
        //
        this.runnablequeue = runnablequeue;
        this.succeed = succeed;
        this.fail = fail;
    }
    //
    public final boolean wasContinued()
    {
        return continued;
    }
    //
    public final void continueWith(final Symbol name, final Sexpression value)
    {
        if (continued) return;
        //
        runnablequeue.add(() -> {
            if (name == null)
            {
                succeed.succeed(value);
                //
                return;
            }
            //
            fail.fail(name, value);
        });
        //
        continued = true;
    }
}