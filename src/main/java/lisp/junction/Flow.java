package lisp.junction;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.LinkedList;
/*
 * Created by andreasm 06.11.13 19:24
 */
abstract class Flow
{
    private final LinkedList<OrderedLock> locks;
    //
    public Flow()
    {
        super();
        //
        locks = new LinkedList<>();
    }
    //
    public final void addLock(OrderedLock lock)
    {
        locks.addLast(lock);
    }
    //
    public final LinkedList<OrderedLock> getLocks()
    {
        return locks;
    }
    //
    public void crossWith(Flow flow)
    {
    }
    //
    public void junctionClosed()
    {
    }
    //
    public abstract boolean isDone();
}