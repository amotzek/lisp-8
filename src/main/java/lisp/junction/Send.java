package lisp.junction;
/*
 * Copyright (C) 2012, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.concurrent.RunnableQueue;
/*
 * Created by  andreasm 17.11.12 11:45
 */
final class Send extends TimedFlow
{
    private final Sexpression message;
    private boolean done;
    //
    public Send(RunnableQueue runnablequeue, SuccessContinuation succeed, FailureContinuation fail, long timeout, Sexpression message)
    {
        super(runnablequeue, succeed, fail, timeout);
        //
        this.message = message;
    }
    //
    @Override
    public boolean isDone()
    {
        return done;
    }
    //
    @Override
    public void crossWith(Flow flow)
    {
        if (flow instanceof Receive)
        {
            Receive receive = (Receive) flow;
            receive.continueWith(null, message);
            continueWith(null, message);
            done = true;
        }
    }
    //
    @Override
    public void junctionClosed()
    {
        continueWith(null, null);
        done = true;
    }
    //
    @Override
    public void abort()
    {
        continueWith(null, null);
        done = true;
    }
    //
    public void unblock()
    {
       continueWith(null, message);
       // not done
    }
}