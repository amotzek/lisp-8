package lisp.junction;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.List;
import lisp.Sexpression;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.concurrent.RunnableQueue;
/*
 * Created by andreasm 06.11.13 22:09
 */
public abstract class ChannelJunction extends Junction
{
    private final int capacity;
    //
    public ChannelJunction(int capacity)
    {
        super();
        //
        this.capacity = capacity;
    }
    //
    public final void addSender(RunnableQueue runnablequeue, SuccessContinuation succeed, FailureContinuation fail, long timeout, Sexpression message)
    {
        Send send = new Send(runnablequeue, succeed, fail, timeout, message);
        add(send);
        lock.lock();
        send.enrollForTimeout();
        lock.unlock();
        //
        if (capacity <= 0) return;
        //
        int count = capacity;
        lock.lock();
        //
        for (Flow flow : flows)
        {
            if (flow instanceof Send)
            {
                send = (Send) flow;
                //
                if (send.wasContinued()) count--;
                //
                if (count <= 0) break;
            }
        }
        //
        if (count > 0)
        {
            for (Flow flow : flows)
            {
                if (flow instanceof Send)
                {
                    send = (Send) flow;
                    //
                    if (!send.wasContinued())
                    {
                        send.unblock();
                        count--;
                    }
                    //
                    if (count <= 0) break;
                }
            }
        }
        //
        lock.unlock();
    }
    //
    public static void addReceiverTo(RunnableQueue runnablequeue, SuccessContinuation succeed, FailureContinuation fail, long timeout, List channellist)
    {
        int initialchannels = List.length(channellist);
        Receive receive = new Receive(runnablequeue, succeed, fail, timeout, initialchannels);
        //
        while (channellist != null)
        {
            if (receive.isDone()) return;
            //
            ChannelJunction channel = (ChannelJunction) channellist.first();
            channel.add(receive);
            channellist = channellist.rest();
        }
        //
        OrderedLockSet locks = new OrderedLockSet();
        locks.addLocksOf(receive);
        locks.lockAll();
        receive.enrollForTimeout();
        locks.unlockAll();
    }
}