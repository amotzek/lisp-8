package lisp.junction;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/*
 * Created by andreasm 06.11.13 21:05
 */
final class Release extends Flow
{
    private final LockJunction lock;
    private boolean done;
    //
    public Release(LockJunction lock)
    {
        super();
        //
        this.lock = lock;
    }
    //
    @Override
    public void crossWith(Flow flow)
    {
        if (flow instanceof Acquire)
        {
            Acquire acquire = (Acquire) flow;
            acquire.continueWith(null, lock);
            done = true;
        }
    }
    //
    @Override
    public boolean isDone()
    {
        return done;
    }
}