package lisp.junction;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.concurrent.RunnableQueue;
/*
 * Created by andreasm 06.11.13 19:33
 */
abstract class TimedFlow extends ContinuableFlow implements Comparable<TimedFlow>
{
    private final long timeout;
    private volatile long validuntil;
    //
    public TimedFlow(RunnableQueue runnablequeue, SuccessContinuation succeed, FailureContinuation fail, long timeout)
    {
        super(runnablequeue, succeed, fail);
        //
        this.timeout = timeout;
    }
    //
    public final void enrollForTimeout()
    {
        if (timeout == Long.MAX_VALUE) return;
        //
        if (timeout <= 0L)
        {
            abort();
            //
            return;
        }
        //
        long now = System.currentTimeMillis();
        //
        if (wasContinued()) return;
        //
        validuntil = now + timeout;
        timeoutlist.add(this);
    }
    //
    public final long getValidUntil()
    {
        return validuntil;
    }
    //
    public final int compareTo(TimedFlow that)
    {
        if (this.validuntil < that.validuntil) return -1;
        if (this.validuntil > that.validuntil) return 1;
        //
        return 0;
    }
    //
    public abstract void abort();
    //
    private static final TimeoutList timeoutlist;
    //
    static
    {
        timeoutlist = new TimeoutList();
        timeoutlist.start();
    }
}