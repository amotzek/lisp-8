/*
 * Copyright (C) 2001, 2006, 2007, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp;
/*
 * @author Andreasm
 */
public final class Chars extends Constant implements Comparable<Chars>
{
    private final String string;
    /**
     * Constructor for Chars
     *
     * @param string Sting to contain
     */
    public Chars(String string)
    {
        super();
        //
        this.string = string;
    }
    /**
     * Method getString
     *
     * @return String that was contained
     */
    public String getString()
    {
        return string;
    }
    /**
     * @see Sexpression#getType()
     */
    public String getType()
    {
        return "string";
    }
    /**
     * @see Comparable#compareTo(Object)
     */
    public int compareTo(Chars chars)
    {
        return string.compareTo(chars.string);
    }
    /**
     * @see Object#hashCode()
     */
    public int hashCode()
    {
        return string.hashCode();
    }
    /**
     * @see Object#equals(Object)
     */
    public boolean equals(Object obj)
    {
        if (obj instanceof Chars)
        {
            Chars chars = (Chars) obj;
            //
            return string.equals(chars.string);
        }
        //
        return false;
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("\"");
        //
        for (int i = 0; i < string.length(); i++)
        {
            char c = string.charAt(i);
            //
            switch (c)
            {
                case '"': builder.append("\\\""); break;
                case '\\': builder.append("\\\\"); break;
                case '\n': builder.append("\\n"); break;
                case '\r': builder.append("\\r"); break;
                default: builder.append(c);
            }
        }
        //
        builder.append("\"");
        //
        return builder.toString();
    }
}