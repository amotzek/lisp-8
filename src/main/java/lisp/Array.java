/*
 * Copyright (C) 2013, 2014, 2016, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp;
/*
 * Created on 28.4.2013
 */
import lisp.formatter.Formattable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
/*
 * @author andreasm
 */
public final class Array extends AssociativeContainer<List> implements Formattable
{
    private final int[] dimensions;
    private final int[] products;
    private final Sexpression[] elements;
    private int size;
    /**
     * Constructor for Array
     *
     * @param dimensions Dimensions
     */
    public Array(int[] dimensions)
    {
        super();
        //
        this.dimensions = dimensions;
        //
        products = new int[dimensions.length];
        int product = 1;
        int index = 0;
        //
        while (index < dimensions.length)
        {
            products[index] = product;
            product *= dimensions[index];
            index++;
        }
        //
        elements = new Sexpression[product];
    }
    /**
     * Constructor for Array
     *
     * @param dimensions Dimensions
     * @throws CannotEvalException
     */
    public Array(List dimensions) throws CannotEvalException
    {
        this(toArray(dimensions));
    }
    /**
     * Returns the Dimensions
     *
     * @return Dimensions
     */
    public int[] getDimensions()
    {
        return dimensions;
    }
    /**
     * Checks, if the Subscripts are in bounds
     *
     * @param subscripts List of Subscripts
     * @throws CannotEvalException if the Subscripts are out of bounds
     */
    public void checkBounds(List subscripts) throws CannotEvalException
    {
        try
        {
            for (int dimension : dimensions)
            {
                if (subscripts == null) throw new CannotEvalException("not enough subscripts, array dimensions are " + Arrays.toString(dimensions));
                //
                Rational subscript = (Rational) subscripts.first();
                subscripts = subscripts.rest();
                int index = subscript.intValue();
                //
                if (index < 0) throw new CannotEvalException("subscript is below 0");
                //
                if (index >= dimension) throw new CannotEvalException("subscript is out of bounds");
            }
            //
            if (subscripts != null) throw new CannotEvalException("too many subscripts");
        }
        catch (CannotEvalException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new CannotEvalException("subscript is not an integer");
        }
    }
    /**
     * @see AssociativeContainer#size()
     */
    @Override
    public int size()
    {
        lockShared();
        //
        try
        {
            return size;
        }
        finally
        {
            unlockShared();
        }
    }
    /**
     * @see AssociativeContainer#hasComplexKeys()
     */
    @Override
    public boolean hasComplexKeys()
    {
        return false;
    }
    /**
     * @see AssociativeContainer#keyIterator()
     */
    @Override
    public Iterator<List> keyIterator()
    {
        HashMap<Integer, Rational> flyweights = new HashMap<>();
        ArrayList<List> keys = new ArrayList<>(size);
        lockShared();
        //
        try
        {
            for (int index = 0; index < elements.length; index++)
            {
                if (elements[index] == null) continue;
                //
                keys.add(getSubscripts(index, flyweights));
            }
        }
        finally
        {
            unlockShared();
        }
        //
        return keys.iterator();
    }
    /**
     * @see AssociativeContainer#get(Object)
     */
    @Override
    public Sexpression get(Object key)
    {
        try
        {
            List subscripts = (List) key;
            int index = getRowMajorIndex(subscripts);
            lockShared();
            //
            try
            {
                return elements[index];
            }
            finally
            {
                unlockShared();
            }
        }
        catch (Exception e)
        {
            // return null
        }
        //
        return null;
    }
    /**
     * @see AssociativeContainer#put(Sexpression, Sexpression)
     */
    @Override
    public void put(List subscripts, Sexpression value)
    {
        try
        {
            int index = getRowMajorIndex(subscripts);
            lockExclusively();
            //
            try
            {
                if (elements[index] == null)
                {
                    if (value != null) size++;
                }
                else if (value == null)
                {
                    size--;
                }
                //
                elements[index] = value;
            }
            finally
            {
                unlockExclusively();
            }
        }
        catch (CannotEvalException e)
        {
            // do not put
        }
    }
    /**
     * @see AssociativeContainer#duplicate()
     */
    @Override
    public Array duplicate()
    {
        Array duplicate = new Array(dimensions);
        lockShared();
        //
        try
        {
            System.arraycopy(elements, 0, duplicate.elements, 0, elements.length);
        }
        finally
        {
            unlockShared();
        }
        //
        return duplicate;
    }
    /**
     * @see AssociativeContainer#metaHashCode()
     */
    @Override
    public int metaHashCode()
    {
        return Arrays.hashCode(dimensions);
    }
    /*
     * @see lisp.Sexpression#getType()
     */
    public String getType()
    {
        return "array";
    }
    /**
     * Returns the Subscripts for a Row Major Index
     *
     * @param index Row Major Index
     * @param flyweights Map of Rationals for Integers
     * @return List of Subscripts
     */
    private List getSubscripts(int index, HashMap<Integer, Rational> flyweights)
    {
        List subscripts = null;
        //
        for (int dimension : dimensions)
        {
            Integer integer = index % dimension;
            index /= dimension;
            Rational subscript = flyweights.get(integer);
            //
            if (subscript == null)
            {
                subscript = new Rational(integer);
                flyweights.put(integer, subscript);
            }
            //
            subscripts = new List(subscript, subscripts);
        }
        //
        return List.reverse(subscripts);
    }
    /**
     * Returns the Row Major Index for the Subscripts
     *
     * @param subscripts List of Subscripts
     * @return Row Major Index
     * @throws CannotEvalException if a Subscript is not an int
     */
    private int getRowMajorIndex(List subscripts) throws CannotEvalException
    {
        int index = 0;
        //
        for (int product : products)
        {
            Rational subscript = (Rational) subscripts.first();
            index += (subscript.intValue() * product);
            subscripts = subscripts.rest();
        }
        //
        return index;
    }
    /**
     * Converts the List to an Int Array
     *
     * @param list List
     * @return Int Array
     * @throws CannotEvalException if the List is not wellformed
     */
    private static int[] toArray(List list) throws CannotEvalException
    {
        int length = List.length(list);
        int[] array = new int[length];
        int index = 0;
        //
        while (list != null)
        {
            Sexpression element = list.first();
            list = list.rest();
            //
            if (!(element instanceof Rational)) throw new CannotEvalException("not a rational in make-array");
            //
            Rational rational = (Rational) element;
            array[index] = rational.intValue();
            //
            if (array[index] <= 0) throw new CannotEvalException("dimension must be positive in make-array");
            //
            index++;
        }
        //
        return array;
    }
}