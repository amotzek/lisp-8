package lisp.compiler.aggregator;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.combinator.TypeCheckCombinator;
import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * Created by andreasm 24.12.13 10:26
 */
final class CombinatorFactory
{
    private static final Logger logger = Logger.getLogger("lisp.compiler.CombinatorFactory");
    //
    private final HashMap<String, TypeCheckCombinator> combinatorbyname;
    private final Lock readlock;
    private final Lock writelock;
    //
    public CombinatorFactory()
    {
        super();
        /*
         * List of predefined higher order Combinators
         */
        combinatorbyname = new HashMap<>();
        combinatorbyname.put("OOOOOOOXCCCCCCC", new OOOOOOOXCCCCCCC());
        combinatorbyname.put("OOOOOOXCCCCCC", new OOOOOOXCCCCCC());
        combinatorbyname.put("OOOOOXCCCCC", new OOOOOXCCCCC());
        combinatorbyname.put("OOOOXCCCC", new OOOOXCCCC());
        combinatorbyname.put("OOOXCCC", new OOOXCCC());
        combinatorbyname.put("OOOXXCCC", new OOOXXCCC());
        combinatorbyname.put("OOXCC", new OOXCC());
        combinatorbyname.put("OOXCOQCC", new OOXCOQCC());
        combinatorbyname.put("OOXCOXCC", new OOXCOXCC());
        combinatorbyname.put("OOXCXC", new OOXCXC());
        combinatorbyname.put("OOXXCC", new OOXXCC());
        combinatorbyname.put("OOXXCXC", new OOXXCXC());
        combinatorbyname.put("OOXXXCC", new OOXXXCC());
        combinatorbyname.put("OXOXCC", new OXOXCC());
        combinatorbyname.put("OXOXOXXCCC", new OXOXOXXCCC());
        combinatorbyname.put("OXOXXCC", new OXOXXCC());
        combinatorbyname.put("OXOXXCXC", new OXOXXCXC());
        combinatorbyname.put("OXOXXXCC", new OXOXXXCC());
        combinatorbyname.put("OXOXXXXCC", new OXOXXXXCC());
        combinatorbyname.put("OXXOXCC", new OXXOXCC());
        //
        ReentrantReadWriteLock readwritelock = new ReentrantReadWriteLock();
        readlock = readwritelock.readLock();
        writelock = readwritelock.writeLock();
    }
    /**
     * Checks if the Name is invalid
     *
     * @param name Name
     * @return true if the Name is invalid
     */
    private static boolean isInvalid(String name)
    {
        return !name.startsWith("O") || name.lastIndexOf('O') <= 0;
    }
    /**
     * Gets or generates a Combinator if possible
     *
     * @param name Combinator Name
     * @return Combinator or Null
     */
    public TypeCheckCombinator getCombinator(String name)
    {
        if (isInvalid(name)) return null;
        //
        readlock.lock();
        //
        try
        {
            if (combinatorbyname.containsKey(name)) return combinatorbyname.get(name);
        }
        finally
        {
            readlock.unlock();
        }
        //
        writelock.lock();
        //
        try
        {
            if (combinatorbyname.containsKey(name)) return combinatorbyname.get(name);
            //
            TypeCheckCombinator combinator = null;
            //
            try
            {
                CombinatorGenerator generator = new CombinatorGenerator(name);
                generator.generate();
                combinator = generator.loadAndNewInstance();
                logger.fine("generated combinator " + name);
            }
            catch (Throwable e)
            {
                logger.log(Level.INFO, "cannot generate combinator " + name, e);
            }
            //
            combinatorbyname.put(name, combinator);
            //
            return combinator;
        }
        finally
        {
            writelock.unlock();
        }
    }
}