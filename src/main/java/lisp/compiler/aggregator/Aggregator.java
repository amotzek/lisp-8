package lisp.compiler.aggregator;
/*
 * Copyright (C) 2012, 2013, 2015, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.List;
import lisp.Sexpression;
import lisp.combinator.TypeCheckCombinator;
import java.util.LinkedList;
import static lisp.List.length;
/*
 * Created by andreasm on 26.07.12 at 17:04
 */
public final class Aggregator
{
    private static final CombinatorFactory combinatorfactory = new CombinatorFactory();
    //
    private final List source;
    private final LinkedList<Sexpression> combinators;
    private final LinkedList<Sexpression> arguments;
    private final StringBuilder namebuilder;
    private int size;
    private List result;
    /**
     * Constructor for Aggregator
     *
     * @param source Source
     */
    public Aggregator(List source)
    {
        super();
        //
        this.source = source;
        //
        combinators = new LinkedList<>();
        arguments = new LinkedList<>();
        namebuilder = new StringBuilder();
        result = source;
    }
    /**
     * Replaces multiple Applications of Combinators with one Application of a higher order Combinator
     *
     * (first (rest x)) -> (C first rest x)
     * (equal? (type-of x) (quote list)) -> (C equal? type-of x (quote list))
     * (cons e1 (cons e2 nil)) -> (C cons cons e1 e2 nil)
     * (cons (first l1) (append2 (rest l1) l2)) -> (C cons first append2 rest l1 l1 l2)
     * (cons (cons place (the-empty-path)) nil) -> (C cons cons place (the-empty-path) nil)
     */
    public void aggregate()
    {
        traverseList(source);
        synthesize();
    }
    /**
     * Returns the Result of the Aggregation
     *
     * @return Result
     */
    public List getResult()
    {
        return result;
    }
    /**
     * Traverses the Source and collects Combinators, Arguments and Name
     *
     * @param sexpression S-Expression
     */
    private void traverse(Sexpression sexpression)
    {
        if (sexpression instanceof List)
        {
            List list = (List) sexpression;
            //
            if (getsTooBigWith(list))
            {
                Aggregator aggregator = new Aggregator(list);
                aggregator.aggregate();
                addEvaluatedArgument(aggregator.getResult());
                //
                return;
            }
            //
            traverseList(list);
            //
            return;
        }
        //
        addEvaluatedArgument(sexpression);
    }
    /**
     * Checks if the Aggregation gets to big
     *
     * @param list List of Arguments
     * @return true if the Aggregation gets too big
     */
    private boolean getsTooBigWith(List list)
    {
        int length = length(list);
        //
        return size + length >= CombinatorGenerator.PARAMETER_LIMIT;
    }
    /**
     * Traverses a List
     *
     * @param list List
     */
    private void traverseList(List list)
    {
        Sexpression first = list.first();
        //
        if (!(first instanceof TypeCheckCombinator) || !wellformed(first, list))
        {
            addEvaluatedArgument(list);
            //
            return;
        }
        //
        TypeCheckCombinator combinator = (TypeCheckCombinator) first;
        startTerm(combinator);
        list = list.rest();
        int position = 0;
        //
        while (list != null)
        {
            Sexpression argument = list.first();
            //
            if (combinator.mustBeQuoted(position))
            {
                addQuotedArgument(argument);
            }
            else
            {
                traverse(argument);
            }
            //
            list = list.rest();
            position++;
        }
        //
        endTerm();
    }
    /**
     * Checks if this is a syntactically wellformed Function Application
     *
     * @param first Function
     * @param list List with Function and Arguments
     * @return true if the List is wellformed
     */
    private static boolean wellformed(Sexpression first, List list)
    {
        TypeCheckCombinator combinator = (TypeCheckCombinator) first;
        //
        return combinator.acceptsArgumentCount(length(list) - 1);
    }
    /**
     * Marks the beginning of a Combinator Application
     *
     * @param combinator Combinator
     */
    private void startTerm(TypeCheckCombinator combinator)
    {
        namebuilder.append("O");
        combinators.addFirst(combinator);
        size++;
    }
    /**
     * Marks the end of a Combinator Application
     */
    private void endTerm()
    {
        namebuilder.append("C");
    }
    /**
     * Adds an evaluated Argument
     *
     * @param argument Argument
     */
    private void addEvaluatedArgument(Sexpression argument)
    {
        namebuilder.append("X");
        addArgument(argument);
    }
    /**
     * Adds a quoted Argument
     *
     * @param argument Argument
     */
    private void addQuotedArgument(Sexpression argument)
    {
        namebuilder.append("Q");
        addArgument(argument);
    }
    /**
     * Adds an Argument
     *
     * @param argument Argument
     */
    private void addArgument(Sexpression argument)
    {
        arguments.addFirst(argument);
        size++;
    }
    /**
     * Synthesizes the Result from the Intermediate Representation
     */
    private void synthesize()
    {
        if (combinators.size() < 2) return;
        //
        String name = namebuilder.toString();
        TypeCheckCombinator combinator = combinatorfactory.getCombinator(name);
        //
        if (combinator == null) return;
        //
        List translatedlist = null;
        //
        while (!arguments.isEmpty())
        {
            translatedlist = new List(arguments.removeFirst(), translatedlist);
        }
        //
        while (!combinators.isEmpty())
        {
            translatedlist = new List(combinators.removeFirst(), translatedlist);
        }
        //
        result = new List(combinator, translatedlist);
    }
}