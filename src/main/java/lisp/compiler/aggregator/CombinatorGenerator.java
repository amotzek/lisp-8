package lisp.compiler.aggregator;
/*
 * Copyright (C) 2013, 2014, 2016, 2017 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.compiler.GeneratedTypeCheckCombinator;
import lisp.compiler.bcel.Constants;
import lisp.compiler.bcel.classfile.JavaClass;
import lisp.compiler.bcel.generic.ArrayType;
import lisp.compiler.bcel.generic.ClassGen;
import lisp.compiler.bcel.generic.ConstantPoolGen;
import lisp.compiler.bcel.generic.InstructionConstants;
import lisp.compiler.bcel.generic.InstructionFactory;
import lisp.compiler.bcel.generic.InstructionList;
import lisp.compiler.bcel.generic.MethodGen;
import lisp.compiler.bcel.generic.ObjectType;
import lisp.compiler.bcel.generic.PUSH;
import lisp.compiler.bcel.generic.Type;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.LinkedList;
/*
 * Created by andreasm 23.12.13 13:42
 */
public final class CombinatorGenerator extends ClassLoader
{
    public static final int PARAMETER_LIMIT = 30; // depends on representation of Quote Mask
    //
    private static final int THIS_LOCAL = 0;
    private static final int ENVIRONMENT_LOCAL = 1;
    private static final int ARGUMENTS_LOCAL = 2;
    private static final String PACKAGE_NAME = "lisp.compiler.aggregator";
    private static final String TYPE_CHECK_COMBINATOR = "lisp.combinator.TypeCheckCombinator";
    private static final String SUPERCLASS_NAME = "lisp.compiler.GeneratedTypeCheckCombinator";
    private static final String[] NO_INTERFACE_NAMES = new String[0];
    private static final String[] NO_ARGUMENT_NAMES = new String[0];
    private static final String[] ARG01 = new String[] { "arg0", "arg1" };
    private static final String INIT = "<init>";
    private static final String APPLY = "apply";
    private static final String GET_TYPE_CHECK_COMBINATOR = "getTypeCheckCombinator";
    private static final Type[] INT_INT_TYPES = new Type[] { Type.INT, Type.INT };
    private static final Type SEXPRESSION_TYPE = new ObjectType("lisp.Sexpression");
    private static final Type[] SEXPRESSIONS_INT_TYPES = new Type[] { new ArrayType(SEXPRESSION_TYPE, 1), Type.INT };
    private static final Type ENVIRONMENT_TYPE = new ObjectType("lisp.environment.Environment");
    private static final Type[] ENVIRONMENT_SEXPRESSIONS_TYPES = new Type[] { ENVIRONMENT_TYPE , new ArrayType(SEXPRESSION_TYPE, 1) };
    private static final Type TYPE_CHECK_COMBINATOR_TYPE = new ObjectType("lisp.combinator.TypeCheckCombinator");
    //
    private final String spec;
    private final String classname;
    private final ClassGen classgen;
    private final ConstantPoolGen constantpool;
    private final InstructionFactory instructionfactory;
    private InstructionList instructionlist;
    private MethodGen method;
    private int local;
    //
    public CombinatorGenerator(String spec)
    {
        super(Thread.currentThread().getContextClassLoader());
        //
        this.spec = spec;
        //
        classname = getClassName(spec);
        classgen = new ClassGen(classname, SUPERCLASS_NAME, getFileName(spec), Constants.ACC_PUBLIC | Constants.ACC_FINAL | Constants.ACC_SUPER, NO_INTERFACE_NAMES);
        constantpool = classgen.getConstantPool();
        instructionfactory = new InstructionFactory(classgen, constantpool);
    }
    //
    private static int getQuoteMask(String spec)
    {
        int power = 1;
        int quotemask = 0;
        //
        for (int index = 0; index < spec.length(); index++)
        {
            char c = spec.charAt(index);
            //
            if (c == 'O')
            {
                quotemask += power;
                power *= 2;
            }
        }
        //
        for (int index = 0; index < spec.length(); index++)
        {
            char c = spec.charAt(index);
            //
            if (c == 'X')
            {
                power *= 2;
            }
            else if (c == 'Q')
            {
                quotemask += power;
                power *= 2;
            }
        }
        //
        return quotemask;
    }
    //
    private static int getParameters(String spec)
    {
        int count = 0;
        //
        for (int index = 0; index < spec.length(); index++)
        {
            char c = spec.charAt(index);
            //
            if (c == 'O' || c == 'X' || c == 'Q') count++;
        }
        //
        return count;
    }
    //
    private static int getCombinators(String spec)
    {
        int count = 0;
        //
        for (int index = 0; index < spec.length(); index++)
        {
            char c = spec.charAt(index);
            //
            if (c == 'O') count++;
        }
        //
        return count;
    }
    //
    private static String getClassName(String spec)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(PACKAGE_NAME);
        builder.append('.');
        builder.append(spec);
        //
        return builder.toString();
    }
    //
    private static String getFileName(String spec)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(spec);
        builder.append(".java");
        //
        return builder.toString();
    }
    //
    public void generate()
    {
        createConstructor();
        createApply();
    }
    //
    public GeneratedTypeCheckCombinator loadAndNewInstance() throws IOException, IllegalAccessException, InstantiationException
    {
        byte[] bytes = dump();
        Class clazz = defineClass(classname, bytes, 0, bytes.length);
        //
        return (GeneratedTypeCheckCombinator) clazz.newInstance();
    }
    //
    private void createConstructor()
    {
        beginPublicMethod(Type.VOID, Type.NO_ARGS, NO_ARGUMENT_NAMES, INIT);
        createLoadLocal(THIS_LOCAL);
        //
        int parameters = getParameters(spec);
        //
        if (parameters > PARAMETER_LIMIT) throw new IllegalStateException("too many parameters");
        //
        createPushConstant(getQuoteMask(spec));
        createPushConstant(parameters);
        instructionlist.append(instructionfactory.createInvoke(SUPERCLASS_NAME, INIT, Type.VOID, INT_INT_TYPES, Constants.INVOKESPECIAL));
        instructionlist.append(InstructionFactory.createReturn(Type.VOID));
        endMethod();
    }
    //
    private void beginPublicMethod(Type returntype, Type[] argtypes, String[] argnames, String methodname)
    {
        instructionlist = new InstructionList();
        method = new MethodGen(Constants.ACC_PUBLIC, returntype, argtypes, argnames, methodname, classname, instructionlist, constantpool);
        local = argtypes.length;
    }
    //
    private void endMethod()
    {
        method.setMaxStack();
        method.setMaxLocals();
        classgen.addMethod(method.getMethod());
        instructionlist.dispose();
        instructionlist = null;
        method = null;
    }
    //
    private void createApply()
    {
        beginPublicMethod(SEXPRESSION_TYPE, ENVIRONMENT_SEXPRESSIONS_TYPES, ARG01, APPLY);
        createNewArray(PARAMETER_LIMIT);
        int arraylocal = createStoreLocal();
        LinkedList<Integer> arities = new LinkedList<>();
        LinkedList<Integer> combinatorindices = new LinkedList<>();
        int parameterindex = getCombinators(spec);
        int combinatorindex = 0;
        //
        for (int i = 0; i < spec.length(); i++)
        {
            switch (spec.charAt(i))
            {
                case 'O':
                    arities.addFirst(0);
                    combinatorindices.addFirst(combinatorindex);
                    combinatorindex++;
                    //
                    break;
                //
                case 'X':
                case 'Q':
                {
                    int arity = arities.removeFirst();
                    arity++;
                    arities.addFirst(arity);
                    createArrayLoadArguments(parameterindex);
                    parameterindex++;
                    //
                    break;
                }
                //
                case 'C':
                {
                    int index = arities.removeFirst() - 1;
                    //
                    while (index >= 0)
                    {
                        createArrayStoreLocal(arraylocal, index);
                        index--;
                    }
                    //
                    createInvokeApply(combinatorindices.removeFirst(), arraylocal);
                    //
                    if (!arities.isEmpty())
                    {
                        int arity = arities.removeFirst();
                        arity++;
                        arities.addFirst(arity);
                    }
                }
            }
        }
        //
        instructionlist.append(InstructionFactory.createReturn(Type.OBJECT));
        endMethod();
    }
    //
    private void createNewArray(int length)
    {
        createPushConstant(length);
        instructionlist.append(instructionfactory.createNewArray(SEXPRESSION_TYPE, (short) 1));
    }
    //
    private int createStoreLocal()
    {
        instructionlist.append(InstructionFactory.createStore(Type.OBJECT, ++local));
        //
        return local;
    }
    //
    private void createArrayLoadArguments(int index)
    {
        createLoadLocal(ARGUMENTS_LOCAL);
        createPushConstant(index);
        instructionlist.append(InstructionConstants.AALOAD);
    }
    //
    private void createLoadLocal(int indexlocal)
    {
        instructionlist.append(InstructionFactory.createLoad(Type.OBJECT, indexlocal));
    }
    //
    private void createPushConstant(int constant)
    {
        instructionlist.append(new PUSH(constantpool, constant));
    }
    //
    private void createArrayStoreLocal(int arraylocal, int index)
    {
        createLoadLocal(arraylocal); // a v
        instructionlist.append(InstructionConstants.SWAP); // v a
        createPushConstant(index); // i v a
        instructionlist.append(InstructionConstants.SWAP); // v i a
        instructionlist.append(InstructionConstants.AASTORE);
    }
    //
    private void createInvokeApply(int combinatorindex, int arraylocal)
    {
        createLoadLocal(THIS_LOCAL);
        createLoadLocal(ARGUMENTS_LOCAL);
        createPushConstant(combinatorindex);
        instructionlist.append(instructionfactory.createInvoke(classname, GET_TYPE_CHECK_COMBINATOR, TYPE_CHECK_COMBINATOR_TYPE, SEXPRESSIONS_INT_TYPES, Constants.INVOKEVIRTUAL));
        createLoadLocal(ENVIRONMENT_LOCAL);
        createLoadLocal(arraylocal);
        instructionlist.append(instructionfactory.createInvoke(TYPE_CHECK_COMBINATOR, APPLY, SEXPRESSION_TYPE, ENVIRONMENT_SEXPRESSIONS_TYPES, Constants.INVOKEVIRTUAL));
    }
    //
    private byte[] dump() throws IOException
    {
        ByteArrayOutputStream outstream = new ByteArrayOutputStream();
        JavaClass javaclass = classgen.getJavaClass();
        javaclass.dump(outstream);
        //
        return outstream.toByteArray();
    }
    /*
    public static void main(String[] args)
    {
        try
        {
            BCELifier.main(new String[] { "lisp.compiler.aggregator.OOXCC" });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    */
}