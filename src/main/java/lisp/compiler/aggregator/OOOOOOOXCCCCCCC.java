package lisp.compiler.aggregator;
/*
 * Copyright (C) 2012, 2017 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.combinator.TypeCheckCombinator;
import lisp.compiler.GeneratedTypeCheckCombinator;
import lisp.environment.Environment;
/*
 * Created by  andreasm 27.03.12 18:25
 */
final class OOOOOOOXCCCCCCC extends GeneratedTypeCheckCombinator
{
    /**
     * Constructor for OOOOOOOXCCCCCCC -> (C (C (C (C (C (C (C X)))))))
     */
    public OOOOOOOXCCCCCCC()
    {
        super(127, 8);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        TypeCheckCombinator c1 = getTypeCheckCombinator(arguments, 0);
        TypeCheckCombinator c2 = getTypeCheckCombinator(arguments, 1);
        TypeCheckCombinator c3 = getTypeCheckCombinator(arguments, 2);
        TypeCheckCombinator c4 = getTypeCheckCombinator(arguments, 3);
        TypeCheckCombinator c5 = getTypeCheckCombinator(arguments, 4);
        TypeCheckCombinator c6 = getTypeCheckCombinator(arguments, 5);
        TypeCheckCombinator c7 = getTypeCheckCombinator(arguments, 6);
        Sexpression[] innerarguments = new Sexpression[1];
        innerarguments[0] = arguments[7];
        innerarguments[0] = c7.apply(environment, innerarguments);
        innerarguments[0] = c6.apply(environment, innerarguments);
        innerarguments[0] = c5.apply(environment, innerarguments);
        innerarguments[0] = c4.apply(environment, innerarguments);
        innerarguments[0] = c3.apply(environment, innerarguments);
        innerarguments[0] = c2.apply(environment, innerarguments);
        //
        return c1.apply(environment, innerarguments);
    }
}