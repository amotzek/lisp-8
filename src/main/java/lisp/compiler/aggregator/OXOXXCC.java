package lisp.compiler.aggregator;
/*
 * Copyright (C) 2012, 2017 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Sexpression;
import lisp.combinator.TypeCheckCombinator;
import lisp.compiler.GeneratedTypeCheckCombinator;
import lisp.environment.Environment;
/*
 * Created by  andreasm 26.07.12 19:37
 */
final class OXOXXCC extends GeneratedTypeCheckCombinator
{
    /**
     * Constructor for OXOXXCC -> (C X (C X X))
     */
    public OXOXXCC()
    {
        super(3, 5);
    }
    /**
     * @see lisp.combinator.TypeCheckCombinator#apply(lisp.environment.Environment, lisp.Sexpression[])
     */
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        TypeCheckCombinator c1 = getTypeCheckCombinator(arguments, 0);
        TypeCheckCombinator c2 = getTypeCheckCombinator(arguments, 1);
        Sexpression[] innerarguments = new Sexpression[2];
        innerarguments[0] = arguments[3];
        innerarguments[1] = arguments[4];
        innerarguments[1] = c2.apply(environment, innerarguments);
        innerarguments[0] = arguments[2];
        //
        return c1.apply(environment, innerarguments);
    }
}