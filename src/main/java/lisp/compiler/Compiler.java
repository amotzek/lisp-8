package lisp.compiler;
/*
 * Copyright (C) 2012 - 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import static lisp.List.reverse;
import lisp.CannotEvalException;
import lisp.Closure;
import lisp.Function;
import lisp.List;
import lisp.Mlambda;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.combinator.*;
import lisp.compiler.aggregator.Aggregator;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
import lisp.concurrent.RunnableQueueFactory;
/*
 * Created by andreasm 24.07.12 19:23
 */
public final class Compiler
{
    private final List parameters;
    private final Environment environment;
    private final Sexpression source;
    private Sexpression result;
    /**
     * Constructor for Compiler
     *
     * @param parameters  List of Parameters
     * @param source      S-Expression to compile
     * @param environment Environment
     */
    public Compiler(List parameters, Sexpression source, Environment environment)
    {
        super();
        //
        this.parameters = parameters;
        this.environment = environment;
        this.source = source;
    }
    //
    private static Sexpression compile(Sexpression sexpression, boolean isroot, Scope scope) throws CannotEvalException
    {
        if (sexpression instanceof List) return compileList((List) sexpression, isroot, scope);
        //
        return sexpression;
    }
    //
    private static Sexpression compileList(List list, boolean isroot, Scope scope) throws CannotEvalException
    {
        Sexpression first = list.first();
        List rest = list.rest();
        //
        if (first instanceof Symbol) first = scope.getSymbolOrValue((Symbol) first);
        //
        if (first instanceof If) return compile3(first, rest, scope);
        //
        if (first instanceof Or2) return compile2(first, rest, scope);
        //
        if (first instanceof Lambda) return compileLambda(rest, scope);
        //
        if (first instanceof Letrec) return compileLetrec(first, rest, scope);
        //
        if (first instanceof Sequential) return compileSequential(first, rest, scope);
        //
        if (first instanceof CatchAndApply) return compile3(first, rest, scope);
        //
        if (first instanceof Mlambda) return expandAndCompileMacro((Mlambda) first, rest, scope);
        //
        if (first instanceof Function) return compileFunction((Function) first, rest, isroot, scope);
        //
        return list;
    }
    //
    private static Sexpression compile3(Sexpression function, List arguments, Scope scope) throws CannotEvalException
    {
        Sexpression argument1 = arguments.first();
        arguments = arguments.rest();
        Sexpression argument2 = arguments.first();
        arguments = arguments.rest();
        Sexpression argument3 = arguments.first();
        arguments = arguments.rest();
        //
        if (arguments != null) throw new CannotEvalException("wrong number of arguments for " + function);
        //
        Sexpression translatedargument1 = compile(argument1, true, scope);
        Sexpression translatedargument2 = compile(argument2, true, scope);
        Sexpression translatedargument3 = compile(argument3, true, scope);
        List translatedlist = new List(translatedargument3, null);
        translatedlist = new List(translatedargument2, translatedlist);
        translatedlist = new List(translatedargument1, translatedlist);
        translatedlist = new List(function, translatedlist);
        //
        return translatedlist;
    }
    //
    private static Sexpression compile2(Sexpression function, List arguments, Scope scope) throws CannotEvalException
    {
        Sexpression argument1 = arguments.first();
        arguments = arguments.rest();
        Sexpression argument2 = arguments.first();
        arguments = arguments.rest();
        //
        if (arguments != null) throw new CannotEvalException("wrong number of arguments for " + function);
        //
        Sexpression translatedargument1 = compile(argument1, true, scope);
        Sexpression translatedargument2 = compile(argument2, true, scope);
        //
        if (translatedargument2 == null) return translatedargument1;
        //
        List translatedlist = new List(translatedargument2, null);
        translatedlist = new List(translatedargument1, translatedlist);
        translatedlist = new List(function, translatedlist);
        //
        return translatedlist;
    }
    //
    private static Sexpression compileSequential(Sexpression function, List arguments, Scope scope) throws CannotEvalException
    {
        List translatedarguments = null;
        arguments = (List) arguments.first();
        //
        while (arguments != null)
        {
            Sexpression argument = arguments.first();
            arguments = arguments.rest();
            Sexpression translatedargument = compile(argument, true, scope);
            translatedarguments = new List(translatedargument, translatedarguments);
        }
        //
        List translatedlist = new List(reverse(translatedarguments), null);
        translatedlist = new List(function, translatedlist);
        //
        return translatedlist;
    }
    //
    private static Sexpression compileLambda(List arguments, Scope parent) throws CannotEvalException
    {
        List parameters = (List) arguments.first();
        arguments = arguments.rest();
        Sexpression body = arguments.first();
        Scope child = parent.pushParameters(parameters);
        Sexpression translatedbody = compile(body, true, child);
        LambdaFactory factory = new LambdaFactory(parameters, body, translatedbody);
        //
        return new List(factory, null);
    }
    //
    private static Sexpression compileLetrec(Sexpression function, List arguments, Scope parent) throws CannotEvalException
    {
        List definitions = (List) arguments.first();
        arguments = arguments.rest();
        Sexpression body = arguments.first();
        List parameters = getParameters(definitions);
        Scope child = parent.pushParameters(parameters);
        Sexpression translatedbody = compile(body, true, child);
        List translateddefinitions = compileDefinitions(definitions, child);
        List translatedlist = new List(translatedbody, null);
        translatedlist = new List(translateddefinitions, translatedlist);
        translatedlist = new List(function, translatedlist);
        //
        return translatedlist;
    }
    //
    private static List getParameters(List definitions)
    {
        List parameters = null;
        //
        while (definitions != null)
        {
            List definition = (List) definitions.first();
            Sexpression parameter = definition.first();
            parameters = new List(parameter, parameters);
            definitions = definitions.rest();
        }
        //
        return parameters;
    }
    //
    private static List compileDefinitions(List definitions, Scope scope) throws CannotEvalException
    {
        List translateddefinitions = null;
        //
        while (definitions != null)
        {
            List definition = (List) definitions.first();
            Sexpression key = definition.first();
            definition = definition.rest();
            Sexpression value = definition.first();
            Sexpression translatedvalue = compile(value, true, scope);
            List translateddefinition = new List(translatedvalue, null);
            translateddefinition = new List(key, translateddefinition);
            translateddefinitions = new List(translateddefinition, translateddefinitions);
            definitions = definitions.rest();
        }
        //
        return reverse(translateddefinitions);
    }
    //
    private static Sexpression expandAndCompileMacro(Mlambda mlambda, List arguments, Scope scope) throws CannotEvalException
    {
        /*
         * Expand the macro
         */
        RunnableQueue runnablequeue = RunnableQueueFactory.createPassiveRunnableQueue();
        Sexpression body = mlambda.getCompiledBody();
        List parameters = mlambda.getParameters();
        Environment parent = mlambda.getEnvironment();
        Environment child = new Environment(parent, parameters, new Sexpression[] { arguments });
        Closure closure = new Closure(runnablequeue, child, body);
        Sexpression intermediate = closure.eval();
        /*
         * Compile the expansion
         */
        return compile(intermediate, true, scope);
    }
    //
    private static Sexpression compileFunction(Function function, List arguments, boolean isroot, Scope scope) throws CannotEvalException
    {
        /*
         * Compile the arguments
         */
        boolean isaggregatable = function instanceof TypeCheckCombinator;
        List translatedlist = new List(function, null);
        int position = 0;
        //
        while (arguments != null)
        {
            Sexpression argument = arguments.first();
            Sexpression translatedargument = function.mustBeQuoted(position) ? argument : compile(argument, !isaggregatable, scope);
            translatedlist = new List(translatedargument, translatedlist);
            arguments = arguments.rest();
            position++;
        }
        //
        translatedlist = reverse(translatedlist);
        //
        if (isroot && isaggregatable)
        {
            /*
             * Aggregate nested function calls
             */
            Aggregator aggregator = new Aggregator(translatedlist);
            aggregator.aggregate();
            //
            return aggregator.getResult();
        }
        //
        return translatedlist;
    }
    /**
     * Compiles the S-Expression
     *
     * @throws CannotEvalException if the Expression is not wellformed
     */
    public void compile() throws CannotEvalException
    {
        Scope scope = new Scope(parameters, environment);
        //
        try
        {
            result = compile(source, true, scope);
            //
            return;
        }
        catch (CannotEvalException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            // no compilation
        }
        //
        result = source;
    }
    /**
     * Returns the Result of the Compilation
     *
     * @return Compiled S-Expression
     */
    public Sexpression getResult()
    {
        return result;
    }
}