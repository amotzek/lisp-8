package lisp.compiler;
/*
 * Copyright (C) 2012 - 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.*;
import lisp.environment.Environment;
import lisp.environment.NotBoundException;
import java.util.HashSet;
/*
 * Created by Andreas on 29.08.2015.
 */
final class Scope
{
    private final Environment environment;
    private final Scope parent;
    private final HashSet<Symbol> parameters;
    //
    private Scope(List parameters, Environment environment, Scope parent) throws CannotEvalException
    {
        super();
        //
        this.environment = environment;
        this.parent = parent;
        this.parameters = new HashSet<>();
        //
        while (parameters != null)
        {
            Sexpression parameter = parameters.first();
            /*
             * Security of lisp.environment.Copier depends on the fact that
             * Parameter Lists only contain Symbols
             */
            if (!(parameter instanceof Symbol)) throw new CannotEvalException("parameter list contains a non-symbol: " + parameter);
            //
            this.parameters.add((Symbol) parameter);
            parameters = parameters.rest();
        }
    }
    /**
     * Creates the Start Scope
     *
     * @param parameters Parameters
     * @param environment Environment
     * @throws CannotEvalException
     */
    public Scope(List parameters, Environment environment) throws CannotEvalException
    {
        this(parameters, environment, null);
    }
    /**
     * Creates a Child Scope from this Scope and the given Parameters
     *
     * @param parameters Parameters
     * @return Child Scope
     * @throws CannotEvalException
     */
    public Scope pushParameters(List parameters) throws CannotEvalException
    {
        return new Scope(parameters, this.environment, this);
    }
    /**
     * Returns the Value of the Symbol if it is a Constant or the Symbol itself
     *
     * @param symbol Symbol
     * @return Value or Symbol
     */
    public Sexpression getSymbolOrValue(Symbol symbol)
    {
        if (!isParameter(symbol))
        {
            try
            {
                Sexpression value = environment.at(symbol);
                //
                if (value instanceof Function) return value;
            }
            catch (NotBoundException e)
            {
                // return the symbol
            }
        }
        //
        return symbol;
    }
    //
    private boolean isParameter(Symbol symbol)
    {
        Scope scope = this;
        //
        do
        {
            if (scope.parameters.contains(symbol)) return true;
            //
            scope = scope.parent;
        }
        while (scope != null);
        //
        return false;
    }
}