package lisp.compiler;
/*
 * Copyright (C) 2015 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.CannotEvalException;
import lisp.Lambda;
import lisp.List;
import lisp.Sexpression;
import lisp.combinator.TypeCheckCombinator;
import lisp.environment.Environment;
/*
 * Created by Andreas on 23.08.2015.
 */
public final class LambdaFactory extends TypeCheckCombinator
{
    private final List parameters;
    private final Sexpression body;
    private final Sexpression compiledbody;
    /**
     * Creates a LambdaFactory that will create Lambdas with the given Parameters and Body
     *
     * @param parameters Parameters
     * @param body Body
     * @param compiledbody Compiled Body
     */
    public LambdaFactory(List parameters, Sexpression body, Sexpression compiledbody)
    {
        super(0, 0);
        //
        this.parameters = parameters;
        this.body = body;
        this.compiledbody = compiledbody;
    }
    //
    @Override
    public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
    {
        return new Lambda(parameters, body, compiledbody, environment);
    }
}