/*
 * Copyright (C) 2001, 2006, 2007, 2010, 2011, 2012, 2016, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp;
//
// Rational.java
// LL
//
// Created by andreasm on Sun Apr 08 2001.
//
import java.math.BigInteger;
import java.util.LinkedList;
/*
 * @author Andreasm
 */
public final class Rational extends Constant implements Comparable<Rational>
{
    private static final double LOG2 = Math.log10(2.0d);
    private static final BigInteger TEN = BigInteger.valueOf(10L);
    //
    private BigInteger numerator;
    private BigInteger denominator;
    /**
     * Constructor for Rational
     *
     * @param numerator   Numerator
     * @param denominator Denominator
     */
    private Rational(BigInteger numerator, BigInteger denominator)
    {
        super();
        //
        this.numerator = numerator;
        this.denominator = denominator;
    }
    /**
     * Constructor for Rational
     *
     * @param value Long value
     */
    public Rational(long value)
    {
        super();
        //
        numerator = BigInteger.valueOf(value);
        denominator = BigInteger.ONE;
    }
    /**
     * Constructor for Rational
     *
     * @param value BigInteger value
     */
    public Rational(BigInteger value)
    {
        super();
        //
        numerator = value;
        denominator = BigInteger.ONE;
    }
    /**
     * Constructor for Rational
     *
     * @param value Rational value
     */
    public Rational(Rational value)
    {
        super();
        //
        numerator = value.numerator;
        denominator = value.denominator;
    }
    /**
     * Constructor for Rational
     *
     * @param value String value
     * @throws NumberFormatException if String is not a Rational
     */
    public Rational(String value) throws NumberFormatException
    {
        super();
        //
        if (!parseRatio(value)) parseDecimal(value);
        //
        shorten();
    }
    /**
     * Parses a Rational if the Value has the form n/d 
     * 
     * @param value Value
     * @return true, if Value has the form n/d
     */
    private boolean parseRatio(String value)
    {
        int islash = value.indexOf('/');
        //
        if (islash < 0) return false;
        //
        numerator = new BigInteger(value.substring(0, islash));
        denominator = new BigInteger(value.substring(++islash));
        //
        return true;
    }
    /**
     * Parses a Rational if the Value is a Decimal
     * 
     * @param value Value
     */
    private void parseDecimal(String value)
    {
        final int length = value.length();
        int i = 0;
        boolean isnegative = false;
        boolean hassign = false;
        boolean hasdot = false;
        int exponent = 0;
        numerator = BigInteger.valueOf(0L);
        denominator = BigInteger.valueOf(1L);
        //
        while (i < length)
        {
            char c = value.charAt(i++);
            //
            if (c == '+')
            {
                if (hassign) throw new NumberFormatException("duplicate sign in " + value);
                //
                hassign = true;
            }
            else if (c == '-')
            {
                if (hassign) throw new NumberFormatException("duplicate sign in " + value);
                //
                hassign = true;
                isnegative = true;
            }
            else if (c >= '0' && c <= '9')
            {
                numerator = numerator.multiply(TEN);
                numerator = numerator.add(BigInteger.valueOf(c - '0'));
                //
                if (hasdot) denominator = denominator.multiply(TEN);
            }
            else if (c == '.')
            {
                if (hasdot) throw new NumberFormatException("duplicate dot in " + value);
                //
                hasdot = true;
            }
            else if (c == 'e' || c == 'E')
            {
                if (value.charAt(i) == '+') i++;
                //
                exponent = Integer.parseInt(value.substring(i));
                //
                break;
            }
            else
            {
                throw new NumberFormatException("unexpected character in " + value);
            }
        }
        //
        if (exponent > 0)
        {
            numerator = numerator.multiply(TEN.pow(exponent));
        }
        else if (exponent < 0)
        {
            denominator = denominator.multiply(TEN.pow(-exponent));
        }
        //
        if (isnegative) numerator = numerator.negate();        
    }
    /**
     * Returns the Numerator
     *
     * @return Numerator
     */
    public BigInteger getNumerator()
    {
        return numerator;
    }
    /**
     * Returns the Denominator
     *
     * @return Denominator
     */
    public BigInteger getDenominator()
    {
        return denominator;
    }
    /**
     * Checks if the Rational is 0
     *
     * @return true, if value is 0
     */
    public boolean isZero()
    {
        return numerator.signum() == 0;
    }
    /**
     * Checks if the Rational is negative
     *
     * @return true, if value is negative
     */
    public boolean isNegative()
    {
        return numerator.signum() < 0;
    }
    /**
     * Checks if the Rational is an Integer
     *
     * @return true, if Denominator is 1
     */
    public boolean isInteger()
    {
        return isOne(denominator);
    }
    /**
     * Shortens the Rational
     */
    private void shorten()
    {
        if (isNegative(denominator))
        {
            numerator = numerator.negate();
            denominator = denominator.negate();
        }
        //
        if (isOne(denominator)) return;
        //
        BigInteger absolutenumerator = numerator.abs();
        BigInteger divisor = absolutenumerator.gcd(denominator);
        //
        if (isOne(divisor)) return;
        //
        numerator = divide(numerator, divisor);
        denominator = divide(denominator, divisor);
    }
    /**
     * Adds to this Rational
     *
     * @param value Value to add
     */
    public void add(Rational value)
    {
        BigInteger x = numerator;
        BigInteger y = value.numerator;
        BigInteger d1 = denominator;
        BigInteger d2 = value.denominator;
        numerator = x.multiply(d2).add(y.multiply(d1));
        denominator = d1.multiply(d2);
        shorten();
    }
    /**
     * Multiplies this Rational
     *
     * @param value Value to multiply
     */
    public void times(Rational value)
    {
        BigInteger x = numerator;
        BigInteger y = value.numerator;
        BigInteger d1 = denominator;
        BigInteger d2 = value.denominator;
        numerator = x.multiply(y);
        denominator = d1.multiply(d2);
        shorten();
    }
    /**
     * Truncates this Rational
     */
    public void truncate()
    {
        numerator = numerator.divide(denominator);
        denominator = BigInteger.ONE;
    }
    /**
     * Returns the additive Inverse
     *
     * @return Additive inverse
     */
    public Rational additiveInverse()
    {
        return new Rational(numerator.negate(), denominator);
    }
    /**
     * Returns the multiplicative Inverse
     *
     * @return Multiplicative inverse
     */
    public Rational multiplicativeInverse()
    {
        if (isNegative(numerator)) return new Rational(denominator.negate(), numerator.negate());
        //
        return new Rational(denominator, numerator);
    }
    /**
     * Converts this Rational into a continued fraction,
     * cuts the continued fraction after the product
     * of the coefficients exceeds the multiplicative inverse
     * of the Precision.
     *
     * @param precision Precision
     * @return Approximation of this Rational
     */
    public Rational approximate(Rational precision)
    {
        if (isZero() || precision.isZero() || isNegative(precision.numerator)) return new Rational(numerator, denominator);
        //
        BigInteger limit = precision.denominator.divide(precision.numerator);
        BigInteger product = BigInteger.ONE;
        LinkedList<BigInteger> coefficients = new LinkedList<>();
        BigInteger a = this.numerator;
        BigInteger b = this.denominator;
        //
        while (!BigInteger.ZERO.equals(b))
        {
            BigInteger[] qr = a.divideAndRemainder(b);
            BigInteger q = qr[0];
            BigInteger r = qr[1];
            //
            if (BigInteger.ZERO.equals(q))
            {
                if (a.signum() == b.signum())
                {
                    q = BigInteger.ONE;
                    r = a.subtract(b);
                }
                else
                {
                    q = BigInteger.valueOf(-1);
                    r = a.add(b);
                }
            }
            //
            product = product.multiply(q.abs().add(BigInteger.ONE));
            //
            if (product.compareTo(limit) > 0) break;
            //
            coefficients.addLast(q);
            a = b;
            b = r;
        }
        //
        if (coefficients.isEmpty())
        {
            Rational c = new Rational(numerator, denominator);
            c.truncate();
            //
            return c;
        }
        //
        Rational c = new Rational(coefficients.removeLast());
        //
        while (!coefficients.isEmpty())
        {
            c = c.multiplicativeInverse();
            c.add(new Rational(coefficients.removeLast()));
        }
        //
        return c;
    }
    /**
     * Returns this Rational as Int
     *
     * @return Integer value
     * @throws CannotEvalException if Rational is not an Integer
     */
    public int intValue() throws CannotEvalException
    {
        try
        {
            if (isInteger()) return numerator.intValueExact();
        }
        catch (ArithmeticException ignore)
        {
        }
        //
        throw new CannotEvalException("not an integer or size is greater than 32 bits");
    }
    /**
     * Returns this Rational as Long
     *
     * @return Long value
     * @throws CannotEvalException if Rational is not an Integer
     */
    public long longValue() throws CannotEvalException
    {
        try
        {
            if (isInteger()) return numerator.longValueExact();
        }
        catch (ArithmeticException ignore)
        {
        }
        //
        throw new CannotEvalException("not an integer or size is greater than 64 bits");
    }
    /**
     * Returns this Rational as BigInteger
     *
     * @return BigInteger value
     * @throws CannotEvalException if Rational is not an Integer
     */
    public BigInteger bigIntegerValue() throws CannotEvalException
    {
        if (isInteger()) return numerator;
        //
        throw new CannotEvalException("not an integer");
    }
    /**
     * Returns this Rational as Double
     *
     * @return Double value
     */
    public double doubleValue()
    {
        if (isZero()) return 0d;
        //
        if (isInteger()) return numerator.doubleValue();
        //
        double n = numerator.doubleValue();
        double d = denominator.doubleValue();
        //
        if (Double.isInfinite(n) || Double.isInfinite(d))
        {
            try
            {
                return Double.parseDouble(toString());
            }
            catch (NumberFormatException e)
            {
                return Double.NaN;
            }
        }
        //
        return n / d;
    }
    /**
     * @see Sexpression#getType()
     */
    public String getType()
    {
        if (isInteger()) return "integer";
        //
        return "ratio";
    }
    /*
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return denominator.hashCode() ^ numerator.hashCode();
    }
    /*
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        //
        if (obj == null) return false;
        //
        if (getClass() != obj.getClass()) return false;
        //
        Rational other = (Rational) obj;
        //
        return denominator.equals(other.denominator) && numerator.equals(other.numerator);
    }
    /**
     * @see Object#toString()
     */
    public String toString()
    {
        if (isZero()) return "0";
        //
        if (isInteger()) return numerator.toString();
        //
        BigInteger n = numerator.abs();
        BigInteger d = denominator;
        final int l1 = log10(n);
        final int l2 = log10(d);
        int m = 4 + Math.max(l1, l2);
        int e = l1 - l2;
        //
        if (e > 0)
        {
            d = d.multiply(TEN.pow(e));
        }
        else if (e < 0)
        {
            n = n.multiply(TEN.pow(-e));
        }
        //
        while (n.compareTo(d) < 0)
        {
            n = n.multiply(TEN);
            e--;
        }
        //
        while (n.compareTo(d.multiply(TEN)) > 0)
        {
            d = d.multiply(TEN);
            e++;
        }
        //
        StringBuilder decimalbuilder = new StringBuilder();
        //
        while (m > 0 && n.compareTo(BigInteger.ZERO) > 0)
        {
            BigInteger[] qr = n.divideAndRemainder(d);
            n = qr[1].multiply(TEN);
            //
            try
            {
                BigInteger g = n.gcd(d);
                n = n.divide(g);
                d = d.divide(g);
            }
            catch (ArithmeticException ae)
            {
                assert false;
            }
            //
            decimalbuilder.append(qr[0].toString());
            m--;
        }
        //
        if (e > 0 && e < decimalbuilder.length())
        {
            if (e + 1 < decimalbuilder.length()) decimalbuilder.insert(e + 1, ".");
        }
        else if (e >= 0 && e < 7)
        {
            if (e + 1 < decimalbuilder.length())
            {
                decimalbuilder.insert(e + 1, ".");
            }
            else
            {
                while (decimalbuilder.length() <= e)
                {
                    decimalbuilder.append("0");
                }
            }
        }
        else
        {
            if (decimalbuilder.length() > 1) decimalbuilder.insert(1, ".");
            //
            if (e != 0)
            {
                decimalbuilder.append("e");
                decimalbuilder.append(e);
            }
        }
        //
        if (numerator.compareTo(BigInteger.ZERO) < 0) decimalbuilder.insert(0, "-");
        //
        // wenn die Dezimaldarstellung exakt ist, wird sie bevorzugt
        //
        if (BigInteger.ZERO.equals(n)) return decimalbuilder.toString();
        //
        StringBuilder ratiobuilder = new StringBuilder();
        ratiobuilder.append(numerator);
        ratiobuilder.append("/");
        ratiobuilder.append(denominator);
        //
        // ansonsten wird die kürzere Darstellung verwendet
        //
        if (decimalbuilder.length() < ratiobuilder.length()) return decimalbuilder.toString();
        //
        return ratiobuilder.toString();
    }
    /**
     * @see Comparable#compareTo(Object)
     */
    public int compareTo(final Rational r)
    {
        int cn = numerator.compareTo(r.numerator);
        int cd = denominator.compareTo(r.denominator);
        int c = 0;
        //
        if (cn == 0) c++;
        //
        if (cn > 0) c += 2;
        //
        c *= 3;
        //
        if (cd == 0) c++;
        //
        if (cd > 0) c += 2;
        //
        if (numerator.signum() == -1) c += 9;
        //
        switch (c)
        {
            // numerator < r.numerator && denominator < r.denominator
            case 0:
                break;
            // numerator < r.numerator && denominator == r.denominator
            case 1:
                return -1;
            // numerator < r.numerator && denominator > r.denominator
            case 2:
                return -1;
            // numerator == r.numerator && denominator < r.denominator
            case 3:
                return 1;
            // numerator == r.numerator && denominator == r.denominator
            case 4:
                return 0;
            // numerator == r.numerator && denominator > r.denominator
            case 5:
                return -1;
            // numerator > r.numerator && denominator < r.denominator
            case 6:
                return 1;
            // numerator > r.numerator && denominator == r.denomiator
            case 7:
                return 1;
            // numerator > r.numerator && denominator > r.denominator
            case 8:
                break;
        }
        //
        BigInteger d = numerator.multiply(r.denominator).subtract(r.numerator.multiply(denominator));
        //
        return d.signum();
    }
    /**
     * Checks if the Value is negative
     *
     * @param value Value
     * @return true, if Value is negative
     */
    private static boolean isNegative(BigInteger value)
    {
        return value.signum() < 0;
    }
    /**
     * Checks if the Value is 1
     *
     * @param value Value
     * @return true, if Value is 1
     */
    private static boolean isOne(BigInteger value)
    {
        return BigInteger.ONE.equals(value);
    }
    /**
     * Divides 
     * @param dividend Dividend
     * @param divisor Divisor
     * @return Dididend / Divisor
     */
    private static BigInteger divide(BigInteger dividend, BigInteger divisor)
    {
        if (dividend.equals(divisor)) return BigInteger.ONE;
        //
        return dividend.divide(divisor);
    }
    /**
     * Calculates the decadic logarithm
     *
     * @param biginteger Value
     * @return int Decadic logarithm of Value
     */
    private static int log10(BigInteger biginteger)
    {
        int bitlength = biginteger.bitLength();
        //
        return (int) (bitlength * LOG2);
    }
}