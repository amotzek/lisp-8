package lisp;
/*
 * Copyright (C) 2011 - 2013, 2016, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import static java.util.Collections.unmodifiableMap;
import static lisp.List.reverse;
import java.util.Collection;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.Map;
/*
 * Created by andreasm on 11.10.11 at 12:14
 */
public final class SimpleClass extends Constant
{
    private final Symbol classname;
    private final LinkedList<SimpleClass> superclasses;
    private final boolean allocatable;
    private final Map<SimpleClass, Integer> linearization;
    /**
     * Constructor for SimpleClass
     */
    public SimpleClass(Symbol classname, boolean allocatable, Collection<SimpleClass> superclasses)
    {
        super();
        //
        this.classname = classname;
        this.allocatable = allocatable;
        this.superclasses = new LinkedList<>();
        //
        if (superclasses != null)
        {
            for (SimpleClass simpleclass : superclasses)
            {
                if (!allocatable && simpleclass.allocatable) throw new IllegalArgumentException();
                //
                this.superclasses.addLast(simpleclass);
            }
        }
        //
        if (allocatable)
        {
            linearization = unmodifiableMap(findLinearization(this));
        }
        else
        {
            linearization = null;
        }
    }
    /**
     * Constructor for SimpleClass
     */
    public SimpleClass()
    {
        this(null, true, null);
    }
    /**
     * Finds the Linearization of Superclasses for the given Class
     *
     * @param simpleclass Given Class
     * @return Linearization
     */
    private static Map<SimpleClass, Integer> findLinearization(SimpleClass simpleclass)
    {
        /*
         * For every Class, calculate the Set of Subclasses
         */
        IdentityHashMap<SimpleClass, HashSet<SimpleClass>> subclassesbyclass = new IdentityHashMap<>();
        LinkedList<SimpleClass> path = new LinkedList<>();
        findSubclasses(simpleclass, path, subclassesbyclass);
        /*
         * Traverse the Superclass Hierarchy breadth first
         */
        HashSet<SimpleClass> visited = new HashSet<>();
        LinkedList<SimpleClass> linearization = new LinkedList<>();
        LinkedList<SimpleClass> agenda = new LinkedList<>();
        agenda.addFirst(simpleclass);
        //
        while (!agenda.isEmpty())
        {
            simpleclass = agenda.removeFirst();
            HashSet<SimpleClass> subclasses = subclassesbyclass.get(simpleclass);
            /*
             * A Class can only be added to the Linearization
             * if all its Subclasses are already in it
             */
            if (!visited.contains(simpleclass) && isSubset(subclasses, visited))
            {
                visited.add(simpleclass);
                linearization.addLast(simpleclass);
            }
            //
            for (SimpleClass superclass : simpleclass.superclasses)
            {
                agenda.addLast(superclass);
            }
        }
        //
        int position = 0;
        IdentityHashMap<SimpleClass, Integer> positionbyclass = new IdentityHashMap<>(linearization.size());
        //
        while (!linearization.isEmpty())
        {
            simpleclass = linearization.removeFirst();
            positionbyclass.put(simpleclass, position++);
        }
        //
        return positionbyclass;
    }
    /**
     * Find Subclasses
     *
     * @param subclass Subclass
     * @param path Path to Subclass
     * @param subclassesbyclass Subclasses by Class
     */
    private static void findSubclasses(SimpleClass subclass, LinkedList<SimpleClass> path, IdentityHashMap<SimpleClass, HashSet<SimpleClass>> subclassesbyclass)
    {
        HashSet<SimpleClass> subclasses = subclassesbyclass.computeIfAbsent(subclass, s -> new HashSet<>());
        subclasses.addAll(path);
        path.addLast(subclass);
        //
        for (SimpleClass superclass : subclass.superclasses)
        {
            findSubclasses(superclass, path, subclassesbyclass);
        }
        //
        path.removeLast();
    }
    /**
     * Checks if the first Set is a Subset of the second Set
     *
     * @param set1 First Set
     * @param set2 Second Set
     * @return true if first Set is a subset of the second Set
     */
    private static boolean isSubset(HashSet<SimpleClass> set1, HashSet<SimpleClass> set2)
    {
        for (SimpleClass simpleclass : set1)
        {
            if (!set2.contains(simpleclass)) return false;
        }
        //
        return true;
    }
    /**
     * Returns the Name of this Class
     *
     * @return Class Name
     */
    public Symbol getClassName()
    {
        return classname;
    }
    /**
     * Checks if Instances of this Class can be created
     *
     * @return true, if Instances of this Class can be created
     */
    public boolean isAllocatable()
    {
        return allocatable;
    }
    /**
     * Returns the List of Superclasses from most to least specific
     *
     * @return List of Superclasses
     */
    public List getSuperclasses()
    {
        List list = null;
        //
        for (SimpleClass superclass : superclasses)
        {
            list = new List(superclass, list);
        }
        //
        return reverse(list);
    }
    /**
     * Returns the Linearization of Superclasses,
     * must only be called if this Class is allocatable
     *
     * @return Linearization
     */
    public Map<SimpleClass, Integer> getLinearization()
    {
        return linearization;
    }
    /**
     * Checks if this Class is compatible to the given Class
     *
     * @param superclass Class
     * @return true, if the Classes are same or the given Class is a Superclass of this Class
     */
    public boolean isCompatibleTo(SimpleClass superclass)
    {
        if (this == superclass) return true;
        //
        for (SimpleClass simpleclass : superclasses)
        {
            if (simpleclass.isCompatibleTo(superclass)) return true;
        }
        //
        return false;
    }
    /**
     * @see lisp.Sexpression#getType()
     */
    public String getType()
    {
        if (allocatable) return "class";
        //
        return "trait";
    }
    /**
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(Object object)
    {
        return object == this;
    }
    /**
     * @see Object#toString()
     */
    @Override
    public String toString()
    {
        if (classname == null) return "#.(throw (quote error) \"anonymous classes or traits cannot be read or written\")";
        //
        return classname.getName();
    }
}