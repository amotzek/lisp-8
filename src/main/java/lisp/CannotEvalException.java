/*
 * Copyright (C) 2001, 2006, 2007, 2010, 2011 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package lisp;
/*
 * @author Andreasm
 */
public class CannotEvalException extends Exception
{
    private final Symbol symbol;
    private final Sexpression sexpression;
    /**
     * Constructor for CannotEvalException
     *
     * @param symbol      Symbol thrown
     * @param sexpression Sexpression thrown
     */
    public CannotEvalException(Symbol symbol, Sexpression sexpression)
    {
        super("throw " +  symbol +  " " + sexpression);
        //
        this.symbol = symbol;
        this.sexpression = sexpression;
    }
    /**
     * Constructor for CannotEvalException
     *
     * @param message Message thrown
     */
    public CannotEvalException(String message)
    {
        this(Symbol.createSymbol("error"), message == null ? null : new Chars(message));
    }
    /**
     * Method getSymbol
     *
     * @return Symbol thrown
     */
    public final Symbol getSymbol()
    {
        return symbol;
    }
    /**
     * Method getSexpression
     *
     * @return Sexpression thrown
     */
    public final Sexpression getSexpression()
    {
        return sexpression;
    }
    /**
     * @see Throwable#fillInStackTrace()
     */
    @Override
    public Throwable fillInStackTrace()
    {
        return this;
    }
}