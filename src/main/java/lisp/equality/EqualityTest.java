package lisp.equality;
/*
 * Copyright (C) 2012, 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.equality.strategy.EqualityStrategy;
import lisp.equality.strategy.StrategyFactory;
/*
 * Created by  andreasm 26.12.12 12:12
 */
public final class EqualityTest
{
    private final StrategyFactory strategyfactory;
    //
    public EqualityTest()
    {
        super();
        //
        strategyfactory = new StrategyFactory(this);
    }
    /**
     * Checks if two (eventually cyclic) S-Expressions are equal
     *
     * @param sexpression1 First S-Expression
     * @param sexpression2 Second S-Expression
     * @return true if the S-Expressions are equal
     */
    @SuppressWarnings("unchecked")
    public boolean areEqual(Sexpression sexpression1, Sexpression sexpression2)
    {
        if (sexpression1 == null) return sexpression2 == null;
        //
        if (sexpression2 == null) return false;
        //
        if (sexpression1.getClass() != sexpression2.getClass()) return false;
        //
        EqualityStrategy<Sexpression> strategy = strategyfactory.findStrategyFor(sexpression1);
        //
        return strategy.areEqual(sexpression1, sexpression2);
    }
}