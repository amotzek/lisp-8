package lisp.equality.strategy;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Array;
import lisp.List;
import lisp.equality.EqualityTest;
import java.util.Arrays;
/*
 * Created by andreasm 30.04.13 20:26
 */
final class ArrayStrategy extends AssociativeContainerStrategy<List, Array>
{
    ArrayStrategy(EqualityTest equality)
    {
        super(equality);
    }
    //
    @Override
    protected boolean haveEqualMeta(Array array1, Array array2)
    {
        if (!Arrays.equals(array1.getDimensions(), array2.getDimensions())) return false;
        //
        return array1.size() == array2.size();
    }
}