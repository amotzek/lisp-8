package lisp.equality.strategy;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.equality.EqualityTest;
import java.util.HashMap;
/*
 * Created by andreas-motzek@t-online.de 02.05.13 17:21
 */
public final class StrategyFactory
{
    private static final EqualityStrategy EQUALS_STRATEGY = new EqualsStrategy();
    //
    private final HashMap<String, EqualityStrategy> strategiesbytype;
    //
    public StrategyFactory(EqualityTest test)
    {
        super();
        //
        strategiesbytype = new HashMap<>();
        strategiesbytype.put("array", new ArrayStrategy(test));
        strategiesbytype.put("hash-table", new HashTableStrategy(test));
        strategiesbytype.put("instance", new InstanceStrategy(test));
        strategiesbytype.put("list", new ListStrategy(test));
    }
    //
    public EqualityStrategy findStrategyFor(Sexpression sexpression)
    {
        String type = sexpression.getType();
        EqualityStrategy strategy = strategiesbytype.get(type);
        //
        if (strategy != null) return strategy;
        //
        return EQUALS_STRATEGY;
    }
}