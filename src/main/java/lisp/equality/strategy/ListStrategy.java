package lisp.equality.strategy;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.List;
import lisp.Sexpression;
import lisp.equality.EqualityTest;
/*
 * Created by andreasm 30.04.13 20:10
 */
final class ListStrategy extends BaseStrategy<List>
{
    ListStrategy(EqualityTest equality)
    {
        super(equality);
    }
    //
    @Override
    protected boolean isMutable(List sexpression)
    {
        return false;
    }
    //
    @Override
    protected boolean haveEqualMeta(List list1, List list2)
    {
        return true;
    }
    //
    @Override
    protected boolean haveEqualContents(List list1, List list2)
    {
        while (list1 != null && list2 != null)
        {
            if (list1 == list2) return true;
            //
            Sexpression sexpression1 = list1.first();
            Sexpression sexpression2 = list2.first();
            //
            if (sexpression1 == null)
            {
                if (sexpression2 != null) return false;
            }
            else
            {
                if (sexpression2 == null) return false;
                //
                if (!equality.areEqual(sexpression1, sexpression2)) return false;
            }
            //
            list1 = list1.rest();
            list2 = list2.rest();
        }
        //
        return (list1 == list2);
    }
}