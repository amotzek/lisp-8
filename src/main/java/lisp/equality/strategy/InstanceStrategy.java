package lisp.equality.strategy;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.RubyStyleObject;
import lisp.Symbol;
import lisp.equality.EqualityTest;
/*
 * Created by andreasm 30.04.13 20:37
 */
final class InstanceStrategy extends AssociativeContainerStrategy<Symbol, RubyStyleObject>
{
    InstanceStrategy(EqualityTest equality)
    {
        super(equality);
    }
    //
    @Override
    protected boolean haveEqualMeta(RubyStyleObject instance1, RubyStyleObject instance2)
    {
        if (!instance1.classOf().equals(instance2.classOf())) return false;
        //
        return instance1.size() == instance2.size();
    }
}