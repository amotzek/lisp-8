package lisp.equality.strategy;
/*
 * Copyright (C) 2013, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.AssociativeContainer;
import lisp.Sexpression;
import lisp.equality.EqualityTest;
import java.util.Iterator;
/*
 * Created by andreasm 30.04.13 20:22
 */
abstract class AssociativeContainerStrategy<K extends Sexpression, T extends AssociativeContainer<K>> extends BaseStrategy<T>
{
    AssociativeContainerStrategy(EqualityTest equality)
    {
        super(equality);
    }
    //
    @Override
    protected final boolean isMutable(T freezable)
    {
        return !freezable.isFrozen();
    }
    //
    @Override
    protected final boolean haveEqualContents(T map1, T map2)
    {
        Iterator<K> keys = map1.keyIterator();
        //
        while (keys.hasNext())
        {
            K key = keys.next();
            Sexpression sexpression1 = map1.get(key);
            Sexpression sexpression2 = map2.get(key);
            //
            if (!equality.areEqual(sexpression1, sexpression2)) return false;
        }
        //
        return true;
    }
}