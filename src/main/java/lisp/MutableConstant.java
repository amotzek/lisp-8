package lisp;
/*
 * Copyright (C) 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.concurrent.locks.ReentrantReadWriteLock;
/*
 * Created by andreasm
 * Date: 07.01.14
 * Time: 07:52
 */
public abstract class MutableConstant extends Constant
{
    private final ReentrantReadWriteLock lock;
    /**
     * Constructor for MutableConstant
     */
    protected MutableConstant()
    {
        super();
        //
        lock = new ReentrantReadWriteLock();
    }
    /**
     * Acquires the Shared Lock
     */
    protected final void lockShared()
    {
        lock.readLock().lock();
    }
    /**
     * Releases the Shared Lock
     */
    protected final void unlockShared()
    {
        lock.readLock().unlock();
    }
    /**
     * Acquires the Exclusive Lock
     */
    protected final void lockExclusively()
    {
        lock.writeLock().lock();
    }
    /**
     * Releases the Exclusive Lock
     */
    protected final void unlockExclusively()
    {
        lock.writeLock().unlock();
    }
}