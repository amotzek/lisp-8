package lisp;
/*
 * Copyright (C) 2011 - 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.environment.Environment;
import static lisp.List.reverse;
/*
 * Created by andreasm on 13.10.11
 */
public final class GuardedMethod extends ParameterizedBody
{
    private final SimpleClassReference[] parameterclassreferences;
    private final Sexpression guard;
    private volatile boolean stale;
    /**
     * Constructor for GuardedMethod
     *
     * @param parameters List of Parameters
     * @param parameterclasses Class Constraints for Parameters
     * @param guard Guard
     * @param body Body
     * @param compiledbody Compiled Body
     * @param environment Environment
     * @throws CannotEvalException if Body cannot be compiled
     */
    public GuardedMethod(List parameters, SimpleClass[] parameterclasses, Sexpression guard, Sexpression body, Sexpression compiledbody, Environment environment) throws CannotEvalException
    {
        super(parameters, body, compiledbody, environment);
        //
        this.guard = guard;
        //
        parameterclassreferences = new SimpleClassReference[parametercount];
        //
        for (int position = 0; position < parametercount; position++)
        {
            SimpleClass parameterclass = parameterclasses[position];
            //
            if (parameterclass == null) continue;
            //
            parameterclassreferences[position] = new SimpleClassReference(parameterclass);
        }
    }
    /**
     * Checks if the Method refers to a Class that will have no Instances
     *
     * @return true if a Class with no instances is referred
     */
    public boolean isStale()
    {
        return stale;
    }
    /**
     * Checks if the Methods accepts a first Argument of the given Class
     *
     * @param argumentclass Class
     * @return true, if first Arguments of the Class are accepted
     */
    public boolean acceptsFirstArgumentOf(SimpleClass argumentclass)
    {
        if (parametercount == 0) return false;
        //
        SimpleClassReference parameterclassreference = parameterclassreferences[0];
        //
        if (parameterclassreference == null) return true;
        //
        SimpleClass parameterclass = parameterclassreference.get();
        //
        if (parameterclass == null)
        {
            /*
             * Class disappeared so this Method will never be called
             */
            stale = true;
            //
            return false;
        }
        //
        return argumentclass.isCompatibleTo(parameterclass);
    }
    /**
     * Returns the Guard
     *
     * @return Guard
     */
    public Sexpression getGuard()
    {
        return guard;
    }
    /**
     * Returns the List of Spezializers or null
     * if the Method is stale
     *
     * @return List of Spezializers
     */
    public List getSpezializers()
    {
        if (stale) return null;
        //
        List list = null;
        //
        for (SimpleClassReference parameterclassreference : parameterclassreferences)
        {
            if (parameterclassreference == null)
            {
                list = new List(null, list);
                //
                continue;
            }
            //
            SimpleClass parameterclass = parameterclassreference.get();
            //
            if (parameterclass == null)
            {
                stale = true;
                //
                return null;
            }
            //
            list = new List(parameterclass, list);
        }
        //
        return reverse(list);
    }
    /**
     * Checks if the Arguments fit to the Class Constraints
     *
     * @param arguments Arguments
     * @return true, if the Constraints are satisfied
     */
    public boolean matches(Sexpression[] arguments)
    {
        for (int position = 0; position < arguments.length; position++)
        {
            SimpleClassReference parameterclassreference = parameterclassreferences[position];
            //
            if (parameterclassreference == null) continue;
            //
            SimpleClass parameterclass = parameterclassreference.get();
            //
            if (parameterclass == null)
            {
                /*
                 * Class disappeared so this Method will never be called
                 */
                stale = true;
                //
                return false;
            }
            //
            Sexpression argument = arguments[position];
            //
            if (!(argument instanceof RubyStyleObject)) return false;
            //
            RubyStyleObject argumentobject = (RubyStyleObject) argument;
            SimpleClass argumentclass = argumentobject.classOf();
            //
            if (!argumentclass.isCompatibleTo(parameterclass)) return false;
        }
        //
        return true;
    }
    /**
     * @see Sexpression#getType()
     */
    public String getType()
    {
        return "method";
    }
    /**
     * @see Object#toString()
     */
    @Override
    public String toString()
    {
        return "#.(throw (quote error) \"methods cannot be read or written\")";
    }
}