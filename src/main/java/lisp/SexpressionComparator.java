package lisp;
/*
 * Copyright (C) 2013, 2016, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.util.Comparator;
import java.util.IdentityHashMap;
/*
 * Created by andreas-motzek@t-online.de 29.04.13 06:44
 */
final class SexpressionComparator implements Comparator<Sexpression>
{
    private final IdentityHashMap<Object, Integer> keys;
    private int sequencenumber;
    /**
     * Creates a Comparator for S-Expressions
     */
    SexpressionComparator()
    {
        super();
        //
        keys = new IdentityHashMap<>();
    }
    /**
     * @see Comparable#compareTo(Object)
     */
    @SuppressWarnings("unchecked")
    public int compare(Sexpression sexpression1, Sexpression sexpression2)
    {
        if (sexpression1 == null)
        {
            if (sexpression2 == null) return 0;
            //
            return -1;
        }
        else if (sexpression2 == null)
        {
            return 1;
        }
        //
        String type1 = sexpression1.getType();
        String type2 = sexpression2.getType();
        //
        if (type1.equals(type2))
        {
            if (sexpression1 instanceof Comparable)
            {
                Comparable comparable1 = (Comparable) sexpression1;
                //
                return comparable1.compareTo(sexpression2);
            }
            /*
             * hashCode() cannot be called here,
             * because this code is used in hash code
             * calculation, use identityHashCode(Object)
             * instead -> equal hash tables must not have
             * the same order of keys
             */
            int value1 = System.identityHashCode(sexpression1);
            int value2 = System.identityHashCode(sexpression2);
            int result = value1 - value2;
            //
            if (result != 0) return result;
            //
            value1 = getArtificialKey(sexpression1);
            value2 = getArtificialKey(sexpression2);
            //
            return value1 - value2;
        }
        //
        return type1.compareTo(type2);
    }
    //
    private int getArtificialKey(Object object)
    {
        Integer key = keys.get(object);
        //
        if (key == null)
        {
            key = sequencenumber++;
            keys.put(object, key);
        }
        //
        return key;
    }
}