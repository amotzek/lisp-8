package lisp.remote.protocol;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.DataOutputStream;
import java.io.IOException;
import lisp.Sexpression;
import lisp.Symbol;
/**
 * A Reply transports the result of a Call.
 *
 * Created by andreasm 16.10.13 12:15
 */
public final class OutgoingReply extends OutgoingMessage
{
    private final Symbol symbol;
    private final Sexpression expression;
    private final IncomingCall call;
    /**
     * Creates a Reply
     *
     * @param symbol Thrown Symbol or null
     * @param expression Result of the Call
     * @param call Call that corresponds to this Reply
     */
    public OutgoingReply(IncomingCall call, Symbol symbol, Sexpression expression)
    {
        super();
        //
        this.call = call;
        this.symbol = symbol;
        this.expression = expression;
    }
    /**
     * Returns the Call that is replied with this Message
     *
     * @return IncomingCall
     */
    public IncomingCall getCall()
    {
        return call;
    }
    /**
     * @see lisp.remote.protocol.Message#getType()
     */
    @Override
    public int getType()
    {
        return TYPE_REPLY;
    }
    /**
     * @see lisp.remote.protocol.Message#getId()
     */
    @Override
    public Long getId()
    {
        return call.getId();
    }
    /**
     * @see OutgoingMessage#marshal(java.io.DataOutputStream)
     */
    @Override
    public void marshal(DataOutputStream out) throws IOException
    {
        writeMessageHeader(out);
        Marshaller.writeSymbol(symbol, out);
        Marshaller marshaller = call.getMarshaller();
        marshaller.writeExpression(expression, out);
    }
}