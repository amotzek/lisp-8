package lisp.remote.protocol;
/*
 * Copyright (C) 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
/*
 * Created by andreasm on 17.10.2014.
 */
final class LimitedOutputStream extends OutputStream
{
    private final OutputStream out;
    private int remaining;
    //
    public LimitedOutputStream(OutputStream out, int limit)
    {
        super();
        //
        this.out = out;
        remaining = limit;
    }
    //
    public void write(int b) throws IOException
    {
        if (remaining <= 0) throw new EOFException("excessive message size");
        //
        out.write(b);
        remaining--;
    }
}