package lisp.remote.protocol;
/*
 * Copyright (C) 2013, 2014, 2018 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import lisp.Place;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
/**
 * Instances represents the technical Protocol for communication between Places
 *
 * Created by andreasm 15.10.13 16:44
 */
public final class Protocol
{
    private static final long ANNOUNCING_INTERVAL = 30000L;
    //
    private final String groupname;
    private final int port;
    private final ConcurrentHashMap<Long, OutgoingCall> callsbyid;
    //
    private InetAddress groupaddress;
    private ServerSocket serversocket;
    private DatagramSocket groupsocket;
    private DatagramSocket membersocket;
    private Place here;
    /**
     * Creates a Place with the given Group Name and Port
     *
     * @param groupname Group Name for broadcasting
     * @param port Port for broadcasting
     */
    public Protocol(String groupname, int port)
    {
        super();
        //
        this.groupname = groupname;
        this.port = port;
        //
        callsbyid = new ConcurrentHashMap<>();
    }
    /**
     * Checks if the Address is a multicast address
     *
     * @param address InetAddress
     * @return true, if the Address is a multicast address
     */
    private static boolean isMulticast(InetAddress address)
    {
        return address.isMCNodeLocal()
                || address.isMCLinkLocal()
                || address.isMCSiteLocal()
                || address.isMCOrgLocal()
                || address.isMCGlobal();
    }
    //
    private static DatagramSocket createMulticastSocket(InetAddress groupaddress, int port) throws IOException
    {
        MulticastSocket socket = new MulticastSocket(port);
        socket.joinGroup(groupaddress);
        //
        return socket;
    }
    //
    private static DatagramSocket createDatagramSocket(int port) throws IOException
    {
        DatagramSocket socket = new DatagramSocket(null);
        socket.setReuseAddress(true);
        socket.bind(new InetSocketAddress(port));
        //
        return socket;
    }
    //
    private static Place createPlace(ServerSocket serversocket) throws IOException
    {
        InetAddress address = InetAddress.getLocalHost();
        String hostname = address.getHostName();
        int port = serversocket.getLocalPort();
        //
        return new Place(hostname, port);
    }
    /**
     * Initializes this Protocol
     *
     * @throws IOException if the Sockets cannot be created
     */
    public void init() throws IOException
    {
        serversocket = new ServerSocket(0, 1);
        groupaddress = InetAddress.getByName(groupname);
        groupsocket = new DatagramSocket();
        groupsocket.setBroadcast(true);
        //
        if (isMulticast(groupaddress))
        {
            membersocket = createMulticastSocket(groupaddress, port);
        }
        else
        {
            membersocket = createDatagramSocket(port);
        }
        //
        here = createPlace(serversocket);
    }
    /**
     * Closes the Sockets that belong to this Protocol
     */
    public void destruct()
    {
        groupsocket.close();
        membersocket.close();
        //
        try
        {
            serversocket.close();
        }
        catch (IOException ignored)
        {
        }
    }
    /**
     * Returns the local Place
     *
     * @return Place
     */
    public Place getHere()
    {
        return here;
    }
    /**
     * Returns the number of milliseconds to wait between two announces
     *
     * @return Interval in milliseconds
     */
    public long getAnnouncingInterval()
    {
        return ANNOUNCING_INTERVAL;
    }
    /**
     * Returns the number of milliseconds to wait until a Place expires
     * if no announces from this Place are received
     *
     * @return Interval in milliseconds
     */
    public long getExpiryInterval()
    {
        return 3 * ANNOUNCING_INTERVAL;
    }
    /**
     * Announces the local Place to the Group
     *
     * @throws IOException if the broadcast cannot be sent
     */
    public void announce() throws IOException
    {
        String name = here.getName();
        byte[] message = name.getBytes("UTF-8");
        DatagramPacket packet = new DatagramPacket(message, message.length, groupaddress, port);
        groupsocket.send(packet);
    }
    /**
     * Receives an announcement
     *
     * @return Place that was announced
     * @throws IOException if the announcement cannot be received
     */
    public Place notice() throws IOException
    {
        byte[] message = new byte[256];
        DatagramPacket packet = new DatagramPacket(message, message.length);
        membersocket.receive(packet);
        int length = packet.getLength();
        String name = new String(message, 0, length, "UTF-8");
        Place place = Place.getInstance(name);
        //
        if (place.equals(here)) return null;
        //
        return place;
    }
    /**
     * Invokes a computation at a Place
     *
     * @param callee Place that should execute the computation
     * @param expression S-Expression to evaluate
     * @param environment Environment for evaluation
     * @param succeed Success Continuation
     * @param fail Failure Continuation
     * @return Call
     * @throws IOException if the Call cannot be made
     */
    public OutgoingCall invoke(Place callee, Sexpression expression, Environment environment, SuccessContinuation succeed, FailureContinuation fail) throws IOException
    {
        OutgoingCall call = new OutgoingCall(here, callee, environment, expression, succeed, fail);
        registerCall(call);
        //
        try
        {
            Connection connection = Connection.getInstance(callee);
            connection.send(call);
        }
        catch (IOException e)
        {
            unregisterCall(call);
            //
            throw e;
        }
        //
        return call;
    }
    //
    private void registerCall(OutgoingCall call)
    {
        /*
         * Call is put into map because it is needed
         * when the Reply is received or will be sent
         */
        Long id = call.getId();
        callsbyid.put(id, call);
    }
    //
    private void unregisterCall(OutgoingCall call)
    {
        Long id = call.getId();
        callsbyid.remove(id);
    }
    /**
     * Sends a Reply for a Call
     *
     * @param call Call that gets a Reply
     * @param symbol Thrown Symbol or null
     * @param expression Result
     * @return Reply
     * @throws IOException if the Reply cannot be send
     */
    public OutgoingReply reply(IncomingCall call, Symbol symbol, Sexpression expression) throws IOException
    {
        Place caller = call.getCaller();
        OutgoingReply reply = new OutgoingReply(call, symbol, expression);
        Connection connection = Connection.getInstance(caller);
        connection.send(reply);
        //
        return reply;
    }
    /**
     * Accepts a new Connection
     *
     * @return Connection
     * @throws IOException if the Connection cannot be accepted
     */
    public Connection accept() throws IOException
    {
        Socket clientsocket = serversocket.accept();
        //
        return Connection.createInstance(clientsocket);
    }
    /**
     * Finds a matching registered Call and unregisters it,
     * used when a Reply must be sent or is received
     *
     * @param id Call Id
     * @return matching Call or null
     */
    public OutgoingCall removeCall(Long id)
    {
        return callsbyid.remove(id);
    }
    /**
     * Returns all registered Calls
     *
     * @return Collection of Calls
     */
    public Collection<OutgoingCall> getCalls()
    {
        return callsbyid.values();
    }
}