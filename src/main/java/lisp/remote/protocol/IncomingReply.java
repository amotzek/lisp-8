package lisp.remote.protocol;
/*
 * Copyright (C) 2013, 2014, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.DataInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import lisp.Chars;
import lisp.Sexpression;
import lisp.Symbol;
/**
 * A Reply transports the result of a Call.
 *
 * Created by andreasm 16.10.13 12:15
 */
public final class IncomingReply extends IncomingMessage
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars CANNOT_UNMARSHAL = new Chars("cannot unmarshal reply");
    //
    private static final Logger logger = Logger.getLogger("lisp.remote.protocol.Protocol");
    //
    private final OutgoingCall call;
    private final Symbol symbol;
    private final Sexpression expression;
    //
    private IncomingReply(OutgoingCall call, Symbol symbol, Sexpression expression)
    {
        super();
        //
        this.call = call;
        this.symbol = symbol;
        this.expression = expression;
    }
    /**
     * Parses a Reply from an Input Stream
     *
     * @param protocol Protocol
     * @param in Input Stream
     * @return Reply
     * @throws IOException if the Reply cannot be parsed
     */
    public static IncomingReply unmarshal(Protocol protocol, DataInputStream in) throws IOException
    {
        Long id = readMessageHeader(TYPE_REPLY, in);
        OutgoingCall call = protocol.removeCall(id);
        //
        if (call == null) throw new IOException("no call with id " + id);
        //
        Marshaller marshaller = call.getMarshaller();
        //
        try
        {
            Symbol symbol = Marshaller.readSymbol(in);
            Sexpression expression = marshaller.readExpression(in);
            logger.info("received response for call " + id);
            //
            return new IncomingReply(call, symbol, expression);
        }
        catch (IOException e)
        {
            logger.log(Level.WARNING, "cannot unmarshal", e);
        }
        //
        return new IncomingReply(call, ERROR, CANNOT_UNMARSHAL);
    }
    /**
     * @see Message#getType()
     */
    @Override
    public int getType()
    {
        return TYPE_REPLY;
    }
    /**
     * @see Message#getId()
     */
    @Override
    public Long getId()
    {
        return call.getId();
    }
    /**
     * Returns the Call
     *
     * @return Call
     */
    public OutgoingCall getCall()
    {
        return call;
    }
    /**
     * Thrown Symbol or null
     *
     * @return Symbol or null
     */
    public Symbol getSymbol()
    {
        return symbol;
    }
    /**
     * Result of a Call
     *
     * @return Result
     */
    public Sexpression getExpression()
    {
        return expression;
    }
}