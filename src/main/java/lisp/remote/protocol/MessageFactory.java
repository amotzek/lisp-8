package lisp.remote.protocol;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Combinator;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Collection;
/**
 * Created by andreasm 16.10.13 08:49
 */
final class MessageFactory
{
    private final Protocol protocol;
    private final Collection<Combinator> combinators;
    private final DataInputStream in;
    //
    public MessageFactory(Protocol protocol, Collection<Combinator> combinators, byte[] bytes)
    {
        super();
        //
        this.protocol = protocol;
        this.combinators = combinators;
        //
        in = new DataInputStream(new ByteArrayInputStream(bytes));
    }
    //
    public Message getMessage() throws IOException
    {
        in.mark(4);
        int messagetype = in.readInt();
        in.reset();
        //
        switch (messagetype)
        {
            case Message.TYPE_CALL:
                return IncomingCall.unmarshal(combinators, in);
            //
            case Message.TYPE_REPLY:
                return IncomingReply.unmarshal(protocol, in);
        }
        //
        throw new IOException("unknown message type " + messagetype);
    }
}