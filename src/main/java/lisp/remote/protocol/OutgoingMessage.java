package lisp.remote.protocol;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.DataOutputStream;
import java.io.IOException;
/**
 * Instances represent outgoing messages
 *
 * Created by andreas-motzek@t-online.de 25.10.13
 */
public abstract class OutgoingMessage extends Message
{
    /**
     * Serializes this Message to an Output Stream
     *
     * @param out Output Stream
     * @throws java.io.IOException if this Message cannot be serialized
     */
    public abstract void marshal(DataOutputStream out) throws IOException;
    /**
     * Writes the Header of this Message to a Output Stream
     *
     * @param out Output Stream
     * @throws IOException if the Message Header cannot be written
     */
    protected final void writeMessageHeader(DataOutputStream out) throws IOException
    {
        int type = getType();
        long id = getId();
        /*
         * Message type must be first in header
         * because MessageFactory looks at it there
         */
        out.writeInt(type);
        out.writeLong(id);
    }
}