package lisp.remote.protocol;
/*
 * Copyright (C) 2013, 2014, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import lisp.Array;
import lisp.AssociativeContainer;
import lisp.Bytes;
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.Combinator;
import lisp.Function;
import lisp.GenericFunction;
import lisp.GuardedMethod;
import lisp.HashTable;
import lisp.Lambda;
import lisp.List;
import lisp.Mlambda;
import lisp.ParameterizedBody;
import lisp.Place;
import lisp.Rational;
import lisp.RubyStyleObject;
import lisp.Sexpression;
import lisp.SimpleClass;
import lisp.Symbol;
import lisp.combinator.EnvironmentFactory;
import lisp.combinator.InlinedLambda;
import lisp.compiler.Compiler;
import lisp.environment.Environment;
import lisp.environment.NotBoundException;
/**
 * Every Call has a Marshaller. The Marshaller is used to serialize and parse
 * Environments and S-Expressions to Output Streams or from Input Streams.
 *
 * Created by andreasm 17.10.13
 */
final class Marshaller
{
    private static final Function UNSAFE_FUNCTION = new UnsaveFunction();
    private static final Symbol MAKE_COMBINATOR = Symbol.createSymbol("make-combinator");
    private static final int COLLECTION_SIZE = 10000;
    private static final int BYTES_SIZE = 524288;
    //
    private static final int NULL = 0;
    private static final int ENVIRONMENT_REFERENCE = 1;
    private static final int EXPRESSION_REFERENCE = 2;
    private static final int ENVIRONMENT = 3;
    private static final int SYMBOL = 4;
    private static final int CHARS = 5;
    private static final int RATIONAL = 6;
    private static final int BYTES = 7;
    private static final int PLACE = 8;
    private static final int CHANNEL = 9;
    private static final int LOCK = 10;
    private static final int FUTURE = 11;
    private static final int LIST = 12;
    private static final int CLASS = 13;
    private static final int TRAIT = 14;
    private static final int INSTANCE = 15;
    private static final int ARRAY = 16;
    private static final int HASH_TABLE = 17;
    private static final int LAMBDA = 18;
    private static final int INLINED_LAMBDA = 19;
    private static final int MLAMBDA = 20;
    private static final int GENERIC_FUNCTION = 21;
    private static final int COMBINATOR = 22;
    private static final int ROOT_ENVIRONMENT = 23;
    //
    private final IdentityHashMap<Environment, Integer> idsbyenvironment;
    private final IdentityHashMap<Sexpression, Integer> idsbyexpression;
    private final HashMap<Integer, Environment> environmentsbyid;
    private final HashMap<Integer, Sexpression> expressionsbyid;
    private final Collection<Combinator> combinators;
    private int nextid;
    /**
     * Creates a Marshaller
     */
    public Marshaller()
    {
        this(null);
    }
    /**
     * Creates a Marshaller
     */
    public Marshaller(Collection<Combinator> combinators)
    {
        super();
        //
        idsbyenvironment = new IdentityHashMap<>();
        idsbyexpression = new IdentityHashMap<>();
        environmentsbyid = new HashMap<>();
        expressionsbyid = new HashMap<>();
        //
        this.combinators = combinators;
    }
    /**
     * Writes a Symbol to an Output Stream
     *
     * @param symbol Symbol
     * @param out Output Stream
     * @throws IOException if the Symbol cannot be written
     */
    public static void writeSymbol(Symbol symbol, DataOutputStream out) throws IOException
    {
        if (symbol == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        String name = symbol.getName();
        out.writeInt(SYMBOL);
        writeString(name, out);
    }
    /**
     * Reads a Symbol from an Input Stream
     *
     * @param in Input Stream
     * @return Symbol
     * @throws IOException if the Symbol cannot be read
     */
    public static Symbol readSymbol(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == SYMBOL)
        {
            String name = readString(in);
            //
            return Symbol.createSymbol(name);
        }
        //
        throw new IOException("unexpected tag for symbol " + tag);
    }
    //
    private static void writeChars(Chars chars, DataOutputStream out) throws IOException
    {
        if (chars == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        String string = chars.getString();
        out.writeInt(CHARS);
        writeString(string, out);
    }
    //
    private static Chars readChars(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == CHARS)
        {
            String string = readString(in);
            //
            return new Chars(string);
        }
        //
        throw new IOException("unexpected tag for string " + tag);
    }
    //
    private static void writeRational(Rational rational, DataOutputStream out) throws IOException
    {
        if (rational == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        StringBuilder builder = new StringBuilder();
        builder.append(rational.getNumerator());
        builder.append("/");
        builder.append(rational.getDenominator());
        out.writeInt(RATIONAL);
        writeString(builder.toString(), out);
    }
    //
    private static Rational readRational(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == RATIONAL)
        {
            String string = readString(in);
            //
            return new Rational(string);
        }
        //
        throw new IOException("unexpected tag for rational " + tag);
    }
    //
    private static void writeBytes(Bytes bytes, DataOutputStream out) throws IOException
    {
        byte[] bytearray = bytes.getByteArray();
        int length = bytearray.length;
        out.writeInt(BYTES);
        out.writeInt(length);
        out.write(bytearray);
    }
    //
    private static Bytes readBytes(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == BYTES)
        {
            int length = in.readInt();
            //
            if (length > BYTES_SIZE) throw new IOException("byte array too large");
            //
            byte[] bytearray = new byte[length];
            in.readFully(bytearray);
            //
            return new Bytes(bytearray);
        }
        //
        throw new IOException("unexpected tag for bytes " + tag);
    }
    /**
     * Writes a Place to an Output Stream
     *
     * @param place Place
     * @param out Output Stream
     * @throws IOException if the Place cannot be writted
     */
    public static void writePlace(Place place, DataOutputStream out) throws IOException
    {
        String name = place.getName();
        out.writeInt(PLACE);
        writeString(name, out);
    }
    /**
     * Reads a Place from an Input Stream
     *
     * @param in Input Stream
     * @return Place
     * @throws IOException if the Place cannot be read
     */
    public static Place readPlace(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == PLACE)
        {
            String name = readString(in);
            //
            try
            {
                return Place.getInstance(name);
            }
            catch (NoSuchElementException | NumberFormatException ignored)
            {
            }
            //
            throw new IOException("invalid syntax for a place " + name);
        }
        //
        throw new IOException("unexpected tag for place " + tag);
    }
    /**
     * Writes a String to an Output Stream
     *
     * @param string String
     * @param out Output Stream
     * @throws IOException if the String cannot be written
     */
    private static void writeString(String string, DataOutputStream out) throws IOException
    {
        if (string.isEmpty())
        {
            out.writeInt(0);
            //
            return;
        }
        //
        byte[] bytes = string.getBytes("UTF-8");
        out.writeInt(bytes.length);
        out.write(bytes);
    }
    /**
     * Reads a String from an Input Stream
     *
     * @param in Input Stream
     * @return String
     * @throws IOException if the String cannot be read
     */
    private static String readString(DataInputStream in) throws IOException
    {
        int length = in.readInt();
        //
        if (length > BYTES_SIZE) throw new IOException("string too large");
        //
        byte[] bytes = new byte[length];
        in.readFully(bytes);
        //
        return new String(bytes, "UTF-8");
    }
    //
    private static List toSingletonList(Sexpression first)
    {
        return new List(first, null);
    }
    //
    private static LinkedList<SimpleClass> toSimpleClassLinkedList(List list) throws IOException
    {
        LinkedList<SimpleClass> linkedlist = new LinkedList<>();
        //
        while (list != null)
        {
            Sexpression element = list.first();
            //
            if (!(element instanceof SimpleClass)) throw new IOException("unexpected non-superclass " + element);
            //
            linkedlist.addLast((SimpleClass) element);
            list = list.rest();
        }
        //
        return linkedlist;
    }
    //
    private static SimpleClass[] toSimpleClassArray(List list) throws IOException
    {
        int length = List.length(list);
        SimpleClass[] array = new SimpleClass[length];
        int i = 0;
        //
        while (list != null)
        {
            Sexpression element = list.first();
            //
            if (element != null && !(element instanceof SimpleClass)) throw new IOException("expected a class");
            //
            array[i] = (SimpleClass) element;
            list = list.rest();
            i++;
        }
        //
        return array;
    }
    //
    private static Function createCombinator(String name) throws IOException
    {
        if (!isUnsafeCombinatorName(name))
        {
            try
            {
                Class combinatorclass = Class.forName(name);
                //
                return (Combinator) combinatorclass.newInstance();
            }
            catch (Exception ignore)
            {
            }
        }
        //
        return UNSAFE_FUNCTION;
    }
    //
    private static boolean isUnsafeCombinatorName(String name)
    {
        if (name == null) return true;
        //
        if (name.equals("lisp.combinator.MakeCombinator")) return true;
        //
        if (name.startsWith("lisp.combinator.") && name.lastIndexOf('.') == 15) return false;
        //
        return true;
    }
    /**
     * Returns the current Object Id
     *
     * @return Id
     */
    public int getCurrentId()
    {
        return nextid;
    }
    /**
     * Serializes an Environment to an Output Stream
     *
     * @param environment Environment
     * @param out Output Stream
     * @throws IOException if the Environment cannot be serialized
     */
    public void writeEnvironment(Environment environment, DataOutputStream out) throws IOException
    {
        if (environment == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        if (environment.getParent() == null && environment.isFrozen())
        {
            out.writeInt(ROOT_ENVIRONMENT);
            //
            return;
        }
        //
        Integer id = idsbyenvironment.get(environment);
        //
        if (id != null)
        {
            out.writeInt(ENVIRONMENT_REFERENCE);
            out.writeInt(id);
            //
            return;
        }
        //
        id = register(environment);
        Environment parent = environment.getParent();
        LinkedList<Symbol> names = environment.getNames();
        int size = names.size();
        out.writeInt(ENVIRONMENT);
        out.writeInt(id);
        writeEnvironment(parent, out);
        out.writeInt(size);
        //
        for (Symbol name : names)
        {
            try
            {
                Sexpression value = environment.at(name);
                writeSymbol(name, out);
                writeExpression(value, out);
            }
            catch (NotBoundException e)
            {
                throw new IOException("no value for " + name);
            }
        }
    }
    /**
     * Parses an Environment from an Input Stream
     *
     * @param in Input Stream
     * @return Environment
     * @throws IOException if the Environment cannot be parsed
     */
    public Environment readEnvironment(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == ROOT_ENVIRONMENT)
        {
            Environment environment = EnvironmentFactory.createEnvironment();
            Environment sibling = new Environment(environment.getParent());
            //
            if (combinators != null)
            {
                for (Combinator combinator : combinators)
                {
                    Symbol name = Symbol.createSymbol(combinator.toString());
                    sibling.add(false, name, combinator);
                }
            }
            //
            sibling.add(false, MAKE_COMBINATOR, null); // make-combinator is unsafe
            //
            return sibling;
        }
        //
        if (tag == ENVIRONMENT_REFERENCE)
        {
            Integer id = in.readInt();
            Environment environment = environmentsbyid.get(id);
            //
            if (environment == null) throw new IOException("no environment for id " + id);
            //
            return environment;
        }
        //
        if (tag == ENVIRONMENT)
        {
            Integer id = in.readInt();
            Environment environment = new Environment();
            register(id, environment);
            Environment parent = readEnvironment(in);
            environment.setParent(parent);
            int size = in.readInt();
            //
            for (int i = 0; i < size; i++)
            {
                Symbol name = readSymbol(in);
                Sexpression value = readExpression(in);
                environment.add(false, name, value);
            }
            //
            return environment;
        }
        //
        throw new IOException("unexpected tag for environment " + tag);
    }
    /**
     * Serializes a S-Expression to an Output Stream
     *
     * @param expression S-Expression
     * @param out Output Stream
     * @throws IOException if the S-Expression cannot be serialized
     */
    public void writeExpression(Sexpression expression, DataOutputStream out) throws IOException
    {
        if (expression == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        if (expression instanceof Symbol)
        {
            writeSymbol((Symbol) expression, out);
            //
            return;
        }
        //
        if (expression instanceof Chars)
        {
            writeChars((Chars) expression, out);
            //
            return;
        }
        //
        if (expression instanceof Rational)
        {
            writeRational((Rational) expression, out);
            //
            return;
        }
        //
        if (expression instanceof Bytes)
        {
            writeBytes((Bytes) expression, out);
            //
            return;
        }
        //
        if (expression instanceof Place)
        {
            writePlace((Place) expression, out);
            //
            return;
        }
        //
        Integer id = idsbyexpression.get(expression);
        //
        if (id != null)
        {
            out.writeInt(EXPRESSION_REFERENCE);
            out.writeInt(id);
            //
            return;
        }
        //
        if (expression instanceof List)
        {
            writeList((List) expression, out);
            //
            return;
        }
        //
        if (expression instanceof SimpleClass)
        {
            writeClass((SimpleClass) expression, out);
            //
            return;
        }
        //
        if (expression instanceof RubyStyleObject)
        {
            writeInstance((RubyStyleObject) expression, out);
            //
            return;
        }
        //
        if (expression instanceof Array)
        {
            writeArray((Array) expression, out);
            //
            return;
        }
        //
        if (expression instanceof HashTable)
        {
            writeHashTable((HashTable) expression, out);
            //
            return;
        }
        //
        if (expression instanceof Lambda)
        {
            writeLambda((Lambda) expression, out);
            //
            return;
        }
        //
        if (expression instanceof InlinedLambda)
        {
            writeInlinedLambda((InlinedLambda) expression, out);
            //
            return;
        }
        //
        if (expression instanceof Mlambda)
        {
            writeMlambda((Mlambda) expression, out);
            //
            return;
        }
        //
        if (expression instanceof GenericFunction)
        {
            writeGenericFunction((GenericFunction) expression, out);
            //
            return;
        }
        //
        if (expression instanceof Combinator)
        {
            writeCombinator((Combinator) expression, out);
            //
            return;
        }
        //
        throw new IOException("cannot marshal expressions of type " + expression.getType());
    }
    /**
     * Parses a S-Expression from an Input Stream
     *
     * @param in Input Stream
     * @return S-Expression
     * @throws IOException if the S-Expression cannot be parsed
     */
    public Sexpression readExpression(DataInputStream in) throws IOException
    {
        in.mark(4);
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == EXPRESSION_REFERENCE)
        {
            Integer id = in.readInt();
            Sexpression expression = expressionsbyid.get(id);
            //
            if (expression == null) throw new IOException("no expression for id " + id);
            //
            return expression;
        }
        //
        in.reset();
        //
        switch (tag)
        {
            case SYMBOL:
                return readSymbol(in);
            //
            case CHARS:
                return readChars(in);
            //
            case RATIONAL:
                return readRational(in);
            //
            case BYTES:
                return readBytes(in);
            //
            case PLACE:
                return readPlace(in);
            //
            case LIST:
                return readList(in);
            //
            case CLASS:
            case TRAIT:
                return readClass(in);
            //
            case INSTANCE:
                return readInstance(in);
            //
            case ARRAY:
                return readArray(in);
            //
            case HASH_TABLE:
                return readHashTable(in);
            //
            case LAMBDA:
                return readLambda(in);
            //
            case INLINED_LAMBDA:
                return readInlinedLambda(in);
            //
            case MLAMBDA:
                return readMlambda(in);
            //
            case GENERIC_FUNCTION:
                return readGenericFunction(in);
            //
            case COMBINATOR:
                return readCombinator(in);
        }
        //
        throw new IOException("unexpected tag " + tag);
    }
    //
    private void writeList(List list, DataOutputStream out) throws IOException
    {
        if (list == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        int id = register(list);
        int size = List.length(list);
        out.writeInt(LIST);
        out.writeInt(id);
        out.writeInt(size);
        //
        while (list != null)
        {
            Sexpression expression = list.first();
            writeExpression(expression, out);
            list = list.rest();
        }
    }
    //
    private List readList(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == LIST)
        {
            Integer id = in.readInt();
            int size = in.readInt();
            //
            if (size < 1) throw new IOException("list length must be positive");
            //
            if (size > COLLECTION_SIZE) throw new IOException("list length is too large");
            //
            Sexpression element = readExpression(in);
            List head = toSingletonList(element);
            register(id, head);
            List tail = head;
            size--;
            //
            while (size > 0)
            {
                element = readExpression(in);
                List next = toSingletonList(element);
                tail.setRest(next);
                tail = next;
                size--;
            }
            //
            return head;
        }
        //
        throw new IOException("unexpected tag for list " + tag);
    }
    //
    private void writeClass(SimpleClass simpleclass, DataOutputStream out) throws IOException
    {
        if (simpleclass == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        int id = register(simpleclass);
        boolean allocatable = simpleclass.isAllocatable();
        Symbol name = simpleclass.getClassName();
        List superclasses = simpleclass.getSuperclasses();
        out.writeInt(allocatable ? CLASS : TRAIT);
        out.writeInt(id);
        writeSymbol(name, out);
        writeList(superclasses, out);
    }
    //
    private SimpleClass readClass(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag != CLASS && tag != TRAIT) throw new IOException("unexpected tag for class " + tag);
        //
        Integer id = in.readInt();
        boolean allocatable = (tag == CLASS);
        Symbol name = readSymbol(in);
        LinkedList<SimpleClass> superclasses = toSimpleClassLinkedList(readList(in));
        SimpleClass simpleclass = new SimpleClass(name, allocatable, superclasses);
        register(id, simpleclass);
        //
        return simpleclass;
    }
    //
    private void writeInstance(RubyStyleObject object, DataOutputStream out) throws IOException
    {
        if (object == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        int id = register(object);
        SimpleClass simpleclass = object.classOf();
        out.writeInt(INSTANCE);
        out.writeInt(id);
        writeExpression(simpleclass, out);
        writeContainer(object, out);
    }
    //
    private RubyStyleObject readInstance(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == INSTANCE)
        {
            Integer id = in.readInt();
            Sexpression expression = readExpression(in);
            //
            if (!(expression instanceof SimpleClass)) throw new IOException("expected a class");
            //
            SimpleClass simpleclass = (SimpleClass) expression;
            RubyStyleObject object = new RubyStyleObject(simpleclass);
            register(id, object);
            fillContainer(in, object);
            //
            return object;
        }
        //
        throw new IOException("unexpected tag for instance " + tag);
    }
    //
    private void writeArray(Array array, DataOutputStream out) throws IOException
    {
        if (array == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        int id = register(array);
        int[] dimensions = array.getDimensions();
        int rank = dimensions.length;
        out.writeInt(ARRAY);
        out.writeInt(id);
        out.writeInt(rank);
        //
        for (int dimension : dimensions)
        {
            out.writeInt(dimension);
        }
        //
        writeContainer(array, out);
    }
    //
    private Array readArray(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == ARRAY)
        {
            Integer id = in.readInt();
            int rank = in.readInt();
            //
            if (rank <= 0) throw new IOException("array rank must be positive");
            //
            if (rank > COLLECTION_SIZE) throw new IOException("array rank is too large");
            //
            int[] dimensions = new int[rank];
            int size = 1;
            //
            for (int i = 0; i < rank; i++)
            {
                int dimension = in.readInt();
                //
                if (dimension <= 0) throw new IOException("array dimension must be positive");
                //
                dimensions[i] = dimension;
                size *= dimension;
                //
                if (size > COLLECTION_SIZE) throw new IOException("array size is too large");
            }
            //
            Array array = new Array(dimensions);
            register(id, array);
            fillContainer(in, array);
            //
            return array;
        }
        //
        throw new IOException("unexpected tag for array " + tag);
    }
    //
    private void writeHashTable(HashTable hashtable, DataOutputStream out) throws IOException
    {
        if (hashtable == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        int id = register(hashtable);
        out.writeInt(HASH_TABLE);
        out.writeInt(id);
        writeContainer(hashtable, out);
    }
    //
    private HashTable readHashTable(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == HASH_TABLE)
        {
            HashTable hashtable = new HashTable();
            Integer id = in.readInt();
            register(id, hashtable);
            fillContainer(in, hashtable);
            //
            return hashtable;
        }
        //
        throw new IOException("unexpected tag for hash-table " + tag);
    }
    //
    private void writeLambda(Lambda lambda, DataOutputStream out) throws IOException
    {
        if (lambda == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        int id = register(lambda);
        out.writeInt(LAMBDA);
        out.writeInt(id);
        writeParameterizedBody(lambda, out);
    }
    //
    private Lambda readLambda(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == LAMBDA)
        {
            Integer id = in.readInt();
            Environment environment = readEnvironment(in);
            List parameters = readList(in);
            Sexpression body = readExpression(in);
            //
            try
            {
                Compiler compiler = new Compiler(parameters, body, environment);
                compiler.compile();
                Sexpression compiledbody = compiler.getResult();
                Lambda lambda = new Lambda(parameters, body, compiledbody, environment);
                register(id, lambda);
                //
                return lambda;
            }
            catch (CannotEvalException e)
            {
                throw new IOException("cannot compile");
            }
        }
        //
        throw new IOException("unexpected tag for lambda " + tag);
    }
    //
    private void writeInlinedLambda(InlinedLambda lambda, DataOutputStream out) throws IOException
    {
        if (lambda == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        int id = register(lambda);
        List parameters = lambda.getParameters();
        List body = lambda.getUncompiledBody();
        out.writeInt(INLINED_LAMBDA);
        out.writeInt(id);
        writeList(parameters, out);
        writeList(body, out);
    }
    //
    private InlinedLambda readInlinedLambda(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == INLINED_LAMBDA)
        {
            Integer id = in.readInt();
            List parameters = readList(in);
            List body = readList(in);
            InlinedLambda lambda = new InlinedLambda(parameters, body);
            register(id, lambda);
            //
            return lambda;
        }
        //
        throw new IOException("unexpected tag for inlined-lambda " + tag);
    }
    //
    private void writeMlambda(Mlambda mlambda, DataOutputStream out) throws IOException
    {
        if (mlambda == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        int id = register(mlambda);
        out.writeInt(MLAMBDA);
        out.writeInt(id);
        writeParameterizedBody(mlambda, out);
    }
    //
    private Mlambda readMlambda(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == MLAMBDA)
        {
            Integer id = in.readInt();
            Environment environment = readEnvironment(in);
            List parameters = readList(in);
            Sexpression body = readExpression(in);
            //
            if (parameters.rest() != null) throw new IOException("more than one parameter for mlambda");
            //
            try
            {
                Compiler compiler = new Compiler(parameters, body, environment);
                compiler.compile();
                Sexpression compiledbody = compiler.getResult();
                Mlambda mlambda = new Mlambda(parameters, body, compiledbody, environment);
                register(id, mlambda);
                //
                return mlambda;
            }
            catch (CannotEvalException e)
            {
                throw new IOException("cannot compile");
            }
        }
        //
        throw new IOException("unexpected tag for mlambda " + tag);
    }
    //
    private void writeGenericFunction(GenericFunction function, DataOutputStream out) throws IOException
    {
        if (function == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        int id = register(function);
        Symbol name = function.getFunctionName();
        List methods = function.getMethods();
        int size = List.length(methods);
        out.writeInt(GENERIC_FUNCTION);
        out.writeInt(id);
        writeSymbol(name, out);
        out.writeInt(size);
        //
        while (methods != null)
        {
            GuardedMethod method = (GuardedMethod) methods.first();
            Sexpression guard = method.getGuard();
            List specializers = method.getSpezializers();
            writeExpression(guard, out);
            writeList(specializers, out);
            writeParameterizedBody(method, out);
            methods = methods.rest();
        }
    }
    //
    private GenericFunction readGenericFunction(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == GENERIC_FUNCTION)
        {
            Integer id = in.readInt();
            Symbol name = readSymbol(in);
            int size = in.readInt();
            //
            if (size > COLLECTION_SIZE) throw new IOException("too many methods in generic function");
            //
            GenericFunction function = new GenericFunction(name);
            register(id, function);
            //
            while (size > 0)
            {
                Sexpression guard = readExpression(in);
                SimpleClass[] parameterclasses = toSimpleClassArray(readList(in));
                Environment environment = readEnvironment(in);
                List parameters = readList(in);
                Sexpression body = readExpression(in);
                //
                try
                {
                    Compiler compiler = new Compiler(parameters, body, environment);
                    compiler.compile();
                    Sexpression compiledbody = compiler.getResult();
                    GuardedMethod method = new GuardedMethod(parameters, parameterclasses, guard, body, compiledbody, environment);
                    function.addMethod(method);
                }
                catch (CannotEvalException e)
                {
                    throw new IOException("cannot compile");
                }
                //
                size--;
            }
            //
            return function;
        }
        //
        throw new IOException("unexpected tag for generic-function " + tag);
    }
    //
    private void writeCombinator(Combinator combinator, DataOutputStream out) throws IOException
    {
        if (combinator == null)
        {
            out.writeInt(NULL);
            //
            return;
        }
        //
        String name = combinator.getClass().getCanonicalName();
        //
        if (isUnsafeCombinatorName(name)) name = "";
        //
        int id = register(combinator);
        out.writeInt(COMBINATOR);
        out.writeInt(id);
        writeString(name, out);
    }
    //
    private Function readCombinator(DataInputStream in) throws IOException
    {
        int tag = in.readInt();
        //
        if (tag == NULL) return null;
        //
        if (tag == COMBINATOR)
        {
            Integer id = in.readInt();
            String name = readString(in);
            Function combinator = createCombinator(name);
            register(id, combinator);
            //
            return combinator;
        }
        //
        throw new IOException("unexpected tag for combinator " + tag);
    }
    //
    private <K extends Sexpression> void writeContainer(AssociativeContainer<K> container, DataOutputStream out) throws IOException
    {
        boolean frozen = container.isFrozen();
        int size = container.size();
        Iterator<K> keys = container.keyIterator();
        out.writeBoolean(frozen);
        out.writeInt(size);
        //
        while (keys.hasNext())
        {
            K key = keys.next();
            Sexpression value = container.get(key);
            writeExpression(key, out);
            writeExpression(value, out);
        }
    }
    //
    @SuppressWarnings("unchecked")
    private <K extends Sexpression> void fillContainer(DataInputStream in, AssociativeContainer<K> container) throws IOException
    {
        boolean frozen = in.readBoolean();
        int size = in.readInt();
        //
        if (size > COLLECTION_SIZE) throw new IOException("container size too large");
        //
        try
        {
            while (size > 0)
            {
                Sexpression key = readExpression(in);
                Sexpression value = readExpression(in);
                container.put((K) key, value);
                size--;
            }
        }
        catch (ClassCastException e)
        {
            throw new IOException("key with wrong type in container");
        }
        //
        if (frozen) container.freeze();
    }
    //
    private void writeParameterizedBody(ParameterizedBody parameterizebody, DataOutputStream out) throws IOException
    {
        Environment environment = parameterizebody.getEnvironment();
        List parameters = parameterizebody.getParameters();
        Sexpression body = parameterizebody.getBody();
        writeEnvironment(environment, out);
        writeList(parameters, out);
        writeExpression(body, out);
    }
    //
    private Integer register(Environment environment)
    {
        Integer id = getNextId();
        register(id, environment);
        //
        return id;
    }
    //
    private Integer register(Sexpression expression)
    {
        Integer id = getNextId();
        register(id, expression);
        //
        return id;
    }
    //
    private void register(Integer id, Environment environment)
    {
        idsbyenvironment.put(environment, id);
        environmentsbyid.put(id, environment);
        //
        if (nextid <= id) nextid = id + 1;
    }
    //
    private void register(Integer id, Sexpression expression)
    {
        idsbyexpression.put(expression, id);
        expressionsbyid.put(id, expression);
        //
        if (nextid <= id) nextid = id + 1;
    }
    //
    private Integer getNextId()
    {
        return nextid++;
    }
}