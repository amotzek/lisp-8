package lisp.remote.protocol;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.DataInputStream;
import java.io.IOException;
/**
 * Instances represent incoming messages.
 *
 * Created by andreasm 25.10.13
 */
abstract class IncomingMessage extends Message
{
    /**
     * Reads a Message Header from an Input Stream
     *
     * @param expectedtype Message Type
     * @param in Input Stream
     * @return Message Id
     * @throws IOException if the Message Header cannot be read
     */
    protected static Long readMessageHeader(int expectedtype, DataInputStream in) throws IOException
    {
        int type = in.readInt();
        long id = in.readLong();
        //
        if (type != expectedtype) throw new IOException("unexpected message type " + type);
        //
        return id;
    }
}