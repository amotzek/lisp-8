package lisp.remote.protocol;
/*
 * Copyright (C) 2013, 2014, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import lisp.Chars;
import lisp.Combinator;
import lisp.Place;
import lisp.Symbol;
/*
 * Instances represent one-way Connections between
 * the local Place and another Place
 *
 * Created by andreasm 16.10.13 08:13
 */
public final class Connection
{
    private static final int MESSAGE_SIZE = 524288;
    private static final int SOCKET_TIMEOUT = 30000;
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars EXCESSIVE_MESSAGE_SIZE = new Chars("excessive message size");
    //
    private static final ConcurrentHashMap<Place, Connection> connectionsbyplace = new ConcurrentHashMap<>(32);
    //
    private final Place place;
    private final Socket clientsocket;
    private final DataInputStream in;
    private final DataOutputStream out;
    private final Object sendlock;
    private final Object receivelock;
    private volatile long lastuse;
    //
    private Connection(Place place, Socket clientsocket) throws IOException
    {
        super();
        //
        this.place = place;
        this.clientsocket = clientsocket;
        //
        in = new DataInputStream(clientsocket.getInputStream());
        out = new DataOutputStream(clientsocket.getOutputStream());
        sendlock = new Object();
        receivelock = new Object();
        lastuse = System.currentTimeMillis();
    }
    /**
     * Recycles a Connection to the given Place or creates a new one
     * and registers it for recycling
     *
     * @param place Place to connect to
     * @return Connection
     * @throws IOException if no TCP connection can be established
     */
    public static Connection getInstance(Place place) throws IOException
    {
        Connection connection = connectionsbyplace.get(place);
        //
        if (canUse(connection)) return connection;
        //
        String host = place.getHost();
        int port = place.getPort();
        Socket clientsocket = new Socket(host, port);
        clientsocket.setTcpNoDelay(true);
        clientsocket.setSoTimeout(SOCKET_TIMEOUT);
        connection = new Connection(place, clientsocket);
        connectionsbyplace.put(place, connection);
        //
        return connection;
    }
    //
    private static boolean canUse(Connection connection)
    {
        if (connection == null) return false;
        //
        long now = System.currentTimeMillis();
        long age = now - connection.lastuse;
        //
        return age < SOCKET_TIMEOUT;
    }
    /**
     * Creates an unregistered Connection from an existing Socket
     *
     * @param clientsocket Socket
     * @return Connection
     * @throws IOException if the Socket options cannot be set
     */
    public static Connection createInstance(Socket clientsocket) throws IOException
    {
        clientsocket.setTcpNoDelay(true);
        clientsocket.setSoTimeout(SOCKET_TIMEOUT);
        //
        return new Connection(null, clientsocket);
    }
    /**
     * Creates a Reply signalling an error as replacement for an other Reply
     *
     * @param original Reply
     * @return replacement Reply
     */
    private static OutgoingReply createErrorReply(OutgoingReply original)
    {
        IncomingCall call = original.getCall();
        OutgoingReply replacement = new OutgoingReply(call, ERROR, EXCESSIVE_MESSAGE_SIZE);
        //
        return replacement;
    }
    /**
     * Sends a Message on this Connection
     *
     * @param message Message
     * @throws IOException if the message cannot be serialized or sent
     */
    public void send(OutgoingMessage message) throws IOException
    {
        ByteArrayOutputStream byteout = new ByteArrayOutputStream();
        LimitedOutputStream limitout = new LimitedOutputStream(byteout, MESSAGE_SIZE);
        DataOutputStream dataout = new DataOutputStream(limitout);
        //
        try
        {
            message.marshal(dataout);
            dataout.flush();
        }
        catch (EOFException e)
        {
            if (message instanceof OutgoingReply)
            {
                OutgoingReply replacementreply = createErrorReply((OutgoingReply) message);
                byteout = new ByteArrayOutputStream();
                dataout = new DataOutputStream(byteout);
                replacementreply.marshal(dataout);
                dataout.flush();
            }
            else
            {
                throw e;
            }
        }
        //
        byte[] body = byteout.toByteArray();
        //
        synchronized (sendlock)
        {
            out.writeInt(body.length);
            out.write(body);
            out.flush();
        }
        //
        lastuse = System.currentTimeMillis();
    }
    /**
     * Receives and parses a Message from this Connection
     *
     * @param protocol Protocol
     * @param combinators Combinators
     * @return Message
     * @throws IOException if a Message cannot be received or parsed
     */
    public Message receive(Protocol protocol, Collection<Combinator> combinators) throws IOException
    {
        byte[] body;
        //
        synchronized (receivelock)
        {
            int length = in.readInt();
            //
            if (length <= 0) throw new IOException("corrupted message size: " + length);
            //
            if (length > MESSAGE_SIZE) throw new IOException("excessive message size: " + length);
            //
            body = new byte[length];
            in.readFully(body);
        }
        //
        MessageFactory factory = new MessageFactory(protocol, combinators, body);
        //
        return factory.getMessage();
    }
    /**
     * Closes and (if needed) unregisters this Connection and the Socket
     */
    public void close()
    {
        if (place != null) connectionsbyplace.remove(place, this);
        //
        try
        {
            clientsocket.close();
        }
        catch (IOException ignored)
        {
        }
    }
}