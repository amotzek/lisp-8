package lisp.remote.protocol;
/*
 * Copyright (C) 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Constant;
import lisp.Function;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.concurrent.RunnableQueue;
/*
 * Created by andreasm on 20.03.16.
 */
final class UnsaveFunction extends Constant implements Function
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars MESSAGE = new Chars("application of unsafe combinator");
    //
    public UnsaveFunction()
    {
    }
    //
    @Override
    public boolean acceptsArgumentCount(int argumentcount)
    {
        return true;
    }
    //
    @Override
    public boolean isMacro()
    {
        return false;
    }
    //
    @Override
    public boolean mustBeQuoted(int position)
    {
        return true;
    }
    //
    @Override
    public void apply(RunnableQueue runnablequeue, Environment environment, Sexpression[] arguments, SuccessContinuation succeed, FailureContinuation fail)
    {
        fail.fail(ERROR, MESSAGE);
    }
    //
    @Override
    public String getType()
    {
        return "combinator";
    }
}