package lisp.remote.protocol;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Place;
import lisp.Sexpression;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;
/**
 * A Call represents the remote evaluation of
 * a S-Expression in an Environment issued by
 * one Place and executed at another.
 *
 * Created by andreasm 16.10.13 07:57
 */
public final class OutgoingCall extends OutgoingMessage
{
    private static final AtomicLong counter = new AtomicLong();
    //
    private final Place caller;
    private final Place callee;
    private final Environment environment;
    private final Sexpression expression;
    private final SuccessContinuation succeed;
    private final FailureContinuation fail;
    private final Long id;
    private final Marshaller marshaller;
    /**
     * Creates a Call, used in the calling Place
     *
     * @param caller Calling place
     * @param callee Called place
     * @param environment Environment
     * @param expression S-Expression
     * @param succeed Success Continuation
     * @param fail Failure Continuation
     */
    public OutgoingCall(Place caller, Place callee, Environment environment, Sexpression expression, SuccessContinuation succeed, FailureContinuation fail)
    {
        super();
        //
        this.caller = caller;
        this.callee = callee;
        this.environment = environment;
        this.expression = expression;
        this.succeed = succeed;
        this.fail = fail;
        //
        id = counter.incrementAndGet();
        marshaller = new Marshaller();
    }
    /**
     * @see lisp.remote.protocol.Message#getType()
     */
    @Override
    public int getType()
    {
        return TYPE_CALL;
    }
    /**
     * @see lisp.remote.protocol.Message#getId()
     */
    @Override
    public Long getId()
    {
        return id;
    }
    /**
     * Returns the Marshaller that is used to serialize or parse the Call
     *
     * @return Marshaller
     */
    public Marshaller getMarshaller()
    {
        return marshaller;
    }
    /**
     * @see OutgoingMessage#marshal(java.io.DataOutputStream)
     */
    @Override
    public void marshal(DataOutputStream out) throws IOException
    {
        writeMessageHeader(out);
        Marshaller.writePlace(caller, out);
        marshaller.writeEnvironment(environment, out);
        marshaller.writeExpression(expression, out);
    }
    /**
     * Returns the Callee of this Call
     *
     * @return Callee
     */
    public Place getCallee()
    {
        return callee;
    }
    /**
     * Returns the Success Continuation that will be invoked
     * if evaluation the Expression is successful
     *
     * @return Success Continuation
     */
    public SuccessContinuation getSuccessContinuation()
    {
        return succeed;
    }
    /**
     * Returns the Failure Continuation that will be invoked
     * if the evaluation fails
     *
     * @return Failure Continuation
     */
    public FailureContinuation getFailureContinuation()
    {
        return fail;
    }
}