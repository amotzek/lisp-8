package lisp.remote.protocol;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
/**
 * Messages are exchanged between Places and
 * transported over Connections
 *
 * Created by andreasm 16.10.13 08:20
 */
public abstract class Message
{
    public static final int TYPE_CALL = 1;
    public static final int TYPE_REPLY = 2;
    /**
     * Returns the Type
     *
     * @return Type
     */
    public abstract int getType();
    /**
     * Returns the Id
     *
     * @return Id
     */
    public abstract Long getId();
}