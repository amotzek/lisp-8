package lisp.remote.server;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Combinator;
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.environment.Environment;
import lisp.Place;
import lisp.remote.protocol.Connection;
import lisp.remote.protocol.IncomingCall;
import lisp.remote.protocol.IncomingReply;
import lisp.remote.protocol.Message;
import lisp.remote.protocol.OutgoingCall;
import lisp.remote.protocol.Protocol;
import lisp.concurrent.RunnableQueue;
import lisp.concurrent.RunnableQueueFactory;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * Created by andreasm 16.10.13 10:24
 */
final class Receiving implements Runnable
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars CANNOT_UNMARSHAL = new Chars("cannot unmarshal");
    //
    private static final Logger logger = Logger.getLogger("lisp.remote.server.Server");
    //
    private final Server server;
    private final Connection connection;
    //
    public Receiving(Server server, Connection connection)
    {
        super();
        //
        this.server = server;
        this.connection = connection;
    }
    //
    public void run()
    {
        try
        {
            while (!server.isStopped())
            {
                Protocol protocol = server.getProtocol();
                Collection<Combinator> combinators = server.getCombinators();
                Message message = connection.receive(protocol, combinators);
                int type = message.getType();
                //
                switch (type)
                {
                    case Message.TYPE_CALL:
                        executeCall(message);
                        break;
                    //
                    case Message.TYPE_REPLY:
                        executeReply(message);
                        break;
                }
            }
        }
        catch (SocketTimeoutException e)
        {
            logger.info("socket timeout");
        }
        catch (SocketException e)
        {
            logger.info("cannot receive: " + e.getMessage());
        }
        catch (IOException e)
        {
            logger.log(Level.WARNING, "cannot receive", e);
        }
        finally
        {
            connection.close();
        }
    }
    //
    private void executeCall(Message message)
    {
        IncomingCall call = (IncomingCall) message;
        Environment environment = call.getEnvironment();
        Sexpression expression = call.getExpression();
        Protocol protocol = server.getProtocol();
        //
        if (environment == null || expression == null)
        {
            try
            {
                protocol.reply(call, ERROR, CANNOT_UNMARSHAL);
            }
            catch (IOException e)
            {
                logger.log(Level.WARNING, "cannot reply", e);
            }
            //
            return;
        }
        //
        SendResponse sendresponse = new SendResponse(call, protocol);
        RunnableQueue runnablequeue = RunnableQueueFactory.getConcurrentRunnableQueue();
        expression.enQueueEvaluator(runnablequeue, environment, sendresponse, sendresponse);
        Place caller = call.getCaller();
        StringBuilder infobuilder = new StringBuilder();
        infobuilder.append("received call from ");
        infobuilder.append(caller.getName());
        infobuilder.append(" with size ");
        infobuilder.append(call.size());
        logger.info(infobuilder.toString());
    }
    //
    private void executeReply(Message message)
    {
        IncomingReply reply = (IncomingReply) message;
        OutgoingCall call = reply.getCall();
        Symbol symbol = reply.getSymbol();
        Sexpression expression = reply.getExpression();
        //
        if (symbol == null)
        {
            SuccessContinuation success = call.getSuccessContinuation();
            success.succeed(expression);
        }
        else
        {
            FailureContinuation fail = call.getFailureContinuation();
            fail.fail(symbol, expression);
        }
    }
}