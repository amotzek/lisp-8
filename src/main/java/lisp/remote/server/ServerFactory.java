package lisp.remote.server;
/*
 * Copyright (C) 2013, 2014, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Combinator;
import java.io.IOException;
import java.util.Collection;
import java.util.logging.Logger;
/**
 * Creates Servers
 *
 * Created by andreasm 17.10.13 21:36
 */
public final class ServerFactory
{
    private static final Logger logger = Logger.getLogger("lisp.remote.server.Server");
    //
    private static Collection<Combinator> combinators;
    private static Server server;
    //
    private ServerFactory()
    {
    }
    /**
     * Sets additional Combinators for Incoming Calls
     *
     * @param combinators Combinators
     */
    public synchronized static void setCombinators(Collection<Combinator> combinators)
    {
        if (server != null) throw new IllegalStateException("server already started");
        //
        ServerFactory.combinators = combinators;
    }
    /**
     * Returns a Server that is configured with a Protocol with Port from System Property lrc.port
     * and Group Name from lrc.groupname. Defaults are 1234 and ff02::1.
     *
     * @return Server
     * @throws IOException if the Server cannot be started
     */
    public synchronized static Server getServer() throws IOException
    {
        if (server == null)
        {
            String port = System.getProperty("lrc.port", "1234");
            String groupname = System.getProperty("lrc.groupname", "ff02::1");
            Server server = new Server(combinators, groupname, Integer.parseInt(port));
            server.start();
            logger.info("server started with port " +  port + " and groupname " +  groupname);
            Runtime runtime = Runtime.getRuntime();
            runtime.addShutdownHook(new Thread() {
                @Override
                public void run()
                {
                    server.stop();
                }
            });
            ServerFactory.server = server;
        }
        //
        return server;
    }
}