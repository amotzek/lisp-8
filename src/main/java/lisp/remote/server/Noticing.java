package lisp.remote.server;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Place;
import lisp.remote.protocol.Protocol;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;
/*
 * Created by andreasm 15.10.13 15:51
 */
final class Noticing extends Thread
{
    private static final Logger logger = Logger.getLogger("lisp.remote.server.Server");
    //
    private final Server server;
    private final ReentrantReadWriteLock readwritelock;
    private final HashMap<Place, Long> timestampsbyplace;
    //
    public Noticing(Server server)
    {
        super();
        //
        this.server = server;
        //
        readwritelock = new ReentrantReadWriteLock(true);
        timestampsbyplace = new HashMap<>();
        //
        setDaemon(true);
        setName("noticing");
    }
    //
    @Override
    public void run()
    {
        while (!server.isStopped())
        {
            Place place = notice();
            //
            if (place == null) continue;
            //
            register(place);
        }
    }
    //
    private Place notice()
    {
        try
        {
            Protocol protocol = server.getProtocol();
            Place place = protocol.notice();
            //
            if (place != null)
            {
                logger.info("noticed " + place.getName());
                //
                return place;
            }
        }
        catch (Exception e)
        {
            logger.warning("cannot notice: " + e.getMessage());
        }
        //
        return null;
    }
    //
    private void register(Place place)
    {
        Lock writelock = readwritelock.writeLock();
        writelock.lock();
        //
        try
        {
            Long timestamp = System.currentTimeMillis();
            timestampsbyplace.put(place, timestamp);
        }
        finally
        {
            writelock.unlock();
        }
    }
    //
    public LinkedList<Place> getPlaces()
    {
        Protocol protocol = server.getProtocol();
        LinkedList<Place> active = new LinkedList<>();
        LinkedList<Place> stale = new LinkedList<>();
        Lock readlock = readwritelock.readLock();
        readlock.lock();
        //
        try
        {
            long now = System.currentTimeMillis();
            //
            for (Map.Entry<Place, Long> entry : timestampsbyplace.entrySet())
            {
                Place place = entry.getKey();
                long then = entry.getValue();
                long elapsed = now - then;
                //
                if (elapsed > protocol.getExpiryInterval())
                {
                    stale.addLast(place);
                }
                else
                {
                    active.addLast(place);
                }
            }
        }
        finally
        {
            readlock.unlock();
        }
        //
        if (!stale.isEmpty())
        {
            Lock writelock = readwritelock.writeLock();
            writelock.lock();
            //
            try
            {
                for (Place place : stale)
                {
                    timestampsbyplace.remove(place);
                }
            }
            finally
            {
                writelock.unlock();
            }
        }
        //
        Collections.sort(active);
        //
        return active;
    }
}