package lisp.remote.server;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.remote.protocol.Protocol;
import java.io.IOException;
import java.util.logging.Logger;
/*
 * Created by andreasm 15.10.13 15:51
 */
final class Announcing extends Thread
{
    private static final Logger logger = Logger.getLogger("lisp.remote.server.Server");
    //
    private final Server server;
    //
    public Announcing(Server server)
    {
        super();
        //
        this.server = server;
        //
        setDaemon(true);
        setName("announcing");
    }
    //
    @Override
    public void run()
    {
        try
        {
            while (!server.isStopped())
            {
                announce();
                sleep();
            }
        }
        catch (InterruptedException ignored)
        {
        }
    }
    //
    private void announce()
    {
        try
        {
            Protocol protocol = server.getProtocol();
            protocol.announce();
        }
        catch (IOException e)
        {
            logger.warning("cannot announce: " + e.getMessage());
        }
    }
    //
    private void sleep() throws InterruptedException
    {
        Protocol protocol = server.getProtocol();
        sleep(protocol.getAnnouncingInterval());
    }
}