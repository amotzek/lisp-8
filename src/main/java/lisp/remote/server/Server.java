package lisp.remote.server;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Combinator;
import lisp.remote.protocol.Connection;
import lisp.Place;
import lisp.remote.protocol.Protocol;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 * Bundles the threads for noticing and expiring remote Places,
 * accepting new Connections, executing Calls and announcing the local Place.
 *
 * Created by andreasm 15.10.13 14:09
 */
public final class Server
{
    private final Collection<Combinator> combinators;
    private final Protocol protocol;
    private final Noticing noticing;
    private final Accepting accepting;
    private final Announcing announcing;
    private final Expiring expiring;
    private final ExecutorService receivingexecutor;
    private volatile boolean stopped;
    /**
     * Creates a non-started Server, should only be used in test cases.
     *
     * @see ServerFactory#getServer()
     * @param combinators Additional Combinators for Incoming Calls
     * @param groupname Group Name for announcing
     * @param port Port for announcing
     */
    public Server(Collection<Combinator> combinators, String groupname, int port)
    {
        super();
        //
        this.combinators = combinators;
        //
        protocol = new Protocol(groupname, port);
        noticing = new Noticing(this);
        accepting = new Accepting(this);
        announcing = new Announcing(this);
        expiring = new Expiring(this);
        receivingexecutor = Executors.newCachedThreadPool();
    }
    /**
     * Starts the Server. Should only be used in test cases
     *
     * @see ServerFactory#getServer()
     * @throws IOException if the Server cannot be started
     */
    public void start() throws IOException
    {
        protocol.init();
        noticing.start();
        accepting.start();
        announcing.start();
        expiring.start();
    }
    /**
     * Stops the Server
     */
    public void stop()
    {
        stopped = true;
        receivingexecutor.shutdown();
        protocol.destruct();
    }
    /**
     * Checks if the Server is stopped
     *
     * @return true if the Server is stopped
     */
    public boolean isStopped()
    {
        return stopped;
    }
    /**
     * Returns the Protocol used by this Server
     *
     * @return Protocol
     */
    public Protocol getProtocol()
    {
        return protocol;
    }
    /**
     * Returns Combinators that are added to the Environment for Incoming Calls
     *
     * @return Combinators
     */
    public Collection<Combinator> getCombinators()
    {
        return combinators;
    }
    /**
     * Returns a list of noticed remote Places
     *
     * @return List of Places
     */
    public LinkedList<Place> getPlaces()
    {
        return noticing.getPlaces();
    }
    /**
     * Receives and processes Messages from the Connection
     *
     * @param connection Connection
     */
    public void receiveFrom(Connection connection)
    {
        Receiving receiving = new Receiving(this, connection);
        receivingexecutor.execute(receiving);
    }
}