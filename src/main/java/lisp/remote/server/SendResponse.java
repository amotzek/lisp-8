package lisp.remote.server;
/*
 * Copyright (C) 2013 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Sexpression;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.continuation.SuccessContinuation;
import lisp.remote.protocol.IncomingCall;
import lisp.remote.protocol.Protocol;
import java.io.IOException;
import java.net.ConnectException;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * @author andreasm
 */
final class SendResponse implements SuccessContinuation, FailureContinuation
{
    private static final Logger logger = Logger.getLogger("lisp.remote.server.Server");
    //
    private final IncomingCall call;
    private final Protocol protocol;
    //
    public SendResponse(IncomingCall call, Protocol protocol)
    {
        super();
        //
        this.call = call;
        this.protocol = protocol;
    }
    //
    public void succeed(Sexpression value)
    {
        try
        {
            protocol.reply(call, null, value);
        }
        catch (ConnectException e)
        {
            logger.warning("cannot reply: " +  e.getMessage());
        }
        catch (IOException e)
        {
            logger.log(Level.WARNING, "cannot reply", e);
        }
    }
    //
    public void fail(Symbol name, Sexpression value)
    {
        try
        {
            protocol.reply(call, name, value);
        }
        catch (ConnectException e)
        {
            logger.warning("cannot reply: " +  e.getMessage());
        }
        catch (IOException e)
        {
            logger.log(Level.WARNING, "cannot reply", e);
        }
    }
    //
    public SuccessContinuation getNext()
    {
        return null;
    }
}