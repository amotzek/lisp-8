package lisp.remote.server;
/*
 * Copyright (C) 2013, 2016 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.Chars;
import lisp.Symbol;
import lisp.continuation.FailureContinuation;
import lisp.Place;
import lisp.remote.protocol.OutgoingCall;
import lisp.remote.protocol.Protocol;
import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Logger;
/*
 * Created by andreas-motzek@t-online.de 21.10.13 07:34
 */
final class Expiring extends Thread
{
    private static final Symbol ERROR = Symbol.createSymbol("error");
    private static final Chars CALL_EXPIRED = new Chars("call expired");
    //
    private static final Logger logger = Logger.getLogger("lisp.remote.server.Server");
    //
    private final Server server;
    //
    public Expiring(Server server)
    {
        super();
        //
        this.server = server;
        //
        setDaemon(true);
        setName("expiring");
    }
    //
    @Override
    public void run()
    {
        try
        {
            while (!server.isStopped())
            {
                sleep();
                expire();
            }
        }
        catch (InterruptedException ignored)
        {
        }
    }
    //
    private void expire()
    {
        Protocol protocol = server.getProtocol();
        Collection<Place> places = getPlaces();
        Collection<OutgoingCall> calls = protocol.getCalls();
        //
        for (OutgoingCall call : calls)
        {
            Place callee = call.getCallee();
            //
            if (places.contains(callee)) continue;
            //
            Long id = call.getId();
            call = protocol.removeCall(id);
            //
            if (call == null) continue;
            //
            FailureContinuation fail = call.getFailureContinuation();
            //
            if (fail == null) continue;
            //
            fail.fail(ERROR, CALL_EXPIRED);
            StringBuilder infobuilder = new StringBuilder();
            infobuilder.append("call #");
            infobuilder.append(id);
            infobuilder.append(" expired");
            logger.info(infobuilder.toString());
        }
    }
    //
    private HashSet<Place> getPlaces()
    {
        Protocol protocol = server.getProtocol();
        Collection<Place> placecollection = server.getPlaces();
        HashSet<Place> placeset = new HashSet<>(placecollection);
        placeset.add(protocol.getHere());
        //
        return placeset;
    }
    //
    private void sleep() throws InterruptedException
    {
        Protocol protocol = server.getProtocol();
        sleep(protocol.getExpiryInterval());
    }
}