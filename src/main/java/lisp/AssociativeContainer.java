package lisp;
/*
 * Copyright (C) 2013, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Lisp package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
import lisp.equality.EqualityTest;
import lisp.formatter.Formatter;
import lisp.traversal.CalculateHashCode;
import lisp.traversal.Traversal;
import java.util.Iterator;
/*
 * Created by andreas-motzek@t-online.de 30.04.13 17:52
 */
public abstract class AssociativeContainer<K extends Sexpression> extends MutableConstant
{
    private boolean frozen;
    /**
     * Freezes this Object
     */
    public final void freeze()
    {
        lockExclusively();
        frozen = true;
        unlockExclusively();
    }
    /**
     * Checks is the Object is frozen
     *
     * @return true, if the Object is frozen
     */
    public final boolean isFrozen()
    {
        lockShared();
        //
        try
        {
            return frozen;
        }
        finally
        {
            unlockShared();
        }
    }
    /**
     * Returns the Count of Key Value Pairs in this Container
     *
     * @return Count of Key Value Pairs
     */
    public abstract int size();
    /**
     * Checks if this Container can have Keys that are Containers too
     *
     * @return true if Keys can be Containers
     */
    public abstract boolean hasComplexKeys();
    /**
     * Returns an Iterator with all Keys
     *
     * @return Iterator with Keys
     */
    public abstract Iterator<K> keyIterator();
    /**
     * Returns the Value for a Key
     *
     * @param key Key
     * @return Value for Key
     */
    public abstract Sexpression get(Object key);
    /**
     * Sets a Value for a Key
     *
     * @param key Key
     * @param value Value
     */
    public abstract void put(K key, Sexpression value);
    /**
     * Duplicates this Container, the duplicated Container will not be frozen
     *
     * @return Duplicated Container
     */
    public abstract AssociativeContainer<K> duplicate();
    /**
     * Returns the Hash Code of the Meta Data
     *
     * @return Hash Code
     */
    public abstract int metaHashCode();
    /**
     * @see Object#hashCode()
     */
    @Override
    public final int hashCode()
    {
        if (frozen)
        {
            Traversal traversal = new Traversal(this);
            CalculateHashCode hashcode = new CalculateHashCode();
            traversal.accept(hashcode);
            //
            return hashcode.hashCode();
        }
        //
        return super.hashCode();
    }
    /**
     * @see Object#equals(Object)
     */
    @Override
    public final boolean equals(Object object)
    {
        if (this == object) return true;
        //
        if (object instanceof Sexpression)
        {
            Sexpression that = (Sexpression) object;
            EqualityTest test = new EqualityTest();
            //
            return test.areEqual(this, that);
        }
        //
        return false;
    }
    /**
     * @see Object#toString()
     */
    @Override
    public final String toString()
    {
        Formatter formatter = new Formatter();
        formatter.format(this);
        //
        return formatter.toString();
    }
}