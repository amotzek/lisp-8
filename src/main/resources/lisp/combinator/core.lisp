#|
This file is part of the Lisp package.

You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
See http://simplysomethings.de/open+source/modified+artistic+license.html for details.

This file is distributed in the hope that it will be useful, but without any warranty; without even
the implied warranty of merchantability or fitness for a particular purpose.
|#

(setq car first)
(setq cdr rest)

(setq first-or-nil (lambda (x) (if x (first x) nil)))

(setq cadr (lambda (x) (first (rest x))))
(setq cdar (lambda (x) (rest (first x))))
(setq caar (lambda (x) (first (first x))))
(setq cddr (lambda (x) (rest (rest x))))

(setq caadr (lambda (x) (first (cadr x))))
(setq cadar (lambda (x) (first (cdar x))))
(setq caaar (lambda (x) (first (caar x))))
(setq caddr (lambda (x) (first (cddr x))))

(setq cdadr (lambda (x) (rest (cadr x))))
(setq cddar (lambda (x) (rest (cdar x))))
(setq cdaar (lambda (x) (rest (caar x))))
(setq cdddr (lambda (x) (rest (cddr x))))

(setq second cadr)
(setq second-or-nil (lambda (x) (if x (second x) nil)))
(setq third caddr)
(setq fourth (lambda (x) (first (rest (rest (rest x))))))
(setq fifth (lambda (x) (first (rest (rest (rest (rest x)))))))
(setq sixth (lambda (x) (first (rest (rest (rest (rest (rest x))))))))
(setq seventh (lambda (x) (first (rest (rest (rest (rest (rest (rest x)))))))))
(setq eighth (lambda (x) (first (rest (rest (rest (rest (rest (rest (rest x))))))))))
(setq ninth (lambda (x) (first (rest (rest (rest (rest (rest (rest (rest (rest x)))))))))))
(setq tenth (lambda (x) (first (rest (rest (rest (rest (rest (rest (rest (rest (rest x))))))))))))

(setq equal? eq?)
(setq not null?)

(setq list1 (lambda (e) (cons e nil)))
(setq list2 (lambda (e1 e2) (cons e1 (cons e2 nil))))
(setq list3 (lambda (e1 e2 e3) (cons e1 (list2 e2 e3))))
(setq list4 (lambda (e1 e2 e3 e4) (cons e1 (list3 e2 e3 e4))))

(setq map-with
  (lambda (f l)
    (if
      (null? l)
      nil
      (cons
        (f (first l))
        (map-with f (rest l))))))

(setq mapcar map-with)

(setq let
  (mlambda l
	 (cons
	   (list3
	     lambda
		 (map-with first (first l))
		 (cadr l))
	   (map-with cadr (first l)))))

(setq or
  (let
    ((or2 (make-combinator "lisp.combinator.Or2" nil)))
    (mlambda args
      (if
        (null? args)
        nil
        (list3
          or2
          (first args)
          (cons (quote or) (rest args)))))))

(setq cond
  (let
    ((or2 (make-combinator "lisp.combinator.Or2" nil)))
    (mlambda l
      (if
        (null? l)
	    nil
	    (if
	      (null? (cdar l))
          (list3
            or2
            (caar l)
            (cons (quote cond) (rest l)))
		  (list4
		    if
            (caar l)
            (cadar l)
            (cons (quote cond) (rest l))))))))

(setq and
  (mlambda l
    (if
      (null? l)
      (quote t)
      (if
        (null? (rest l))
        (first l)
        (list4
          if
          (first l)
          (cons (quote and) (rest l))
          nil)))))

(setq labels
  (mlambda l
    (list3
      letrec
      (map-with
        (lambda (e)
          (list2
            (first e)
            (cons
              lambda
              (cons
                (second e)
                (cddr e)))))
        (first l))
      (second l))))

(setq single?
  (lambda (l)
    (and l (null? (rest l)))))

(setq list-length
  (lambda (l)
    (if
      (null? l)
      0
      (add 1 (list-length (rest l))))))

(setq progn
  (mlambda args
    (cond
      ((null? args) nil)
      ((single? args) (first args))
      (t (list2 first (list2 last (list2 sequential args)))))))

(setq prog1
  (mlambda args
    (if
      (single? args)
      (first args)
      (list2 first (list2 sequential args)))))

(setq prog2
  (mlambda args
    (list2 second (list2 sequential args))))

(setq when
  (mlambda l
    (list4
      if
      (first l)
      (cons
        progn
        (rest l))
      nil)))

(setq unless
  (mlambda l
    (list4
      if
      (first l)
      nil
      (cons
        progn
        (rest l)))))

(setq list
  (mlambda l
    (if
      (null? l)
      nil
      (list3
        cons
        (first l)
        (cons
          (quote list)
          (rest l))))))

(setq quasi-quote
  (let
    ((quasi-quote-list
      (lambda (elem)
        (list2
          (quote quasi-quote)
          elem))))
    (mlambda args
      (let
        ((expr (first args)))
        (if
          (null? expr)
          nil
          (if
            (list? expr)
            (if
              (equal? (first expr) (quote unquote))
              (second expr)
              (cons
                list
                (map-with quasi-quote-list expr)))
            (list2 quote expr)))))))

(setq atom?
  (lambda (x)
    (or
      (null? x)
      (not (list? x)))))

(setq list? (lambda (x) (eq? (type-of x) (quote list))))

(setq integer? (lambda (x) (eq? (type-of x) (quote integer))))

(setq number?
  (lambda (x)
    (or
      (eq? (type-of x) (quote ratio))
      (eq? (type-of x) (quote integer)))))

(setq string? (lambda (x) (eq? (type-of x) (quote string))))

(setq symbol? (lambda (x) (eq? (type-of x) (quote atom))))

(setq array? (lambda (x) (eq? (type-of x) (quote array))))

(setq hash-table? (lambda (x) (eq? (type-of x) (quote hash-table))))

(setq class? (lambda (x) (eq? (type-of x) (quote class))))

(setq trait? (lambda (x) (eq? (type-of x) (quote trait))))

(setq instance? (lambda (x) (eq? (type-of x) (quote instance))))

(setq generic-function? (lambda (x) (eq? (type-of x) (quote generic-function))))

(setq defproc
  (mlambda args
    (if
      (equal? (list-length args) 3)
      (quasi-quote
        (if
          (and
            (bound? (quote (unquote (first args))))
            (generic-function? (unquote (first args))))
          (defmethod (unquote (first args)) (unquote (second args))
            t
            (unquote (third args)))
          (setq (unquote (first args))
            (lambda (unquote (second args))
              (unquote (third args))))))
      (throw (quote error) "wrong number of arguments for defproc"))))

(setq defmacro
  (mlambda args
    (if
      (equal? (list-length args) 3)
      (list3
        setq
        (first args)
        (list3 mlambda (second args) (third args)))
      (throw (quote error) "wrong number of arguments for defmacro"))))

(defmacro aif args
  (quasi-quote
    (let
      ((it (unquote (first args))))
      (if
        it
        (unquote (second args))
        (unquote (third args))))))

(defmacro aand args
  (cond
    ((null? args) t)
    ((single? args) (first args))
    (t
      (list4
        aif
        (first args)
        (cons aand (rest args))
        nil))))

(defmacro awhen args
  (list
    aif
    (first args)
    (cons progn (rest args))
    nil))

(defmacro dotimes args
  (let
    ((symbol (first (first args)))
     (count-form (second (first args)))
     (statements (rest args))
     (fn (gensym)))
    (quasi-quote
      (letrec
        (((unquote fn)
          (lambda ((unquote symbol))
            (if
              (less? (unquote symbol) (unquote count-form))
              (progn
                (unquote (cons progn statements))
                ((unquote fn) (add 1 (unquote symbol))))
              (unquote
                (aand
                  (rest (rest (first args)))
                  (first it)))))))
        ((unquote fn) 0)))))

(defmacro dolist args
  (let
    ((symbol (first (first args)))
     (list-form (second (first args)))
     (statements (rest args))
     (fn (gensym))
     (rest-list (gensym)))
    (quasi-quote
      (letrec
        (((unquote fn)
          (lambda ((unquote rest-list))
            (if
              (unquote rest-list)
              (progn
                (let
                  (((unquote symbol) (first (unquote rest-list))))
                  (unquote (cons progn statements)))
                ((unquote fn) (rest (unquote rest-list))))
              (unquote
                (aand
                  (rest (rest (first args)))
                  (first it)))))))
        ((unquote fn) (unquote list-form))))))

#| wenn die Reihenfolge von letrec und catch vertauscht wird, tritt ein Compilerfehler auf |#

(defmacro loop args
  (let
    ((continue (gensym))
     (break (gensym)))
    (quasi-quote
      (letrec
        ((return
          (lambda (result) (throw (quote (unquote break)) result)))
         ((unquote continue)
          (lambda ()
            (progn
              (unquote (cons (quote progn) args))
              ((unquote continue))))))
        (catch (quote (unquote break))
           ((unquote continue)))))))

(defproc greater? (x y) (less? y x))

(defproc greater-or-equal? (x y) (not (less? x y)))

(defproc less-or-equal? (x y) (not (less? y x)))

(setq < less?)

(setq <= less-or-equal?)

(setq = equal?)

(setq > greater?)

(setq >= greater-or-equal?)

(defproc zero? (x)
  (equal? x 0))

(setq t (quote t))

(defproc zip-with (f l r)
  (cond
    ((null? l) nil)
    ((null? r) nil)
    ((cons
      (f (first l) (first r))
      (zip-with f (rest l) (rest r))))))

(defproc subst (x y z)
  (cond
    ((equal? y z) x)
    ((atom? z) z)
    ((cons
      (subst x y (first z))
      (subst x y (rest z))))))

(defproc subst-if (x p z)
  (cond
    ((p z) x)
    ((atom? z) z)
    ((cons
      (subst-if x p (first z))
      (subst-if x p (rest z))))))

(defproc member-if? (p l)
  (cond
    ((null? l) nil)
    ((p (first l)) l)
    ((member-if? p (rest l)))))

(defproc find-if (p l)
  (let
    ((m (member-if? p l)))
    (and m (first m))))

(defproc position (x l)
  (position-if (lambda (y) (equal? x y)) l))

(setq position-if
  (letrec
    ((inner-position-if
      (lambda (p l i)
        (cond
          ((null? l) nil)
          ((p (first l)) i)
          (t (inner-position-if p (rest l) (add 1 i)))))))
    (lambda (p l)
      (inner-position-if p l 0))))

(defproc adjoin (x l)
  (if (member? x l) l (cons x l)))

(defproc union (u v)
  (if
    (null? u)
    v
    (union (rest u) (adjoin (first u) v))))

(defproc intersection (u v)
  (cond
    ((null? u) nil)
    ((member? (first u) v)
      (cons (first u) (intersection (rest u) v)))
    ((intersection (rest u) v))))

(defproc set-difference (u v)
  (remove-if
    (lambda (x) (member? x v))
    u))

(defproc set-exclusive-or (u v)
  (union
    (set-difference u v)
    (set-difference v u)))

(defproc subset? (small large)
  (cond
    ((null? small) t)
    ((member? (first small) large)
     (subset? (rest small) large))))
      
(defproc disjoint? (set-1 set-2)
  (not
    (member-if?
      (lambda (element) (member? element set-2))
      set-1)))

(defproc acons (key val plist)
  (cons
    (list2 key val)
    plist))

(defproc pairlis (x y a)
  (if
    (null? x)
    a
    (acons
      (first x)
      (first y)
      (pairlis
        (rest x)
        (rest y)
        a))))

(defmacro with-associations args
  (let
    ((alist (second args)))
    (list3
      let
      (map-with
        (lambda (association)
          (list2
            association
            (list2
              second-or-nil
              (list3
                assoc
                (list2 quote association)
                alist))))
        (first args))
      (third args))))

(defproc sublis (a y)
  (let
    ((p (assoc y a)))
    (cond
      (p (cadr p))
      ((atom? y) y)
      ((cons
        (sublis a (first y))
        (sublis a (rest y)))))))

(defmethod length (l) (list? l) (list-length l))

(defmethod length (s) (string? s) (string-length s))

(defmethod length (i) (integer? i) (integer-length i))

(defproc append2 (l1 l2)
  (cond
    ((null? l1) l2)
    ((null? l2) l1)
    ((cons
      (first l1)
      (append2 (rest l1) l2)))))

(defmacro append args
  (cond
    ((null? args) nil)
    ((null? (rest args)) (first args))
    ((list3
      append2
      (first args)
      (cons (quote append) (rest args))))))

(defproc range (from to)
  (if
    (less? from to)
    (cons from (range (add 1 from) to))
    nil))

(defproc cartesian-product (l1 l2)
  (apply
    append
    (map-with
      (lambda (e1)
        (map-with
          (lambda (e2) (list e1 e2))
          l2))
      l1)))

(setq permute
  (letrec
    ((permute-help
      (lambda (l r)
        (if
          (null? r)
          nil
          (append
            (map-with (lambda (x) (cons (first r) x))
              (permute (append l (rest r))))
            (permute-help
              (append l (list (first r)))
              (rest r)))))))
    (lambda (l)
      (if
        (null? (rest l))
        (list l)
        (permute-help nil l)))))

(defproc nthcdr (l n)
  (if
    (eq? n 0)
    l
    (nthcdr (rest l) (sub n 1))))

(defproc nth (l n)
  (if
    (eq? n 0)
    (first l)
    (nth (rest l) (sub n 1))))

(defproc pick (l)
  (nth l (random (list-length l))))

(defproc select-if (p l)
  (if
    (null? l)
    nil
    (if
      (p (first l))
      (cons
        (first l)
        (select-if p (rest l)))
      (select-if p (rest l)))))

(setq count-if
  (letrec
    ((count
      (lambda (p l n)
        (if
          (null? l)
          n
          (if
            (p (first l))
            (count p (rest l) (add 1 n))
            (count p (rest l) n))))))
    (lambda (p l)
      (count p l 0))))

(defproc remove-if (p l)
  (cond
    ((null? l) nil)
    ((p (first l)) (remove-if p (rest l)))
    ((cons
       (first l)
       (remove-if p (rest l))))))

(defproc remove (x l)
  (remove-if (lambda (y) (equal? x y)) l))

(defproc tail? (sh lo)
  (cond
    ((equal? sh lo))
    ((null? lo) nil)
    ((tail? sh (rest lo)))))

(defproc some? (fn l)
  (cond
    ((null? l) nil)
    ((fn (first l)) (first l))
    ((some? fn (rest l)))))

(defproc every? (fn l)
  (cond
    ((null? l))
    ((not (fn (first l))) nil)
    ((every? fn (rest l)))))

(defproc last (l)
  (if  (null? (rest l)) l (last (rest l))))

(defproc butlast (l)
  (reverse
    (rest
      (reverse l))))

(defmacro push args
  (list3
    setq
    (second args)
    (list3
      cons
      (first args)
      (second args))))

(defmacro pop args
  (let
    ((arg (first args)))
    (list3
      prog1
      (list2 first arg)
      (list3
        setq
        arg
        (list2 rest arg)))))

(defproc abs (x)
  (if
    (less? x 0)
    (sub 0 x)
    x))

(defmacro concatenate args
  (list3
    concatenate-list
    (cons list args)
    ""))

(defmacro alambda args
  (quasi-quote
    (letrec
      ((self
        (lambda (unquote (first args))
          (unquote (second args)))))
      self)))

(defproc identity (x) x)

(defproc complement (f)
  (mlambda x
    (list2
      not
      (cons f x))))

(defproc constantly (x) (lambda (y) x))

(setq curry
  (letrec
    ((make-parameter (lambda (underscore) (gensym)))
     (underscore? (lambda (x) (equal? x (quote _))))
     (replace-underscores
       (lambda (arguments parameters result)
         (cond
           ((null? arguments)
             (reverse result))
           ((underscore? (first arguments))
             (replace-underscores
               (rest arguments)
               (rest parameters)
               (cons (first parameters) result)))
           (t
             (replace-underscores
               (rest arguments)
               parameters
               (cons (first arguments) result)))))))
    (mlambda args
      (let
        ((parameters
          (map-with
            make-parameter
            (select-if
              underscore?
              (first args)))))
        (list3
          lambda
          parameters
          (replace-underscores
            (first args)
            parameters
            nil))))))

(setq catch
  (let
    ((get-value (lambda (name value) value)))
    (mlambda args
      (list4
        catch-and-apply
        (first args)
        get-value
        (second args)))))

(defmacro assignq args
  (list4
    assign
    (first args)
    (list2
      quote
      (second args))
    (third args)))

(defmacro ensure args
  (quasi-quote
    (catch-and-apply
      nil
      (lambda (name value)
        (prog1
          (catch nil (unquote (second args)))
          (throw name value)))
      (prog1
        (unquote (first args))
        (catch nil (unquote (second args)))))))

(defproc instance-of? (instance class)
  (and
    (equal? (type-of instance) (quote instance))
    (equal? (class-of instance) class)))

(setq is-a?
  (letrec
    ((is-subclass?
      (lambda (sub super)
        (let
          ((classes (direct-superclasses sub)))
          (or
            (member? super classes)
            (some?
              (lambda (class) (is-subclass? class super))
              classes))))))
    (lambda (instance class)
      (and
        (equal? (type-of instance) (quote instance))
        (let
          ((instance-class (class-of instance)))
          (or
            (equal? instance-class class)
            (is-subclass? instance-class class)))))))

(defmacro make-instance args
  (cons
    (quote initialize)
    (cons
      (list2
        allocate-instance
        (first args))
      (rest args))))

(setq new make-instance)

(defmacro slot-valueq args
  (list3
    slot-value
    (first args)
    (list2 quote (second args))))

(setq . slot-valueq)

(setq .= assignq)

(defmacro defstruct args
  (quasi-quote
    (progn
      (defclass (unquote (first args)) nil)
      (defmethod initialize
        (unquote
          (cons
            (list (quote this) (first args))
            (second args)))
        (unquote (third args))
        (progn
          (unquote
            (cons
              progn
              (map-with
                (lambda (slot) (list .= (quote this) slot slot))
                (second args))))
          (freeze this))))))

(defmacro with-slots args
  (let
    ((slots (first args))
     (instance (second args)))
    (list3
      let
      (map-with
        (lambda (slot)
          (list2
            slot
            (list3
              slot-valueq
              instance
              slot)))
        slots)
      (cons
        prog1
        (cons
          (third args)
          (map-with
            (lambda (slot)
              (list3
                or
                (list3
                  equal?
                  slot
                  (list3
                    slot-valueq
                    instance
                    slot))
                (list4
                  assignq
                  (second args)
                  slot
                  slot)))
            slots))))))

(defmacro with-slots-read-only args
  (let
    ((slots (first args))
     (instance (second args)))
    (list3
      let
      (map-with
        (lambda (slot)
          (list2
            slot
            (list3
              slot-valueq
              instance
              slot)))
        slots)
      (third args))))

(setq with-lock
  (let
    ((lock (gensym)))
    (mlambda args
      (quasi-quote
        (let
          (((unquote lock) (unquote (first args))))
          (progn
            (acquire-lock (unquote lock))
            (ensure
              (unquote (cons progn (rest args)))
              (release-lock (unquote lock)))))))))

(defmacro synchronized args
  (list3
    with-lock
    (list3
      slot-value
      (second args)
      (list2 quote (first args)))
    (third args)))

(setq tree-sort
  (letrec
    ((make-tree list4)
     (get-color first)
     (get-left-subtree second)
     (get-value third)
     (get-right-subtree fourth)
     (is-red?
       (lambda (tree)
         (and tree (equal? (quote red) (get-color tree)))))
     (make-balanced-tree
       (lambda (left-left left left-right middle right-left right right-right)
         (make-tree
           (quote red)
           (make-tree
             (quote black)
             left-left
             left
             left-right)
           middle
           (make-tree
             (quote black)
             right-left
             right
             right-right))))
     (balance
       (lambda (tree)
         (if
           (equal? (quote red) (get-color tree)) 
           tree
           (let
             ((left (get-left-subtree tree))
              (right (get-right-subtree tree)))
             (cond
               ((is-red? left)
                 (let
                   ((left-left (get-left-subtree left))
                    (left-right (get-right-subtree left)))
                   (cond
                     ((is-red? left-left)
                       (make-balanced-tree
                         (get-left-subtree left-left)
                         (get-value left-left)
                         (get-right-subtree left-left)
                         (get-value left)
                         left-right
                         (get-value tree)
                         right))
                     ((is-red? left-right)
                       (make-balanced-tree
                         left-left
                         (get-value left)
                         (get-left-subtree left-right)
                         (get-value left-right)
                         (get-right-subtree left-right)
                         (get-value tree)
                         right))
                     (tree))))
               ((is-red? right)
                 (let
                   ((right-left (get-left-subtree right))
                    (right-right (get-right-subtree right)))
                   (cond
                     ((is-red? right-right)
                       (make-balanced-tree
                         left
                         (get-value tree)
                         right-left
                         (get-value right)
                         (get-left-subtree right-right)
                         (get-value right-right)
                         (get-right-subtree right-right)))
                     ((is-red? right-left)
                       (make-balanced-tree
                         left
                         (get-value tree)
                         (get-left-subtree right-left)
                         (get-value right-left)
                         (get-right-subtree right-left)
                         (get-value right)
                         right-right))
                     (tree))))
               (tree))))))
     (collect-in-order (make-combinator "lisp.combinator.CollectInOrder" nil))
     (insert-into-tree
       (lambda (new-value predicate tree)
         (if
           (null? tree)
           (make-tree 
             (quote red) 
             nil 
             new-value 
             nil)
           (let
             ((color (get-color tree))
              (left-subtree (get-left-subtree tree))
              (value (get-value tree))
              (right-subtree (get-right-subtree tree)))
             (balance
               (if
                 (predicate new-value value)
                 (make-tree
                   color
                   (insert-into-tree new-value predicate left-subtree)
                   value
                   right-subtree)
                 (make-tree
                   color
                   left-subtree
                   value
                   (insert-into-tree new-value predicate right-subtree))))))))
     (accumulate-tree
       (lambda (elements predicate tree)
         (if
           (null? elements)
           tree
           (accumulate-tree
             (rest elements)
             predicate
             (insert-into-tree (first elements) predicate tree))))))
    (lambda (elements predicate)
      (if
        (null? elements)
        nil
        (collect-in-order
          (accumulate-tree
            (rest elements) 
            predicate
            (make-tree 
              (quote black) 
              nil 
              (first elements) 
              nil)))))))

(setq merge
  (letrec
    ((merge-sort
      (lambda (left right predicate result)
        (if
          (and (null? left) (null? right))
          (reverse result)
          (cond
            ((null? left) (merge-sort left (rest right) predicate (cons (first right) result)))
            ((null? right) (merge-sort (rest left) right predicate (cons (first left) result)))
            ((predicate (first right) (first left)) (merge-sort left (rest right) predicate (cons (first right) result)))
            ((merge-sort (rest left) right predicate (cons (first left) result))))))))
    (lambda (result-type left right predicate)
      (if
        (equal? result-type (quote list))
        (merge-sort left right predicate nil)
        (throw (quote error) "merge only supports result-type list")))))

(setq sort
  (letrec
    ((tree-merge
       (lambda (parts predicate)
         (if
           (single? parts)
           (first parts)
           (let
             ((grouped-parts (split parts 2)))
             (tree-merge
               (zip-with
                 (lambda (left right) (merge (quote list) left right predicate))
                 (first grouped-parts)
                 (second grouped-parts))
               predicate))))))
    (lambda (elements predicate)
      (if
        (null? elements)
        nil
        (tree-merge
          (map-with
            (lambda (part) (tree-sort part predicate))
            (split elements *thread-count*))
          predicate)))))

(defmacro assert args
  (quasi-quote
    (unless (unquote (second args))
      (throw (quote error) (unquote (first args))))))

(defmacro + args
  (if
    (null? args)
    0
    (if
      (single? args)
      (first args)
      (list3
        (quote add)
        (first args)
        (cons (quote +) (rest args))))))

(defmacro * args
  (if
    (null? args)
    1
    (if
      (single? args)
      (first args)
      (list3
        (quote times)
        (first args)
        (cons (quote *) (rest args))))))

(defmacro - args
  (cond
    ((null? args) 0)
    ((single? args) (list3 (quote sub) 0 (first args)))
    ((list3
      (quote sub)
      (first args)
      (cons (quote +) (rest args))))))

(defmacro / args
  (cond
    ((null? args) 1)
    ((single? args) (list3 (quote quotient) 1 (first args)))
    ((list3
      (quote quotient)
      (first args)
      (cons (quote *) (rest args))))))

(defmacro deffoldr outer-args
  (let
    ((f (gensym))
     (z (gensym)))
    (quasi-quote
      (setq
        (unquote (first outer-args))
        (let
          (((unquote f) (unquote (second outer-args)))
           ((unquote z) (unquote (third outer-args))))
          (mlambda args
            (cond
              ((null? args)
                (unquote z))
              ((single? args)
                (first args))
              ((single? (rest args))
                (list3
                  (unquote f)
                  (first args)
                  (second args)))
              (t
                (list3
                  (unquote f)
                  (first args)
                  (cons
                    (quote (unquote (first outer-args)))
                    (rest args)))))))))))

(deffoldr logand (make-combinator "lisp.combinator.LogAnd2" nil) -1)

(deffoldr logior (make-combinator "lisp.combinator.LogIor2" nil) 0)

(deffoldr logxor (make-combinator "lisp.combinator.LogXor2" nil) 0)

(defproc odd? (n) (logbit? 0 n))

(defproc even? (n) (not (odd? n)))