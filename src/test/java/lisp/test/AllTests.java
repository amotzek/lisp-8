/*
 * Created on 13.08.2009
 */
package lisp.test;
//
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
//
@RunWith(Suite.class)
@Suite.SuiteClasses({CompilerTest.class, ParserTest.class, ControlTest.class, LetTest.class, FutureTest.class, ChannelTest.class, LockTest.class,
        ExceptionTest.class, ArithmeticTest.class, StringTest.class, ListTest.class, SetTest.class,
        AssociationListTest.class, ArrayTest.class, HashTableTest.class, ObjectTest.class, AtTest.class,
        ModuleTest.class, TransitivityTest.class, RandomExceptionTest.class, SymbolTest.class})
public class AllTests
{
}