/*
 * Created on 13.08.2009
 */
package lisp.test;
//
import lisp.CannotEvalException;
import lisp.Rational;
import org.junit.Test;
import java.util.Random;
import static org.junit.Assert.assertEquals;
//
public class ArithmeticTest extends AbstractTest
{
    private static Random random = new Random();
    //
    public ArithmeticTest()
    {
        super();
    }
    //
    @Test
    public void testParse() throws CannotEvalException
    {
        checkValue(1d);
        checkValue(0.1d);
        checkValue(0.5d);
        checkValue(0.25d);
        checkValue(0.2d);
        checkValue(0.125d);
        checkValue(10d);
        checkValue(1e10d);
        checkValue(2345.6789d);
        checkValue(-983e6d);
        checkValue(4.723e-12d);
        checkValue(-1d);
        //
        for (int i = 0; i < 30; i++)
        {
            checkValue(random.nextDouble());
        }
    }
    //
    private static void checkValue(double value)
    {
        String doubleToString = Double.toString(value);
        Rational rational = new Rational(doubleToString);
        String rationalToString = rational.toString();
        assertEquals(value, Double.parseDouble(rationalToString), 0d);
        logger.info(doubleToString + " -> " + rationalToString);
    }
    //
    @Test
    public void testAbs() throws CannotEvalException
    {
        expectValue("(abs 3)", "3");
        expectValue("(abs 0)", "0");
        expectValue("(abs -5)", "5");
    }
    //
    @Test
    public void testAdd() throws CannotEvalException
    {
        expectValue("(add 3 4)", "7");
        expectValue("(add 3.2 3.9)", "7.1");
        expectValue("(add 4 -3)", "1");
        expectValue("(add 3.9 -2.9)", "1");
        expectValue("(add 3 -4)", "-1");
        expectValue("(add 2.9 -3.1)", "-0.2");
        expectValue("(+ 3 7 5 1 4)", "20");
        expectValue("(+ 1/3 2/3)", "1");
        expectValue("(+ 1/16 1/16)", "1/8");
        equalValues("(+ 2 3 4)", "(+ 3 4 2)");
    }
    //
    @Test
    public void testSub() throws CannotEvalException
    {
        expectValue("(sub 3 4)", "-1");
        expectValue("(sub 3.2 3.9)", "-0.7");
        expectValue("(sub 4 -3)", "7");
        expectValue("(sub 3.9 -2.9)", "6.8");
        expectValue("(sub 3 -4)", "7");
        expectValue("(sub 2.9 -3.1)", "6");
        expectValue("(- 3 7 5 1 4)", "-14");
        equalValues("(- 2 3 4)", "(- 2 4 3)");
    }
    //
    @Test
    public void testTimes() throws CannotEvalException
    {
        expectValue("(times 2 3)", "6");
        expectValue("(times 1.1 2.2)", "2.42");
        expectValue("(times 1.23 -2.34)", "-2.8782");
        expectValue("(times -3.21 -4.32)", "13.8672");
        expectValue("(* 2/3 3/2)", "1");
        expectValue("(* -5/7 -7/5)", "1");
        expectValue("(* 2 3 4 5)", "120");
        equalValues("(* 2 3 4 5)", "(* 3 5 2 4)");
    }
    //
    @Test
    public void testQuotient() throws CannotEvalException
    {
        expectValue("(quotient 9 3)", "3");
        expectValue("(quotient 1.21 1.1)", "1.1");
        expectValue("(quotient 13 -2)", "-6.5");
        expectValue("(quotient -8 4)", "-2");
        expectValue("(quotient -81 -9)", "9");
        expectValue("(/ 1 3)", "1/3");
        expectValue("(/ 120 3 4 5)", "2");
        equalValues("(/ 120 3 4 5)", "(/ 120 5 3 4)");
    }
    //
    @Test
    public void testDistributive() throws CannotEvalException
    {
        equalValues("(* 2 (+ 3 4))", "(+ (* 2 3) (* 2 4))");
        equalValues("(* (+ 3 4) 5)", "(+ (* 3 5) (* 4 5))");
        equalValues("(* 5 5 5)", "(+ (* 3 3 3) (* 3 3 3 2) (* 3 3 2 2) (* 2 2 2))");
    }
    //
    @Test
    public void testFloor() throws CannotEvalException
    {
        expectValue("(floor 3.1)", "3");
        expectValue("(floor 2.9)", "2");
        expectValue("(floor 4)", "4");
        expectValue("(floor -4.1)", "-5");
        expectValue("(floor -3.9)", "-4");
        expectValue("(floor -2)", "-2");
    }
    //
    @Test
    public void testApproximate() throws CannotEvalException
    {
        expectValue("(numerator (approximate 3.14159265 1e-5))", "355");
        expectValue("(denominator (approximate 3.14159265 1e-5))", "113");
        expectValue("(approximate 0.333333 1e-5)", "1/3");
        expectValue("(approximate (- 1e-6 1/7) 1e-5)", "-1/7");
        expectValue("(approximate 1023/13 1e-5)", "1023/13");
        expectValue("(approximate 1000/3 1e-2)", "333");
    }
    //
    @Test
    public void testEqual() throws CannotEvalException
    {
        expectValue("(equal? 9 3)", "nil");
        expectValue("(equal? 13 13)", "t");
        expectValue("(equal? -9 4)", "nil");
        expectValue("(equal? -80 -80)", "t");
        expectValue("(= 9 3)", "nil");
        expectValue("(= 13 13)", "t");
        expectValue("(= -9 4)", "nil");
        expectValue("(= -80 -80)", "t");
    }
    //
    @Test
    public void testGreater() throws CannotEvalException
    {
        expectValue("(greater? 9 3)", "t");
        expectValue("(greater? 13 13)", "nil");
        expectValue("(greater? -9 4)", "nil");
        expectValue("(greater? -80 -81)", "t");
        expectValue("(greater? -79.9 -80)", "t");
        expectValue("(greater? -80.1 -80)", "nil");
        expectValue("(> 9 3)", "t");
        expectValue("(> 13 13)", "nil");
        expectValue("(> -9 4)", "nil");
        expectValue("(> -80 -81)", "t");
        expectValue("(> -79.9 -80)", "t");
        expectValue("(> -80.1 -80)", "nil");
    }
    @Test
    public void testGreaterOrEqual() throws CannotEvalException
    {
        expectValue("(greater-or-equal? 9 3)", "t");
        expectValue("(greater-or-equal? 13 13)", "t");
        expectValue("(greater-or-equal? -9 4)", "nil");
        expectValue("(greater-or-equal? -80 -81)", "t");
        expectValue("(greater-or-equal? -79.9 -80)", "t");
        expectValue("(greater-or-equal? -80.1 -80)", "nil");
        expectValue("(>= 9 3)", "t");
        expectValue("(>= 13 13)", "t");
        expectValue("(>= -9 4)", "nil");
        expectValue("(>= -80 -81)", "t");
        expectValue("(>= -79.9 -80)", "t");
        expectValue("(>= -80.1 -80)", "nil");
    }
    //
    @Test
    public void testLess() throws CannotEvalException
    {
        expectValue("(less? 9.78 3)", "nil");
        expectValue("(less? 13 13)", "nil");
        expectValue("(less? -9 4)", "t");
        expectValue("(less? -80.23 -81.645)", "nil");
        expectValue("(less? -5.1 5)", "t");
        expectValue("(less? -5 -4.9)", "t");
        expectValue("(< 9.78 3)", "nil");
        expectValue("(< 13 13)", "nil");
        expectValue("(< -9 4)", "t");
        expectValue("(< -80.23 -81.645)", "nil");
        expectValue("(< -5.1 5)", "t");
        expectValue("(< -5 -4.9)", "t");
    }
    //
    @Test
    public void testLessOrEqual() throws CannotEvalException
    {
        expectValue("(less-or-equal? 9.78 3)", "nil");
        expectValue("(less-or-equal? 13 13)", "t");
        expectValue("(less-or-equal? -9 4)", "t");
        expectValue("(less-or-equal? -80.23 -81.645)", "nil");
        expectValue("(less-or-equal? -5.1 5)", "t");
        expectValue("(less-or-equal? -5 -4.9)", "t");
        expectValue("(<= 9.78 3)", "nil");
        expectValue("(<= 13 13)", "t");
        expectValue("(<= -9 4)", "t");
        expectValue("(<= -80.23 -81.645)", "nil");
        expectValue("(<= -5.1 5)", "t");
        expectValue("(<= -5 -4.9)", "t");
    }
    //
    @Test
    public void testType() throws CannotEvalException
    {
        expectValue("(number? -14)", "t");
        expectValue("(number? 6.22)", "t");
        expectValue("(integer? 2)", "t");
        expectValue("(integer? 2.3)", "nil");
        expectValue("(type-of -4)", "integer");
        expectValue("(type-of 0.22)", "ratio");
    }
    //
    @Test
    public void testRandom() throws CannotEvalException
    {
        expectValue("(less? (random 5) 5)", "t");
        expectValue("(less? (random 5) 5)", "t");
        expectValue("(less? (random 5) 5)", "t");
        expectValue("(less? (random 5) 5)", "t");
        expectValue("(greater? (random 5) -1)", "t");
        expectValue("(greater? (random 5) -1)", "t");
        expectValue("(greater? (random 5) -1)", "t");
        expectValue("(greater? (random 5) -1)", "t");
    }
    //
    @Test
    public void testRem() throws CannotEvalException
    {
        expectValue("(rem 9 3)", "0");
        expectValue("(rem 13 -2)", "1");
        expectValue("(rem -9 4)", "-1");
        expectValue("(rem -80 -9)", "-8");
    }
    //
    @Test
    public void testExptMod() throws CannotEvalException
    {
        expectValue("(exptmod 2 5 9)", "5");
        expectValue("(exptmod 3 -1 11)", "4");
        expectValue("(exptmod 244 -1 117)", "82");
        expectValue("(string? (catch nil (exptmod 3 -1 9)))", "t");
    }
    //
    @Test
    public void testModInv() throws CannotEvalException
    {
        expectValue("(modinv 3 11)", "4");
        expectValue("(modinv 244 117)", "82");
        expectValue("(string? (catch nil (modinv 3 9)))", "t");
    }
    //
    @Test
    public void testGcd() throws CannotEvalException
    {
        expectValue("(gcd 2 3)", "1");
        expectValue("(gcd 3 11)", "1");
        expectValue("(gcd 244 117)", "1");
        expectValue("(gcd 18 27)", "9");
    }
    //
    @Test
    public void testPrime() throws CannotEvalException
    {
        expectValue("(prime? 0)", "nil");
        expectValue("(prime? 1)", "nil");
        expectValue("(prime? 2)", "t");
        expectValue("(prime? 3)", "t");
        expectValue("(prime? 5)", "t");
        expectValue("(prime? 7)", "t");
        expectValue("(prime? 11)", "t");
        expectValue("(prime? 1009)", "t");
        expectValue("(prime? 4)", "nil");
        expectValue("(prime? 9)", "nil");
        expectValue("(prime? 15)", "nil");
        expectValue("(prime? 21)", "nil");
        expectValue("(prime? -21)", "nil");
        expectValue("(prime? -3)", "nil");
        expectValue("(prime? (* 99991 99989))", "nil");
    }
    //
    @Test
    public void testAsh() throws CannotEvalException
    {
        expectValue("(ash 16 1)", "32");
        expectValue("(ash 16 0)", "16");
        expectValue("(ash 16 -1)", "8");
        expectValue("(ash -100000000000000000000000000000000 -100)", "-79");
    }
    //
    @Test
    public void testLogAnd() throws CannotEvalException
    {
        expectValue("(logand)", "-1");
        expectValue("(logand 13)", "13");
        expectValue("(logand 0 0)", "0");
        expectValue("(logand 0 1)", "0");
        expectValue("(logand 1 0)", "0");
        expectValue("(logand 1 1)", "1");
        expectValue("(logand 16 31)", "16");
        expectValue("(logand 7 15 3)", "3");
        expectValue("(logand 2 3 15)", "2");
        expectValue("(logand 8 3 4)", "0");
    }
    //
    @Test
    public void testLogIor() throws CannotEvalException
    {
        expectValue("(logior)", "0");
        expectValue("(logior 13)", "13");
        expectValue("(logior 0 0)", "0");
        expectValue("(logior 0 1)", "1");
        expectValue("(logior 1 0)", "1");
        expectValue("(logior 1 1)", "1");
        expectValue("(logior 1 2 4)", "7");
        expectValue("(logior 1 2 4 8)", "15");
        expectValue("(logior 8 1 4 2)", "15");
        expectValue("(logior 9 3)", "11");
    }
    //
    @Test
    public void testLogXor() throws CannotEvalException
    {
        expectValue("(logxor)", "0");
        expectValue("(logxor 12)", "12");
        expectValue("(logxor 0 0)", "0");
        expectValue("(logxor 0 1)", "1");
        expectValue("(logxor 1 0)", "1");
        expectValue("(logxor 1 1)", "0");
        expectValue("(logxor 1 3 7 15)", "10");
    }
    //
    @Test
    public void testLogNot() throws CannotEvalException
    {
        expectValue("(lognot 0)", "-1");
        expectValue("(lognot 1)", "-2");
        expectValue("(lognot -1)", "0");
        expectValue("(lognot (+ 1 (lognot 1000)))", "999");
    }
    //
    @Test
    public void testLogBit() throws CannotEvalException
    {
        expectValue("(logbit? 0 0)", "nil");
        expectValue("(logbit? 0 1)", "t");
        expectValue("(logbit? 0 2)", "nil");
        expectValue("(logbit? 1 1)", "nil");
        expectValue("(logbit? 67 -1)", "t");
        equalValues("(logbit? 0 (lognot 6723))", "(not (logbit? 0 6723))");
        equalValues("(logbit? 1 (lognot 6723))", "(not (logbit? 1 6723))");
        equalValues("(logbit? 2 (lognot 6723))", "(not (logbit? 2 6723))");
        equalValues("(logbit? 3 (lognot 6723))", "(not (logbit? 3 6723))");
    }
    //
    @Test
    public void testOdd() throws CannotEvalException
    {
        expectValue("(odd? 0)", "nil");
        expectValue("(odd? 1)", "t");
        expectValue("(odd? 2)", "nil");
        expectValue("(odd? -1)", "t");
        expectValue("(odd? -2)", "nil");
    }
    //
    @Test
    public void testEven() throws CannotEvalException
    {
        expectValue("(even? 0)", "t");
        expectValue("(even? 1)", "nil");
        expectValue("(even? 2)", "t");
        expectValue("(even? -1)", "nil");
        expectValue("(even? -2)", "t");
    }
    //
    @Test
    public void testIntegerLength() throws CannotEvalException
    {
        expectValue("(integer-length 0)", "0");
        expectValue("(integer-length 4)", "3");
        expectValue("(integer-length -7)", "3");
        expectValue("(length -8)", "3");
        expectValue("(length 512)", "10");
    }
    //
    @Test
    public void testWriteToString() throws CannotEvalException
    {
        expectValue("(write-to-string (/ 6525 625))", "\"10.44\"");
        expectValue("(write-to-string (/ 6524 625))", "\"10.4384\"");
        expectValue("(write-to-string (/ 6527503 625))", "\"10444.0048\"");
    }
}