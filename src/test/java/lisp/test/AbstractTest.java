/*
 * Created on 13.08.2009
 */
package lisp.test;
//
import lisp.CannotEvalException;
import lisp.Closure;
import lisp.Sexpression;
import lisp.combinator.EnvironmentFactory;
import lisp.environment.Environment;
import lisp.parser.Parser;
import org.junit.Before;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
//
public abstract class AbstractTest
{
    protected static final Logger logger = Logger.getLogger("lisp.tests.Test");
    //
    protected Environment environment;
    //
    protected AbstractTest()
    {
        super();
    }
    //
    @Before
    public void setUp() throws Exception
    {
        environment = EnvironmentFactory.createEnvironment().copy();
    }
    //
    protected void expectValue(String form, String expectedvalue) throws CannotEvalException
    {
        Parser parser = new Parser(environment, form, 0);
        Sexpression request = parser.getSexpression();
        Closure closure = new Closure(environment, request);
        Sexpression actualresult = closure.eval();
        parser = new Parser(environment, expectedvalue, 0);
        Sexpression expectedresult = parser.getSexpression();
        logger.info(request + " -> " + actualresult);
        assertEquals(expectedresult, actualresult);
    }
    //
    protected void expectUnparsedValue(String form, String expectedvalue) throws CannotEvalException
    {
        Parser parser = new Parser(environment, form, 0);
        Sexpression request = parser.getSexpression();
        Closure closure = new Closure(environment, request);
        Sexpression actualresult = closure.eval();
        logger.info(request + " -> " + actualresult);
        //
        if (actualresult == null)
        {
            assertEquals(expectedvalue, "nil");
        }
        else
        {
            assertEquals(expectedvalue, actualresult.toString());
        }
    }
    //
    protected void equalValues(String form1, String form2) throws CannotEvalException
    {
        Parser parser = new Parser(environment, form1, 0);
        Sexpression request1 = parser.getSexpression();
        Closure closure = new Closure(environment, request1);
        Sexpression result1 = closure.eval();
        parser = new Parser(environment, form2, 0);
        Sexpression request2 = parser.getSexpression();
        closure = new Closure(environment, request2);
        Sexpression result2 = closure.eval();
        logger.info(request1 + " -> " + result1);
        logger.info(request2 + " -> " + result2);
        assertEquals(result1, result2);
    }
}