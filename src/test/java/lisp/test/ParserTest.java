/*
 * Created on 19.12.2012
 */
package lisp.test;
//
import lisp.Bytes;
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.List;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
//
public class ParserTest extends AbstractTest
{
    public ParserTest()
    {
        super();
    }
    //
    @Test
    public void testString() throws CannotEvalException
    {
        expectValue("(concatenate \"abc\" \"\\r\" \"def\")", "\"abc\\rdef\"");
    }
    //
    @Test
    public void testReadTimeEvaluation() throws CannotEvalException
    {
        expectValue("#.(+ 2 ", "nil");
        expectValue("#.(+ 2 #.(+ 1 1) 1", "nil");
        expectValue("#.(+ 2 3)", "5");
        expectValue("(quote (1 #.(+ 1 1) 3))", "(1 2 3)");
        expectValue("(quote (1 2 #.(+ 1 1 1) #.(+ #.(+ 1 1) 2)))", "(1 2 3 4)");
    }
    //
    @Test
    public void testBytes() throws CannotEvalException
    {
        Bytes bytes = new Bytes(new byte[] { 1, 0, 23, -100});
        expectValue(bytes.toString(), "#.(base64-decode \"AQAXnA==\")");
    }
    //
    @Test
    public void testFuture() throws CannotEvalException
    {
        expectUnparsedValue("(go (* 2 3 4))", "#.(throw (quote error) \"futures cannot be read or written\")");
    }
    //
    @Test
    public void testGenericFunction() throws CannotEvalException
    {
        expectUnparsedValue("(let ((function nil)) (progn (defmethod function (a b) t (+ a b)) function))", "#.(throw (quote error) \"generic-functions cannot be read or written\")");
    }
    //
    @Test
    public void testMethod() throws CannotEvalException
    {
        expectUnparsedValue("(let ((function nil)) (defmethod function (a b) t (+ a b)))", "#.(throw (quote error) \"methods cannot be read or written\")");
    }
    //
    @Test
    public void testClass() throws CannotEvalException
    {
        expectUnparsedValue("(let ((thing nil)) (defclass thing ()))", "thing");
    }
    //
    @Test
    public void testInstance() throws CannotEvalException
    {
        StringBuilder builder1 = new StringBuilder();
        builder1.append("(let ((a-thing nil)) ");
        builder1.append("(progn ");
        builder1.append("(setq a-thing (allocate-instance thing)) ");
        builder1.append("(.= a-thing hm 1) ");
        builder1.append("(.= a-thing gut (quote zwo)) ");
        builder1.append("(.= a-thing ih 3) ");
        builder1.append("(.= a-thing fl nil) ");
        builder1.append("(freeze a-thing)))");
        StringBuilder builder2 = new StringBuilder();
        builder2.append("(let ((instance (allocate-instance thing))) ");
        builder2.append("(progn (.= instance fl nil) ");
        builder2.append("(.= instance gut (quote zwo)) ");
        builder2.append("(.= instance hm 1) ");
        builder2.append("(.= instance ih 3) ");
        builder2.append("(freeze instance)))");
        expectValue("(null? (defclass thing ()))", "nil");
        expectUnparsedValue(builder1.toString(), "#." + builder2);
        expectValue(builder2.toString(), "#." + builder2);
    }
    //
    @Test
    public void testCyclicInstance() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("#1=#.(let ((instance (allocate-instance thing))) ");
        builder.append("(progn (assignq instance value ");
        builder.append("#.(let ((instance (allocate-instance thing))) ");
        builder.append("(progn (assignq instance value #1#) (freeze instance)))) ");
        builder.append("(freeze instance)))");
        expectValue("(null? (defclass thing ()))", "nil");
        expectValue(builder.toString(), "#." + builder);
    }
    //
    @Test
    public void testCyclicList() throws CannotEvalException
    {
        expectValue("(quote #1=(1 2 #1#))", "#1=(1 2 #1#)");
    }
    //
    @Test
    public void testArray() throws CannotEvalException
    {
        StringBuilder builder1 = new StringBuilder();
        builder1.append("(let ((array (make-array (quote (4))))) ");
        builder1.append("(progn (set-array-element array (quote (2)) (quote zwo)) ");
        builder1.append("(set-array-element array (quote (1)) \"eins\") ");
        builder1.append("(set-array-element array (quote (3)) 3) ");
        builder1.append("array))");
        StringBuilder builder2 = new StringBuilder();
        builder2.append("(let ((array (make-array (quote (4))))) ");
        builder2.append("(progn (set-array-element array (quote (1)) \"eins\") ");
        builder2.append("(set-array-element array (quote (2)) (quote zwo)) ");
        builder2.append("(set-array-element array (quote (3)) 3) array))");
        expectUnparsedValue(builder1.toString(), "#." + builder2);
    }
    //
    @Test
    public void testFrozenCyclicArray() throws CannotEvalException
    {
        StringBuilder builder1 = new StringBuilder();
        builder1.append("#1=#.(let ");
        builder1.append("((array (make-array (quote (1))))) ");
        builder1.append("(progn ");
        builder1.append("(set-array-element array (quote (0)) #1#) ");
        builder1.append("(freeze array)))");
        expectUnparsedValue(builder1.toString(), builder1.toString());
    }
    //
    @Test
    public void testFrozenCyclicHashTable1() throws CannotEvalException
    {
        StringBuilder builder1 = new StringBuilder();
        builder1.append("#1=#.(let ");
        builder1.append("((hash-table (make-hash-table))) ");
        builder1.append("(progn ");
        builder1.append("(put-hash-table-value hash-table (quote a) #1#) ");
        builder1.append("(freeze hash-table)))");
        expectUnparsedValue(builder1.toString(), builder1.toString());
    }
    //
    @Test
    public void testFrozenCyclicHashTable2() throws CannotEvalException
    {
        StringBuilder builder1 = new StringBuilder();
        builder1.append("#1=#.(let ");
        builder1.append("((hash-table (make-hash-table))) ");
        builder1.append("(progn ");
        builder1.append("(put-hash-table-value hash-table #1# (quote a)) ");
        builder1.append("(freeze hash-table)))");
        expectUnparsedValue(builder1.toString(), "nil"); // #1# is mutable and cannot be key of a hash table
    }
    //
    @Test
    public void testHashHash() throws CannotEvalException
    {
        List list1 = List.list(new Chars("a"), new Chars("b"), new Chars("c"));
        List list2 = List.list(list1, list1, list1);
        assertEquals(list2.toString(), "(#1=(\"a\" \"b\" \"c\") #1# #1#)");
        expectValue("(quote (1 #1=(2 3) #1# 4 #1#))", "(1 (2 3) (2 3) 4 (2 3))");
        expectValue("(quote (1 #1=(2 3) #1=(3 4) #1#))", "nil");
        expectValue("(quote (1 #1=(2 3) #2#))", "nil");
        expectValue("(quote (1 #12345678901234567890=(2 3)))", "nil");
        expectUnparsedValue("(let ((x (quote (1 2)))) (list x x))", "(#1=(1 2) #1#)");
    }
    //
    @Test
    public void testComment() throws CannotEvalException
    {
        expectValue("(list 1 #| comment |# 2 3)", "(1 2 3)");
        expectValue("(list 1 #| comment ||# 2 3)", "(1 2 3)");
        expectValue("(list 1 #| comment | |# 2 3)", "(1 2 3)");
    }
}