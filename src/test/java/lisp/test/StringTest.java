/*
 * Created on 14.08.2009
 */
package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
//
public class StringTest extends AbstractTest
{
    public StringTest()
    {
        super();
    }
    //
    @Test
    public void testCharCode() throws CannotEvalException
    {
        expectValue("(char-code \"A\")", "65");
    }
    //
    @Test
    public void testCodeChar() throws CannotEvalException
    {
        expectValue("(code-char 65)", "\"A\"");
    }
    //
    @Test
    public void testConcatenate() throws CannotEvalException
    {
        expectValue("(concatenate \"A\" \"B\" \"C\")", "\"ABC\"");
        expectValue("(concatenate \"C\")", "\"C\"");
        expectValue("(concatenate)", "\"\"");
    }
    //
    @Test
    public void testEqual() throws CannotEvalException
    {
        expectValue("(equal? \"ist ein string\" \"ist ein string\")", "t");
        expectValue("(equal? \"ist ein string\" \"ist ein anderer string\")", "nil");
    }
    //
    @Test
    public void testGreater() throws CannotEvalException
    {
        expectValue("(greater? \"B\" \"A\")", "t");
        expectValue("(greater? \"A\" \"B\")", "nil");
        expectValue("(greater? \"C\" \"C\")", "nil");
    }
    //
    @Test
    public void testLess() throws CannotEvalException
    {
        expectValue("(less? \"B\" \"A\")", "nil");
        expectValue("(less? \"A\" \"B\")", "t");
        expectValue("(less? \"C\" \"C\")", "nil");
    }
    //
    @Test
    public void testReadFromString() throws CannotEvalException
    {
        expectValue("(read-from-string \"symb\")", "symb");
        expectValue("(read-from-string \"87.2\")", "87.2");
        expectValue("(read-from-string \"nil\")", "nil");
        expectValue("(read-from-string \"(A B C)\")", "(A B C)");
    }
    //
    @Test
    public void testWriteToString() throws CannotEvalException
    {
        expectValue("(write-to-string \"doppelt\")", "\"\\\"doppelt\\\"\"");
        expectValue("(write-to-string 94.7)", "\"94.7\"");
        expectValue("(write-to-string (quote (a b c)))", "\"(a b c)\"");
    }
    //
    @Test
    public void testSearchString() throws CannotEvalException
    {
        expectValue("(search-string \"ein\" \"ist ein string\" 1)", "5");
        expectValue("(search-string \"ein\" \"ist ein string\" 6)", "nil");
        expectValue("(search-string \"kein\" \"ist ein string\" 1)", "nil");
        expectValue("(search-string \"ein\" \"\" 1)", "nil");
        expectValue("(search-string \"\" \"\" 1)", "1");
        expectValue("(search-string \"\" \"was geht\" 1)", "1");
    }
    //
    @Test
    public void testSubtring() throws CannotEvalException
    {
        expectValue("(substring \"ist ein string\" 1 3)", "\"ist\"");
        expectValue("(substring \"ist ein string\" 5 3)", "\"ein\"");
        expectValue("(substring \"ist ein string\" 5 0)", "\"\"");
        expectValue("(substring \"\" 1 0)", "\"\"");
    }
    //
    @Test
    public void testStringCapitalize() throws CannotEvalException
    {
        expectValue("(string-capitalize \"hallo welt\")", "\"Hallo Welt\"");
        expectValue("(string-capitalize \"\")", "\"\"");
    }
    //
    @Test
    public void testStringDowncase() throws CannotEvalException
    {
        expectValue("(string-downcase \"LAUT!!\")", "\"laut!!\"");
        expectValue("(string-downcase \"���\")", "\"���\"");
        expectValue("(string-downcase \"\u03b1\u0391\")", "\"\u03b1\u03b1\"");
        expectValue("(string-downcase \"leise!!\")", "\"leise!!\"");
        expectValue("(string-downcase \"\")", "\"\"");
    }
    //
    @Test
    public void testStringUpcase() throws CannotEvalException
    {
        expectValue("(string-upcase \"leise!!\")", "\"LEISE!!\"");
        expectValue("(string-upcase \"���\")", "\"���\"");
        expectValue("(string-upcase \"\u03b1\u0391\")", "\"\u0391\u0391\"");
        expectValue("(string-upcase \"LEISE!!\")", "\"LEISE!!\"");
        expectValue("(string-upcase \"\")", "\"\"");
    }
    //
    @Test
    public void testStringTrim() throws CannotEvalException
    {
        expectValue("(string-trim \" mit rand \")", "\"mit rand\"");
        expectValue("(string-trim \"ohne rand\")", "\"ohne rand\"");
        expectValue("(string-trim \"\")", "\"\"");
    }
    //
    @Test
    public void testStringLength() throws CannotEvalException
    {
        expectValue("(string-length \"1234\")", "4");
        expectValue("(string-length \"\")", "0");
        expectValue("(length \"1234\")", "4");
        expectValue("(length \"\")", "0");
    }
    //
    @Test
    public void testType() throws CannotEvalException
    {
        expectValue("(string? \"la la la\")", "t");
        expectValue("(string? 1)", "nil");
        expectValue("(string? (quote a))", "nil");
        expectValue("(type-of \"ho ho ho\")", "string");
    }
    //
    @Test
    public void testTokenizeString() throws CannotEvalException
    {
        expectValue("(tokenize-string \"1;2;3\" \";\" nil)", "(\"1\" \"2\" \"3\")");
        expectValue("(tokenize-string \"1;2;3\" \";\" t)", "(\"1\" \";\" \"2\" \";\" \"3\")");
        expectValue("(tokenize-string (concatenate \"1\" (code-char 10) \"2\" (code-char 10) \"3\") (concatenate (code-char 10) (code-char 13)) nil)", "(\"1\" \"2\" \"3\")");
    }
    //
    @Test
    public void testRegexpMatches() throws CannotEvalException
    {
        expectValue("(regexp-matches \"x\" \"y\")", "nil");
        expectValue("(regexp-matches \"x\" \"xx\")", "nil");
        expectValue("(regexp-matches \"x+\" \"x\")", "(\"x\")");
        expectValue("(regexp-matches \"x+\" \"xy\")", "nil");
        expectValue("(regexp-matches \"x+\" \"xx\")", "(\"xx\")");
        expectValue("(regexp-matches \"(x*)(y*)(z*)\" \"xx\")", "(\"xx\" \"xx\" \"\" \"\")");
        expectValue("(regexp-matches \"(x*)(y*)(z*)\" \"xyyzzz\")", "(\"xyyzzz\" \"x\" \"yy\" \"zzz\")");
        expectValue("(regexp-matches \"(a)(b)?\" \"a\")", "(\"a\" \"a\" nil)");
    }
}

