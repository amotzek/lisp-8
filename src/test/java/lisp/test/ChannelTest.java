package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
/**
 * Created by andreasm on 18.11.2012 13:39
 */
public class ChannelTest extends AbstractTest
{
    @Test
    public void testMakeChannel() throws CannotEvalException
    {
        expectValue("(type-of (make-channel 0))", "channel");
    }
    //
    @Test
    public void testClosed() throws CannotEvalException
    {
        expectValue("(closed-channel? (close-channel (make-channel 0)))", "t");
    }
    //
    @Test
    public void testOpen() throws CannotEvalException
    {
        expectValue("(closed-channel? (make-channel 0))", "nil");
    }
    //
    @Test
    public void testSendReceive0() throws CannotEvalException
    {
        expectValue("(let ((channel (make-channel 0))) (list (send-on-channel channel \"hallo\" nil) (receive-from-channels (list channel) nil)))", "(\"hallo\" \"hallo\")");
    }
    //
    @Test
    public void testSendReceive1() throws CannotEvalException
    {
        expectValue("(let ((channel (make-channel 1))) (list (send-on-channel channel \"hallo\" nil) (receive-from-channels (list channel) nil)))", "(\"hallo\" \"hallo\")");
    }
    //
    @Test
    public void testSend2() throws CannotEvalException
    {
        expectValue("(let ((channel (make-channel 1)) (v 0)) (progn (send-on-channel channel 1 nil) (setq v (+ v 1)) (go (progn (send-on-channel channel 1 nil) (setq v (+ v 1)))) (receive-from-channels nil 100) v))", "1");
    }
    //
    @Test
    public void testSendAltReceive0() throws CannotEvalException
    {
        expectValue("(let ((channel1 (make-channel 0)) (channel2 (make-channel 0))) (list (send-on-channel channel2 \"hallo\" nil) (receive-from-channels (list channel1 channel2) nil)))", "(\"hallo\" \"hallo\")");
    }
    //
    @Test
    public void testSendAltReceive1() throws CannotEvalException
    {
        expectValue("(let ((channel1 (make-channel 1)) (channel2 (make-channel 1))) (progn (send-on-channel channel1 \"hallo\" nil) (send-on-channel channel2 \"welt\" nil) (receive-from-channels (list channel2 channel1) nil)))", "\"welt\"");
    }
    //
    @Test
    public void testReceiveSend0() throws CannotEvalException
    {
        expectValue("(let ((channel (make-channel 0))) (list (receive-from-channels (list channel) nil) (send-on-channel channel \"hallo\" nil)))", "(\"hallo\" \"hallo\")");
    }
    //
    @Test
    public void testReceiveSend1() throws CannotEvalException
    {
        expectValue("(let ((channel (make-channel 1))) (list (receive-from-channels (list channel) nil) (send-on-channel channel \"hallo\" nil)))", "(\"hallo\" \"hallo\")");
    }
    //
    @Test
    public void testCloseReceive() throws CannotEvalException
    {
        expectValue("(let ((channel (close-channel (make-channel 0)))) (receive-from-channels (list channel) nil))", "nil");
    }
    //
    @Test
    public void testCloseSend() throws CannotEvalException
    {
        expectValue("(let ((channel (close-channel (make-channel 0)))) (send-on-channel channel \"hallo\" nil))", "nil");
    }
    //
    @Test
    public void testTimeoutReceive() throws CannotEvalException
    {
        expectValue("(let ((channel (make-channel 0))) (receive-from-channels (list channel) 10))", "nil");
    }
    //
    @Test
    public void testWait() throws CannotEvalException
    {
        expectValue("(receive-from-channels nil 10)", "nil");
    }
    //
    @Test
    public void testTimeoutSend() throws CannotEvalException
    {
        expectValue("(let ((channel (make-channel 0))) (send-on-channel channel \"hallo\" 10))", "nil");
    }
    //
    @Test
    public void testNoTimeoutSend() throws CannotEvalException
    {
        expectValue("(let ((channel (make-channel 1))) (send-on-channel channel \"hallo\" 10000))", "\"hallo\"");
    }
}