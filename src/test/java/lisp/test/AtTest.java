package lisp.test;
//
import lisp.CannotEvalException;
import lisp.Place;
import lisp.SimpleClass;
import lisp.Symbol;
import lisp.remote.protocol.Protocol;
import lisp.remote.server.Server;
import org.junit.*;

import java.util.Collections;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
/**
 * Created by andreasm on 24.10.2013 06:45
 */
public class AtTest extends AbstractTest
{
    private static Server server;
    private static Place place;
    //
    @BeforeClass
    public static void setUp2() throws Exception
    {
        server = new Server(null, "127.0.0.255", 4321);
        server.start();
        Protocol protocol = server.getProtocol();
        place = protocol.getHere();
    }
    //
    @AfterClass
    public static void tearDown2()
    {
        server.stop();
    }
    //
    @Test
    public void testAt() throws CannotEvalException
    {
        expectValue("(sort (list (+ 2 3 4 6) (or nil 3) 7) less?)", "(3 7 15)");
        expectValue(call("(sort (list (+ 2 3 4 6) (or nil 3) 7) less?)"), "(3 7 15)");
    }
    //
    @Test
    public void testArraySerialization() throws CannotEvalException
    {
        expectValue(call("(let ((a (make-array (quote (1))))) (progn (set-array-element a (list 0) 23) (freeze a)))"), "#.(let ((array (make-array (quote (1))))) (progn (set-array-element array (quote (0)) 23) (freeze array)))");
    }
    //
    @Test
    public void testHashTableSerialization() throws CannotEvalException
    {
        expectValue(call("(let ((h (make-hash-table))) (progn (put-hash-table-value h (quote two-three) 23) (freeze h)))"), "#.(let ((hash-table (make-hash-table))) (progn (put-hash-table-value hash-table (quote two-three) 23) (freeze hash-table)))");
    }
    //
    @Test
    public void testMakeCombinator1()
    {
        try
        {
            expectValue("(let ((c (make-combinator \"lisp.combinator.MakeCombinator\" nil))) " + call("(type-of (c \"lisp.combinator.Add\" nil))") +  ")", "combinator");
            fail();
        }
        catch (CannotEvalException e)
        {
            assertEquals("\"application of unsafe combinator\"", e.getSexpression().toString());
        }
    }
    //
    @Test
    public void testMakeCombinator2()
    {
        try
        {
            expectValue("(type-of " + call("(make-combinator \"lisp.combinator.MakeCombinator\" nil)") +  ")", "combinator");
            fail();
        }
        catch (CannotEvalException e)
        {
            assertEquals("\"null is not a function\"", e.getSexpression().toString());
        }
    }
    //
    @Test
    public void testMakeCombinator3()
    {
        try
        {
            expectValue(call("(((read-from-string \"#.make-combinator\") \"lisp.combinator.Add\" nil) 4 3)"), "7");
            fail();
        }
        catch (CannotEvalException e)
        {
            assertEquals("\"null is not a function\"", e.getSexpression().toString());
        }
    }
    //
    @Test
    public void testObjectSerialization() throws CannotEvalException
    {
        Symbol object = Symbol.createSymbol("object");
        environment.add(false, object, new SimpleClass(object, true, Collections.<SimpleClass>emptyList()));
        expectValue(call("(let ((some-object nil)) (progn (setq some-object (allocate-instance object)) (assignq some-object seven 7) (freeze some-object)))"), "#.(let ((instance (allocate-instance object))) (progn (assignq instance seven 7) (freeze instance)))");
    }
    //
    private String call(String inner)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(at ");
        builder.append(place);
        builder.append(" ");
        builder.append(inner);
        builder.append(")");
        //
        return builder.toString();
    }
}
