/*
 * Created on 14.08.2009
 */
package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
//
public class SetTest extends AbstractTest
{
    public SetTest()
    {
        super();
    }
    //
    @Test
    public void testAdjoin() throws CannotEvalException
    {
        expectValue("(subset? (adjoin (quote b) (quote (a c))) (quote (a b c)))", "t");
        expectValue("(subset? (quote (a b c)) (adjoin (quote b) (quote (a c))))", "t");
        expectValue("(adjoin (quote b) (quote (a b c)))", "(a b c)");
        expectValue("(adjoin (quote b) nil)", "(b)");
    }
    //
    @Test
    public void testDisjoint() throws CannotEvalException
    {
        expectValue("(disjoint? (quote (a b c)) (quote (a b c)))", "nil");
        expectValue("(disjoint? (quote (a b c)) (quote (e d c)))", "nil");
        expectValue("(disjoint? (quote (a b c)) (quote (d e f)))", "t");
        expectValue("(disjoint? nil (quote (a b c)))", "t");
        expectValue("(disjoint? (quote (a b c)) nil)", "t");
    }
    //
    @Test
    public void testMember() throws CannotEvalException
    {
        expectValue("(member? (quote d) (quote (a b c)))", "nil");
        expectValue("(not (member? (quote c) (quote (a b c))))", "nil");
        expectValue("(not (member? (quote b) (quote (a b c))))", "nil");
        expectValue("(not (member? (quote a) (quote (a b c))))", "nil");
        expectValue("(member? (quote a) nil)", "nil");
        expectValue("(member-if? number? (quote (a b c)))", "nil");
        expectValue("(not (member-if? number? (quote (a b 3))))", "nil");
        expectValue("(member-if? number? nil)", "nil");
    }
    //
    @Test
    public void testSubset() throws CannotEvalException
    {
        expectValue("(subset? (quote (c a b)) (quote (a b c)))", "t");
        expectValue("(subset? (quote (c a)) (quote (a b c)))", "t");
        expectValue("(subset? (quote (c a)) (quote (a b c)))", "t");
        expectValue("(subset? nil (quote (a b c)))", "t");
        expectValue("(subset? (quote (d)) (quote (a b c)))", "nil");
        expectValue("(subset? (quote (a b c d)) (quote (a b c)))", "nil");
        expectValue("(subset? (quote (a)) nil)", "nil");
    }
    //
    @Test
    public void testUnion() throws CannotEvalException
    {
        expectValue("(list-length (union (quote (c a b)) (quote (a b c))))", "3");
        expectValue("(list-length (union (quote (c)) (quote (a b c))))", "3");
        expectValue("(not (member? (quote a) (union (quote (c d)) (quote (a b c)))))", "nil");
        expectValue("(not (member? (quote b) (union (quote (c d)) (quote (a b c)))))", "nil");
        expectValue("(not (member? (quote c) (union (quote (c d)) (quote (a b c)))))", "nil");
        expectValue("(not (member? (quote d) (union (quote (c d)) (quote (a b c)))))", "nil");
    }
    //
    @Test
    public void testIntersection() throws CannotEvalException
    {
        expectValue("(list-length (intersection (quote (c a b)) (quote (a b c))))", "3");
        expectValue("(list-length (intersection (quote (c)) (quote (a b c))))", "1");
        expectValue("(not (member? (quote a) (intersection (quote (c d)) (quote (a b c)))))", "t");
        expectValue("(not (member? (quote b) (intersection (quote (c d)) (quote (a b c)))))", "t");
        expectValue("(not (member? (quote c) (intersection (quote (c d)) (quote (a b c)))))", "nil");
        expectValue("(not (member? (quote d) (intersection (quote (c d)) (quote (a b c)))))", "t");
    }
    //
    @Test
    public void testDifference() throws CannotEvalException
    {
        expectValue("(list-length (set-difference (quote (c a b)) (quote (a b c))))", "0");
        expectValue("(list-length (set-difference (quote (c)) (quote (a b c))))", "0");
        expectValue("(list-length (set-difference (quote (a b c)) (quote (c))))", "2");
        expectValue("(not (member? (quote a) (set-difference (quote (a b c)) (quote (c d)))))", "nil");
        expectValue("(not (member? (quote b) (set-difference (quote (a b c)) (quote (c d)))))", "nil");
        expectValue("(not (member? (quote c) (set-difference (quote (a b c)) (quote (c d)))))", "t");
        expectValue("(not (member? (quote d) (set-difference (quote (a b c)) (quote (c d)))))", "t");
    }
    //
    @Test
    public void testXor() throws CannotEvalException
    {
        expectValue("(list-length (set-exclusive-or (quote (c a b)) (quote (a b c))))", "0");
        expectValue("(list-length (set-exclusive-or (quote (c)) (quote (a b c))))", "2");
        expectValue("(list-length (set-exclusive-or (quote (a b c)) (quote (c))))", "2");
        expectValue("(not (member? (quote a) (set-exclusive-or (quote (a b c)) (quote (c d)))))", "nil");
        expectValue("(not (member? (quote b) (set-exclusive-or (quote (a b c)) (quote (c d)))))", "nil");
        expectValue("(not (member? (quote c) (set-exclusive-or (quote (a b c)) (quote (c d)))))", "t");
        expectValue("(not (member? (quote d) (set-exclusive-or (quote (a b c)) (quote (c d)))))", "nil");
    }
}