/*
 * Created on 14.08.2009
 */
package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
//
public class AssociationListTest extends AbstractTest
{
    public AssociationListTest()
    {
        super();
    }
    //
    @Test
    public void testAcons() throws CannotEvalException
    {
        expectValue("(assoc (quote b) (acons (quote b) \"be\" nil))", "(b \"be\")");
        expectValue("(assoc (quote b) (acons (quote a) \"ah\" (quote ((b \"be\")))))", "(b \"be\")");
    }
    //
    @Test
    public void testAssoc() throws CannotEvalException
    {
        expectValue("(assoc (quote b) (quote ((a \"ah\") (b \"be\"))))", "(b \"be\")");
        expectValue("(assoc (quote a) (quote ((a \"ah\") (b \"be\"))))", "(a \"ah\")");
        expectValue("(assoc (quote c) (quote ((a \"ah\") (b \"be\"))))", "nil");
    }
    //
    @Test
    public void testPairlis() throws CannotEvalException
    {
        expectValue("(assoc (quote b) (pairlis (quote (a b c)) (quote (\"ah\" \"be\" \"ce\")) nil))", "(b \"be\")");
        expectValue("(assoc (quote c) (pairlis (quote (a b)) (quote (\"ah\" \"be\")) (quote ((c \"ce\")))))", "(c \"ce\")");
    }
    //
    @Test
    public void testWithAssociations() throws CannotEvalException
    {
        expectValue("(with-associations (one two) (quote ((one 1) (two 2))) (list one two))", "(1 2)");
        expectValue("(with-associations (one three) (quote ((one 1) (two 2))) (list one three))", "(1 nil)");
    }
}