package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
/*
 * Created by Andreas on 26.08.2015.
 */
public class LetTest extends AbstractTest
{
    @Test
    public void testLet1() throws CannotEvalException
    {
        expectValue("(let ((add1 (lambda (n) (+ n 1)))) (add1 3))", "4");
    }
    //
    @Test
    public void testLet2() throws CannotEvalException
    {
        expectValue("(progn (setq add2 (let ((add1 (lambda (n) (+ n 1)))) (lambda (n) (add1 (add1 n))))) (add2 2)) ", "4");
    }
}