/*
 * Created on 13.08.2009
 */
package lisp.test;
//
import lisp.CannotEvalException;
import lisp.Closure;
import lisp.Sexpression;
import lisp.combinator.EnvironmentFactory;
import lisp.environment.Environment;
import lisp.parser.Parser;
import org.junit.Before;
import org.junit.Test;
import lisp.concurrent.LimitExceededException;
import lisp.concurrent.RunnableQueue;
import lisp.concurrent.RunnableQueueFactory;
import java.security.SecureRandom;
import java.util.HashSet;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.fail;
//
public class RandomExceptionTest
{
    private static final String[] CONSTANTS = new String[] {"nil", "unquote", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "\"a\"",
            "\"b\"", "\"c\""};
    private static final String[] ARITY0 = new String[] {"get-date-and-time", "make-hash-table"};
    private static final String[] ARITY1 = new String[] {"array?", "atom?", "bound?", "char-code", "code-char", "complement", "constantly", "eval", "fifth",
            "first", "floor", "fourth", "identity", "intern", "last", "less?", "let", "letrec", "list-length", "list?", "not", "null?", "number?", "permute",
            "quasi-quote", "quote", "random", " read-from-string", "rest", "reverse", "second", "string-capitalize", "string-downcase", "string-length",
            "string-trim", "string-upcase", "string?", "symbol?", "third", "type-of", "write-to-string", "sixth", "seventh", "eighth", "ninth", "tenth",
            "numerator", "denominator", "class-of", "class?", "direct-superclasses", "duplicate", "freeze", "frozen?", "generic-function?", "instance?", "length",
            "parameters", "body", "make-array"};
    private static final String[] ARITY2 = new String[] {"adjoin", "apply", "assoc", "catch", "cons", "disjoint?", "equal?", "every?", "get-array-element",
            "intersection", "lambda", "mapcar", "member-if?", "member?", "mlambda", "nth", "nthcdr", "rem", "remove", "remove-if", "select-if", "single?",
            "set-difference", "set-exclusive-or", "some?", "sort", "subset?", "tail?", "union", "find-if", "count-if", "defclass", "instance-of?", "is-a?",
            "respond-to?", "slot-value", "change-class", "get-hash-table-value",};
    private static final String[] ARITY3 = new String[] {"acons", "if", "pairlis", "search-string", "set-array-element", "subst", "subst-if", "substring",
            "catch-and-apply", "assignq", "defmethod", "put-hash-table-value"};
    private static final String[] ARITYN = new String[] {"+", "-", "*", "/", "and", "append", "concatenate", "cond", "list", "or", "prog1", "prog2", "progn",
            "when", "unless", "ensure", "make-instance", "new"};
    private static final int INCLUDING0 = ARITY0.length;
    private static final int INCLUDING1 = INCLUDING0 + ARITY1.length;
    private static final int INCLUDING2 = INCLUDING1 + ARITY2.length;
    private static final int INCLUDING3 = INCLUDING2 + ARITY3.length;
    private static final int INCLUDINGN = INCLUDING3 + ARITYN.length;
    //
    private static final Logger logger = Logger.getLogger("lisp.tests.Test");
    //
    private Random random;
    private Environment environment;
    private Sexpression sexpression;
    private HashSet<String> set;
    //
    public RandomExceptionTest()
    {
        super();
    }
    //
    public static void main(String[] args)
    {
        try
        {
            while (true)
            {
                RandomExceptionTest test = new RandomExceptionTest();
                test.setUp();
                test.testEval();
                Thread.sleep(1000L);
            }
        }
        catch (Exception e)
        {
            logger.log(Level.SEVERE, "", e);
        }
    }
    //
    @Before
    public void setUp() throws Exception
    {
        random = SecureRandom.getInstance("SHA1PRNG", "SUN");
        environment = EnvironmentFactory.createEnvironment().copy();
        set = new HashSet<String>();
    }
    //
    @Test
    public void testEval()
    {
        try
        {
            int i = 1000;
            //
            while (i > 0)
            {
                Parser parser = new Parser(randomList(), 0);
                sexpression = parser.getSexpression();
                String elem = sexpression.toString();
                //
                if (set.contains(elem)) continue;
                //
                try
                {
                    RunnableQueue queue = RunnableQueueFactory.getConcurrentRunnableQueue();
                    RunnableQueue limitedqueue = RunnableQueueFactory.limit(queue, 100000);
                    Closure closure = new Closure(limitedqueue, environment, sexpression);
                    Sexpression value = closure.eval();
                    //
                    logger.info(elem + " -> " + value);
                    set.add(elem);
                    i--;
                }
                catch (CannotEvalException e)
                {
                }
                catch (LimitExceededException e)
                {
                    logger.info(elem + " [*]");
                }
            }
        }
        catch (Exception e)
        {
            fail("unexpected exception " + e + " for " + sexpression);
        }
    }
    //
    private String randomString(String[] strs)
    {
        int count = strs.length;
        int index = random.nextInt(count);
        //
        return strs[index];
    }
    //
    private String randomConst()
    {
        return randomString(CONSTANTS);
    }
    //
    private String randomList()
    {
        int r = random.nextInt(INCLUDINGN);
        String[] strs;
        int arity;
        //
        if (r >= INCLUDING3)
        {
            r -= INCLUDING3;
            strs = ARITYN;
            arity = random.nextInt(6);
        }
        else if (r >= INCLUDING2)
        {
            r -= INCLUDING2;
            strs = ARITY3;
            arity = 3;
        }
        else if (r >= INCLUDING1)
        {
            r -= INCLUDING1;
            strs = ARITY2;
            arity = 2;
        }
        else if (r >= INCLUDING0)
        {
            r -= INCLUDING0;
            strs = ARITY1;
            arity = 1;
        }
        else
        {
            strs = ARITY0;
            arity = 0;
        }
        //
        StringBuilder builder = new StringBuilder();
        builder.append("(");
        builder.append(randomString(strs));
        //
        while (arity > 0)
        {
            builder.append(" ");
            //
            if (random.nextBoolean())
            {
                builder.append(randomConst());
            }
            else
            {
                builder.append(randomList());
            }
            //
            arity--;
        }
        //
        builder.append(")");
        //
        return builder.toString();
    }
}