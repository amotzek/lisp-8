package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
/**
 * Created by andreasm on 28.08.2010 18:52:19
 */
public class ExceptionTest extends AbstractTest
{
    @Test
    public void test1() throws CannotEvalException
    {
        expectValue("(string? (catch nil ((lambda (x 4) x) 4)))", "t");
    }
    //
    @Test
    public void test2() throws CannotEvalException
    {
        expectValue("(string? (catch nil (catch-and-apply nil (complement 6) (select-if 2 \"a\"))))", "t");
    }
    //
    @Test
    public void test3() throws CannotEvalException
    {
        expectValue("(string? (catch nil ((lambda (x) (first (less? x))) 4)))", "t");
    }
    //
    @Test
    public void test4() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((x 0) (c (make-channel 0)) (l (make-lock)) (f (go (throw (quote ex) nil))))");
        builder.append("(progn ");
        builder.append("(go (catch-and-apply (quote ex)");
        builder.append("(lambda (s v) (progn (acquire-lock l) (setq x (+ 1 x)) (release-lock l)))");
        builder.append("(list4 (await-future f) (await-future f) (await-future f) (await-future f))))");
        builder.append("(receive-from-channels (list c) 1000)");
        builder.append("x))");
        expectValue(builder.toString(), "1");
    }
    //
    @Test
    public void test5() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((x 0) (y 0) (c (make-channel 0)) (l (make-lock)) (f1 (go (throw (quote ex1) nil))) (f2 (go (throw (quote ex2) nil))))");
        builder.append("(progn ");
        builder.append("(go (catch-and-apply (quote ex1)");
        builder.append("(lambda (s v) (with-lock l (setq x (+ 1 x))))");
        builder.append("(catch-and-apply (quote ex2)");
        builder.append("(lambda (s v) (with-lock l (setq y (+ 1 y))))");
        builder.append("(list4 (await-future f1) (await-future f2) (await-future f1) (await-future f2)))))");
        builder.append("(receive-from-channels (list c) 1000)");
        builder.append("(+ x y))))");
        expectValue(builder.toString(), "1");
    }
}
