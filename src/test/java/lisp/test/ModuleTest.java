package lisp.test;
//
import lisp.CannotEvalException;
import lisp.module.AbstractRepository;
import lisp.module.Module;
import lisp.module.ModuleDependency;
import org.junit.Test;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
/*
 * Created by Andreas on 31.08.2015.
 */
public class ModuleTest extends AbstractTest
{
    @Test
    public void testExport() throws CannotEvalException, URISyntaxException, IOException
    {
        AbstractRepository repository = new AbstractRepository()
        {
            @Override
            protected Module loadModule(URI uri) throws IOException
            {
                if (uri.getPath().equals("/1"))
                {
                    return new Module("1", 1, uri, "commentde", "commenten", Collections.singletonList("square"), "(defproc square (x) (* x x))", Collections.emptyList());
                }
                //
                if (uri.getPath().equals("/2"))
                {
                    try
                    {
                        ModuleDependency dependency = new ModuleDependency("1", 1, new URI("http://localhost/1"), null);
                        //
                        return new Module("2", 1, uri, "commentde", "commenten", Collections.singletonList("cube"), "(defproc cube (x) (* x (square x)))", Collections.singletonList(dependency));
                    }
                    catch (URISyntaxException e)
                    {
                        throw new IOException(e);
                    }
                }
                //
                throw new IOException("not found");
            }
        };
        //
        repository.addModule(new URI("http://localhost/2"));
        repository.satisfyDependencies();
        environment = repository.createEnvironment(environment);
        expectValue("(square 3)", "9");
        expectValue("(cube 3)", "27");
    }
    //
    @Test
    public void testMerge() throws CannotEvalException, URISyntaxException, IOException
    {
        AbstractRepository repository = new AbstractRepository()
        {
            @Override
            protected Module loadModule(URI uri) throws IOException
            {
                if (uri.getPath().equals("/1"))
                {
                    return new Module("1", 1, uri, "commentde", "commenten", Collections.singletonList("plus"), "(defmethod plus (x y) (and (number? x) (number? y)) (+ x y))", Collections.emptyList());
                }
                //
                if (uri.getPath().equals("/2"))
                {
                    return new Module("2", 1, uri, "commentde", "commenten", Collections.singletonList("plus"), "(defmethod plus (x y) (and (string? x) (string? y)) (concatenate x y))", Collections.emptyList());
                }
                //
                throw new IOException("not found");
            }
        };
        //
        repository.addModule(new URI("http://localhost/1"));
        repository.addModule(new URI("http://localhost/2"));
        repository.satisfyDependencies();
        environment = repository.createEnvironment(environment);
        expectValue("(plus 3 4)", "7");
        expectValue("(plus \"3\" \"4\")", "\"34\"");
    }
}