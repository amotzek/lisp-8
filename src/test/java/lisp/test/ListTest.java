/*
 * Created on 14.08.2009
 */
package lisp.test;
//
import lisp.CannotEvalException;
import lisp.Chars;
import lisp.List;
import lisp.Symbol;
import org.junit.Test;
import java.util.Random;
import java.util.TreeSet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
//
public class ListTest extends AbstractTest
{
    public ListTest()
    {
        super();
    }
    //
    @Test
    public void testAppend() throws CannotEvalException
    {
        expectValue("(append)", "nil");
        expectValue("(append (quote (a)))", "(a)");
        expectValue("(append (quote (a)) (quote (b)))", "(a b)");
        expectValue("(append (quote (a)) (quote (b c)) (quote (d)))", "(a b c d)");
    }
    //
    @Test
    public void testCons() throws CannotEvalException
    {
        expectValue("(cons (quote a) nil)", "(a)");
        expectValue("(cons (quote a) (quote (b)))", "(a b)");
    }
    //
    @Test
    public void testEqual() throws CannotEvalException
    {
        expectValue("(equal? nil nil)", "t");
        expectValue("(equal? (quote (a b)) nil)", "nil");
        expectValue("(equal? (quote (a b)) (quote (a b)))", "t");
        expectValue("(equal? (quote (b a)) (quote (a b)))", "nil");
    }
    //
    @Test
    public void testEvery() throws CannotEvalException
    {
        expectValue("(every? identity nil)", "t");
        expectValue("(every? symbol? nil)", "t");
        expectValue("(every? symbol? (quote (a b c)))", "t");
        expectValue("(every? symbol? (quote (a \"be\" c)))", "nil");
        expectValue("(every? symbol? (quote (\"ah\" \"be\" \"ce\")))", "nil");
    }
    //
    @Test
    public void testSome() throws CannotEvalException
    {
        expectValue("(some? symbol? nil)", "nil");
        expectValue("(some? symbol? (quote (a b c)))", "a");
        expectValue("(some? symbol? (quote (a \"be\" c)))", "a");
        expectValue("(some? symbol? (quote (\"ah\" \"be\" \"ce\")))", "nil");
    }
    //
    @Test
    public void testFirst() throws CannotEvalException
    {
        expectValue("(first (quote (1 2 3 4 5 6 7 8 9 10 11)))", "1");
    }
    //
    @Test
    public void testSecond() throws CannotEvalException
    {
        expectValue("(second (quote (1 2 3 4 5 6 7 8 9 10 11)))", "2");
    }
    //
    @Test
    public void testSecondOrNil() throws CannotEvalException
    {
        expectValue("(second-or-nil (quote (1 2 3 4 5 6 7 8 9 10 11)))", "2");
        expectValue("(second-or-nil nil)", "nil");
    }
    //
    @Test
    public void testThird() throws CannotEvalException
    {
        expectValue("(third (quote (1 2 3 4 5 6 7 8 9 10 11)))", "3");
    }
    //
    @Test
    public void testFourth() throws CannotEvalException
    {
        expectValue("(fourth (quote (1 2 3 4 5 6 7 8 9 10 11)))", "4");
    }
    //
    @Test
    public void testFifth() throws CannotEvalException
    {
        expectValue("(fifth (quote (1 2 3 4 5 6 7 8 9 10 11)))", "5");
    }
    //
    @Test
    public void testSixth() throws CannotEvalException
    {
        expectValue("(sixth (quote (1 2 3 4 5 6 7 8 9 10 11)))", "6");
    }
    //
    @Test
    public void testSeventh() throws CannotEvalException
    {
        expectValue("(seventh (quote (1 2 3 4 5 6 7 8 9 10 11)))", "7");
    }
    //
    @Test
    public void testEighth() throws CannotEvalException
    {
        expectValue("(eighth (quote (1 2 3 4 5 6 7 8 9 10 11)))", "8");
    }
    //
    @Test
    public void testNinth() throws CannotEvalException
    {
        expectValue("(ninth (quote (1 2 3 4 5 6 7 8 9 10 11)))", "9");
    }
    //
    @Test
    public void testTenth() throws CannotEvalException
    {
        expectValue("(tenth (quote (1 2 3 4 5 6 7 8 9 10 11)))", "10");
    }
    //
    @Test
    public void testLast() throws CannotEvalException
    {
        expectValue("(last (quote (1 2 3 4 5 6)))", "(6)");
    }
    //
    @Test
    public void testButlast() throws CannotEvalException
    {
        expectValue("(butlast (quote (1 2 3 4 5 6)))", "(1 2 3 4 5)");
        expectValue("(butlast (quote (1)))", "nil");
    }
    //
    @Test
    public void testList() throws CannotEvalException
    {
        expectValue("(list)", "nil");
        expectValue("(list 1 2 3 4 5 6)", "(1 2 3 4 5 6)");
    }
    //
    @Test
    public void testType() throws CannotEvalException
    {
        expectValue("(list? nil)", "t");
        expectValue("(list? (quote (a)))", "t");
        expectValue("(type-of nil)", "list");
        expectValue("(type-of (quote (a)))", "list");
    }
    //
    @Test
    public void testListLength() throws CannotEvalException
    {
        expectValue("(list-length nil)", "0");
        expectValue("(list-length (quote (a)))", "1");
        expectValue("(list-length (quote (1 b 3 d 5 f)))", "6");
        expectValue("(length nil)", "0");
        expectValue("(length (quote (a)))", "1");
        expectValue("(length (quote (1 b 3 d 5 f)))", "6");
    }
    //
    @Test
    public void testMapWith() throws CannotEvalException
    {
        expectValue("(map-with (lambda (elem) (* 2 elem)) nil)", "nil");
        expectValue("(map-with (lambda (elem) (* 2 elem)) (quote (1 2 3 4 5 6)))", "(2 4 6 8 10 12)");
    }
    //
    @Test
    public void testZipWith() throws CannotEvalException
    {
        expectValue("(zip-with list nil nil)", "nil");
        expectValue("(zip-with list (quote (1 2)) (quote (3 4)))", "((1 3) (2 4))");
    }
    //
    @Test
    public void testCartesianProduct() throws CannotEvalException
    {
        expectValue("(cartesian-product nil nil)", "nil");
        expectValue("(cartesian-product nil (quote (3 4)))", "nil");
        expectValue("(cartesian-product (quote (1 2)) nil)", "nil");
        expectValue("(cartesian-product (quote (1 2)) (quote (3 4)))", "((1 3) (1 4) (2 3) (2 4))");
        expectValue("(cartesian-product (quote (1)) (quote (3 4)))", "((1 3) (1 4))");
        expectValue("(cartesian-product (quote (1 2)) (quote (3)))", "((1 3) (2 3))");
    }
    //
    @Test
    public void testNth() throws CannotEvalException
    {
        expectValue("(nth (quote (1 2 3 4 5 6)) 0)", "1");
        expectValue("(nth (quote (1 2 3 4 5 6)) 1)", "2");
        expectValue("(nth (quote (1 2 3 4 5 6)) 2)", "3");
        expectValue("(nth (quote (1 2 3 4 5 6)) 3)", "4");
        expectValue("(nth (quote (1 2 3 4 5 6)) 4)", "5");
        expectValue("(nth (quote (1 2 3 4 5 6)) 5)", "6");
    }
    //
    @Test
    public void testNthcdr() throws CannotEvalException
    {
        expectValue("(nthcdr (quote (1 2 3 4 5 6)) 0)", "(1 2 3 4 5 6)");
        expectValue("(nthcdr (quote (1 2 3 4 5 6)) 1)", "(2 3 4 5 6)");
        expectValue("(nthcdr (quote (1 2 3 4 5 6)) 2)", "(3 4 5 6)");
        expectValue("(nthcdr (quote (1 2 3 4 5 6)) 3)", "(4 5 6)");
        expectValue("(nthcdr (quote (1 2 3 4 5 6)) 4)", "(5 6)");
        expectValue("(nthcdr (quote (1 2 3 4 5 6)) 5)", "(6)");
    }
    //
    @Test
    public void testNull() throws CannotEvalException
    {
        expectValue("(null? nil)", "t");
        expectValue("(null? (quote (a)))", "nil");
    }
    //
    @Test
    public void testPermute() throws CannotEvalException
    {
        expectValue("(permute (quote (a b)))", "((a b) (b a))");
        expectValue("(list-length (permute (quote (a b c d e f g))))", "5040");
    }
    //
    @Test
    public void testQuasiQuote() throws CannotEvalException
    {
        expectValue("(quasi-quote (a b c d))", "(a b c d)");
        expectValue("(quasi-quote (a b (unquote (+ 1 2)) d))", "(a b 3 d)");
    }
    //
    @Test
    public void testRemove() throws CannotEvalException
    {
        expectValue("(remove (quote c) (quote (a b c d)))", "(a b d)");
        expectValue("(remove (quote d) (quote (a b c)))", "(a b c)");
        expectValue("(remove-if number? (quote (a b 3 d)))", "(a b d)");
        expectValue("(remove-if number? (quote (a b c)))", "(a b c)");
    }
    //
    @Test
    public void testRest() throws CannotEvalException
    {
        expectValue("(rest (quote (a b c d)))", "(b c d)");
        expectValue("(rest (quote (a)))", "nil");
    }
    //
    @Test
    public void testReverse() throws CannotEvalException
    {
        expectValue("(reverse (quote (a b c d)))", "(d c b a)");
    }
    //
    @Test
    public void testSelectIf() throws CannotEvalException
    {
        expectValue("(select-if number? (quote (a 2 3 d)))", "(2 3)");
        expectValue("(select-if number? (quote (a b c d)))", "nil");
        expectValue("(select-if number? nil)", "nil");
    }
    //
    @Test
    public void testFindIf() throws CannotEvalException
    {
        expectValue("(find-if number? (quote (ah be 3 de)))", "3");
        expectValue("(find-if number? (quote (ah be ce)))", "nil");
        expectValue("(find-if number? nil)", "nil");
    }
    //
    @Test
    public void testCountIf() throws CannotEvalException
    {
        expectValue("(count-if number? (quote (a 2 3 d)))", "2");
        expectValue("(count-if number? (quote (a b c d)))", "0");
        expectValue("(count-if number? nil)", "0");
    }
    //
    @Test
    public void testSingle() throws CannotEvalException
    {
        expectValue("(single? (quote (a b c d)))", "nil");
        expectValue("(single? (quote (a)))", "t");
        expectValue("(single? nil)", "nil");
    }
    //
    @Test
    public void testPosition() throws CannotEvalException
    {
        expectValue("(position 3 (quote (1 2 3 4 5 6 7 8 9 10 11)))", "2");
        expectValue("(position 43 (quote (1 2 3 4 5 6 7 8 9 10 11)))", "nil");
    }
    //
    @Test
    public void testMerge() throws CannotEvalException
    {
        expectValue("(merge (quote list) nil nil less?)", "nil");
        expectValue("(merge (quote list) (list 1 2) nil less?)", "(1 2)");
        expectValue("(merge (quote list) nil (list 1 2) less?)", "(1 2)");
        expectValue("(merge (quote list) (list 1 3 8) (list 2 4 5 6 7) less?)", "(1 2 3 4 5 6 7 8)");
    }
    //
    @Test
    public void testSort() throws CannotEvalException
    {
        expectValue("(sort nil less?)", "nil");
        expectValue("(sort (quote (1)) less?)", "(1)");
        expectValue("(sort (quote (2 1)) less?)", "(1 2)");
        expectValue("(sort (quote (3 1 2)) less?)", "(1 2 3)");
        expectValue("(sort (quote (4 2 1 3)) less?)", "(1 2 3 4)");
        expectValue("(sort (quote (20 2 6 13 5 3 19 9 4 12 8 18 16 10 1 7 11 14 15 17)) less?)", "(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)");
        expectValue("(sort (quote (4 2 1 3)) greater?)", "(4 3 2 1)");
        expectValue("(sort (quote (1)) less?)", "(1)");
        expectValue("(sort nil less?)", "nil");
        expectValue("(sort (quote (1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40)) less?)", "(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40)");
        expectValue("(sort (quote (40 39 38 37 36 35 34 33 32 31 30 29 28 27 26 25 24 23 22 21 20 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1)) less?)", "(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40)");
        Random random = new Random();
        //
        for (int i = 1; i < 6; i++)
        {
            int length = 1 + 2000 * i;
            TreeSet<Integer> set = new TreeSet<Integer>();
            StringBuilder expression = new StringBuilder();
            expression.append("(sort (quote (");
            //
            while (length > 0)
            {
                int r = random.nextInt(1000000);
                //
                if (set.contains(r)) continue;
                //
                set.add(r);
                expression.append(r);
                expression.append(" ");
                length--;
            }
            //
            expression.append(")) less?)");
            StringBuilder result = new StringBuilder();
            result.append("(");
            //
            while (!set.isEmpty())
            {
                result.append(set.pollFirst());
                result.append(" ");
            }
            //
            result.append(")");
            expectValue(expression.toString(), result.toString());
        }
    }
    //
    @Test
    public void testSubst() throws CannotEvalException
    {
        expectValue("(subst (quote be) (quote b) (quote ((a b) c (d (b c)))))", "((a be) c (d (be c)))");
        expectValue("(subst (quote be) (quote x) (quote ((a b) c (d (b c)))))", "((a b) c (d (b c)))");
        expectValue("(subst (quote be) (quote b) nil)", "nil");
        expectValue("(subst-if (quote be) number? (quote ((a 2) c (d (2 c)))))", "((a be) c (d (be c)))");
        expectValue("(subst-if (quote be) number? (quote ((a b) c (d (b c)))))", "((a b) c (d (b c)))");
        expectValue("(subst-if (quote be) number? nil)", "nil");
    }
    //
    @Test
    public void testTail() throws CannotEvalException
    {
        expectValue("(tail? (quote (b c)) (quote (a b c)))", "t");
        expectValue("(tail? (quote (b c)) (quote (a b c d)))", "nil");
        expectValue("(tail? nil (quote (a b c)))", "t");
        expectValue("(tail? (quote (b c)) nil)", "nil");
        expectValue("(tail? nil nil)", "t");
    }
    //
    @Test
    public void testSplitUnsplit() throws CannotEvalException
    {
        expectValue("(length (split (list 1 2 3 4 5) 2))", "2");
        expectValue("(unsplit (split (list 1 2 3 4) 2))", "(1 2 3 4)");
        expectValue("(unsplit (split (list 1 2 3 4 5) 3))", "(1 2 3 4 5)");
        expectValue("(letrec ((p2 (lambda (x) (or (equal? x 1) (and (integer? x) (p2 (/ x 2))))))) (p2 *thread-count*))", "t");
    }
    //
    @Test
    public void testPush() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((l nil)) ");
        builder.append("(progn ");
        builder.append("(push 1 l) ");
        builder.append("(push 2 l) ");
        builder.append("(push 3 l) ");
        builder.append("l))");
        expectValue(builder.toString(), "(3 2 1)");
    }
    //
    @Test
    public void testStatic()
    {
        List alist = List.acons(Symbol.createSymbol("a"), new Chars("a"), null);
        alist = List.acons(Symbol.createSymbol("b"), new Chars("b"), alist);
        assertEquals(new Chars("a"), List.second(alist.lookup(Symbol.createSymbol("a"))));
        assertEquals(new Chars("b"), List.secondOrNull(alist.lookup(Symbol.createSymbol("b"))));
        List list2 = new List(new Chars("a"), new List(new Chars("b"), null));
        assertEquals(new Chars("b"), List.second(list2));
        List list1 = new List(new Chars("a"), null);
        assertNull(List.secondOrNull(list1));
    }
}