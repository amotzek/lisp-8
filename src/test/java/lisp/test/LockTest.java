package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
/**
 * Created by andreasm on 3.3.2013 9:50
 */
public class LockTest extends AbstractTest
{
    @Test
    public void testMakeLock() throws CannotEvalException
    {
        expectValue("(type-of (make-lock))", "lock");
    }
    //
    @Test
    public void testRelease() throws CannotEvalException
    {
        expectValue("(let ((lock (make-lock))) (list (with-lock lock (+ 2 3 4)) (with-lock lock (* 3 3))))", "(9 9)");
    }
    //
    @Test
    public void testThrowRelease() throws CannotEvalException
    {
        expectValue("(let ((lock (make-lock))) (progn (catch nil (with-lock lock (throw (quote error) nil))) (with-lock lock (+ 3 3 3))))", "9");
    }
    //
    @Test
    public void testMutualExclusion() throws CannotEvalException
    {
        for (int i = 0; i < 5; i++)
        {
            expectValue("(let ((lock (make-lock)) (x 0)) (list (with-lock lock (setq x (+ x 1)) (setq x (- x 1))) (with-lock lock (setq x (+ x 1)) (setq x (- x 1))) (with-lock lock (setq x (+ x 1)) (setq x (- x 1)))))", "(0 0 0)");
        }
    }
}