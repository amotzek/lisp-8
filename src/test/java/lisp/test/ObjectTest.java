/*
 * Created on 11.10.2011
 */
package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
//
public class ObjectTest extends AbstractTest
{
    public ObjectTest()
    {
        super();
    }
    //
    @Test
    public void testDefClass() throws CannotEvalException
    {
        expectValue("(let ((test nil)) (type-of (defclass test nil)))", "class");
        expectValue("(let ((test nil)) (progn (defclass test nil) (type-of test)))", "class");
    }
    //
    @Test
    public void testAllocateInstance() throws CannotEvalException
    {
        expectValue("(let ((object nil) (some-object nil)) (progn (defclass object nil) (setq some-object (allocate-instance object)) (type-of some-object)))", "instance");
    }
    //
    @Test
    public void testChangeClass() throws CannotEvalException
    {
        expectValue("(let ((object1 nil) (object2 nil) (some-object nil)) (progn (defclass object1 nil) (defclass object2 nil) (setq some-object (allocate-instance object1)) (equal? object2 (class-of (change-class some-object object2)))))", "t");
    }
    //
    @Test
    public void testClassInstance() throws CannotEvalException
    {
        expectValue("(let ((object nil)) (progn (defclass object nil) (list (class? object) (instance? (allocate-instance object)) (class? 1) (instance? nil))))", "(t t nil nil)");
    }
    //
    @Test
    public void testClassOf() throws CannotEvalException
    {
        expectValue("(let ((object nil) (some-object nil)) (progn (defclass object nil) (setq some-object (allocate-instance object)) (equal? object (class-of some-object))))", "t");
    }
    //
    @Test
    public void testInstanceOf() throws CannotEvalException
    {
        expectValue("(let ((object nil) (some-object nil)) (progn (defclass object nil) (setq some-object (allocate-instance object)) (instance-of? some-object object)))", "t");
    }
    //
    @Test
    public void testAssignq() throws CannotEvalException
    {
        expectValue("(let ((object nil) (some-object nil)) (progn (defclass object nil) (setq some-object (allocate-instance object)) (assignq some-object x 1) (assignq some-object x 2) (slot-value some-object (quote x))))", "2");
        expectValue("(let ((object nil) (some-object nil)) (progn (defclass object nil) (setq some-object (allocate-instance object)) (assignq some-object x 1) (assignq some-object x 2) (slot-valueq some-object x)))", "2");
    }
    //
    @Test
    public void testEqual() throws CannotEvalException
    {
        expectValue("(let ((object nil) (some-object1 nil) (some-object2 nil)) (progn (defclass object nil) (setq some-object1 (allocate-instance object)) (setq some-object2 (allocate-instance object)) (assignq some-object1 x 1) (assignq some-object2 x 1) (equal? some-object1 some-object2)))", "nil");
        expectValue("(let ((object nil) (some-object1 nil) (some-object2 nil)) (progn (defclass object nil) (setq some-object1 (allocate-instance object)) (setq some-object2 (allocate-instance object)) (assignq some-object1 x 1) (assignq some-object2 x 1) (freeze some-object1) (freeze some-object2) (equal? some-object1 some-object2)))", "t");
        expectValue("(let ((object nil) (some-object1 nil) (some-object2 nil)) (progn (defclass object nil) (setq some-object1 (allocate-instance object)) (setq some-object2 (allocate-instance object)) (assignq some-object1 x 1) (assignq some-object2 x 2) (freeze some-object1) (freeze some-object2) (equal? some-object1 some-object2)))", "nil");
    }
    //
    @Test
    public void testSlotValues() throws CannotEvalException
    {
        expectValue("(let ((object nil) (some-object nil)) (progn (defclass object nil) (setq some-object (allocate-instance object)) (assignq some-object y 2) (slot-values some-object)))", "((y 2))");
    }
    //
    @Test
    public void testFreeze() throws CannotEvalException
    {
        expectValue("(let ((object nil) (some-object nil)) (progn (defclass object nil) (setq some-object (allocate-instance object)) (frozen? some-object)))", "nil");
        expectValue("(let ((object nil) (some-object nil)) (progn (defclass object nil) (setq some-object (allocate-instance object)) (freeze some-object) (frozen? some-object)))", "t");
        expectValue("(let ((object nil) (some-object nil)) (progn (defclass object nil) (setq some-object (allocate-instance object)) (freeze some-object) (frozen? (duplicate some-object))))", "nil");
        expectValue("(type-of (catch (quote error) (let ((object nil) (some-object nil)) (progn (defclass object nil) (setq some-object (allocate-instance object)) (freeze some-object) (assignq some-object x 2) (slot-value some-object (quote x))))))", "string");
    }
    //
    @Test
    public void testDuplicate() throws CannotEvalException
    {
        expectValue("(let ((object nil) (some-object nil)) (progn (defclass object nil) (setq some-object (allocate-instance object)) (assignq some-object value 1) (slot-value (duplicate some-object) (quote value))))", "1");
        expectValue("(let ((object nil) (some-object nil)) (progn (defclass object nil) (setq some-object (allocate-instance object)) (equal? (class-of some-object) (class-of (duplicate some-object)))))", "t");
    }
    //
    @Test
    public void testIsA() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((a nil) (b nil) (c nil) (d nil) (oc nil))");
        builder.append("(progn ");
        builder.append("(defclass a ())");
        builder.append("(defclass b (a))");
        builder.append("(defclass c (b))");
        builder.append("(defclass d (a))");
        builder.append("(setq oc (allocate-instance c))");
        builder.append("(list (not (is-a? oc a)) (not (is-a? oc b)) (not (is-a? oc c)) (is-a? oc d))))");
        expectValue(builder.toString(), "(nil nil nil nil)");
    }
    //
    @Test
    public void testDefmethod() throws CannotEvalException
    {
        expectValue("(let ((function nil)) (progn (defmethod function (a b) t (+ a b)) (function 2 3)))", "5");
        expectValue("(let ((function nil)) (progn (defmethod function (a b) (and (number? a) (number? b)) (+ a b)) (defmethod function (a b) (and (string? a) (string? b)) (concatenate a b)) (list (function 7 3) (function \"hallo\" \" welt\"))))", "(10 \"hallo welt\")");
        expectValue("(let ((function nil)) (progn (defmethod function (a b) (and (number? a) (number? b)) (+ a b)) (defmethod function (a b) t (concatenate a b)) (list (function 7 3) (function \"hallo\" \" welt\"))))", "(10 \"hallo welt\")");
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((a nil) (b nil) (c nil) (d nil) (oa nil) (ob nil) (oc nil) (od nil) (function nil))");
        builder.append("(progn ");
        builder.append("(defclass a ())");
        builder.append("(defclass b (a))");
        builder.append("(defclass c (b))");
        builder.append("(defclass d (c))");
        builder.append("(defmethod function ((pb b)) t 2)");
        builder.append("(defmethod function ((pa a)) t 1)");
        builder.append("(defmethod function ((pc c)) t 3)");
        builder.append("(defmethod function (x) t 4)");
        builder.append("(setq oa (allocate-instance a))");
        builder.append("(setq ob (allocate-instance b))");
        builder.append("(setq oc (allocate-instance c))");
        builder.append("(setq od (allocate-instance d))");
        builder.append("(list (function oa) (function ob) (function oc) (function od) (function nil) (generic-function? function) (generic-function? (quote a)))))");
        expectValue(builder.toString(), "(1 2 3 3 4 t nil)");
    }
    //
    @Test
    public void testAddLambda() throws CannotEvalException
    {
        expectValue("(let ((f (lambda (x y) (add x y)))) (progn (defmethod f (x y) (and (string? x) (string? y)) (concatenate x y)) (list (f 2 3) (f \"a\" \"b\"))))", "(5 \"ab\")");
    }
    //
    @Test
    public void testAddCombinator() throws CannotEvalException
    {
        expectValue("(let ((f add)) (progn (defmethod f (x y) (and (string? x) (string? y)) (concatenate x y)) (list (f 2 3) (f \"a\" \"b\"))))", "(5 \"ab\")");
    }
    //
    @Test
    public void testGenericFunctionMethods() throws CannotEvalException
    {
        expectValue("(let ((function nil)) (progn (defmethod function (a b) t (+ a b)) (list-length (generic-function-methods function))))", "1");
        expectValue("(let ((function nil)) (progn (defmethod function (a b) t (+ a b)) (type-of (first (generic-function-methods function)))))", "method");
        expectValue("(let ((function nil)) (progn (defmethod function (a b) t (+ a b)) (defmethod function (a b c) t (+ a b c)) (list-length (generic-function-methods function))))", "2");
    }
    //
    @Test
    public void testMethodGuard() throws CannotEvalException
    {
        expectValue("(let ((function nil)) (progn (defmethod function (a b) t (+ a b)) (method-guard (first (generic-function-methods function)))))", "t");
        expectValue("(let ((function nil)) (progn (defmethod function (a b) (and (number? a) (number? b)) (+ a b)) (method-guard (first (generic-function-methods function)))))", "(and (number? a) (number? b))");
    }
    //
    @Test
    public void testMethodSpezializers() throws CannotEvalException
    {
        expectValue("(let ((function nil)) (progn (defmethod function (a b) t (+ a b)) (method-specializers (first (generic-function-methods function)))))", "(nil nil)");
        expectValue("(let ((function nil) (object nil)) (progn (defclass object ()) (defmethod function ((a object) b) (and (number? a) (number? b)) (+ a b)) (equal? object (first (method-specializers (first (generic-function-methods function)))))))", "t");
        expectValue("(let ((function nil) (object nil)) (progn (defclass object ()) (defmethod function ((a object) b) nil (+ a b)) (equal? nil (second (method-specializers (first (generic-function-methods function)))))))", "t");
    }
    //
    @Test
    public void testRemoveMethod() throws CannotEvalException
    {
        expectValue("(let ((plus nil) (methods nil)) (progn (defmethod plus (a b) t (+ a b)) (setq methods (generic-function-methods plus)) (remove-method plus (first methods)) (list (length methods) (length (generic-function-methods plus)))))", "(1 0)");
    }
    //
    @Test
    public void testMakeInstance() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((a nil) (oa1 nil) (oa2 nil) (initialize nil) (get-value nil))");
        builder.append("(progn ");
        builder.append("(defclass a ())");
        builder.append("(defmethod initialize ((pa a) pv) t (progn (assignq pa value pv) pa))");
        builder.append("(defmethod get-value ((pa a)) t (slot-value pa (quote value)))");
        builder.append("(setq oa1 (new a \"Hallo\"))");
        builder.append("(setq oa2 (new a \"Welt\"))");
        builder.append("(list (get-value oa1) (get-value oa2))))");
        expectValue(builder.toString(), "(\"Hallo\" \"Welt\")");
    }
    //
    @Test
    public void testRespondTo() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((a nil) (b nil) (oa nil) (ob nil) (get-value nil))");
        builder.append("(progn ");
        builder.append("(defclass a ())");
        builder.append("(defclass b ())");
        builder.append("(defmethod get-value ((pa a)) t (slot-value pa (quote value)))");
        builder.append("(defmethod get-value2 (pa) t (slot-value pa (quote value)))");
        builder.append("(setq oa (allocate-instance a))");
        builder.append("(setq ob (allocate-instance b))");
        builder.append("(list (respond-to? oa get-value) (respond-to? ob get-value) (respond-to? oa get-value2) (respond-to? ob get-value2))))");
        expectValue(builder.toString(), "(t nil t t)");
    }
    //
    @Test
    public void testWithSlots() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((a nil) (oa nil) (xout nil) (yout nil))");
        builder.append("(progn ");
        builder.append("(defclass a ())");
        builder.append("(setq oa (allocate-instance a))");
        builder.append("(assignq oa x 1)");
        builder.append("(assignq oa y 3)");
        builder.append("(with-slots (x y) oa (progn ");
        builder.append("(setq xout x)");
        builder.append("(setq yout y)");
        builder.append("(setq y 2)))");
        builder.append("(list (slot-value oa (quote y)) xout yout)))");
        expectValue(builder.toString(), "(2 1 3)");
    }
    //
    @Test
    public void testWithSlotsReadOnly() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((a nil) (oa nil))");
        builder.append("(progn ");
        builder.append("(defclass a ())");
        builder.append("(setq oa (allocate-instance a))");
        builder.append("(assignq oa x 3)");
        builder.append("(assignq oa y 4)");
        builder.append("(with-slots-read-only (x y) oa (list y x))))");
        expectValue(builder.toString(), "(4 3)");
    }
    //
    @Test
    public void testCallNextMethod() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((a nil) (aa nil) (b nil) (bb nil) (oaa nil) (obb nil) (function nil))");
        builder.append("(progn ");
        builder.append("(defclass a ())");
        builder.append("(defclass aa (a))");
        builder.append("(defclass b ())");
        builder.append("(defclass bb (b))");
        builder.append("(defmethod function ((paa aa) (pbb bb)) t (cons (quote aabb) (call-next-method)))");
        builder.append("(defmethod function ((pa a) (pbb bb)) t (list (quote abb) (call-next-method)))");
        builder.append("(defmethod function ((paa aa) (pb b)) t (cons (quote aab) (call-next-method)))");
        builder.append("(defmethod function ((pa a) (pb b)) t (quote ab))");
        builder.append("(setq oaa (allocate-instance aa))");
        builder.append("(setq obb (allocate-instance bb))");
        builder.append("(function oaa obb)))");
        expectValue(builder.toString(), "(aabb aab abb ab)");
    }
    //
    @Test
    public void testMixin() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((c1 nil) (c2 nil) (m nil) (c3 nil) (o3 nil) (hello nil))");
        builder.append("(progn ");
        builder.append("(defclass c1 ())");
        builder.append("(defclass c2 (c1))");
        builder.append("(deftrait m ())");
        builder.append("(defclass c3 (m c2))");
        builder.append("(defmethod hello ((o m)) t (concatenate \"Hallo \" (call-next-method)))");
        builder.append("(defmethod hello ((o c2)) t \"Welt\")");
        builder.append("(setq o3 (allocate-instance c3))");
        builder.append("(hello o3)))");
        expectValue(builder.toString(), "\"Hallo Welt\"");
    }
    //
    @Test
    public void testTrait() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((t1 nil) (t2 nil))");
        builder.append("(progn ");
        builder.append("(deftrait t1 ())");
        builder.append("(deftrait t2 (t1))");
        builder.append("(list (write-to-string t1) (write-to-string t2) (trait? t1) (trait? t2) (class? t1) (class? t2))))");
        expectValue(builder.toString(), "(\"t1\" \"t2\" t t nil nil)");
        builder = new StringBuilder();
        builder.append("(let ((t1 nil))");
        builder.append("(progn ");
        builder.append("(deftrait t1 ())");
        builder.append("(catch nil (allocate-instance t1))))");
        expectValue(builder.toString(), "\"cannot allocate instance of trait t1 in last in first\"");
        builder = new StringBuilder();
        builder.append("(let ((c1 nil) (t1 nil))");
        builder.append("(progn ");
        builder.append("(defclass c1 ())");
        builder.append("(catch nil (deftrait t1 (c1)))))");
        expectValue(builder.toString(), "\"(c1) contains a class name but traits can only extend other traits in last in first\"");
        builder = new StringBuilder();
        builder.append("(let ((m nil) (t1 nil) (t2 nil) (c nil))");
        builder.append("(progn ");
        builder.append("(deftrait t1 ())");
        builder.append("(deftrait t2 ())");
        builder.append("(defmethod m ((p t1)) t 1)");
        builder.append("(defmethod m ((p t2)) t 2)");
        builder.append("(catch nil (defclass c (t1 t2)))))");
        expectValue(builder.toString(), "\"method with specializers (t1) conflicts with method with specializers (t2) in generic function m in last in first\"");
        builder = new StringBuilder();
        builder.append("(let ((m nil) (t0 nil) (t1 nil) (t2 nil) (c nil))");
        builder.append("(progn ");
        builder.append("(deftrait t0 ())");
        builder.append("(deftrait t1 (t0))");
        builder.append("(deftrait t2 (t0))");
        builder.append("(defmethod m ((p t1)) t 1)");
        builder.append("(defclass c (t1 t2))");
        builder.append("(catch nil (defmethod m ((p t2)) t 2))))");
        expectValue(builder.toString(), "\"method with specializers (t1) conflicts with method with specializers (t2) in generic function m in last in first\"");
        builder = new StringBuilder();
        builder.append("(let ((m nil) (t1 nil) (t2 nil) (c nil))");
        builder.append("(progn ");
        builder.append("(deftrait t1 ())");
        builder.append("(deftrait t2 (t1))");
        builder.append("(catch nil (defclass c (t1 t2)))))");
        expectValue(builder.toString(), "\"superclasses t1 and t2 of class c are related in last in first\"");
        builder = new StringBuilder();
        builder.append("(let ((m nil) (t1 nil) (t2 nil) (c nil))");
        builder.append("(progn ");
        builder.append("(deftrait t1 ())");
        builder.append("(deftrait t2 (t1))");
        builder.append("(defmethod m ((p1 t1) (p2 t2)) t 1)");
        builder.append("(defmethod m ((p1 t2) (p2 t1)) t 2)");
        builder.append("(defclass c (t2))");
        builder.append("(m (allocate-instance c) (allocate-instance c))))");
        expectValue(builder.toString(), "2");
    }
    //
    @Test
    public void testDiamond1() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((a nil) (b nil) (c nil) (d nil) (e nil) (m nil))");
        builder.append("(progn ");
        builder.append("(defclass a ())");
        builder.append("(defclass b (a))");
        builder.append("(defclass c (a))");
        builder.append("(defclass d (c))");
        builder.append("(defclass e (b d))");
        builder.append("(defmethod m ((p a)) t (quote (1)))");
        builder.append("(defmethod m ((p b)) t (cons 2 (call-next-method)))");
        builder.append("(defmethod m ((p c)) t (cons 3 (call-next-method)))");
        builder.append("(defmethod m ((p d)) t (cons 4 (call-next-method)))");
        builder.append("(defmethod m ((p e)) t (cons 5 (call-next-method)))");
        builder.append("(m (allocate-instance e))))");
        expectValue(builder.toString(), "(5 2 4 3 1)");
    }
    //
    @Test
    public void testDiamond2() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((a nil) (b nil) (c nil) (d nil) (e nil) (f nil) (g nil) (h nil) (ij nil) (m nil))");
        builder.append("(progn ");
        builder.append("(defclass a ())");
        builder.append("(defclass b (a))");
        builder.append("(defclass c (b))");
        builder.append("(defclass d (b))");
        builder.append("(defclass e (c))");
        builder.append("(defclass f (c))");
        builder.append("(defclass g (e d))");
        builder.append("(defclass h (f))");
        builder.append("(defclass ij (g h))");
        builder.append("(defmethod m ((p a)) t (quote (1)))");
        builder.append("(defmethod m ((p b)) t (cons 2 (call-next-method)))");
        builder.append("(defmethod m ((p c)) t (cons 3 (call-next-method)))");
        builder.append("(defmethod m ((p d)) t (cons 4 (call-next-method)))");
        builder.append("(defmethod m ((p e)) t (cons 5 (call-next-method)))");
        builder.append("(defmethod m ((p f)) t (cons 6 (call-next-method)))");
        builder.append("(defmethod m ((p g)) t (cons 7 (call-next-method)))");
        builder.append("(defmethod m ((p h)) t (cons 8 (call-next-method)))");
        builder.append("(defmethod m ((p ij)) t (cons 9 (call-next-method)))");
        builder.append("(m (allocate-instance ij))))");
        expectValue(builder.toString(), "(9 7 8 5 4 6 3 2 1)");
    }
    //
    @Test
    public void testDiamond3() throws CannotEvalException
    {
        StringBuilder builder = new StringBuilder();
        builder.append("(let ((q nil) (r nil) (s nil) (u nil) (v nil) (w nil) (m nil))");
        builder.append("(progn ");
        builder.append("(defclass q ())");
        builder.append("(defclass r (q))");
        builder.append("(defclass s (r))");
        builder.append("(defclass u (r))");
        builder.append("(defclass v (s))");
        builder.append("(defclass w (u v))");
        builder.append("(defmethod m ((p q)) t (quote (1)))");
        builder.append("(defmethod m ((p r)) t (cons 2 (call-next-method)))");
        builder.append("(defmethod m ((p s)) t (cons 3 (call-next-method)))");
        builder.append("(defmethod m ((p u)) t (cons 4 (call-next-method)))");
        builder.append("(defmethod m ((p v)) t (cons 5 (call-next-method)))");
        builder.append("(defmethod m ((p w)) t (cons 6 (call-next-method)))");
        builder.append("(m (allocate-instance w))))");
        expectValue(builder.toString(), "(6 4 5 3 2 1)");
    }
}