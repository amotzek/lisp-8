/*
 * Created on 14.08.2009
 */
package lisp.test;
//
import lisp.CannotEvalException;
import org.junit.Test;
//
public class ControlTest extends AbstractTest
{
    public ControlTest()
    {
        super();
    }
    //
    @Test
    public void testAnd() throws CannotEvalException
    {
        expectValue("(and)", "t");
        expectValue("(and (number? 3))", "t");
        expectValue("(and (number? 3) (string? \"ah\"))", "t");
        expectValue("(and (number? 3) (string? (quote ah)))", "nil");
        expectValue("(and (string? (quote ah)) (number? 3))", "nil");
    }
    //
    @Test
    public void testApply() throws CannotEvalException
    {
        expectValue("(apply number? (list 3))", "t");
        expectValue("(apply * (list 2 3 4 5))", "120");
        expectValue("(apply (lambda (x) (* 2 x)) (list 2))", "4");
        expectValue("(apply list (quote ((+ 2 3) 4)))", "((+ 2 3) 4)");
    }
    //
    @Test
    public void testCatch() throws CannotEvalException
    {
        expectValue("(not (number? (catch (quote error) (/ 2 0))))", "t");
        expectValue("(catch (quote crash) (* 2 3 4 (throw (quote crash) \"crash\") 5 6))", "\"crash\"");
    }
    //
    @Test
    public void testCatchAndApply() throws CannotEvalException
    {
        expectValue("(let ((x 5)) (equal? 5 (catch-and-apply (quote error) (lambda (n v) x) (/ 2 0))))", "t");
        expectValue("(let () (+ 2 3))", "5");
        expectValue("(catch-and-apply (quote crash) (lambda (n v) (concatenate \"when-\" v)) (* 2 3 4 (throw (quote crash) \"crash\") 5 6))", "\"when-crash\"");
        expectValue("(catch-and-apply (quote x) (lambda (n v) n) (throw (quote x) 1))", "x");
        expectValue("(catch-and-apply (quote x) (lambda (n v) v) (throw (quote x) 1))", "1");
    }
    //
    @Test
    public void testCond() throws CannotEvalException
    {
        expectValue("(cond)", "nil");
        expectValue("(cond ((number? 3)))", "t");
        expectValue("(cond ((number? 3) (quote number)))", "number");
        expectValue("(cond ((string? 3) (quote string)))", "nil");
        expectValue("(cond ((string? 3) (quote string)) ((number? 3) (quote number)))", "number");
    }
    //
    @Test
    public void testEnsure() throws CannotEvalException
    {
        expectValue("(let ((x 1)) (progn (unless (equal? 2 (ensure (setq x 2) (setq x 3))) (throw (quote error) nil)) x))", "3");
        expectValue("(let ((x 1)) (progn (catch (quote error) (ensure (throw (quote error) nil) (setq x 3))) x))", "3");
        expectValue("(let ((x 1)) (prog1 (catch (quote error) (ensure (throw (quote error) 2) (setq x 3))) x))", "2");
    }
    //
    @Test
    public void testEval() throws CannotEvalException
    {
        expectValue("(eval (quote (+ 2 3 4)))", "9");
    }
    //
    @Test
    public void testIf() throws CannotEvalException
    {
        expectValue("(if nil 1 2)", "2");
        expectValue("(if t 1 2)", "1");
        expectValue("(if (equal? 0 0) 0 (quotient 2 0))", "0");
        expectValue("(if (not (equal? 0 0)) (quotient 2 0) 0)", "0");
    }
    //
    @Test
    public void testLambda() throws CannotEvalException
    {
        expectValue("((lambda (x y) (+ x y)) 5 6)", "11");
        expectValue("((lambda (x y) (first (cons x y))) 5 nil)", "5");
        expectValue("((lambda (x y) (first (cons x y))) 5 nil)", "5");
        expectValue("((lambda (x y) (first (first (cons x y)))) (list 5) nil)", "5");
        expectValue("((lambda (x y) (cons (first x) (first y))) (list 5) (list nil))", "(5)");
        expectValue("((lambda (a i v) (first (set-array-element a i v))) (make-array (list 2)) (list 1) (list 5))", "5");
    }
    //
    @Test
    public void testDefProc() throws CannotEvalException
    {
        expectValue("(let ((p nil)) (progn (defproc p (x y) (+ x y)) (p 5 6)))", "11");
        expectValue("(let ((q nil)) (progn (defmethod q (x y) (and (string? x) (string? y)) (concatenate x y)) (defproc q (x y) (+ x y)) (list (q 5 6) (q \"a\" \"b\"))))", "(11 \"ab\")");
        expectValue("(let ((r nil)) (progn (defproc r (n)\n  (if\n    (< n 2)\n    1\n    (+ (r (- n 1)) (r (- n 2)))))\n (r 5)))", "8");
    }
    //
    @Test
    public void testAlambda() throws CannotEvalException
    {
        expectValue("((alambda (x y) (+ x y)) 5 6)", "11");
        expectValue("((alambda (n) (if (equal? 1 n) 1 (* n (self (- n 1))))) 5)", "120");
    }
    //
    @Test
    public void testCurry() throws CannotEvalException
    {
        expectValue("((curry (+ 1 2 _ _)) 3 4)", "10");
        expectValue("(mapcar (curry (_ 2 3)) (list + - *))", "(5 -1 6)");
    }
    //
    @Test
    public void testParameters() throws CannotEvalException
    {
        expectValue("(parameters (lambda (x y z w) (+ x y z w)))", "(x y z w)");
    }
    //
    @Test
    public void testBody() throws CannotEvalException
    {
        expectValue("(body (lambda (x y z w) (+ x y z w)))", "(+ x y z w)");
    }
    //
    @Test
    public void testLet() throws CannotEvalException
    {
        expectValue("(let ((x 8) (y 2)) (* x y))", "16");
    }
    //
    @Test
    public void testLetrec() throws CannotEvalException
    {
        expectValue("(letrec ((x 3) (y 9)) (* x y))", "27");
        expectValue("(reverse (list 1 2 3 4))", "(4 3 2 1)"); // mit letrec implementiert, siehe core.lisp
    }
    //
    @Test
    public void testMlambda() throws CannotEvalException
    {
        expectValue("((mlambda expr (quasi-quote (* 2 (unquote (first expr))))) 31)", "62");
        expectValue("((mlambda expr (quasi-quote (* 2 (unquote (first expr))))) 31 egal egal egal)", "62");
    }
    //
    @Test
    public void testDefMacro() throws CannotEvalException
    {
        expectValue("(let ((m nil)) (progn (defmacro m args (list2 quote args)) (m (+ 5 6))))", "((+ 5 6))");
    }
    //
    @Test
    public void testOr() throws CannotEvalException
    {
        expectValue("(or)", "nil");
        expectValue("(or (number? 3))", "t");
        expectValue("(or (number? 3) (string? \"ah\"))", "t");
        expectValue("(or (string? (quote ah)) (number? 3))", "t");
        expectValue("(or (string? (quote ah)) (number? \"be\"))", "nil");
    }
    //
    @Test
    public void testProg() throws CannotEvalException
    {
        expectValue("(progn)", "nil");
        expectValue("(progn 1)", "1");
        expectValue("(progn 1 2)", "2");
        expectValue("(prog1 1)", "1");
        expectValue("(prog1 1 2)", "1");
        expectValue("(prog2 1 2)", "2");
        expectValue("(prog2 1 2 3)", "2");
        expectValue("(let ((x 0) (y 0) (z 0))(prog1 (setq x 1) (setq y (+ x 1)) (setq z (+ y 1)) (or (equal? z 3) (throw (quote error) z))))", "1");
        expectValue("(let ((x 0) (y 0) (z 0))(prog2 (setq x 1) (setq y (+ x 1)) (setq z (+ y 1))))", "2");
        expectValue("(let ((x 0) (y 0) (z 0))(progn (setq x 1) (setq y (+ x 1)) (setq z (+ y 1))))", "3");
        expectValue("(let ((x 0) (y 0) (z 0) (w 0))(progn (setq x 1) (setq y (+ x 1)) (setq z (+ y 1)) (setq w (+ z 1))))", "4");
    }
    //
    @Test
    public void testQuote() throws CannotEvalException
    {
        expectValue("(quote (* 5 4))", "(* 5 4)");
    }
    //
    @Test
    public void testThrow() throws CannotEvalException
    {
        expectValue("(catch (quote welt) (throw (quote welt) (quote hallo)))", "hallo");
    }
    //
    @Test
    public void testUnless() throws CannotEvalException
    {
        expectValue("(unless t 1 2)", "nil");
        expectValue("(unless t)", "nil");
        expectValue("(unless nil)", "nil");
        expectValue("(unless nil 1 2 3)", "3");
    }
    //
    @Test
    public void testWhen() throws CannotEvalException
    {
        expectValue("(when nil 1 2)", "nil");
        expectValue("(when t)", "nil");
        expectValue("(when nil)", "nil");
        expectValue("(when t 1 2 3)", "3");
    }
    //
    @Test
    public void testAWhen() throws CannotEvalException
    {
        expectValue("(awhen nil 1 2)", "nil");
        expectValue("(awhen t)", "nil");
        expectValue("(awhen nil)", "nil");
        expectValue("(awhen t 1 2 3)", "3");
        expectValue("(awhen 2 (+ it 3))", "5");
        expectValue("(awhen 2 3 (+ it 4))", "6");
    }
    //
    @Test
    public void testAif() throws CannotEvalException
    {
        expectValue("(aif (list 1 2 3) (apply + it) 0)", "6");
        expectValue("(aif nil (quote lala) it)", "nil");
    }
    //
    @Test
    public void testAand() throws CannotEvalException
    {
        expectValue("(aand)", "t");
        expectValue("(aand (+ 2 3))", "5");
        expectValue("(aand (list 1 2 3 4) (rest it) (rest it))", "(3 4)");
    }
    //
    @Test
    public void testDotimes() throws CannotEvalException
    {
        expectValue("(let ((p 1)) (dotimes (i 10 p) (setq p (* p (+ 1 i)))))", "3628800");
        expectValue("(let ((p 1)) (progn (dotimes (i 10) (setq p (* p (+ 1 i)))) p))", "3628800");
    }
    //
    @Test
    public void testDolist() throws CannotEvalException
    {
        expectValue("(let ((sum 0)) (dolist (el (quote (1 2 3 4)) sum) (setq sum (+ sum el))))", "10");
    }
    //
    @Test
    public void testLoop() throws CannotEvalException
    {
        expectValue("(let ((sum 0) (index 1)) (loop (setq sum (+ sum index)) (when (greater? index 99) (return sum)) (setq index (+ 1 index))))", "5050");
    }
    //
    @Test
    public void testNestedLoop() throws CannotEvalException
    {
        expectValue("(let ((a 5) (b 5) (c 0)) " +
                "(loop" +
                "  (loop" +
                "    (when (equal? b 0)" +
                "      (return nil))" +
                "    (setq c (+ c 1))" +
                "    (setq b (- b 1)))" +
                "  (when (equal? a 1)" +
                "    (return c))" +
                "  (setq a (- a 1))" +
                "  (setq b 5)))", "25");
    }
    //
    @Test
    public void testAssert() throws CannotEvalException
    {
        expectValue("(catch (quote error) (assert \"abc\" (equal? 2 3)))", "\"abc\"");
        expectValue("(catch (quote error) (assert \"123\" (equal? 4 4)))", "nil");
    }
}