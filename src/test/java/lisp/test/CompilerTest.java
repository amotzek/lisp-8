package lisp.test;
//
import lisp.CannotEvalException;
import lisp.Combinator;
import lisp.List;
import lisp.Rational;
import lisp.Sexpression;
import lisp.combinator.Times;
import lisp.combinator.TypeCheckCombinator;
import lisp.compiler.aggregator.Aggregator;
import lisp.compiler.aggregator.CombinatorGenerator;
import lisp.environment.Environment;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
/**
 * Created by andreasm 23.12.13 17:50
 */
public class CompilerTest
{
    @Test
    public void testQuoteMaskAndParameters() throws Exception
    {
        Combinator oooxccc = generateCombinator("OOOXCCC");
        assertEquals(4, oooxccc.getParameterCount());
        assertEquals(true, oooxccc.mustBeQuoted(0));
        assertEquals(true, oooxccc.mustBeQuoted(1));
        assertEquals(true, oooxccc.mustBeQuoted(2));
        assertEquals(false, oooxccc.mustBeQuoted(3));
        Combinator ooxqcc = generateCombinator("OOXQXQCC");
        assertEquals(6, ooxqcc.getParameterCount());
        assertEquals(true, ooxqcc.mustBeQuoted(0));
        assertEquals(true, ooxqcc.mustBeQuoted(1));
        assertEquals(false, ooxqcc.mustBeQuoted(2));
        assertEquals(true, ooxqcc.mustBeQuoted(3));
        assertEquals(false, ooxqcc.mustBeQuoted(4));
        assertEquals(true, ooxqcc.mustBeQuoted(5));
    }
    //
    @Test
    public void testNesting() throws Exception
    {
        TypeCheckCombinator ooxcc = generateCombinator("OOXCC");
        Sexpression[] arguments = new Sexpression[3];
        arguments[0] = new Add1();
        arguments[1] = new Times2();
        arguments[2] = new Rational(3);
        assertEquals(new Rational(7), ooxcc.apply(null, arguments));
        arguments[1] = new Add1();
        arguments[0] = new Times2();
        assertEquals(new Rational(8), ooxcc.apply(null, arguments));
    }
    //
    @Test
    public void testAggregator1() throws Exception
    {
        List list = new List(new Rational(3), null);
        list = new List(new Times2(), list);
        list = new List(list, null);
        list = new List(new Add1(), list);
        Aggregator aggregator = new Aggregator(list);
        aggregator.aggregate();
        list = aggregator.getResult();
        TypeCheckCombinator combinator = (TypeCheckCombinator) list.first();
        Sexpression[] arguments = new Sexpression[3];
        list = list.rest();
        arguments[0] = list.first();
        list = list.rest();
        arguments[1] = list.first();
        list = list.rest();
        arguments[2] = list.first();
        list = list.rest();
        assertEquals(null, list);
        assertEquals(new Rational(7), combinator.apply(null, arguments));
    }
    //
    @Test
    public void testAggregator2() throws Exception
    {
        List list = new List(new Rational(3), null);
        list = new List(new Rational(2), list);
        list = new List(new Times(), list);
        list = new List(list, null);
        list = new List(new Add1(), list);
        Aggregator aggregator = new Aggregator(list);
        aggregator.aggregate();
        list = aggregator.getResult();
        TypeCheckCombinator combinator = (TypeCheckCombinator) list.first();
        Sexpression[] arguments = new Sexpression[4];
        list = list.rest();
        arguments[0] = list.first();
        list = list.rest();
        arguments[1] = list.first();
        list = list.rest();
        arguments[2] = list.first();
        list = list.rest();
        arguments[3] = list.first();
        list = list.rest();
        assertEquals(null, list);
        assertEquals(new Rational(7), combinator.apply(null, arguments));
    }
    //
    @Test
    public void testAggregator3() throws Exception
    {
        List list1 = new List(new Rational(3), null);
        list1 = new List(new Add1(), list1);
        List list = new List(new Rational(2), null);
        list = new List(list1, list);
        list = new List(new List2(), list);
        Aggregator aggregator = new Aggregator(list);
        aggregator.aggregate();
        list = aggregator.getResult();
        TypeCheckCombinator combinator = (TypeCheckCombinator) list.first();
        Sexpression[] arguments = new Sexpression[4];
        list = list.rest();
        arguments[0] = list.first();
        list = list.rest();
        arguments[1] = list.first();
        list = list.rest();
        arguments[2] = list.first();
        list = list.rest();
        arguments[3] = list.first();
        list = list.rest();
        assertEquals(true, combinator.getClass().getSimpleName().startsWith("OOXCXC"));
        assertEquals(null, list);
        assertEquals(List.list(new Rational(4), new Rational(2)), combinator.apply(null, arguments));
    }
    //
    private static TypeCheckCombinator generateCombinator(String spec) throws Exception
    {
        CombinatorGenerator generator = new CombinatorGenerator(spec);
        generator.generate();
        //
        return generator.loadAndNewInstance();
    }
    //
    private static class Times2 extends TypeCheckCombinator
    {
        protected Times2()
        {
            super(0, 1);
        }
        //
        @Override
        public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
        {
            Rational rational = getRational(arguments, 0);
            Rational result = new Rational(2);
            result.times(rational);
            //
            return result;
        }
    }
    //
    private static class Add1 extends TypeCheckCombinator
    {
        protected Add1()
        {
            super(0, 1);
        }
        //
        @Override
        public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
        {
            Rational rational = getRational(arguments, 0);
            Rational result = new Rational(1);
            result.add(rational);
            //
            return result;
        }
    }
    //
    private static class List2 extends TypeCheckCombinator
    {
        protected List2()
        {
            super(0, 2);
        }
        //
        @Override
        public Sexpression apply(Environment environment, Sexpression[] arguments) throws CannotEvalException
        {
            List list = new List(arguments[1], null);
            list = new List(arguments[0], list);
            //
            return list;
        }
    }
}