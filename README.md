Dieses Repository enthält den Lisp Interpreter / Compiler.

Der Lisp Dialekt ist im Wiki http://simplysomethings.de/lisp+package/index.html beschrieben.

Lisp S-Expressions können mit der Klasse lisp.parser.Parser aus Strings geparst werden.

Die Klasse lisp.Closure wertet S-Expressions aus.

Das dafür notwendige Environment kann mit der Klasse lisp.combinator.EnvironmentFactory erzeugt werden.

Beispiel

`try
{
    Parser parser = new Parser("(+ 2 3 4)", 0);
    Sexpression expression = parser.getSexpression();
    Environment environment = EnvironmentFactory.createEnvironment();
    Closure closure = new Closure(environment, expression);
    Sexpression value = closure.eval();
    System.out.println(value); // 9
}
catch (CannotEvalException e)
{
    e.printStackTrace();
}`
